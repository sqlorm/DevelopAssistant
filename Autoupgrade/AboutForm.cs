﻿using ICSharpCode.WinFormsUI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text; 
using System.Windows.Forms;

namespace Autoupgrade
{
    public partial class AboutForm : BaseForm
    {
        public AboutForm()
        {
            InitializeComponent();
        }

        public AboutForm(string Text, ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase XTheme)
            : this()
        {
            this.Text = Text;
            this.XTheme = XTheme;           
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {
            this.label1.Text = "版本升级（" + Application.ProductVersion.ToString() + "）";
        }
    }
}
