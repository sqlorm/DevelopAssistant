﻿using DevelopAssistant.Common;
using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Docking;
using ICSharpCode.WinFormsUI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.DataPie
{
    public partial class MainForm : DockContent
    {
        DataBaseServer dataBaseServer;

        public MainForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        public MainForm(BaseForm ownerForm,DataBaseServer databaseServer)
            : this()
        {
            this.dataBaseServer = databaseServer;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer |
                  ControlStyles.ResizeRedraw |
                  ControlStyles.AllPaintingInWmPaint, true);
        }

        private void InitializeControls()
        {            
            tabControl1.Radius = 1;
            tabControl1.ShowClose = false;
            tabControl1.TabCaptionLm = 4;
            tabControl1.ItemSize = new System.Drawing.Size(84, 26);
            tabControl1.BorderColor = SystemColors.ControlDark;
            //tabControl1.ArrowColor = Color.White;
            tabControl1.BaseColor = SystemColors.Control;
            tabControl1.BackColor = SystemColors.Control;
            tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;

            tabControl1.Controls.Remove(tabPage2);
            tabControl1.Controls.Remove(tabPage3);

            if (AppSettings.WindowTheme != null)
            {
                Type type = AppSettings.WindowTheme.GetType();
                if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeVS2012))
                {
                    tabControl1.Radius = 1;
                    tabControl1.ItemSize = new Size(80, 27);
                }
                else if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeMac))
                {
                    tabControl1.Radius = 10;
                    tabControl1.ItemSize = new Size(90, 27);
                }
                else if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeShadow))
                {
                    tabControl1.Radius = 8;
                    tabControl1.ItemSize = new System.Drawing.Size(84, 27);
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            DataSet dataSetSource = new DataSet();

            OnThemeChanged(new EventArgs());

            CountDownLatch cdl = new CountDownLatch(2);            

            ThreadPool.QueueUserWorkItem(new WaitCallback((object stateInfo) =>
            {
                using (DataBaseHelper db = new DataBaseHelper(dataBaseServer.ConnectionString, dataBaseServer.ProviderName))
                {
                    int table_count = 0;
                    int view_count = 0;
                    int procedure_count = 0;
                    int function_count = 0;

                    DataTable dataTypeSource = new DataTable("dataType");
                    dataTypeSource.Columns.Add(new DataColumn("Id", typeof(UInt32)));
                    dataTypeSource.Columns.Add(new DataColumn("ParentId", typeof(UInt32)));
                    dataTypeSource.Columns.Add(new DataColumn("Name", typeof(String)));
                    dataTypeSource.Columns.Add(new DataColumn("Objects", typeof(UInt32)));
                    dataTypeSource.Columns.Add(new DataColumn("Describ", typeof(String)));

                    DataTable dataListSource = new DataTable("DataList");
                    dataListSource.Columns.Add(new DataColumn("Id", typeof(UInt32)));
                    dataListSource.Columns.Add(new DataColumn("ParentId", typeof(UInt32)));
                    dataListSource.Columns.Add(new DataColumn("Name", typeof(String)));
                    dataListSource.Columns.Add(new DataColumn("Objects", typeof(UInt32)));
                    dataListSource.Columns.Add(new DataColumn("Describ", typeof(String)));


                    DataTable data = db.GetDataBaseObjects();

                    foreach (DataRow row in data.Rows)
                    {
                        DataRow _newrow = dataListSource.NewRow();

                        string ObjectName = row["table_name"] + "";
                        string ObjectType = row["table_type"] + "";
                        switch (ObjectType)
                        {
                            case "table":
                                _newrow["ParentId"] = 1;
                                _newrow["Id"] = UInt32.Parse("1" + table_count);
                                table_count++;
                                break;
                            case "view":
                                _newrow["ParentId"] = 2;
                                _newrow["Id"] = UInt32.Parse("2" + table_count);
                                view_count++;
                                break;
                            case "procedure":
                                _newrow["ParentId"] = 3;
                                _newrow["Id"] = UInt32.Parse("3" + table_count);
                                procedure_count++;
                                break;
                            case "function":
                                _newrow["ParentId"] = 4;
                                _newrow["Id"] = UInt32.Parse("4" + table_count);
                                function_count++;
                                break;
                        }

                        DataTable dt = GetCoumnsOfTable(ObjectName, ObjectType, db);
                        _newrow["Objects"] = dt.Rows.Count;
                        _newrow["Name"] = ObjectName;

                        dataListSource.Rows.Add(_newrow);

                        int row_index = 0;
                        foreach (DataRow rrow in dt.Rows)
                        {
                            string parentid = _newrow["Id"] + "";
                            string newrowid = _newrow["Id"] + "" + row_index;
                            string cisNull = rrow["CisNull"] + "";
                            string length = rrow["Length"] + "";
                            string typeName = rrow["TypeName"] + "";

                            cisNull = " " + cisNull.Trim(',');
                            DataRow _sub_newrow = dataListSource.NewRow();
                            _sub_newrow["ParentId"] = UInt32.Parse(parentid);
                            _sub_newrow["Id"] = UInt32.Parse(newrowid);
                            _sub_newrow["Objects"] = 0;
                            _sub_newrow["Name"] = rrow["ColumnName"] + "";
                            _sub_newrow["Describ"] = SnippetBase.getDataBaseDataType(typeName, length, dataBaseServer.ProviderName) + cisNull;
                            dataListSource.Rows.Add(_sub_newrow);
                        }

                    }

                    dataSetSource.Tables.Add(dataListSource);

                    DataRow newrow = dataTypeSource.NewRow();
                    newrow["ParentId"] = 0;
                    newrow["Id"] = UInt32.Parse("1");
                    newrow["Name"] = "数据表";
                    newrow["Objects"] = table_count;
                    dataTypeSource.Rows.Add(newrow);

                    newrow = dataTypeSource.NewRow();
                    newrow["ParentId"] = 0;
                    newrow["Id"] = UInt32.Parse("2");
                    newrow["Name"] = "数据视图";
                    newrow["Objects"] = view_count;
                    dataTypeSource.Rows.Add(newrow);

                    newrow = dataTypeSource.NewRow();
                    newrow["ParentId"] = 0;
                    newrow["Id"] = UInt32.Parse("3");
                    newrow["Name"] = "存储过程";
                    newrow["Objects"] = procedure_count;
                    dataTypeSource.Rows.Add(newrow);

                    newrow = dataTypeSource.NewRow();
                    newrow["ParentId"] = 0;
                    newrow["Id"] = UInt32.Parse("4");
                    newrow["Name"] = "函数";
                    newrow["Objects"] = function_count;
                    dataTypeSource.Rows.Add(newrow);

                    dataSetSource.Tables.Add(dataTypeSource);

                }

                //任务处理完上报状态 完成
                CountDownLatch counterDownLatch = (CountDownLatch)stateInfo;
                counterDownLatch.CountDown();

            }), cdl);

            ThreadPool.QueueUserWorkItem(new WaitCallback((object stateInfo) =>
            {
                //任务处理完上报状态 完成
                CountDownLatch counterDownLatch = (CountDownLatch)stateInfo;
                counterDownLatch.CountDown();

            }), cdl);

            //ThreadPool.QueueUserWorkItem(new WaitCallback((object stateInfo) =>
            //{              
            //    //任务处理完上报状态 完成
            //    CountDownLatch counterDownLatch = (CountDownLatch)stateInfo;
            //    counterDownLatch.CountDown();

            //}), cdl);

            //ThreadPool.QueueUserWorkItem(new WaitCallback((object stateInfo) =>
            //{
            //    //任务处理完上报状态 完成
            //    CountDownLatch counterDownLatch = (CountDownLatch)stateInfo;
            //    counterDownLatch.CountDown();

            //}), cdl);

            cdl.Await();

            //files1.BindInfo(this.dataBaseServer);
            summary1.BindInfo(this.dataBaseServer);
            //script1.BindInfo(this.dataBaseServer, dataSetSource);
            //tData1.BindInfo(this.dataBaseServer, dataSetSource);
 
        }

        private int GetCoumnsCountOfTable(string name, string type, DataBaseHelper db)
        {
            int val = 0;

            if (type == "table" || type == "view")
            {
                DataTable dt = db.GetColumnsTable(name);
                val = dt.Rows.Count;
            }

            return val;
        }

        private DataTable GetCoumnsOfTable(string name, string type, DataBaseHelper db)
        {
            return db.GetColumnsTable(name);
        }

        public override void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color linkColor = System.Drawing.Color.Blue;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    tabControl1.ForeColor = Color.Black;
                    tabControl1.BorderColor = SystemColors.ControlDark;
                    tabControl1.ArrowColor = Color.White;
                    tabControl1.BaseColor = SystemColors.Control;
                    tabControl1.BackColor = SystemColors.Control;
                    tabControl1.SelectedColor = SystemColors.Control;
                    tabControl1.BaseTabColor = SystemColors.Control;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    linkColor = Color.FromArgb(051, 153, 153);
                    tabControl1.ForeColor = Color.FromArgb(240, 240, 240);
                    tabControl1.BorderColor = SystemColors.ControlDark;
                    tabControl1.ArrowColor = Color.White;
                    tabControl1.BaseColor = Color.FromArgb(045, 045, 048);
                    tabControl1.BackColor = Color.FromArgb(045, 045, 048);
                    tabControl1.SelectedColor = Color.FromArgb(045, 045, 048);
                    tabControl1.BaseTabColor = Color.FromArgb(045, 045, 048);
                    break;
            }
            this.summary1.ForeColor = foreColor;
            this.summary1.BackColor = backColor;
            this.panel1.ForeColor = foreColor;
            this.panel1.BackColor = backColor;
        }
    }
}
