﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Controls;

namespace DevelopAssistant.AddIn.DataPie
{
    [ToolboxItem(false)]
    public partial class TData : UserControl
    {
        DataSet dataSetSource;
        DataBaseServer dataBaseServer;        

        public TData()
        {           
            InitializeComponent();
            InitializeControls();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer |
                   ControlStyles.ResizeRedraw |
                   ControlStyles.AllPaintingInWmPaint, true);
        }

        private void InitializeControls()
        {
            attachmentColumn.DefaultCellStyle.NullValue = null;

            // load image strip
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList1.ImageSize = new Size(16, 16);
            this.imageList1.Images.AddStrip(Properties.Resources.newGroupPostIconStrip);
            this.attachmentColumn.HeaderCell = new AttachmentColumnHeader(imageList1.Images[2]);

            nTabControl1.Radius = 1;
            nTabControl1.ShowClose = false;
            nTabControl1.TabCaptionLm = 4;           
            nTabControl1.ItemSize = new System.Drawing.Size(94, 26);
            nTabControl1.BorderColor = SystemColors.ControlDark;
            //tabControl1.ArrowColor = Color.White;
            nTabControl1.BaseColor = Color.White; ;
            nTabControl1.BackColor = Color.White; ;
            nTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;

            if (AppSettings.WindowTheme != null)
            {
                Type type = AppSettings.WindowTheme.GetType();
                if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeVS2012))
                {
                    nTabControl1.Radius = 1;
                    nTabControl1.ItemSize = new Size(100, 28);
                }
                else if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeMac))
                {
                    nTabControl1.Radius = 10;
                    nTabControl1.ItemSize = new Size(108, 24);
                }
                else if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeShadow))
                {
                    nTabControl1.Radius = 8;
                    nTabControl1.ItemSize = new System.Drawing.Size(102, 26);
                }
            }

            nTabControl1.TabPages.Clear();


        }

        public void BindInfo(DataBaseServer dataBaseServer, DataSet dataSetSource)
        {
            using (DataBaseHelper db = new DataBaseHelper(dataBaseServer.ConnectionString, dataBaseServer.ProviderName))
            {
                DataTable dataTypeSource = dataSetSource.Tables["dataType"];

                TreeGridNode tableNode = this.nTreeDataGridView1.Nodes.AddNode(1, null, dataTypeSource.Rows[0]["Name"] + "", dataTypeSource.Rows[0]["Objects"] + "", "");
                //TreeGridNode viewNode = this.nTreeDataGridView1.Nodes.AddNode(1, null, dataTypeSource.Rows[1]["Name"] + "", dataTypeSource.Rows[1]["Objects"] + "", "");
                //TreeGridNode procedureNode = this.nTreeDataGridView1.Nodes.AddNode(1, null, dataTypeSource.Rows[2]["Name"] + "", dataTypeSource.Rows[2]["Objects"] + "", "");
                //TreeGridNode functionNode = this.nTreeDataGridView1.Nodes.AddNode(1, null, dataTypeSource.Rows[3]["Name"] + "", dataTypeSource.Rows[3]["Objects"] + "", "");

                DataTable dataTableSource = dataSetSource.Tables["DataList"];

                foreach (DataRow row in dataTableSource.Rows)
                {
                    string ObjectType = row["ParentId"] + "";
                    TreeGridNode node = null;
                    switch (ObjectType)
                    {
                        case "1":
                            node = tableNode.Nodes.AddNode(Int32.Parse(row["Id"] + ""), null, row["Name"] + "", row["Objects"] + "", "");
                            break;
                        //case "2":
                        //    node = viewNode.Nodes.AddNode(Int32.Parse(row["Id"] + ""), null, row["Name"] + "", row["Objects"] + "", "");
                        //    break;
                        //case "3":
                        //    node = procedureNode.Nodes.AddNode(Int32.Parse(row["Id"] + ""), null, row["Name"] + "", row["Objects"] + "", "");
                        //    break;
                        //case "4":
                        //    node = functionNode.Nodes.AddNode(Int32.Parse(row["Id"] + ""), null, row["Name"] + "", row["Objects"] + "", "");
                        //    break;
                    }


                    if (node != null)
                    {
                        DataRow[] rows = dataTableSource.Select("ParentId='" + node.Id + "'");

                        foreach (DataRow rrow in rows)
                        {
                            node.Nodes.AddNode(Int32.Parse(rrow["Id"] + ""), null, rrow["Name"] + "", rrow["Objects"] + "", rrow["Describ"] + "");
                        }
                    }


                }

                tableNode.Expand();

                db.Dispose();
            }

            this.dataBaseServer = dataBaseServer;
            this.dataSetSource = dataSetSource;

        }

        public void BindTable(DataBaseServer dataBaseServer, DataSet dataSetSource)
        {
            nTabControl1.TabPages.Clear();

            foreach (TreeGridNode node in this.nTreeDataGridView1.Nodes[0].Nodes)
            {
                if (((TreeGridCell)node.Cells[1]).Checked)
                {
                    TabPage tabPage = new TabPage(node.Cells[1].Value+"");
                    tabPage.ImageIndex = 1;                                        
                    nTabControl1.TabPages.Add(tabPage);

                    NDataGridView datagridview = new NDataGridView();                   
                    datagridview.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
                    datagridview.SetBounds(4, 4, tabPage.Width - 8, tabPage.Height - 8, BoundsSpecified.All);
                    tabPage.Controls.Add(datagridview);
                }
            }
        }

        private void tvTaskList_NodeChecked(object sender, CheckedEventArgs e)
        {
            checkedNode(e.Node);          
        }

        private void checkedNode(TreeGridNode node)
        {
            checkedParentNode(node);
            checkedChildsNode(node);
            nTreeDataGridView1.Refresh();
            BindTable(dataBaseServer, dataSetSource);
        }

        private void checkedParentNode(TreeGridNode node)
        {
            if (node.Level < 1)
                return;

            TreeGridCell cell = (TreeGridCell)node.Cells[1];

            if (node.Parent == null)
                return;

            if (cell.Checked)
            {
                ((TreeGridCell)(node.Parent.Cells[1])).Checked = cell.Checked;
                checkedParentNode(node.Parent);
            }
            else
            {
                bool allunchecked = true;
                foreach (TreeGridNode sub_node in node.Parent.Nodes)
                {
                    if (((TreeGridCell)(sub_node.Cells[1])).Checked)
                    {
                        allunchecked = false;
                        break;
                    }
                }
                if (allunchecked)
                {
                    ((TreeGridCell)(node.Parent.Cells[1])).Checked = false;
                    checkedParentNode(node.Parent);
                }
            }

        }

        private void checkedChildsNode(TreeGridNode node)
        {
            TreeGridCell cell = (TreeGridCell)node.Cells[1];

            if (node.Nodes.Count > 0 )
            {
                foreach (TreeGridNode sub_node in node.Nodes)
                {
                    ((TreeGridCell)(sub_node.Cells[1])).Checked = cell.Checked;
                    checkedChildsNode(sub_node);
                }
            }
        }

        private void nTreeDataGridView1_NodeChecked(object sender, CheckedEventArgs e)
        {
            checkedNode(e.Node);
        }

    }
}
