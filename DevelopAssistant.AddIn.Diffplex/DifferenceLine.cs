﻿using System; 
using System.Drawing; 

namespace DevelopAssistant.AddIn.Diffplex
{
    public class CodeLine
    {
        public int LineNumber { get; set; }

        public CodeLine(int lineNumber)
        {
            this.LineNumber = lineNumber;
        }
    }

    public class DifferenceLine : CodeLine
    {
        public string Type { get; set; }
        public Color LineBackgroundColor { get; set; }

        public DifferenceLine(int lineNumber) : base(lineNumber)
        {
            this.LineNumber = lineNumber;
        }

        public DifferenceLine(int lineNumber, Color backgroundColor, string type) : base(lineNumber)
        {
            this.Type = type;
            this.LineNumber = lineNumber;
            this.LineBackgroundColor = backgroundColor;
        }
    }
}
