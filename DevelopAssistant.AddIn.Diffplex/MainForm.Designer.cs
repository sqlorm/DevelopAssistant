﻿namespace DevelopAssistant.AddIn.Diffplex
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnPrevDifferent = new DevelopAssistant.AddIn.Diffplex.NImageButton();
            this.btnNextDifferent = new DevelopAssistant.AddIn.Diffplex.NImageButton();
            this.btnStartCompare = new DevelopAssistant.AddIn.Diffplex.NImageButton();
            this.comboFontName = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.SourceTextControl = new ICSharpCode.TextEditor.TextEditorControl();
            this.panel2 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.lblSourceFile = new System.Windows.Forms.Label();
            this.lblFile1 = new System.Windows.Forms.Label();
            this.TargetTextControl = new ICSharpCode.TextEditor.TextEditorControl();
            this.panel3 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.lblFile2 = new System.Windows.Forms.Label();
            this.lblTargetFile = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevDifferent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNextDifferent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnStartCompare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox1.Controls.Add(this.panel4);
            this.groupBox1.Controls.Add(this.comboFontName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.GridLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(189)))), ((int)(((byte)(189)))));
            this.groupBox1.Location = new System.Drawing.Point(6, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.groupBox1.MarginWidth = 0;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(608, 44);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Title = "";
            this.groupBox1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Controls.Add(this.btnPrevDifferent);
            this.panel4.Controls.Add(this.btnNextDifferent);
            this.panel4.Controls.Add(this.btnStartCompare);
            this.panel4.Location = new System.Drawing.Point(402, 9);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 26);
            this.panel4.TabIndex = 5;
            // 
            // btnPrevDifferent
            // 
            this.btnPrevDifferent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrevDifferent.IconImage = null;
            this.btnPrevDifferent.Location = new System.Drawing.Point(121, 0);
            this.btnPrevDifferent.Name = "btnPrevDifferent";
            this.btnPrevDifferent.Size = new System.Drawing.Size(24, 24);
            this.btnPrevDifferent.TabIndex = 5;
            this.btnPrevDifferent.TabStop = false;
            this.btnPrevDifferent.ToolTipText = null;
            this.btnPrevDifferent.Click += new System.EventHandler(this.btnPrevDifferent_Click);
            // 
            // btnNextDifferent
            // 
            this.btnNextDifferent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNextDifferent.IconImage = null;
            this.btnNextDifferent.Location = new System.Drawing.Point(147, 0);
            this.btnNextDifferent.Name = "btnNextDifferent";
            this.btnNextDifferent.Size = new System.Drawing.Size(24, 24);
            this.btnNextDifferent.TabIndex = 4;
            this.btnNextDifferent.TabStop = false;
            this.btnNextDifferent.ToolTipText = null;
            this.btnNextDifferent.Click += new System.EventHandler(this.btnNextDifferent_Click);
            // 
            // btnStartCompare
            // 
            this.btnStartCompare.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStartCompare.IconImage = null;
            this.btnStartCompare.Location = new System.Drawing.Point(173, 0);
            this.btnStartCompare.Name = "btnStartCompare";
            this.btnStartCompare.Size = new System.Drawing.Size(24, 24);
            this.btnStartCompare.TabIndex = 3;
            this.btnStartCompare.TabStop = false;
            this.btnStartCompare.ToolTipText = null;
            this.btnStartCompare.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // comboFontName
            // 
            this.comboFontName.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.comboFontName.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboFontName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboFontName.Font = new System.Drawing.Font("宋体", 9F);
            this.comboFontName.FormattingEnabled = true;
            this.comboFontName.ItemIcon = null;
            this.comboFontName.Items.AddRange(new object[] {
            "Courier New",
            "宋体",
            "仿宋",
            "雅黑"});
            this.comboFontName.Location = new System.Drawing.Point(77, 12);
            this.comboFontName.Name = "comboFontName";
            this.comboFontName.Size = new System.Drawing.Size(121, 22);
            this.comboFontName.TabIndex = 2;
            this.comboFontName.SelectedIndexChanged += new System.EventHandler(this.comboFontName_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "文本字体：";
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(6, 44);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(2);
            this.panel1.Size = new System.Drawing.Size(608, 365);
            this.panel1.TabIndex = 1;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(6, 44);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.SourceTextControl);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.TargetTextControl);
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(608, 365);
            this.splitContainer1.SplitterDistance = 277;
            this.splitContainer1.TabIndex = 0;
            // 
            // SourceTextControl
            // 
            this.SourceTextControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.SourceTextControl.BookMarkEnableToggle = true;
            this.SourceTextControl.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.SourceTextControl.ComparisonState = false;
            this.SourceTextControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SourceTextControl.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SourceTextControl.IsReadOnly = false;
            this.SourceTextControl.Location = new System.Drawing.Point(0, 31);
            this.SourceTextControl.Name = "SourceTextControl";
            this.SourceTextControl.ShowGuidelines = false;
            this.SourceTextControl.Size = new System.Drawing.Size(277, 334);
            this.SourceTextControl.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Borth;
            this.panel2.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel2.Controls.Add(this.lblSourceFile);
            this.panel2.Controls.Add(this.lblFile1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.MarginWidth = 0;
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(277, 31);
            this.panel2.TabIndex = 2;
            this.panel2.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // lblSourceFile
            // 
            this.lblSourceFile.AutoSize = true;
            this.lblSourceFile.BackColor = System.Drawing.Color.White;
            this.lblSourceFile.Location = new System.Drawing.Point(7, 11);
            this.lblSourceFile.Name = "lblSourceFile";
            this.lblSourceFile.Size = new System.Drawing.Size(53, 12);
            this.lblSourceFile.TabIndex = 1;
            this.lblSourceFile.Text = "文件一：";
            // 
            // lblFile1
            // 
            this.lblFile1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFile1.AutoSize = true;
            this.lblFile1.BackColor = System.Drawing.Color.White;
            this.lblFile1.ForeColor = System.Drawing.Color.Blue;
            this.lblFile1.Location = new System.Drawing.Point(243, 11);
            this.lblFile1.Name = "lblFile1";
            this.lblFile1.Size = new System.Drawing.Size(29, 12);
            this.lblFile1.TabIndex = 3;
            this.lblFile1.Text = "选择";
            this.lblFile1.Click += new System.EventHandler(this.SelectOpenFile);
            // 
            // TargetTextControl
            // 
            this.TargetTextControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TargetTextControl.BookMarkEnableToggle = true;
            this.TargetTextControl.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.TargetTextControl.ComparisonState = false;
            this.TargetTextControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TargetTextControl.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.TargetTextControl.IsReadOnly = false;
            this.TargetTextControl.Location = new System.Drawing.Point(0, 31);
            this.TargetTextControl.Name = "TargetTextControl";
            this.TargetTextControl.ShowGuidelines = false;
            this.TargetTextControl.Size = new System.Drawing.Size(327, 334);
            this.TargetTextControl.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Borth;
            this.panel3.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel3.Controls.Add(this.lblFile2);
            this.panel3.Controls.Add(this.lblTargetFile);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.MarginWidth = 0;
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(327, 31);
            this.panel3.TabIndex = 3;
            this.panel3.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // lblFile2
            // 
            this.lblFile2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFile2.AutoSize = true;
            this.lblFile2.BackColor = System.Drawing.Color.White;
            this.lblFile2.ForeColor = System.Drawing.Color.Blue;
            this.lblFile2.Location = new System.Drawing.Point(293, 11);
            this.lblFile2.Name = "lblFile2";
            this.lblFile2.Size = new System.Drawing.Size(29, 12);
            this.lblFile2.TabIndex = 4;
            this.lblFile2.Text = "选择";
            this.lblFile2.Click += new System.EventHandler(this.SelectOpenFile);
            // 
            // lblTargetFile
            // 
            this.lblTargetFile.AutoSize = true;
            this.lblTargetFile.BackColor = System.Drawing.Color.White;
            this.lblTargetFile.Location = new System.Drawing.Point(6, 11);
            this.lblTargetFile.Name = "lblTargetFile";
            this.lblTargetFile.Size = new System.Drawing.Size(53, 12);
            this.lblTargetFile.TabIndex = 2;
            this.lblTargetFile.Text = "文件二：";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.splitContainer1);
            this.panel5.Controls.Add(this.panel1);
            this.panel5.Controls.Add(this.groupBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(6, 0, 6, 6);
            this.panel5.Size = new System.Drawing.Size(620, 415);
            this.panel5.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 415);
            this.Controls.Add(this.panel5);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevDifferent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNextDifferent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnStartCompare)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox1;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblFile2;
        private System.Windows.Forms.Label lblFile1;
        private System.Windows.Forms.Label lblTargetFile;
        private System.Windows.Forms.Label lblSourceFile;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel2;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel3;
        private ICSharpCode.WinFormsUI.Controls.NComboBox comboFontName;
        private System.Windows.Forms.Label label3;
        private ICSharpCode.TextEditor.TextEditorControl SourceTextControl;
        private ICSharpCode.TextEditor.TextEditorControl TargetTextControl;
        private NImageButton btnStartCompare;
        private System.Windows.Forms.Panel panel4;
        private NImageButton btnPrevDifferent;
        private NImageButton btnNextDifferent;
        private System.Windows.Forms.Panel panel5;
    }
}