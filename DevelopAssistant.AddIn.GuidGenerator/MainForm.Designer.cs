﻿namespace DevelopAssistant.AddIn.GuidGenerator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.nTextBox1 = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.lblReCreate = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(6, 84);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(470, 189);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.nTextBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(15);
            this.panel2.Size = new System.Drawing.Size(470, 189);
            this.panel2.TabIndex = 7;
            // 
            // nTextBox1
            // 
            this.nTextBox1.BackColor = System.Drawing.Color.White;
            this.nTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nTextBox1.ForeColor = System.Drawing.Color.Gray;
            this.nTextBox1.FousedColor = System.Drawing.Color.Orange;
            this.nTextBox1.Icon = null;
            this.nTextBox1.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.nTextBox1.IsButtonTextBox = false;
            this.nTextBox1.IsClearTextBox = false;
            this.nTextBox1.IsPasswordTextBox = false;
            this.nTextBox1.Location = new System.Drawing.Point(15, 15);
            this.nTextBox1.MaxLength = 32767;
            this.nTextBox1.Multiline = true;
            this.nTextBox1.Name = "nTextBox1";
            this.nTextBox1.PasswordChar = '\0';
            this.nTextBox1.Placeholder = null;
            this.nTextBox1.ReadOnly = false;
            this.nTextBox1.Size = new System.Drawing.Size(440, 159);
            this.nTextBox1.TabIndex = 2;
            this.nTextBox1.UseSystemPasswordChar = false;
            this.nTextBox1.XBackColor = System.Drawing.Color.White;
            this.nTextBox1.XDisableColor = System.Drawing.Color.Empty;
            this.nTextBox1.XForeColor = System.Drawing.Color.Gray;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "生成类型:";
            // 
            // comboBox1
            // 
            this.comboBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.comboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBox1.DropDownButtonWidth = 20;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.DropDownWidth = 0;
            this.comboBox1.Font = new System.Drawing.Font("宋体", 9F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 18;
            this.comboBox1.ItemIcon = null;
            this.comboBox1.Location = new System.Drawing.Point(109, 13);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.SelectedIndex = -1;
            this.comboBox1.SelectedItem = null;
            this.comboBox1.Size = new System.Drawing.Size(121, 28);
            this.comboBox1.TabIndex = 5;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(245, 17);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(115, 24);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "开启大写";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // lblReCreate
            // 
            this.lblReCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReCreate.AutoSize = true;
            this.lblReCreate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblReCreate.ForeColor = System.Drawing.Color.Blue;
            this.lblReCreate.Location = new System.Drawing.Point(377, 17);
            this.lblReCreate.Name = "lblReCreate";
            this.lblReCreate.Size = new System.Drawing.Size(89, 20);
            this.lblReCreate.TabIndex = 3;
            this.lblReCreate.Text = "重新生成";
            this.lblReCreate.Click += new System.EventHandler(this.lblReCreate_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.checkBox1);
            this.panel3.Controls.Add(this.lblReCreate);
            this.panel3.Controls.Add(this.comboBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(6, 34);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(470, 50);
            this.panel3.TabIndex = 8;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(482, 279);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Resizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Guid生成器";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblReCreate;
        private ICSharpCode.WinFormsUI.Controls.NTextBox nTextBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private ICSharpCode.WinFormsUI.Controls.NComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
    }
}