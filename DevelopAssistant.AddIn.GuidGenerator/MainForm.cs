﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.GuidGenerator
{
    public partial class MainForm : ICSharpCode.WinFormsUI.Forms.BaseForm
    {
        private string editTheme = "";
        private string themeName = string.Empty;

        public MainForm(string theme, string editTheme)
        {
            this.themeName = theme;
            this.editTheme = editTheme;
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase theme =
                new ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase();
            switch (themeName)
            {
                case "Mac":
                    theme = new ICSharpCode.WinFormsUI.Theme.ThemeMac();
                    break;
                case "VS2012":
                    theme = new ICSharpCode.WinFormsUI.Theme.ThemeVS2012();
                    break;
                case "Shadow":
                    theme = new ICSharpCode.WinFormsUI.Theme.ThemeShadow();
                    break;
            }
            this.comboBox1.Items.AddRange(new string[] { 
                "全球唯一ID",
                "雪花算法ID"
            });
            this.comboBox1.SelectedIndex = 0;

            this.XTheme = theme;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            OnThemeChange(new EventArgs());
            lblReCreate_Click(this, new EventArgs());
        }

        private void lblReCreate_Click(object sender, EventArgs e)
        {
            getMyID();
        }

        private void OnThemeChange(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color linkColor = System.Drawing.Color.Blue;
            Color textBackColor = SystemColors.Window;
            switch (editTheme)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    textBackColor = SystemColors.Window;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    linkColor = Color.FromArgb(051, 153, 153);
                    textBackColor = Color.FromArgb(038, 038, 038);
                    break;
            }
            lblReCreate.ForeColor = linkColor;
            nTextBox1.XForeColor = foreColor;
            nTextBox1.XBackColor = textBackColor;
            this.ForeColor = foreColor;
            this.BackColor = backColor;
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            getMyID();
        }      

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //getMyID();
        }

        private string getMyID()
        {
            string strVal = Guid.NewGuid().ToString();
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    strVal = Guid.NewGuid().ToString();
                    break;
                case 1:
                    strVal = Snowflake.Instance().nextId().ToString();
                    break;
            }
            if (checkBox1.Checked)
            {
                strVal = strVal.ToUpper();
            }
            else
            {
                strVal = strVal.ToLower();
            }
            nTextBox1.Text = strVal;
            return strVal;
        }
    }
}
