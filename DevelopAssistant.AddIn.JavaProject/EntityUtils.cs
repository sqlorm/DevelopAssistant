﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.AddIn.JavaProject
{
    public class EntityUtils
    {
        public string CreateSnowFlakeId(string entityName, string pkey)
        {
            string primaryKey = (pkey.Substring(0, 1)).ToUpper() + pkey.Substring(1);
            return entityName + "Entity.set" + primaryKey + "(IdUtils.getSnowFlakeId())";
        }
    }
}
