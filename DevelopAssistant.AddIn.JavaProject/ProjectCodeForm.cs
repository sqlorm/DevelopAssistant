﻿using DevelopAssistant.Service;
using ICSharpCode.TextEditor;
using ICSharpCode.TextEditor.Document;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing; 
using System.Text; 
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.JavaProject
{
    public partial class ProjectCodeForm : ICSharpCode.WinFormsUI.Docking.DockContent
    {
        private string WindowTheme;
        private string EditorTheme;

        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        var parms = base.CreateParams;
        //        parms.Style &= ~0x02000000; // Turn off WS_CLIPCHILDREN 
        //        return parms;
        //    }
        //}

        private MainForm MainForm;

        private bool Folding = true;        

        public ProjectCodeForm(MainForm mainForm,
            string windowTheme,string editorTheme)
        {
            InitializeComponent();
            InitializeControls();
            this.MainForm = mainForm;
            this.WindowTheme = windowTheme;
            this.EditorTheme = editorTheme;
        }

        private void InitializeControls()
        {
            nTabControl1.Radius = 1;
            nTabControl1.ShowClose = false;
            nTabControl1.TabCaptionLm = 4;
            nTabControl1.ItemSize = new System.Drawing.Size(94, 26);
            nTabControl1.BorderColor = SystemColors.ControlDark;

            nTabControl1.ArrowColor = Color.White;
            nTabControl1.BaseColor = SystemColors.Control;
            nTabControl1.BackColor = SystemColors.Control;
            nTabControl1.SelectedColor = SystemColors.Control;

            nTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;

            if (AppSettings.WindowTheme != null)
            {
                Type type = AppSettings.WindowTheme.GetType();
                if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeVS2012))
                {
                    nTabControl1.Radius = 1;
                    nTabControl1.ItemSize = new Size(96, 27);
                }
                else if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeMac))
                {
                    nTabControl1.Radius = 8;
                    nTabControl1.ItemSize = new Size(104, 27);
                }
                else if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeShadow))
                {
                    nTabControl1.Radius = 8;
                    nTabControl1.ItemSize = new System.Drawing.Size(102, 27);
                }
            }

            OnThemeChanged(new EventArgs());

        }

        private void TextEditorControlFormat(TextEditorControl textEditorControl1, string HighlightingName)
        {
            textEditorControl1.FoldMarkStyle = AppSettings.EditorSettings.FoldMarkStyle;
            textEditorControl1.SetHighlighting(this.EditorTheme, HighlightingName);
            textEditorControl1.Document.FormattingStrategy = new DefaultFormattingStrategy();
            if (Folding && AppSettings.FoldingStrategyEnable)
            {
                textEditorControl1.Document.FoldingManager.ClearFoldings();
                switch (HighlightingName)
                {
                    case "Java":                   
                        textEditorControl1.Document.FoldingManager.FoldingStrategy = new JavaFoldingStrategy();  
                        break;
                    case "XML":
                        textEditorControl1.Document.FoldingManager.FoldingStrategy = new XmlFoldingStrategy();
                        break;
                }
                
                textEditorControl1.Document.FoldingManager.UpdateFoldings();
            }
            textEditorControl1.ShowGuidelines = true;
        }

        private void OnRequestListChanged(List<RequestEntity> requestList)
        {
            if (requestList == null)
                return;

            foreach (var request in requestList)
            {
                TabPage tabPage = new TabPage();
                tabPage.Padding = new Padding(4, 6, 4, 4);
                tabPage.Text = request.TypeName;  
                tabPage.ImageIndex = request.ImageIndex;
                this.nTabControl1.TabPages.Add(tabPage);

                ICSharpCode.TextEditor.TextEditorControl textEditorControl = new ICSharpCode.TextEditor.TextEditorControl();
                textEditorControl.Dock = DockStyle.Fill;
                tabPage.Controls.Add(textEditorControl);               
                textEditorControl.Font = AppSettings.EditorSettings.EditorFont;
                textEditorControl.IsReadOnly = true;
                textEditorControl.SetHighlighting(AppSettings.EditorSettings.TSQLEditorTheme, "Java");
                //textEditorControl.TextAreaTextChanged += TextEditorControl_TextAreaTextChanged;
            }         
            
        }

        private void TextEditorControl_TextAreaTextChanged(object sender, EventArgs e)
        {
            ICSharpCode.TextEditor.TextEditorControl textEditorControl = (ICSharpCode.TextEditor.TextEditorControl)sender;
            textEditorControl.Document.FoldingManager.UpdateFoldings();
        }        

        private ICSharpCode.TextEditor.TextEditorControl TabControl(string name)
        {
            ICSharpCode.TextEditor.TextEditorControl control = null;
            foreach (TabPage tabPage in this.nTabControl1.TabPages)
            {
                if (tabPage.Text.Equals(name))
                {
                    control = (ICSharpCode.TextEditor.TextEditorControl)tabPage.Controls[0];
                }
            }
            return control;
        }

        public void StartSnipperList(List<RequestEntity> requestList)
        {
            OnRequestListChanged(requestList);
        }

        public void FinishSnipperList()
        {
            this.MainForm.Close();
            GC.Collect();
        }

        public void OnSnipperDataBind(ResultEntity resultEntity)
        {
            string HighlightingName = "Java";

            if (resultEntity == null)
                return;

            ICSharpCode.TextEditor.TextEditorControl textEditorControl = TabControl(resultEntity.TypeName);

            if (textEditorControl == null)
                return;

            switch (resultEntity.TypeName)
            {
                case "Entity":                     
                    HighlightingName = "Java";
                    break;
                case "Mapper":                   
                    HighlightingName = "XML";
                    break;
                case "DAO":
                case "Dao":                  
                    HighlightingName = "Java";
                    break;
                case "IService":                   
                    HighlightingName = "Java";
                    break;
                case "Service":                   
                    HighlightingName = "Java";
                    break;
                case "Controller":                   
                    HighlightingName = "Java";
                    break;
            }
            textEditorControl.Text = resultEntity.SnippetCode;
            TextEditorControlFormat(textEditorControl, HighlightingName);
        }

        public override void OnThemeChanged(EventArgs e)
        {
            switch (AppSettings.EditorSettings
                .TSQLEditorTheme)
            {
                case "Default":
                    BackColor = SystemColors.Control;
                    nTabControl1.ForeColor = Color.Black;
                    nTabControl1.BorderColor = SystemColors.ControlDark;
                    nTabControl1.ArrowColor = Color.White;
                    nTabControl1.BaseColor = SystemColors.Control;
                    nTabControl1.BackColor = SystemColors.Control;
                    nTabControl1.SelectedColor = SystemColors.Control;
                    nTabControl1.BaseTabColor = SystemColors.Control;
                    break;
                case "Black":
                    BackColor = Color.FromArgb(045, 045, 048);
                    nTabControl1.ForeColor = SystemColors.Window;
                    nTabControl1.BorderColor = SystemColors.ControlDark;
                    nTabControl1.ArrowColor = Color.White;
                    nTabControl1.BaseColor = Color.FromArgb(045, 045, 048);
                    nTabControl1.BackColor = Color.FromArgb(045, 045, 048);
                    nTabControl1.SelectedColor = Color.FromArgb(045, 045, 048);
                    nTabControl1.BaseTabColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            foreach (TabPage tab in this.nTabControl1.TabPages)
            {
                TextEditorControl textPad = (TextEditorControl)tab.Controls[0];
                textPad.SetHighlighting(AppSettings.EditorSettings.TSQLEditorTheme, "Java");
            }
        }

        private void ProjectCodeForm_Load(object sender, EventArgs e)
        {
            //OnThemeChanged(new EventArgs());
        }
    }
}
