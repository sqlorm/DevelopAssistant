﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.AddIn.JavaProject
{
    public class QueryEntity
    {
        public string Column { get; set; }
        public string Value { get; set; }
        public string CompareType { get; set; }

        public QueryEntity(string Column,string Value)
        {
            this.Column = Column;
            this.Value = Value;
            this.CompareType = "=";
        }
    }
}
