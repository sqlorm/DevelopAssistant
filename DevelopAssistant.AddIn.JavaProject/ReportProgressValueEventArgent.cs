﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.AddIn.JavaProject
{
    public class ReportProgressValueEventArgent
    {
        public string Message { get; set; }
        public ResultEntity Data { get; set; }
        public double ProgressValue { get; set; }
        public ReportProgressState State { get; set; }

        public ReportProgressValueEventArgent(string Message, ResultEntity Data, double ProgressValue)
        {
            this.State = ReportProgressState.processing;
            this.Data = Data;
            this.Message = Message;
            this.ProgressValue = ProgressValue;
        }

        public ReportProgressValueEventArgent(string Message, ReportProgressState State)
        {
            this.State = State;
            this.Message = Message;           
        }

    }

    public enum ReportProgressState
    {
        started = 0,
        finished = 1,
        processing = 2
    }
}
