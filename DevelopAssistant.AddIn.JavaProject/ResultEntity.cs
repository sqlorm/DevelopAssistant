﻿using System;
using System.Collections.Generic; 

namespace DevelopAssistant.AddIn.JavaProject
{
    public class ResultEntity
    {       
        public string Name { get; set; }
        public string TypeName { get; set; }
        public string SnippetCode { get; set; }
    }

    public class RequestEntity
    {
        public string Name { get; set; }
        public string TypeName { get; set; }
        public int ImageIndex { get; set; }

        public RequestEntity(string Name,string TypeName)
        {
            switch (TypeName)
            {
                case "Entity":
                    this.ImageIndex = 0;                 
                    break;
                case "Mapper":
                    this.ImageIndex = 1;                    
                    break;
                case "DAO":
                case "Dao":
                    this.ImageIndex = 2;                  
                    break;
                case "IService":
                    this.ImageIndex = 3;                   
                    break;
                case "Service":
                case "ServiceImpl":
                    this.ImageIndex = 4;                   
                    break;
                case "Controller":
                    this.ImageIndex = 4;                   
                    break;
            }

            this.Name = Name;
            this.TypeName = TypeName;           
        }
    }
}
