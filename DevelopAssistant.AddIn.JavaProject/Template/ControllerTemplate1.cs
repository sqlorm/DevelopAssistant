﻿package ${PakageName}.controller;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ${PakageName}.exception.BizException;
import ${PakageName}.service.I${EntityName}Service;

import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
public class ${EntityName}Controller {

	@Autowired
    private	I${EntityName}Service ${InstanceName}Service;  

	@RequestMapping("/${EntityName}/list")	 
	@ApiOperation( httpMethod = "POST", value = "分页获取数据", notes = "分页获取数据")
	public ResultEnity<IPage<Map<String, Object>>> List(@ApiParam(name = "params", value = "查询条件",required = true) @RequestBody Map<String,Object> params){	     	
	    IPage<Map<String, Object>> result = null;		
		try {
			result = ${InstanceName}Service.query${EntityName}PageList(params);			
		}
		catch (BizException ex) {
			// TODO: handle exception
			log.error("查询数据出错："+ex.getMessage(),ex);
			return ResultEnity.Error("查询数据出错:"+ex.getMessage(), null);
		}
		catch (Exception ex) {
			// TODO: handle exception
			log.error("查询数据出错："+ex.getMessage(),ex);
			return ResultEnity.Error("查询数据出错", null);
		}		
		return ResultEnity.Success("查询成功", result);
	} 
	
}
