﻿package ${PakageName}.mapper;

import java.util.List;
import java.util.Map; 

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ${PakageName}.entity.${EntityName}Entity;

/**
 * 
 * @author Administrator
 *
 */ 
public interface I${EntityName}Mapper extends BaseMapper<${EntityName}Entity> {

	 /**
	 * 分页获取列表数据
	 * @param page
	 * @param params
	 * @return
	 */
	List<Map<String, Object>> select${EntityName}PageList(Page<Map<String, Object>> page, Map<String,Object> params);
	

	/**
	 * 获取列表数据
	 * @param page
	 * @param params
	 * @return
	 */
	List<Map<String, Object>> select${EntityName}ArrayList(Map<String,Object> params);


	/**
	 * 添加	 
	 * @param entity
	 * @return
	 */
	Integer add${EntityName}(${PakageName}.entity.${EntityName}Entity entity);
	

	/**
	 * 修改	 
	 * @param entity
	 * @return
	 */
	Integer update${EntityName}(${PakageName}.entity.${EntityName}Entity entity);


	/**
	 * 删除 
	 * @param entity
	 * @return
	 */
	Integer delete${EntityName}(${PakageName}.entity.${EntityName}Entity entity);

}
