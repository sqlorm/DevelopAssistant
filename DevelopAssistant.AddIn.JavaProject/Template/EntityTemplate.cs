﻿package ${PakageName}.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Administrator
 *
 */
@Data
@ApiModel
@Api(tags = "${EntityName}")
@TableName("${TableName}")
public class ${EntityName}Entity  extends Model<${EntityName}Entity> implements Serializable {

	private static final long serialVersionUID=1L;

#foreach($EntityProperty in $EntityProperties)	
    /**
	*$EntityProperty.Description
	*/
	@ApiModelProperty(value = "$EntityProperty.Description", name = "$EntityProperty.Description")
#if($EntityProperty.JsonFormat) 
    @JsonFormat(pattern = "$EntityProperty.JsonFormat")	
#end
#if($EntityProperty.IsParmaryKey) 
    @TableId(type = IdType.$EntityProperty.IdType)	
#end
	private $EntityProperty.PropertyDataType $EntityProperty.PropertyName;
#end	 
	
}
