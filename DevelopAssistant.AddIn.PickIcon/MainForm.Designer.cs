﻿namespace DevelopAssistant.AddIn.PickIcon
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnApply = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.textBox1 = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.PickIcon = new System.Windows.Forms.Label();
            this.NToolBar = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnApply.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApply.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApply.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnApply.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApply.FouseColor = System.Drawing.Color.White;
            this.btnApply.Foused = false;
            this.btnApply.FouseTextColor = System.Drawing.Color.Black;
            this.btnApply.Icon = null;
            this.btnApply.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApply.Location = new System.Drawing.Point(260, 116);
            this.btnApply.Name = "btnApply";
            this.btnApply.Radius = 0;
            this.btnApply.Size = new System.Drawing.Size(80, 32);
            this.btnApply.TabIndex = 0;
            this.btnApply.Text = "开始提取";
            this.btnApply.UnableIcon = null;
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Enabled = true;
            this.textBox1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox1.FousedColor = System.Drawing.Color.Orange;
            this.textBox1.Icon = null;
            this.textBox1.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.textBox1.IsButtonTextBox = false;
            this.textBox1.IsClearTextBox = false;
            this.textBox1.IsPasswordTextBox = false;
            this.textBox1.Location = new System.Drawing.Point(66, 17);
            this.textBox1.MaxLength = 32767;
            this.textBox1.Multiline = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.PasswordChar = '\0';
            this.textBox1.Placeholder = null;
            this.textBox1.ReadOnly = false;
            this.textBox1.Size = new System.Drawing.Size(195, 24);
            this.textBox1.TabIndex = 1;
            this.textBox1.UseSystemPasswordChar = false;
            this.textBox1.XBackColor = System.Drawing.Color.White;
            this.textBox1.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "图标：";
            // 
            // PickIcon
            // 
            this.PickIcon.AutoSize = true;
            this.PickIcon.ForeColor = System.Drawing.Color.Blue;
            this.PickIcon.Location = new System.Drawing.Point(267, 22);
            this.PickIcon.Name = "PickIcon";
            this.PickIcon.Size = new System.Drawing.Size(67, 15);
            this.PickIcon.TabIndex = 3;
            this.PickIcon.Text = "从本地选择";
            this.PickIcon.Click += new System.EventHandler(this.PickIcon_Click);
            // 
            // NToolBar
            // 
            this.NToolBar.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.NToolBar.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.NToolBar.BottomBlackColor = System.Drawing.Color.Empty;
            this.NToolBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.NToolBar.Location = new System.Drawing.Point(6, 106);
            this.NToolBar.MarginWidth = 0;
            this.NToolBar.Name = "NToolBar";
            this.NToolBar.Size = new System.Drawing.Size(348, 56);
            this.NToolBar.TabIndex = 13;
            this.NToolBar.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.PickIcon);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(6, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(336, 60);
            this.panel1.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(6, 34);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(6);
            this.panel2.Size = new System.Drawing.Size(348, 72);
            this.panel2.TabIndex = 15;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(360, 168);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.NToolBar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Resizable = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "提取资源图标";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NButton btnApply;
        private ICSharpCode.WinFormsUI.Controls.NTextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label PickIcon;
        private ICSharpCode.WinFormsUI.Controls.NPanel NToolBar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}