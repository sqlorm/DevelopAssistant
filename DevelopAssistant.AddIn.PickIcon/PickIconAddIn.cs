﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevelopAssistant.AddIn;

namespace DevelopAssistant.AddIn.PickIcon
{
    using DevelopAssistant.PickIcon.Properties;
    using ICSharpCode.WinFormsUI.Docking;

    public class PickIconAddIn : WindowAddIn 
    {
        public PickIconAddIn()
        {
            this.IdentityID = "5cacb9c8-f18e-4be2-b043-2d9fd86467ab";
            this.Name = "提取图标V2.0";
            this.Text = "提取应用图标";
            this.Tooltip = "从EXE,DLL中提取ICON应用图标";            
            this.Icon = Resources.plus_shield;
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            //
        }

        public override object Execute(params object[] Parameter)
        {
            MainForm f = new MainForm((Form)Parameter[0],this){ 
               // WindowFloat = new DelegateWindowFloatHandler(FloatParentCenter) 
            };         
            return f;             
        }
    }
}
