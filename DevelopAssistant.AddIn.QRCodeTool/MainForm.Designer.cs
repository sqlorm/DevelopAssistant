﻿namespace DevelopAssistant.AddIn.QRCodeTool
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.txtContent = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new ICSharpCode.WinFormsUI.Controls.NTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.OutQRCodeImage = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtContent2 = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.InputQRCodeImage = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnUploadImage = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.txtPostFilePath = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnApply = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.panel3 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtContent
            // 
            this.txtContent.BackColor = System.Drawing.SystemColors.Window;
            this.txtContent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtContent.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtContent.FousedColor = System.Drawing.Color.Orange;
            this.txtContent.Icon = null;
            this.txtContent.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtContent.IsButtonTextBox = false;
            this.txtContent.IsClearTextBox = false;
            this.txtContent.IsPasswordTextBox = false;
            this.txtContent.Location = new System.Drawing.Point(11, 35);
            this.txtContent.MaxLength = 32767;
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.PasswordChar = '\0';
            this.txtContent.Placeholder = null;
            this.txtContent.ReadOnly = false;
            this.txtContent.Size = new System.Drawing.Size(344, 99);
            this.txtContent.TabIndex = 1;
            this.txtContent.UseSystemPasswordChar = false;
            this.txtContent.XBackColor = System.Drawing.SystemColors.Window;
            this.txtContent.XDisableColor = System.Drawing.Color.Empty;
            this.txtContent.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "请输入内容：";
            // 
            // tabControl1
            // 
            this.tabControl1.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(79)))), ((int)(((byte)(125)))));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabControl1.ItemSize = new System.Drawing.Size(72, 24);
            this.tabControl1.Location = new System.Drawing.Point(10, 38);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowBorder = true;
            this.tabControl1.ShowClose = false;
            this.tabControl1.ShowWaitMessage = false;
            this.tabControl1.Size = new System.Drawing.Size(376, 403);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txtContent);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(368, 371);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "生成二维码";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.OutQRCodeImage);
            this.panel1.Location = new System.Drawing.Point(11, 169);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(344, 190);
            this.panel1.TabIndex = 5;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // OutQRCodeImage
            // 
            this.OutQRCodeImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.OutQRCodeImage.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.OutQRCodeImage.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.OutQRCodeImage.BottomBlackColor = System.Drawing.Color.Empty;
            this.OutQRCodeImage.Location = new System.Drawing.Point(87, 6);
            this.OutQRCodeImage.MarginWidth = 0;
            this.OutQRCodeImage.Name = "OutQRCodeImage";
            this.OutQRCodeImage.Size = new System.Drawing.Size(178, 178);
            this.OutQRCodeImage.TabIndex = 1;
            this.OutQRCodeImage.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "二维码图片：";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtContent2);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.btnUploadImage);
            this.tabPage2.Controls.Add(this.txtPostFilePath);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(368, 371);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "识别二维码";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtContent2
            // 
            this.txtContent2.BackColor = System.Drawing.SystemColors.Window;
            this.txtContent2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtContent2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtContent2.FousedColor = System.Drawing.Color.Orange;
            this.txtContent2.Icon = null;
            this.txtContent2.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtContent2.IsButtonTextBox = false;
            this.txtContent2.IsClearTextBox = false;
            this.txtContent2.IsPasswordTextBox = false;
            this.txtContent2.Location = new System.Drawing.Point(11, 283);
            this.txtContent2.MaxLength = 32767;
            this.txtContent2.Multiline = true;
            this.txtContent2.Name = "txtContent2";
            this.txtContent2.PasswordChar = '\0';
            this.txtContent2.Placeholder = null;
            this.txtContent2.ReadOnly = false;
            this.txtContent2.Size = new System.Drawing.Size(344, 72);
            this.txtContent2.TabIndex = 5;
            this.txtContent2.UseSystemPasswordChar = false;
            this.txtContent2.XBackColor = System.Drawing.SystemColors.Window;
            this.txtContent2.XDisableColor = System.Drawing.Color.Empty;
            this.txtContent2.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 263);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "识别出的内容：";
            // 
            // panel2
            // 
            this.panel2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.panel2.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel2.Controls.Add(this.InputQRCodeImage);
            this.panel2.Location = new System.Drawing.Point(11, 74);
            this.panel2.MarginWidth = 0;
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(344, 182);
            this.panel2.TabIndex = 3;
            this.panel2.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // InputQRCodeImage
            // 
            this.InputQRCodeImage.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.InputQRCodeImage.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.InputQRCodeImage.BottomBlackColor = System.Drawing.Color.Empty;
            this.InputQRCodeImage.Location = new System.Drawing.Point(83, 7);
            this.InputQRCodeImage.MarginWidth = 0;
            this.InputQRCodeImage.Name = "InputQRCodeImage";
            this.InputQRCodeImage.Size = new System.Drawing.Size(168, 168);
            this.InputQRCodeImage.TabIndex = 0;
            this.InputQRCodeImage.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnUploadImage
            // 
            this.btnUploadImage.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnUploadImage.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnUploadImage.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnUploadImage.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnUploadImage.FouseColor = System.Drawing.Color.White;
            this.btnUploadImage.Foused = false;
            this.btnUploadImage.FouseTextColor = System.Drawing.Color.Black;
            this.btnUploadImage.Icon = null;
            this.btnUploadImage.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnUploadImage.Location = new System.Drawing.Point(280, 38);
            this.btnUploadImage.Name = "btnUploadImage";
            this.btnUploadImage.Radius = 2;
            this.btnUploadImage.Size = new System.Drawing.Size(75, 26);
            this.btnUploadImage.TabIndex = 2;
            this.btnUploadImage.Text = "上传";
            this.btnUploadImage.UnableIcon = null;
            this.btnUploadImage.UseVisualStyleBackColor = true;
            this.btnUploadImage.Click += new System.EventHandler(this.btnUploadImage_Click);
            // 
            // txtPostFilePath
            // 
            this.txtPostFilePath.BackColor = System.Drawing.SystemColors.Window;
            this.txtPostFilePath.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPostFilePath.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtPostFilePath.FousedColor = System.Drawing.Color.Orange;
            this.txtPostFilePath.Icon = null;
            this.txtPostFilePath.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPostFilePath.IsButtonTextBox = false;
            this.txtPostFilePath.IsClearTextBox = false;
            this.txtPostFilePath.IsPasswordTextBox = false;
            this.txtPostFilePath.Location = new System.Drawing.Point(12, 39);
            this.txtPostFilePath.MaxLength = 32767;
            this.txtPostFilePath.Multiline = false;
            this.txtPostFilePath.Name = "txtPostFilePath";
            this.txtPostFilePath.PasswordChar = '\0';
            this.txtPostFilePath.Placeholder = null;
            this.txtPostFilePath.ReadOnly = true;
            this.txtPostFilePath.Size = new System.Drawing.Size(261, 24);
            this.txtPostFilePath.TabIndex = 1;
            this.txtPostFilePath.UseSystemPasswordChar = false;
            this.txtPostFilePath.XBackColor = System.Drawing.SystemColors.Window;
            this.txtPostFilePath.XDisableColor = System.Drawing.Color.Empty;
            this.txtPostFilePath.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "请选择二维码图片：";
            // 
            // BtnApply
            // 
            this.BtnApply.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.BtnApply.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BtnApply.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.BtnApply.FouseBorderColor = System.Drawing.Color.Orange;
            this.BtnApply.FouseColor = System.Drawing.Color.White;
            this.BtnApply.Foused = false;
            this.BtnApply.FouseTextColor = System.Drawing.Color.Black;
            this.BtnApply.Icon = null;
            this.BtnApply.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.BtnApply.Location = new System.Drawing.Point(276, 12);
            this.BtnApply.Name = "BtnApply";
            this.BtnApply.Radius = 2;
            this.BtnApply.Size = new System.Drawing.Size(87, 32);
            this.BtnApply.TabIndex = 0;
            this.BtnApply.Text = "确认并提交";
            this.BtnApply.UnableIcon = null;
            this.BtnApply.UseVisualStyleBackColor = true;
            this.BtnApply.Click += new System.EventHandler(this.BtnApply_Click);
            // 
            // panel3
            // 
            this.panel3.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.panel3.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel3.Controls.Add(this.BtnApply);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(6, 444);
            this.panel3.MarginWidth = 0;
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(383, 60);
            this.panel3.TabIndex = 4;
            this.panel3.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::DevelopAssistant.AddIn.QRCodeTool.Properties.Resources.scan_24px;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(395, 510);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Resizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "二维码工具";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private ICSharpCode.WinFormsUI.Controls.NButton BtnApply;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtContent;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NTabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NPanel OutQRCodeImage;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtContent2;
        private System.Windows.Forms.Label label4;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel2;
        private ICSharpCode.WinFormsUI.Controls.NPanel InputQRCodeImage;
        private ICSharpCode.WinFormsUI.Controls.NButton btnUploadImage;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPostFilePath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage2;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel3;

        #endregion


    }
}

