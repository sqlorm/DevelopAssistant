﻿namespace DevelopAssistant.AddIn.SQLProfesser
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnCancel = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnApply = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPassword = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.txtUid = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.txtPort = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtServer = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnApply);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(6, 281);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(360, 72);
            this.panel1.TabIndex = 0;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnCancel
            // 
            this.btnCancel.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnCancel.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnCancel.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnCancel.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnCancel.FouseColor = System.Drawing.Color.White;
            this.btnCancel.Foused = false;
            this.btnCancel.FouseTextColor = System.Drawing.Color.Black;
            this.btnCancel.Icon = null;
            this.btnCancel.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(262, 20);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Radius = 0;
            this.btnCancel.Size = new System.Drawing.Size(75, 34);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "取消";
            this.btnCancel.UnableIcon = null;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApply.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApply.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApply.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApply.FouseColor = System.Drawing.Color.White;
            this.btnApply.Foused = false;
            this.btnApply.FouseTextColor = System.Drawing.Color.Black;
            this.btnApply.Icon = null;
            this.btnApply.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApply.Location = new System.Drawing.Point(169, 20);
            this.btnApply.Name = "btnApply";
            this.btnApply.Radius = 0;
            this.btnApply.Size = new System.Drawing.Size(75, 34);
            this.btnApply.TabIndex = 0;
            this.btnApply.Text = "确认";
            this.btnApply.UnableIcon = null;
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.txtUid);
            this.groupBox1.Controls.Add(this.txtPort);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtServer);
            this.groupBox1.Location = new System.Drawing.Point(13, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(345, 227);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "设置数据库连接";
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.Color.White;
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.Enabled = true;
            this.txtPassword.FousedColor = System.Drawing.Color.Orange;
            this.txtPassword.Icon = null;
            this.txtPassword.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPassword.IsButtonTextBox = false;
            this.txtPassword.IsClearTextBox = false;
            this.txtPassword.IsPasswordTextBox = false;
            this.txtPassword.Location = new System.Drawing.Point(117, 169);
            this.txtPassword.MaxLength = 32767;
            this.txtPassword.Multiline = false;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Placeholder = "";
            this.txtPassword.ReadOnly = false;
            this.txtPassword.Size = new System.Drawing.Size(160, 24);
            this.txtPassword.TabIndex = 7;
            this.txtPassword.UseSystemPasswordChar = false;
            // 
            // txtUid
            // 
            this.txtUid.BackColor = System.Drawing.Color.White;
            this.txtUid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUid.Enabled = true;
            this.txtUid.FousedColor = System.Drawing.Color.Orange;
            this.txtUid.Icon = null;
            this.txtUid.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtUid.IsButtonTextBox = false;
            this.txtUid.IsClearTextBox = false;
            this.txtUid.IsPasswordTextBox = false;
            this.txtUid.Location = new System.Drawing.Point(117, 125);
            this.txtUid.MaxLength = 32767;
            this.txtUid.Multiline = false;
            this.txtUid.Name = "txtUid";
            this.txtUid.PasswordChar = '\0';
            this.txtUid.Placeholder = null;
            this.txtUid.ReadOnly = false;
            this.txtUid.Size = new System.Drawing.Size(120, 24);
            this.txtUid.TabIndex = 6;
            this.txtUid.UseSystemPasswordChar = false;
            // 
            // txtPort
            // 
            this.txtPort.BackColor = System.Drawing.Color.White;
            this.txtPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPort.Enabled = true;
            this.txtPort.FousedColor = System.Drawing.Color.Orange;
            this.txtPort.Icon = null;
            this.txtPort.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPort.IsButtonTextBox = false;
            this.txtPort.IsClearTextBox = false;
            this.txtPort.IsPasswordTextBox = false;
            this.txtPort.Location = new System.Drawing.Point(117, 83);
            this.txtPort.MaxLength = 32767;
            this.txtPort.Multiline = false;
            this.txtPort.Name = "txtPort";
            this.txtPort.PasswordChar = '\0';
            this.txtPort.Placeholder = null;
            this.txtPort.ReadOnly = false;
            this.txtPort.Size = new System.Drawing.Size(100, 24);
            this.txtPort.TabIndex = 5;
            this.txtPort.UseSystemPasswordChar = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "登陆密码：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "登陆帐号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "端口号：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "服务器地址：";
            // 
            // txtServer
            // 
            this.txtServer.BackColor = System.Drawing.Color.White;
            this.txtServer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtServer.Enabled = true;
            this.txtServer.FousedColor = System.Drawing.Color.Orange;
            this.txtServer.Icon = null;
            this.txtServer.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtServer.IsButtonTextBox = false;
            this.txtServer.IsClearTextBox = false;
            this.txtServer.IsPasswordTextBox = false;
            this.txtServer.Location = new System.Drawing.Point(117, 38);
            this.txtServer.MaxLength = 32767;
            this.txtServer.Multiline = false;
            this.txtServer.Name = "txtServer";
            this.txtServer.PasswordChar = '\0';
            this.txtServer.Placeholder = null;
            this.txtServer.ReadOnly = false;
            this.txtServer.Size = new System.Drawing.Size(159, 24);
            this.txtServer.TabIndex = 0;
            this.txtServer.UseSystemPasswordChar = false;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 359);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Resizable = false;
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SettingsForm";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NButton btnCancel;
        private ICSharpCode.WinFormsUI.Controls.NButton btnApply;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtServer;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPassword;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtUid;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPort;
    }
}