﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevelopAssistant.AddIn.Screenshot
{
    
    public enum DrawToolsDockStyle
    {
        None = 0,
        Top,
        BottomUp,
        Bottom
    }
}
