﻿namespace DevelopAssistant.AddIn.Screenshot
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.toolStrip1 = new ICSharpCode.WinFormsUI.Controls.NToolStrip();
            this.toolStripButtonSave = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonDrag = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.Preview = new ICSharpCode.WinFormsUI.Controls.NCanvas();
            this.NToolBar = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnScreenShot = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnHidenAssistant = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Preview)).BeginInit();
            this.NToolBar.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Controls.Add(this.Preview);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(2);
            this.panel1.Size = new System.Drawing.Size(312, 203);
            this.panel1.TabIndex = 2;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonSave,
            this.toolStripButtonDrag,
            this.toolStripLabel3});
            this.toolStrip1.Location = new System.Drawing.Point(2, 173);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(308, 28);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonSave
            // 
            this.toolStripButtonSave.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSave.Image = global::DevelopAssistant.AddIn.Screenshot.Properties.Resources.save2;
            this.toolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSave.Name = "toolStripButtonSave";
            this.toolStripButtonSave.Size = new System.Drawing.Size(23, 25);
            this.toolStripButtonSave.Text = "保存为图片";
            this.toolStripButtonSave.Click += new System.EventHandler(this.toolStripButtonSave_Click);
            // 
            // toolStripButtonDrag
            // 
            this.toolStripButtonDrag.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonDrag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonDrag.Image = global::DevelopAssistant.AddIn.Screenshot.Properties.Resources.move;
            this.toolStripButtonDrag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDrag.Name = "toolStripButtonDrag";
            this.toolStripButtonDrag.Size = new System.Drawing.Size(23, 25);
            this.toolStripButtonDrag.Text = "拖动图像";
            this.toolStripButtonDrag.Click += new System.EventHandler(this.toolStripButtonDrag_Click);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(56, 25);
            this.toolStripLabel3.Text = "准备就绪";
            // 
            // Preview
            // 
            this.Preview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Preview.BackColor = System.Drawing.SystemColors.Control;
            this.Preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Preview.Drag = false;
            this.Preview.Image = null;
            this.Preview.Location = new System.Drawing.Point(5, 5);
            this.Preview.Name = "Preview";
            this.Preview.Size = new System.Drawing.Size(302, 167);
            this.Preview.TabIndex = 0;
            this.Preview.TabStop = false;
            // 
            // NToolBar
            // 
            this.NToolBar.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.NToolBar.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.NToolBar.BottomBlackColor = System.Drawing.Color.Empty;
            this.NToolBar.Controls.Add(this.btnScreenShot);
            this.NToolBar.Controls.Add(this.btnHidenAssistant);
            this.NToolBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.NToolBar.Location = new System.Drawing.Point(6, 257);
            this.NToolBar.MarginWidth = 0;
            this.NToolBar.Name = "NToolBar";
            this.NToolBar.Size = new System.Drawing.Size(332, 60);
            this.NToolBar.TabIndex = 13;
            this.NToolBar.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnScreenShot
            // 
            this.btnScreenShot.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnScreenShot.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnScreenShot.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnScreenShot.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnScreenShot.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnScreenShot.FouseColor = System.Drawing.Color.White;
            this.btnScreenShot.Foused = false;
            this.btnScreenShot.FouseTextColor = System.Drawing.Color.Black;
            this.btnScreenShot.Icon = null;
            this.btnScreenShot.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnScreenShot.Location = new System.Drawing.Point(215, 9);
            this.btnScreenShot.Name = "btnScreenShot";
            this.btnScreenShot.Radius = 0;
            this.btnScreenShot.Size = new System.Drawing.Size(96, 32);
            this.btnScreenShot.TabIndex = 4;
            this.btnScreenShot.Text = "开始截图";
            this.btnScreenShot.UnableIcon = null;
            this.btnScreenShot.UseVisualStyleBackColor = true;
            this.btnScreenShot.Click += new System.EventHandler(this.BtnScreenshot_Click);
            // 
            // btnHidenAssistant
            // 
            this.btnHidenAssistant.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnHidenAssistant.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnHidenAssistant.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnHidenAssistant.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHidenAssistant.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnHidenAssistant.FouseColor = System.Drawing.Color.White;
            this.btnHidenAssistant.Foused = false;
            this.btnHidenAssistant.FouseTextColor = System.Drawing.Color.Black;
            this.btnHidenAssistant.Icon = null;
            this.btnHidenAssistant.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnHidenAssistant.Location = new System.Drawing.Point(109, 9);
            this.btnHidenAssistant.Name = "btnHidenAssistant";
            this.btnHidenAssistant.Radius = 0;
            this.btnHidenAssistant.Size = new System.Drawing.Size(96, 32);
            this.btnHidenAssistant.TabIndex = 3;
            this.btnHidenAssistant.Text = "隐藏开发助手";
            this.btnHidenAssistant.UnableIcon = null;
            this.btnHidenAssistant.UseVisualStyleBackColor = true;
            this.btnHidenAssistant.Click += new System.EventHandler(this.BtnHideAssistant_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(6, 34);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10);
            this.panel2.Size = new System.Drawing.Size(332, 223);
            this.panel2.TabIndex = 14;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 323);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.NToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Resizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "获取屏幕截图";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Preview)).EndInit();
            this.NToolBar.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NCanvas Preview;
        private ICSharpCode.WinFormsUI.Controls.NToolStrip toolStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonDrag;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonSave;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private ICSharpCode.WinFormsUI.Controls.NPanel NToolBar;
        private ICSharpCode.WinFormsUI.Controls.NButton btnScreenShot;
        private ICSharpCode.WinFormsUI.Controls.NButton btnHidenAssistant;
        private System.Windows.Forms.Panel panel2;
    }
}