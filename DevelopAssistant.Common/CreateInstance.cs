﻿using System;
using System.Reflection;

namespace DevelopAssistant.Common
{
    /// <summary>
    /// 动态加载引用集
    /// </summary>
    public abstract class CreateInstance
    {
        /// <summary>
        /// 动态创建引用
        /// </summary>
        /// <param name="AssemblyPath">引用集</param>
        /// <param name="ClassName">命名空间.类名</param>
        /// <returns></returns>
        public static object Instance(string AssemblyPath, string ClassName)
        {
            return Assembly.Load(AssemblyPath).CreateInstance(ClassName, true);
        }

        /// <summary>
        /// 动态创建引用
        /// </summary>
        /// <param name="AssemblyFile">引用集</param>
        /// <param name="ClassName">命名空间.类名</param>
        /// <param name="Directory">引用集路径</param>
        /// <returns></returns>
        public static object Instance(string AssemblyFile, string ClassName, string Directory)
        {
            object instance = null;
            try
            {
                instance = Assembly.LoadFile(Directory + AssemblyFile).CreateInstance(ClassName, true);
            }
            catch
            {
                instance = Assembly.UnsafeLoadFrom(Directory + AssemblyFile).CreateInstance(ClassName, true);
            }
            return instance;
        }

    }
}
