﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Common
{
    public class ListItem
    {
        public string Text { get; set; }

        public string Value { get; set; }

        public object Tag { get; set; }
        
        public override string ToString()
        {
            return Text.ToString();
        }
    }
}
