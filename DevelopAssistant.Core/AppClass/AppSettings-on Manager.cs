﻿using DevelopAssistant.Service;
using System;
using System.Collections.Generic; 
using System.Text; 

namespace DevelopAssistant.Core
{
    public class AppSetting
    {
        public string EditorTheme { get; set; }
        public bool Transaction { get; set; }
        public bool Intellisense { get; set; }
        public bool Supplement { get; set; }
        public bool KeywordsCase { get; set; }
        public bool NewQueryContent { get; set; }
        public bool SpaceButtonComplete { get; set; } 
        public int FoldMarkStyle { get; set; }
        public QueryDataGridStyle QueryStyle { get; set; }
    }

    public class AppSettings_on_Manager
    {
        string connectionString = "";

        private string ConvertToValue(bool val)
        {
            return val ? "1" : "0";
        }

        public AppSettings_on_Manager()
        {
            //connectionString = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "baseset.db;Version=3;Password=fas2015;UseUTF16Encoding=True;";
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            connectionString = connectionString.Replace("|DataDirectory|", AppDomain.CurrentDomain.BaseDirectory);
        }

        public AppSetting Load(ref string Message)
        {
            var settings = new AppSetting();
            Message = "reading";

            try
            {
                using (var db = NORM.DataBase.DataBaseFactory.Create(connectionString,
                    NORM.DataBase.DataBaseTypes.SqliteDataBase))
                {
                    string sql = "select * from T_AppSettings";
                    System.Data.DataTable dt = db.QueryDataSet(System.Data.CommandType.Text, sql, null).Tables[0];
                    foreach (System.Data.DataRow row in dt.Rows)
                    {
                        string key = row["name"] + "";
                        string value= row["value"] + "";

                        if(key== "Intellisense")
                        {
                            settings.Intellisense = value == "1" ? true : false;
                        }
                        if (key == "Transaction")
                        {
                            settings.Transaction = value == "1" ? true : false;
                        }
                        if (key == "KeywordsCase")
                        {
                            settings.KeywordsCase = value == "1" ? true : false;
                        }
                        if (key == "Supplement")
                        {
                            settings.Supplement = value == "1" ? true : false;
                        }
                        if (key == "SpaceButtonComplete")
                        {
                            settings.SpaceButtonComplete = value == "1" ? true : false;
                        }
                        if (key == "NewQueryContent")
                        {
                            settings.NewQueryContent = value == "1" ? true : false;
                        }
                        if (key == "EditorTheme")
                        {
                            settings.EditorTheme = value;
                        }
                        if (key == "FoldMarkStyle")
                        {
                            settings.FoldMarkStyle = Convert.ToInt32(value);
                        }
                        if (key == "QueryStyle")
                        {
                            settings.QueryStyle = (QueryDataGridStyle)Enum.Parse(typeof(QueryDataGridStyle), value);
                        }
                    }
                }
                Message = "Request Settings Success";
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                DevelopAssistant.Common.NLogger.WriteToLine(ex.Message, "错误", DateTime.Now, ex.Source, ex.StackTrace);
            }

            return settings;
        }

        public bool Save(AppSetting Settings)
        {
            try
            {
                using (var db = NORM.DataBase.DataBaseFactory.Create(connectionString,
                    NORM.DataBase.DataBaseTypes.SqliteDataBase))
                {
                    db.BeginTransaction();

                    try
                    {
                        DevelopAssistant.Common.StringPlus sql = new Common.StringPlus();

                        sql.AppendLine("update [T_AppSettings] set [value]='" + ConvertToValue(Settings.KeywordsCase) + "' where [id]='2' ");
                        sql.AppendLine("update [T_AppSettings] set [value]='" + ConvertToValue(Settings.Supplement) + "' where [id]='3' ");
                        sql.AppendLine("update [T_AppSettings] set [value]='" + ConvertToValue(Settings.SpaceButtonComplete) + "' where [id]='4' ");
                        sql.AppendLine("update [T_AppSettings] set [value]='" + ConvertToValue(Settings.Transaction) + "' where [id]='5' ");
                        sql.AppendLine("update [T_AppSettings] set [value]='" + ConvertToValue(Settings.Intellisense) + "' where [id]='6' ");
                        sql.AppendLine("update [T_AppSettings] set [value]='" + Settings.EditorTheme + "' where [id]='7' ");
                        sql.AppendLine("update [T_AppSettings] set [value]='" + ConvertToValue(Settings.NewQueryContent) + "' where [id]='8' ");
                        sql.AppendLine("update [T_AppSettings] set [value]='" + Settings.FoldMarkStyle + "' where [id]='9' ");
                        sql.AppendLine("update [T_AppSettings] set [value]='" + Settings.QueryStyle + "' where [id]='10' ");

                        foreach (string command in sql.Value.Split(System.Environment.NewLine.ToCharArray()))
                        {
                            if (string.IsNullOrEmpty(command)) continue;
                            db.Execute(System.Data.CommandType.Text, command, null);
                        }

                        db.Commit();
                    }
                    catch(Exception ex)
                    {
                        db.RollBack();
                        DevelopAssistant.Common.NLogger.WriteToLine(ex.Message, "错误", DateTime.Now, ex.Source, ex.StackTrace);
                    }

                }
            }
            catch (Exception ex)
            {                
                DevelopAssistant.Common.NLogger.WriteToLine(ex.Message, "错误", DateTime.Now, ex.Source, ex.StackTrace);
            }
            return true;
        }

        private static AppSettings_on_Manager _instance;

        public static AppSettings_on_Manager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AppSettings_on_Manager();
                }
                return _instance;
            }
        }

    }

    
}
