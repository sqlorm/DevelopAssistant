﻿using DevelopAssistant.Service;
using ICSharpCode.TextEditor;
using ICSharpCode.TextEditor.Document;
using ICSharpCode.WinFormsUI.Docking;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.DBMS
{
    public partial class CodeTextDocument : DockContent, IDocumentContent
    {
        int imageIndex = 0;

        private string _selectedDatabaseType;
        public string SelectedDatabaseType
        {
            get { return _selectedDatabaseType; }
            set { _selectedDatabaseType = value; }
        }

        public CodeTextDocument()
        {
            InitializeComponent();
        }

        public CodeTextDocument(MainForm owner)
            : this()
        {
            imageIndex = 0;
            tabControl1.Radius = 1;
            tabControl1.ShowClose = false;
            tabControl1.TabCaptionLm = 4;
            tabControl1.ItemSize = new System.Drawing.Size(94, 26);
            tabControl1.BorderColor = SystemColors.ControlDark;

            tabControl1.ArrowColor = Color.White;
            tabControl1.BaseColor = SystemColors.Control;
            tabControl1.BackColor = SystemColors.Control;
            tabControl1.SelectedColor = SystemColors.Control;

            tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;

            if (AppSettings.WindowTheme != null)
            {
                Type type = AppSettings.WindowTheme.GetType();
                if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeVS2012))
                {
                    tabControl1.Radius = 1;
                    tabControl1.ItemSize = new Size(96, 27);
                }
                else if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeMac))
                {
                    tabControl1.Radius = 8;
                    tabControl1.ItemSize = new Size(104, 27);
                }
                else if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeShadow))
                {
                    tabControl1.Radius = 8;
                    tabControl1.ItemSize = new System.Drawing.Size(102, 27);
                }
            }

            //tabControl1.SelectedIndexChanged += TabControl1_SelectedIndexChanged;

        }

        public TextEditorControl GetCodeTextEditor(string name)
        {
            string tab_name = "tab_" + name;
            TextEditorControl textPad = null;
            foreach (TabPage page in this.tabControl1.TabPages)
            {
                if (page.Name.Equals(tab_name))
                {
                    textPad = (TextEditorControl)page.Controls[0];
                    break;
                }
            }
            return textPad;
        }

        public void AddCodeTabPage(string codeText,string tabText)
        {
            TabPage newTabPage = new TabPage(tabText);
            newTabPage.Name = "tab_" + tabText;
            newTabPage.Padding = new Padding(2, 4, 2, 2);
            newTabPage.ImageIndex = imageIndex;
            
            TextEditorControl textPad = new TextEditorControl();             
            textPad.IsReadOnly = true;
            textPad.ShowGuidelines = true;
            textPad.ShowVRuler = true;            
            //textPad.Text = codeText;
            textPad.Dock = DockStyle.Fill;
            textPad.Font = AppSettings.EditorSettings.EditorFont;

            newTabPage.Controls.Add(textPad);

            switch (AppSettings.EditorSettings
                .TSQLEditorTheme)
            {
                case "Default":
                    textPad.LineViewerStyle = LineViewerStyle.None;                  
                    break;
                case "DefaultAndBackImage":
                    textPad.LineViewerStyle = LineViewerStyle.None;                   
                    break;
                case "Black":
                    textPad.LineViewerStyle = LineViewerStyle.FullRow;                   
                    break;
                case "BlackAndBackImage":
                    textPad.LineViewerStyle = LineViewerStyle.None;                   
                    break;
            }

            textPad.FoldMarkStyle = AppSettings.EditorSettings.FoldMarkStyle;
            textPad.SetHighlighting(AppSettings.EditorSettings.TSQLEditorTheme, "C#");
            textPad.Document.FormattingStrategy = new CSharpFormattingStrategy();
            textPad.Document.FoldingManager.FoldingStrategy = new CSharpFoldingStrategy(); //new IndentFoldingStrategy();
            textPad.Document.FoldingManager.UpdateFoldings();

            textPad.TextAreaTextChanged += TextAreaText_TextChanged;

            tabControl1.TabPages.Add(newTabPage);           
            imageIndex++;

        }

        private void TextAreaText_TextChanged(object sender, EventArgs e)
        {
            TextEditorControl textPad = ((TextEditorControl)(sender));
            textPad.Document.FoldingManager.UpdateFoldings();
        }

        public void SetCodeTabPageText(string codeText, string tabText)
        {
            TextEditorControl textPad = GetCodeTextEditor(tabText);
            if (textPad == null)
                return;
            textPad.Document.GuidelinesManager.Redrawable = true;
            textPad.Text = codeText;             
        }

        public void FindAndReplace(FindAndReplaceRequest request)
        {

        }

        public override void OnThemeChanged(EventArgs e)
        {           
            switch (AppSettings.EditorSettings
                .TSQLEditorTheme)
            {
                case "Default":
                    BackColor = SystemColors.Control;
                    tabControl1.ForeColor = Color.Black;
                    tabControl1.BorderColor = SystemColors.ControlDark;
                    tabControl1.ArrowColor = Color.White;
                    tabControl1.BaseColor = SystemColors.Control;
                    tabControl1.BackColor = SystemColors.Control;
                    tabControl1.SelectedColor = SystemColors.Control;
                    tabControl1.BaseTabColor = SystemColors.Control;
                    break;
                case "Black":
                    BackColor = Color.FromArgb(045, 045, 048);
                    tabControl1.ForeColor = SystemColors.Window;
                    tabControl1.BorderColor = SystemColors.ControlDark;
                    tabControl1.ArrowColor = Color.White;
                    tabControl1.BaseColor = Color.FromArgb(045, 045, 048);
                    tabControl1.BackColor = Color.FromArgb(045, 045, 048);
                    tabControl1.SelectedColor = Color.FromArgb(045, 045, 048);
                    tabControl1.BaseTabColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            foreach (TabPage tab in this.tabControl1.TabPages)
            {
                TextEditorControl textPad = (TextEditorControl)tab.Controls[0];
                textPad.SetHighlighting(AppSettings.EditorSettings.TSQLEditorTheme, "C#");
            }            
        }

        private void CodeTextDocument_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new EventArgs());
        }
    }
}
