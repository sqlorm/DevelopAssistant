﻿namespace DevelopAssistant.Core.DBMS
{
    partial class QueryDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QueryDocument));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.nPanel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.txtExecSqlCommand = new DevelopAssistant.Service.ICSharpCodeTextEditor();
            this.contextMenuStrip2 = new ICSharpCode.WinFormsUI.Controls.NContextMenuStrip(this.components);
            this.排版美化ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.转成一行ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.执行SQLToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.查看表数据ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.复制ToolStripMenuItem1 = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.粘贴ToolStripMenuItem1 = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.添加到收藏夹ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.从收藏夹中获取ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.保存为ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.清除ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.ResultsTabControl = new ICSharpCode.WinFormsUI.Controls.NTabControl();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.ToolBarBox = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.toolStrip1 = new ICSharpCode.WinFormsUI.Controls.NToolStrip();
            this.toolButtonAddRecord = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolButtonDeleteRecord = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolButtonCommitRecord = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolButtonExportTo = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolButtonToScript = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolButtonToJson = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolButtonEx = new System.Windows.Forms.ToolStripDropDownButton();
            this.sQL文件ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.json文件ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.excel文件ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.word文件ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.toolButtonPrintPrewiew = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolButtonPrint = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolButtonSwatch = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolButtonShowNumber = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStrip1 = new ICSharpCode.WinFormsUI.Controls.NContextMenuStrip(this.components);
            this.复制ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.粘贴ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.statusStrip1 = new ICSharpCode.WinFormsUI.Controls.NStatusBar();
            this.ExecuteReulstMessage = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.nPanel1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.ToolBarBox.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.nPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.ResultsTabControl);
            this.splitContainer1.Panel2.Controls.Add(this.ToolBarBox);
            this.splitContainer1.Panel2Collapsed = true;
            this.splitContainer1.Size = new System.Drawing.Size(876, 596);
            this.splitContainer1.SplitterDistance = 340;
            this.splitContainer1.SplitterWidth = 2;
            this.splitContainer1.TabIndex = 0;
            // 
            // nPanel1
            // 
            this.nPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.nPanel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.nPanel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.nPanel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.nPanel1.Controls.Add(this.txtExecSqlCommand);
            this.nPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nPanel1.Location = new System.Drawing.Point(0, 0);
            this.nPanel1.MarginWidth = 0;
            this.nPanel1.Name = "nPanel1";
            this.nPanel1.Padding = new System.Windows.Forms.Padding(4);
            this.nPanel1.Size = new System.Drawing.Size(876, 596);
            this.nPanel1.TabIndex = 1;
            this.nPanel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // txtExecSqlCommand
            // 
            this.txtExecSqlCommand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtExecSqlCommand.BookMarkEnableToggle = true;
            this.txtExecSqlCommand.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.txtExecSqlCommand.ComparisonState = false;
            this.txtExecSqlCommand.ContextMenuStrip = this.contextMenuStrip2;
            this.txtExecSqlCommand.DataBaseServer = null;
            this.txtExecSqlCommand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtExecSqlCommand.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.txtExecSqlCommand.IsReadOnly = false;
            this.txtExecSqlCommand.Location = new System.Drawing.Point(4, 4);
            this.txtExecSqlCommand.Modified = false;
            this.txtExecSqlCommand.Name = "txtExecSqlCommand";
            this.txtExecSqlCommand.OwnerForm = null;
            this.txtExecSqlCommand.Padding = new System.Windows.Forms.Padding(1);
            this.txtExecSqlCommand.ShowGuidelines = false;
            this.txtExecSqlCommand.ShowVRuler = false;
            this.txtExecSqlCommand.Size = new System.Drawing.Size(868, 588);
            this.txtExecSqlCommand.TabIndex = 0;
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.排版美化ToolStripMenuItem,
            this.转成一行ToolStripMenuItem,
            this.执行SQLToolStripMenuItem,
            this.查看表数据ToolStripMenuItem,
            this.复制ToolStripMenuItem1,
            this.粘贴ToolStripMenuItem1,
            this.添加到收藏夹ToolStripMenuItem,
            this.从收藏夹中获取ToolStripMenuItem,
            this.保存为ToolStripMenuItem,
            this.清除ToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(181, 246);
            // 
            // 排版美化ToolStripMenuItem
            // 
            this.排版美化ToolStripMenuItem.AddIn = null;
            this.排版美化ToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.page;
            this.排版美化ToolStripMenuItem.Index = 0;
            this.排版美化ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.排版美化ToolStripMenuItem.Level = 0;
            this.排版美化ToolStripMenuItem.Name = "排版美化ToolStripMenuItem";
            this.排版美化ToolStripMenuItem.Position = null;
            this.排版美化ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.排版美化ToolStripMenuItem.Text = "排版美化";
            this.排版美化ToolStripMenuItem.Click += new System.EventHandler(this.排版ToolStripMenuItem_Click);
            // 
            // 转成一行ToolStripMenuItem
            // 
            this.转成一行ToolStripMenuItem.AddIn = null;
            this.转成一行ToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.sample;
            this.转成一行ToolStripMenuItem.Index = 0;
            this.转成一行ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.转成一行ToolStripMenuItem.Level = 0;
            this.转成一行ToolStripMenuItem.Name = "转成一行ToolStripMenuItem";
            this.转成一行ToolStripMenuItem.Position = null;
            this.转成一行ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.转成一行ToolStripMenuItem.Text = "压缩脚本";
            this.转成一行ToolStripMenuItem.Click += new System.EventHandler(this.转成一行ToolStripMenuItem_Click);
            // 
            // 执行SQLToolStripMenuItem
            // 
            this.执行SQLToolStripMenuItem.AddIn = null;
            this.执行SQLToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.run;
            this.执行SQLToolStripMenuItem.Index = 0;
            this.执行SQLToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.执行SQLToolStripMenuItem.Level = 0;
            this.执行SQLToolStripMenuItem.Name = "执行SQLToolStripMenuItem";
            this.执行SQLToolStripMenuItem.Position = null;
            this.执行SQLToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.执行SQLToolStripMenuItem.Text = "执行语句";
            this.执行SQLToolStripMenuItem.Click += new System.EventHandler(this.执行SQLToolStripMenuItem_Click);
            // 
            // 查看表数据ToolStripMenuItem
            // 
            this.查看表数据ToolStripMenuItem.AddIn = null;
            this.查看表数据ToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.edit_file_write;
            this.查看表数据ToolStripMenuItem.Index = 0;
            this.查看表数据ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.查看表数据ToolStripMenuItem.Level = 0;
            this.查看表数据ToolStripMenuItem.Name = "查看表数据ToolStripMenuItem";
            this.查看表数据ToolStripMenuItem.Position = null;
            this.查看表数据ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.查看表数据ToolStripMenuItem.Text = "查看表数据";
            this.查看表数据ToolStripMenuItem.Click += new System.EventHandler(this.查看表数据ToolStripMenuItem_Click);
            // 
            // 复制ToolStripMenuItem1
            // 
            this.复制ToolStripMenuItem1.AddIn = null;
            this.复制ToolStripMenuItem1.Image = global::DevelopAssistant.Core.Properties.Resources._new;
            this.复制ToolStripMenuItem1.Index = 0;
            this.复制ToolStripMenuItem1.ItemType = "ToolStripMenuItem";
            this.复制ToolStripMenuItem1.Level = 0;
            this.复制ToolStripMenuItem1.Name = "复制ToolStripMenuItem1";
            this.复制ToolStripMenuItem1.Position = null;
            this.复制ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.复制ToolStripMenuItem1.Text = "复制(Ctrl+C)";
            this.复制ToolStripMenuItem1.Click += new System.EventHandler(this.复制ToolStripMenuItem1_Click);
            // 
            // 粘贴ToolStripMenuItem1
            // 
            this.粘贴ToolStripMenuItem1.AddIn = null;
            this.粘贴ToolStripMenuItem1.Image = global::DevelopAssistant.Core.Properties.Resources.paste_24px;
            this.粘贴ToolStripMenuItem1.Index = 0;
            this.粘贴ToolStripMenuItem1.ItemType = "ToolStripMenuItem";
            this.粘贴ToolStripMenuItem1.Level = 0;
            this.粘贴ToolStripMenuItem1.Name = "粘贴ToolStripMenuItem1";
            this.粘贴ToolStripMenuItem1.Position = null;
            this.粘贴ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.粘贴ToolStripMenuItem1.Text = "粘贴(Ctrl+V)";
            this.粘贴ToolStripMenuItem1.Click += new System.EventHandler(this.粘贴ToolStripMenuItem1_Click);
            // 
            // 添加到收藏夹ToolStripMenuItem
            // 
            this.添加到收藏夹ToolStripMenuItem.AddIn = null;
            this.添加到收藏夹ToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.right_arrow;
            this.添加到收藏夹ToolStripMenuItem.Index = 0;
            this.添加到收藏夹ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.添加到收藏夹ToolStripMenuItem.Level = 0;
            this.添加到收藏夹ToolStripMenuItem.Name = "添加到收藏夹ToolStripMenuItem";
            this.添加到收藏夹ToolStripMenuItem.Position = null;
            this.添加到收藏夹ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.添加到收藏夹ToolStripMenuItem.Text = "添加到收藏夹";
            this.添加到收藏夹ToolStripMenuItem.Click += new System.EventHandler(this.添加到收藏ToolStripMenuItem_Click);
            // 
            // 从收藏夹中获取ToolStripMenuItem
            // 
            this.从收藏夹中获取ToolStripMenuItem.AddIn = null;
            this.从收藏夹中获取ToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.left_arrow;
            this.从收藏夹中获取ToolStripMenuItem.Index = 0;
            this.从收藏夹中获取ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.从收藏夹中获取ToolStripMenuItem.Level = 0;
            this.从收藏夹中获取ToolStripMenuItem.Name = "从收藏夹中获取ToolStripMenuItem";
            this.从收藏夹中获取ToolStripMenuItem.Position = null;
            this.从收藏夹中获取ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.从收藏夹中获取ToolStripMenuItem.Text = "从收藏夹中获取";
            this.从收藏夹中获取ToolStripMenuItem.Click += new System.EventHandler(this.从收藏夹中获取ToolStripMenuItem_Click);
            // 
            // 保存为ToolStripMenuItem
            // 
            this.保存为ToolStripMenuItem.AddIn = null;
            this.保存为ToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.save;
            this.保存为ToolStripMenuItem.Index = 0;
            this.保存为ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.保存为ToolStripMenuItem.Level = 0;
            this.保存为ToolStripMenuItem.Name = "保存为ToolStripMenuItem";
            this.保存为ToolStripMenuItem.Position = null;
            this.保存为ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.保存为ToolStripMenuItem.Text = "保存为";
            this.保存为ToolStripMenuItem.Click += new System.EventHandler(this.保存为ToolStripMenuItem_Click);
            // 
            // 清除ToolStripMenuItem
            // 
            this.清除ToolStripMenuItem.AddIn = null;
            this.清除ToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.clear;
            this.清除ToolStripMenuItem.Index = 0;
            this.清除ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.清除ToolStripMenuItem.Level = 0;
            this.清除ToolStripMenuItem.Name = "清除ToolStripMenuItem";
            this.清除ToolStripMenuItem.Position = null;
            this.清除ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.清除ToolStripMenuItem.Text = "清除";
            this.清除ToolStripMenuItem.Click += new System.EventHandler(this.清除ToolStripMenuItem_Click);
            // 
            // ResultsTabControl
            // 
            this.ResultsTabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.ResultsTabControl.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(79)))), ((int)(((byte)(125)))));
            this.ResultsTabControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.ResultsTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResultsTabControl.ImageList = this.imageList1;
            this.ResultsTabControl.ItemSize = new System.Drawing.Size(0, 22);
            this.ResultsTabControl.Location = new System.Drawing.Point(0, 32);
            this.ResultsTabControl.Margin = new System.Windows.Forms.Padding(0);
            this.ResultsTabControl.Name = "ResultsTabControl";
            this.ResultsTabControl.Padding = new System.Drawing.Point(0, 0);
            this.ResultsTabControl.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ResultsTabControl.SelectedIndex = 0;
            this.ResultsTabControl.ShowBorder = true;
            this.ResultsTabControl.ShowClose = false;
            this.ResultsTabControl.ShowWaitMessage = false;
            this.ResultsTabControl.Size = new System.Drawing.Size(150, 14);
            this.ResultsTabControl.TabIndex = 1;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "comments.png");
            this.imageList1.Images.SetKeyName(1, "table.png");
            this.imageList1.Images.SetKeyName(2, "window.png");
            // 
            // ToolBarBox
            // 
            this.ToolBarBox.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.ToolBarBox.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.TopBottom;
            this.ToolBarBox.BottomBlackColor = System.Drawing.Color.Empty;
            this.ToolBarBox.Controls.Add(this.toolStrip1);
            this.ToolBarBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBarBox.Location = new System.Drawing.Point(0, 0);
            this.ToolBarBox.Margin = new System.Windows.Forms.Padding(0);
            this.ToolBarBox.MarginWidth = 0;
            this.ToolBarBox.Name = "ToolBarBox";
            this.ToolBarBox.Padding = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.ToolBarBox.Size = new System.Drawing.Size(150, 32);
            this.ToolBarBox.TabIndex = 0;
            this.ToolBarBox.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Fill;            
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolButtonAddRecord,
            this.toolButtonDeleteRecord,
            this.toolButtonCommitRecord,
            this.toolButtonExportTo,
            this.toolButtonToScript,
            this.toolButtonToJson,
            this.toolButtonEx,
            this.toolButtonPrintPrewiew,
            this.toolButtonPrint,
            this.toolButtonSwatch,
            this.toolButtonShowNumber});
            this.toolStrip1.Location = new System.Drawing.Point(0, 1);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(150, 30);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolButtonAddRecord
            // 
            this.toolButtonAddRecord.Image = global::DevelopAssistant.Core.Properties.Resources.add;
            this.toolButtonAddRecord.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonAddRecord.Name = "toolButtonAddRecord";
            this.toolButtonAddRecord.Size = new System.Drawing.Size(52, 25);
            this.toolButtonAddRecord.Text = "添加";
            this.toolButtonAddRecord.Click += new System.EventHandler(this.toolButton_Click);
            // 
            // toolButtonDeleteRecord
            // 
            this.toolButtonDeleteRecord.Image = global::DevelopAssistant.Core.Properties.Resources.delete;
            this.toolButtonDeleteRecord.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonDeleteRecord.Name = "toolButtonDeleteRecord";
            this.toolButtonDeleteRecord.Size = new System.Drawing.Size(52, 25);
            this.toolButtonDeleteRecord.Text = "删除";
            this.toolButtonDeleteRecord.Click += new System.EventHandler(this.toolButton_Click);
            // 
            // toolButtonCommitRecord
            // 
            this.toolButtonCommitRecord.Image = global::DevelopAssistant.Core.Properties.Resources.ok;
            this.toolButtonCommitRecord.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonCommitRecord.Name = "toolButtonCommitRecord";
            this.toolButtonCommitRecord.Size = new System.Drawing.Size(52, 21);
            this.toolButtonCommitRecord.Text = "提交";
            this.toolButtonCommitRecord.Click += new System.EventHandler(this.toolButton_Click);
            // 
            // toolButtonExportTo
            // 
            this.toolButtonExportTo.Image = global::DevelopAssistant.Core.Properties.Resources.excel;
            this.toolButtonExportTo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonExportTo.Name = "toolButtonExportTo";
            this.toolButtonExportTo.Size = new System.Drawing.Size(113, 21);
            this.toolButtonExportTo.Text = "导出 excel 文件";
            this.toolButtonExportTo.Click += new System.EventHandler(this.toolButton_Click);
            // 
            // toolButtonToScript
            // 
            this.toolButtonToScript.Image = global::DevelopAssistant.Core.Properties.Resources.abiword;
            this.toolButtonToScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonToScript.Name = "toolButtonToScript";
            this.toolButtonToScript.Size = new System.Drawing.Size(101, 21);
            this.toolButtonToScript.Text = "生成 sql 脚本";
            this.toolButtonToScript.Click += new System.EventHandler(this.toolButton_Click);
            // 
            // toolButtonToJson
            // 
            this.toolButtonToJson.Image = global::DevelopAssistant.Core.Properties.Resources.json_16px;
            this.toolButtonToJson.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonToJson.Name = "toolButtonToJson";
            this.toolButtonToJson.Size = new System.Drawing.Size(108, 21);
            this.toolButtonToJson.Text = "生成 json 格式";
            this.toolButtonToJson.Click += new System.EventHandler(this.toolButton_Click);
            // 
            // toolButtonEx
            // 
            this.toolButtonEx.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sQL文件ToolStripMenuItem,
            this.json文件ToolStripMenuItem,
            this.excel文件ToolStripMenuItem,
            this.word文件ToolStripMenuItem});
            this.toolButtonEx.Image = global::DevelopAssistant.Core.Properties.Resources.todown_sml;
            this.toolButtonEx.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonEx.Name = "toolButtonEx";
            this.toolButtonEx.Size = new System.Drawing.Size(82, 21);
            this.toolButtonEx.Text = "导出到...";
            this.toolButtonEx.Visible = false;
            // 
            // sQL文件ToolStripMenuItem
            // 
            this.sQL文件ToolStripMenuItem.AddIn = null;
            this.sQL文件ToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.sqlfile;
            this.sQL文件ToolStripMenuItem.Index = 0;
            this.sQL文件ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.sQL文件ToolStripMenuItem.Level = 0;
            this.sQL文件ToolStripMenuItem.Name = "sQL文件ToolStripMenuItem";
            this.sQL文件ToolStripMenuItem.Position = null;
            this.sQL文件ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.sQL文件ToolStripMenuItem.Text = "SQL 文件";
            // 
            // json文件ToolStripMenuItem
            // 
            this.json文件ToolStripMenuItem.AddIn = null;
            this.json文件ToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.json_16px;
            this.json文件ToolStripMenuItem.Index = 0;
            this.json文件ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.json文件ToolStripMenuItem.Level = 0;
            this.json文件ToolStripMenuItem.Name = "json文件ToolStripMenuItem";
            this.json文件ToolStripMenuItem.Position = null;
            this.json文件ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.json文件ToolStripMenuItem.Text = "Json 文件";
            // 
            // excel文件ToolStripMenuItem
            // 
            this.excel文件ToolStripMenuItem.AddIn = null;
            this.excel文件ToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.excel;
            this.excel文件ToolStripMenuItem.Index = 0;
            this.excel文件ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.excel文件ToolStripMenuItem.Level = 0;
            this.excel文件ToolStripMenuItem.Name = "excel文件ToolStripMenuItem";
            this.excel文件ToolStripMenuItem.Position = null;
            this.excel文件ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.excel文件ToolStripMenuItem.Text = "Excel 文件";
            // 
            // word文件ToolStripMenuItem
            // 
            this.word文件ToolStripMenuItem.AddIn = null;
            this.word文件ToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.document_editing;
            this.word文件ToolStripMenuItem.Index = 0;
            this.word文件ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.word文件ToolStripMenuItem.Level = 0;
            this.word文件ToolStripMenuItem.Name = "word文件ToolStripMenuItem";
            this.word文件ToolStripMenuItem.Position = null;
            this.word文件ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.word文件ToolStripMenuItem.Text = "Word 文件";
            // 
            // toolButtonPrintPrewiew
            // 
            this.toolButtonPrintPrewiew.Image = global::DevelopAssistant.Core.Properties.Resources.print_preview;
            this.toolButtonPrintPrewiew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonPrintPrewiew.Name = "toolButtonPrintPrewiew";
            this.toolButtonPrintPrewiew.Size = new System.Drawing.Size(76, 21);
            this.toolButtonPrintPrewiew.Text = "打印预览";
            this.toolButtonPrintPrewiew.Click += new System.EventHandler(this.toolButton_Click);
            // 
            // toolButtonPrint
            // 
            this.toolButtonPrint.Image = global::DevelopAssistant.Core.Properties.Resources.printer;
            this.toolButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonPrint.Name = "toolButtonPrint";
            this.toolButtonPrint.Size = new System.Drawing.Size(52, 21);
            this.toolButtonPrint.Text = "打印";
            this.toolButtonPrint.Click += new System.EventHandler(this.toolButton_Click);
            // 
            // toolButtonSwatch
            // 
            this.toolButtonSwatch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolButtonSwatch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonSwatch.Image = global::DevelopAssistant.Core.Properties.Resources.arrow_bottom;
            this.toolButtonSwatch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonSwatch.Name = "toolButtonSwatch";
            this.toolButtonSwatch.Size = new System.Drawing.Size(23, 20);
            this.toolButtonSwatch.Text = "收缩";
            this.toolButtonSwatch.ToolTipText = "收缩";
            this.toolButtonSwatch.Click += new System.EventHandler(this.toolButton_Click);
            // 
            // toolButtonShowNumber
            // 
            this.toolButtonShowNumber.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolButtonShowNumber.Image = global::DevelopAssistant.Core.Properties.Resources.unchecked_black;
            this.toolButtonShowNumber.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonShowNumber.Name = "toolButtonShowNumber";
            this.toolButtonShowNumber.Size = new System.Drawing.Size(76, 21);
            this.toolButtonShowNumber.Text = "显示行号";
            this.toolButtonShowNumber.Click += new System.EventHandler(this.toolButton_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.复制ToolStripMenuItem,
            this.粘贴ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 48);
            // 
            // 复制ToolStripMenuItem
            // 
            this.复制ToolStripMenuItem.AddIn = null;
            this.复制ToolStripMenuItem.Index = 0;
            this.复制ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.复制ToolStripMenuItem.Level = 0;
            this.复制ToolStripMenuItem.Name = "复制ToolStripMenuItem";
            this.复制ToolStripMenuItem.Position = null;
            this.复制ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.复制ToolStripMenuItem.Text = "复制";
            this.复制ToolStripMenuItem.Click += new System.EventHandler(this.复制ToolStripMenuItem_Click);
            // 
            // 粘贴ToolStripMenuItem
            // 
            this.粘贴ToolStripMenuItem.AddIn = null;
            this.粘贴ToolStripMenuItem.Index = 0;
            this.粘贴ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.粘贴ToolStripMenuItem.Level = 0;
            this.粘贴ToolStripMenuItem.Name = "粘贴ToolStripMenuItem";
            this.粘贴ToolStripMenuItem.Position = null;
            this.粘贴ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.粘贴ToolStripMenuItem.Text = "粘贴";
            this.粘贴ToolStripMenuItem.Click += new System.EventHandler(this.粘贴ToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.statusStrip1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.statusStrip1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.statusStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExecuteReulstMessage});
            this.statusStrip1.Location = new System.Drawing.Point(0, 596);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(876, 30);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ExecuteReulstMessage
            // 
            this.ExecuteReulstMessage.Name = "ExecuteReulstMessage";
            this.ExecuteReulstMessage.Size = new System.Drawing.Size(32, 25);
            this.ExecuteReulstMessage.Text = "完成";
            this.ExecuteReulstMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // QueryDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 626);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "QueryDocument";
            this.Text = "新建查询";
            this.Load += new System.EventHandler(this.QueryDocument_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.nPanel1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.ToolBarBox.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevelopAssistant.Service.ICSharpCodeTextEditor txtExecSqlCommand;
        private ICSharpCode.WinFormsUI.Controls.NTabControl ResultsTabControl;
        private ICSharpCode.WinFormsUI.Controls.NPanel ToolBarBox;
        private ICSharpCode.WinFormsUI.Controls.NToolStrip toolStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonAddRecord;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonDeleteRecord;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonCommitRecord;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonExportTo;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonToScript;
        private System.Windows.Forms.ImageList imageList1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private ICSharpCode.WinFormsUI.Controls.NContextMenuStrip contextMenuStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 复制ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 粘贴ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NStatusBar statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel ExecuteReulstMessage;
        private ICSharpCode.WinFormsUI.Controls.NContextMenuStrip contextMenuStrip2;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 复制ToolStripMenuItem1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 粘贴ToolStripMenuItem1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 排版美化ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 转成一行ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 清除ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 保存为ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonPrintPrewiew;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonPrint;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonShowNumber;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 添加到收藏夹ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonToJson;
        private System.Windows.Forms.ToolStripDropDownButton toolButtonEx;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem sQL文件ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem json文件ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem excel文件ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem word文件ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 从收藏夹中获取ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NPanel nPanel1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonSwatch;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 执行SQLToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 查看表数据ToolStripMenuItem;
    }
}