﻿namespace DevelopAssistant.Core.DBMS
{
    partial class TableDataEditorDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TableDataEditorDocument));
            this.pager1 = new ICSharpCode.WinFormsUI.Controls.NPager();
            this.dgv_list = new ICSharpCode.WinFormsUI.Controls.NDataGridView();
            this.toolStrip1 = new ICSharpCode.WinFormsUI.Controls.NToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolButtonQuery = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolButtonCommit = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.contextMenuStrip1 = new ICSharpCode.WinFormsUI.Controls.NContextMenuStrip(this.components);
            this.添加数据ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.删除数据ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_list)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pager1
            // 
            this.pager1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pager1.Location = new System.Drawing.Point(10, 428);
            this.pager1.Name = "pager1";
            this.pager1.PageCount = 0;
            this.pager1.PageCurrent = 1;
            this.pager1.Size = new System.Drawing.Size(759, 30);
            this.pager1.TabIndex = 0;
            this.pager1.TotalRecord = 0;
            this.pager1.EventPaging += new ICSharpCode.WinFormsUI.Controls.NPager.EventPagingHandler(this.pager1_EventPaging);
            // 
            // dgv_list
            // 
            this.dgv_list.AllowUserToAddRows = false;
            this.dgv_list.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_list.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_list.BackgroundColor = System.Drawing.Color.White;
            this.dgv_list.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_list.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_list.DatetimeFormat = "yyyy-MM-dd HH:mm:ss";
            this.dgv_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_list.EnableHeadersVisualStyles = false;
            this.dgv_list.Location = new System.Drawing.Point(10, 34);
            this.dgv_list.MultiSelect = false;
            this.dgv_list.Name = "dgv_list";
            this.dgv_list.RowTemplate.Height = 23;
            this.dgv_list.ShowRowNumber = false;
            this.dgv_list.Size = new System.Drawing.Size(759, 394);
            this.dgv_list.TabIndex = 2;
            // 
            // toolStrip1
            //                        
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripTextBox1,
            this.toolButtonQuery,
            this.toolButtonCommit});            
            this.toolStrip1.Location = new System.Drawing.Point(10, 4);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(1);
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(759, 30);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(80, 24);
            this.toolStripLabel1.Text = "搜索关键字：";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripTextBox1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.ReadOnly = true;
            this.toolStripTextBox1.Size = new System.Drawing.Size(210, 23);
            // 
            // toolButtonQuery
            // 
            this.toolButtonQuery.Image = global::DevelopAssistant.Core.Properties.Resources.query;
            this.toolButtonQuery.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonQuery.Name = "toolButtonQuery";
            this.toolButtonQuery.Size = new System.Drawing.Size(76, 24);
            this.toolButtonQuery.Text = "数据检索";
            this.toolButtonQuery.Click += new System.EventHandler(this.toolButtonQuery_Click);
            // 
            // toolButtonCommit
            // 
            this.toolButtonCommit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolButtonCommit.Image = global::DevelopAssistant.Core.Properties.Resources.ok;
            this.toolButtonCommit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonCommit.Name = "toolButtonCommit";
            this.toolButtonCommit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolButtonCommit.Size = new System.Drawing.Size(76, 24);
            this.toolButtonCommit.Text = "提交更改";
            this.toolButtonCommit.Click += new System.EventHandler(this.toolButtonCommit_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加数据ToolStripMenuItem,
            this.删除数据ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 48);
            // 
            // 添加数据ToolStripMenuItem
            // 
            this.添加数据ToolStripMenuItem.Name = "添加数据ToolStripMenuItem";
            this.添加数据ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.添加数据ToolStripMenuItem.Text = "添加数据";
            this.添加数据ToolStripMenuItem.Click += new System.EventHandler(this.添加数据ToolStripMenuItem_Click);
            // 
            // 删除数据ToolStripMenuItem
            // 
            this.删除数据ToolStripMenuItem.Name = "删除数据ToolStripMenuItem";
            this.删除数据ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.删除数据ToolStripMenuItem.Text = "删除数据";
            this.删除数据ToolStripMenuItem.Click += new System.EventHandler(this.删除数据ToolStripMenuItem_Click);
            // 
            // TableDataEditorDocument
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(779, 458);
            this.Controls.Add(this.dgv_list);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.pager1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TableDataEditorDocument";
            this.Padding = new System.Windows.Forms.Padding(10, 4, 10, 0);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TableDataEditorDocument";
            this.Load += new System.EventHandler(this.TableDataEditorDocument_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_list)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPager pager1;
        private ICSharpCode.WinFormsUI.Controls.NDataGridView dgv_list;
        private ICSharpCode.WinFormsUI.Controls.NToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonCommit;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonQuery;
        private ICSharpCode.WinFormsUI.Controls.NContextMenuStrip contextMenuStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 添加数据ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 删除数据ToolStripMenuItem;
    }
}