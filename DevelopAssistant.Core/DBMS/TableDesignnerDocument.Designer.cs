﻿namespace DevelopAssistant.Core.DBMS
{
    partial class TableDesignnerDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TableDesignnerDocument));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new ICSharpCode.WinFormsUI.Controls.NToolStrip();
            this.toolStripButtonPrimaryKey = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonAddColumn = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonDeleteColumn = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripSeparator1 = new ICSharpCode.WinFormsUI.Controls.NToolStripSeparator();
            this.toolStripButtonToUp = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonToDown = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonSave = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonReturn = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.btnApplyOk = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnCancle = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.dgv_design = new ICSharpCode.WinFormsUI.Controls.NDataGridView();
            this.ColumnOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDataType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDataLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPrimaryKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIsnull = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnIdentity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDescrib = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDefaultValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIcon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStrip1 = new ICSharpCode.WinFormsUI.Controls.NContextMenuStrip(this.components);
            this.添加列ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.删除列ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.上移ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.下移ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.设为主键ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.tabControl1 = new ICSharpCode.WinFormsUI.Controls.NTabControl();
            this.tableTabPage = new System.Windows.Forms.TabPage();
            this.triggerTabPage = new System.Windows.Forms.TabPage();
            this.dgv_trigger = new ICSharpCode.WinFormsUI.Controls.NDataGridView();
            this.colTriggerIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTriggerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTriggerType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTableName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTrigger_statement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.indexesTabPage = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgv_indexes = new ICSharpCode.WinFormsUI.Controls.NDataGridView();
            this.colIndexRowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIndexName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIndexType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIndexDescriptoin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIndexTableName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIndexStatement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeTabPage = new System.Windows.Forms.TabPage();
            this.btnCopyto = new ICSharpCode.WinFormsUI.Controls.NImageButton();
            this.textEditorControl1 = new ICSharpCode.TextEditor.TextEditorControl();
            this.ToolBox = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblTableDescription = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_design)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tableTabPage.SuspendLayout();
            this.triggerTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_trigger)).BeginInit();
            this.indexesTabPage.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_indexes)).BeginInit();
            this.codeTabPage.SuspendLayout();
            this.ToolBox.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.GripMargin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonPrimaryKey,
            this.toolStripButtonAddColumn,
            this.toolStripButtonDeleteColumn,
            this.toolStripSeparator1,
            this.toolStripButtonToUp,
            this.toolStripButtonToDown,
            this.toolStripButtonSave,
            this.toolStripButtonReturn});
            this.toolStrip1.Location = new System.Drawing.Point(3, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 2, 1, 2);
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(597, 25);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.Visible = false;
            // 
            // toolStripButtonPrimaryKey
            // 
            this.toolStripButtonPrimaryKey.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPrimaryKey.Image = global::DevelopAssistant.Core.Properties.Resources.pk;
            this.toolStripButtonPrimaryKey.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPrimaryKey.Name = "toolStripButtonPrimaryKey";
            this.toolStripButtonPrimaryKey.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonPrimaryKey.Text = "设为主键";
            this.toolStripButtonPrimaryKey.Click += new System.EventHandler(this.设为主键ToolStripMenuItem_Click);
            // 
            // toolStripButtonAddColumn
            // 
            this.toolStripButtonAddColumn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAddColumn.Image = global::DevelopAssistant.Core.Properties.Resources.add;
            this.toolStripButtonAddColumn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddColumn.Name = "toolStripButtonAddColumn";
            this.toolStripButtonAddColumn.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonAddColumn.Text = "添加";
            this.toolStripButtonAddColumn.Click += new System.EventHandler(this.添加列ToolStripMenuItem_Click);
            // 
            // toolStripButtonDeleteColumn
            // 
            this.toolStripButtonDeleteColumn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonDeleteColumn.Image = global::DevelopAssistant.Core.Properties.Resources.delete;
            this.toolStripButtonDeleteColumn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteColumn.Name = "toolStripButtonDeleteColumn";
            this.toolStripButtonDeleteColumn.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonDeleteColumn.Text = "删除";
            this.toolStripButtonDeleteColumn.Click += new System.EventHandler(this.删除列ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 21);
            // 
            // toolStripButtonToUp
            // 
            this.toolStripButtonToUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonToUp.Image = global::DevelopAssistant.Core.Properties.Resources.toup_sml;
            this.toolStripButtonToUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonToUp.Name = "toolStripButtonToUp";
            this.toolStripButtonToUp.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonToUp.Text = "上移";
            this.toolStripButtonToUp.Click += new System.EventHandler(this.上移ToolStripMenuItem_Click);
            // 
            // toolStripButtonToDown
            // 
            this.toolStripButtonToDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonToDown.Image = global::DevelopAssistant.Core.Properties.Resources.todown_sml;
            this.toolStripButtonToDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonToDown.Name = "toolStripButtonToDown";
            this.toolStripButtonToDown.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonToDown.Text = "下移";
            this.toolStripButtonToDown.Click += new System.EventHandler(this.下移ToolStripMenuItem_Click);
            // 
            // toolStripButtonSave
            // 
            this.toolStripButtonSave.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSave.Image = global::DevelopAssistant.Core.Properties.Resources.save;
            this.toolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSave.Name = "toolStripButtonSave";
            this.toolStripButtonSave.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonSave.Text = "保存";
            this.toolStripButtonSave.Click += new System.EventHandler(this.toolStripButtonSave_Click);
            // 
            // toolStripButtonReturn
            // 
            this.toolStripButtonReturn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonReturn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonReturn.Image = global::DevelopAssistant.Core.Properties.Resources._return;
            this.toolStripButtonReturn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonReturn.Name = "toolStripButtonReturn";
            this.toolStripButtonReturn.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonReturn.Text = "撤销";
            this.toolStripButtonReturn.Click += new System.EventHandler(this.toolStripButtonReturn_Click);
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnApplyOk);
            this.panel1.Controls.Add(this.btnCancle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 396);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(623, 59);
            this.panel1.TabIndex = 4;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(281, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "提示：在表有超过 1000000 (百万) 条数据时请慎用";
            // 
            // btnApplyOk
            // 
            this.btnApplyOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyOk.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApplyOk.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApplyOk.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApplyOk.Enabled = false;
            this.btnApplyOk.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApplyOk.FouseColor = System.Drawing.Color.White;
            this.btnApplyOk.Foused = false;
            this.btnApplyOk.FouseTextColor = System.Drawing.Color.Black;
            this.btnApplyOk.Icon = null;
            this.btnApplyOk.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApplyOk.Location = new System.Drawing.Point(439, 14);
            this.btnApplyOk.Name = "btnApplyOk";
            this.btnApplyOk.Radius = 0;
            this.btnApplyOk.Size = new System.Drawing.Size(75, 30);
            this.btnApplyOk.TabIndex = 1;
            this.btnApplyOk.Text = "保存";
            this.btnApplyOk.UnableIcon = null;
            this.btnApplyOk.UseVisualStyleBackColor = true;
            this.btnApplyOk.Click += new System.EventHandler(this.btnApplyOk_Click);
            // 
            // btnCancle
            // 
            this.btnCancle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancle.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnCancle.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnCancle.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnCancle.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnCancle.FouseColor = System.Drawing.Color.White;
            this.btnCancle.Foused = false;
            this.btnCancle.FouseTextColor = System.Drawing.Color.Black;
            this.btnCancle.Icon = null;
            this.btnCancle.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnCancle.Location = new System.Drawing.Point(531, 14);
            this.btnCancle.Name = "btnCancle";
            this.btnCancle.Radius = 0;
            this.btnCancle.Size = new System.Drawing.Size(75, 30);
            this.btnCancle.TabIndex = 0;
            this.btnCancle.Text = "取消";
            this.btnCancle.UnableIcon = null;
            this.btnCancle.UseVisualStyleBackColor = true;
            this.btnCancle.Click += new System.EventHandler(this.btnCancle_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "表名：";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Enabled = true;
            this.textBox1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox1.FousedColor = System.Drawing.Color.Orange;
            this.textBox1.Icon = null;
            this.textBox1.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.textBox1.IsButtonTextBox = false;
            this.textBox1.IsClearTextBox = false;
            this.textBox1.IsPasswordTextBox = false;
            this.textBox1.Location = new System.Drawing.Point(60, 8);
            this.textBox1.MaxLength = 32767;
            this.textBox1.Multiline = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.PasswordChar = '\0';
            this.textBox1.Placeholder = null;
            this.textBox1.ReadOnly = false;
            this.textBox1.Size = new System.Drawing.Size(169, 24);
            this.textBox1.TabIndex = 6;
            this.textBox1.UseSystemPasswordChar = false;
            this.textBox1.XBackColor = System.Drawing.Color.White;
            this.textBox1.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // dgv_design
            // 
            this.dgv_design.AllowUserToAddRows = false;
            this.dgv_design.AllowUserToOrderColumns = true;
            this.dgv_design.BackgroundColor = System.Drawing.Color.White;
            this.dgv_design.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_design.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_design.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_design.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnOrder,
            this.ColumnName,
            this.ColumnDataType,
            this.ColumnDataLength,
            this.ColumnPrimaryKey,
            this.ColumnIsnull,
            this.ColumnIdentity,
            this.ColumnDescrib,
            this.ColumnDefaultValue,
            this.ColumnIcon});
            this.dgv_design.DatetimeFormat = "yyyy-MM-dd HH:mm:ss";
            this.dgv_design.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_design.EnableHeadersVisualStyles = false;
            this.dgv_design.Location = new System.Drawing.Point(3, 3);
            this.dgv_design.Margin = new System.Windows.Forms.Padding(0);
            this.dgv_design.Name = "dgv_design";
            this.dgv_design.RowHeadersWidth = 42;
            this.dgv_design.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_design.RowTemplate.Height = 23;
            this.dgv_design.ShowRowNumber = false;
            this.dgv_design.Size = new System.Drawing.Size(597, 312);
            this.dgv_design.TabIndex = 7;
            this.dgv_design.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_design_CellMouseClick);
            this.dgv_design.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgv_design_MouseClick);
            // 
            // ColumnOrder
            // 
            this.ColumnOrder.DataPropertyName = "序号";
            this.ColumnOrder.HeaderText = "序号";
            this.ColumnOrder.Name = "ColumnOrder";
            this.ColumnOrder.ReadOnly = true;
            this.ColumnOrder.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColumnOrder.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnOrder.Visible = false;
            this.ColumnOrder.Width = 60;
            // 
            // ColumnName
            // 
            this.ColumnName.DataPropertyName = "名称";
            this.ColumnName.HeaderText = "字段名称";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnDataType
            // 
            this.ColumnDataType.DataPropertyName = "数据类型";
            this.ColumnDataType.HeaderText = "数据类型";
            this.ColumnDataType.Name = "ColumnDataType";
            this.ColumnDataType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnDataType.Width = 120;
            // 
            // ColumnDataLength
            // 
            this.ColumnDataLength.DataPropertyName = "长度";
            this.ColumnDataLength.HeaderText = "长度";
            this.ColumnDataLength.Name = "ColumnDataLength";
            this.ColumnDataLength.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnDataLength.Width = 80;
            // 
            // ColumnPrimaryKey
            // 
            this.ColumnPrimaryKey.DataPropertyName = "主键标识";
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            this.ColumnPrimaryKey.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColumnPrimaryKey.HeaderText = "主键标识";
            this.ColumnPrimaryKey.Name = "ColumnPrimaryKey";
            this.ColumnPrimaryKey.ReadOnly = true;
            this.ColumnPrimaryKey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnPrimaryKey.Visible = false;
            // 
            // ColumnIsnull
            // 
            this.ColumnIsnull.DataPropertyName = "允许为空";
            this.ColumnIsnull.HeaderText = "允许为空";
            this.ColumnIsnull.Name = "ColumnIsnull";
            this.ColumnIsnull.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColumnIsnull.Width = 84;
            // 
            // ColumnIdentity
            // 
            this.ColumnIdentity.DataPropertyName = "自增标识";
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            this.ColumnIdentity.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColumnIdentity.HeaderText = "自增标识";
            this.ColumnIdentity.Name = "ColumnIdentity";
            this.ColumnIdentity.ReadOnly = true;
            this.ColumnIdentity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnIdentity.Visible = false;
            // 
            // ColumnDescrib
            // 
            this.ColumnDescrib.DataPropertyName = "描述";
            this.ColumnDescrib.HeaderText = "描述";
            this.ColumnDescrib.Name = "ColumnDescrib";
            this.ColumnDescrib.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnDescrib.Width = 180;
            // 
            // ColumnDefaultValue
            // 
            this.ColumnDefaultValue.DataPropertyName = "默认值";
            this.ColumnDefaultValue.HeaderText = "默认值";
            this.ColumnDefaultValue.Name = "ColumnDefaultValue";
            this.ColumnDefaultValue.ReadOnly = true;
            // 
            // ColumnIcon
            // 
            this.ColumnIcon.DataPropertyName = "全标识";
            this.ColumnIcon.HeaderText = "全标识";
            this.ColumnIcon.Name = "ColumnIcon";
            this.ColumnIcon.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnIcon.Visible = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "pk.gif");
            this.imageList1.Images.SetKeyName(1, "table.png");
            this.imageList1.Images.SetKeyName(2, "script_full_code.png");
            this.imageList1.Images.SetKeyName(3, "behind_object.png");
            this.imageList1.Images.SetKeyName(4, "00295.png");
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加列ToolStripMenuItem,
            this.删除列ToolStripMenuItem,
            this.上移ToolStripMenuItem,
            this.下移ToolStripMenuItem,
            this.设为主键ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 114);
            // 
            // 添加列ToolStripMenuItem
            // 
            this.添加列ToolStripMenuItem.AddIn = null;
            this.添加列ToolStripMenuItem.Index = 0;
            this.添加列ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.添加列ToolStripMenuItem.Level = 0;
            this.添加列ToolStripMenuItem.Name = "添加列ToolStripMenuItem";
            this.添加列ToolStripMenuItem.Position = null;
            this.添加列ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.添加列ToolStripMenuItem.Text = "添加列";
            this.添加列ToolStripMenuItem.Click += new System.EventHandler(this.添加列ToolStripMenuItem_Click);
            // 
            // 删除列ToolStripMenuItem
            // 
            this.删除列ToolStripMenuItem.AddIn = null;
            this.删除列ToolStripMenuItem.Index = 0;
            this.删除列ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.删除列ToolStripMenuItem.Level = 0;
            this.删除列ToolStripMenuItem.Name = "删除列ToolStripMenuItem";
            this.删除列ToolStripMenuItem.Position = null;
            this.删除列ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.删除列ToolStripMenuItem.Text = "删除列";
            this.删除列ToolStripMenuItem.Click += new System.EventHandler(this.删除列ToolStripMenuItem_Click);
            // 
            // 上移ToolStripMenuItem
            // 
            this.上移ToolStripMenuItem.AddIn = null;
            this.上移ToolStripMenuItem.Index = 0;
            this.上移ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.上移ToolStripMenuItem.Level = 0;
            this.上移ToolStripMenuItem.Name = "上移ToolStripMenuItem";
            this.上移ToolStripMenuItem.Position = null;
            this.上移ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.上移ToolStripMenuItem.Text = "上移";
            this.上移ToolStripMenuItem.Click += new System.EventHandler(this.上移ToolStripMenuItem_Click);
            // 
            // 下移ToolStripMenuItem
            // 
            this.下移ToolStripMenuItem.AddIn = null;
            this.下移ToolStripMenuItem.Index = 0;
            this.下移ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.下移ToolStripMenuItem.Level = 0;
            this.下移ToolStripMenuItem.Name = "下移ToolStripMenuItem";
            this.下移ToolStripMenuItem.Position = null;
            this.下移ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.下移ToolStripMenuItem.Text = "下移";
            this.下移ToolStripMenuItem.Click += new System.EventHandler(this.下移ToolStripMenuItem_Click);
            // 
            // 设为主键ToolStripMenuItem
            // 
            this.设为主键ToolStripMenuItem.AddIn = null;
            this.设为主键ToolStripMenuItem.Index = 0;
            this.设为主键ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.设为主键ToolStripMenuItem.Level = 0;
            this.设为主键ToolStripMenuItem.Name = "设为主键ToolStripMenuItem";
            this.设为主键ToolStripMenuItem.Position = null;
            this.设为主键ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.设为主键ToolStripMenuItem.Text = "设为主键";
            this.设为主键ToolStripMenuItem.Click += new System.EventHandler(this.设为主键ToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl1.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(79)))), ((int)(((byte)(125)))));
            this.tabControl1.Controls.Add(this.tableTabPage);
            this.tabControl1.Controls.Add(this.triggerTabPage);
            this.tabControl1.Controls.Add(this.indexesTabPage);
            this.tabControl1.Controls.Add(this.codeTabPage);
            this.tabControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ImageList = this.imageList1;
            this.tabControl1.ItemSize = new System.Drawing.Size(96, 26);
            this.tabControl1.Location = new System.Drawing.Point(6, 4);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Radius = 1;
            this.tabControl1.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowBorder = true;
            this.tabControl1.ShowClose = false;
            this.tabControl1.ShowWaitMessage = false;
            this.tabControl1.Size = new System.Drawing.Size(611, 352);
            this.tabControl1.TabCaptionLm = -3;
            this.tabControl1.TabIndex = 9;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tableTabPage
            // 
            this.tableTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.tableTabPage.Controls.Add(this.dgv_design);
            this.tableTabPage.Controls.Add(this.toolStrip1);
            this.tableTabPage.Location = new System.Drawing.Point(4, 4);
            this.tableTabPage.Name = "tableTabPage";
            this.tableTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.tableTabPage.Size = new System.Drawing.Size(603, 318);
            this.tableTabPage.TabIndex = 0;
            this.tableTabPage.Text = "设计";
            // 
            // triggerTabPage
            // 
            this.triggerTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.triggerTabPage.Controls.Add(this.dgv_trigger);
            this.triggerTabPage.Location = new System.Drawing.Point(4, 4);
            this.triggerTabPage.Name = "triggerTabPage";
            this.triggerTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.triggerTabPage.Size = new System.Drawing.Size(192, 70);
            this.triggerTabPage.TabIndex = 2;
            this.triggerTabPage.Text = "触发器";
            // 
            // dgv_trigger
            // 
            this.dgv_trigger.AllowUserToAddRows = false;
            this.dgv_trigger.AllowUserToOrderColumns = true;
            this.dgv_trigger.BackgroundColor = System.Drawing.Color.White;
            this.dgv_trigger.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_trigger.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_trigger.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_trigger.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colTriggerIndex,
            this.colTriggerName,
            this.colTriggerType,
            this.colDescription,
            this.colTableName,
            this.colTrigger_statement});
            this.dgv_trigger.DatetimeFormat = "yyyy-MM-dd HH:mm:ss";
            this.dgv_trigger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_trigger.EnableHeadersVisualStyles = false;
            this.dgv_trigger.Location = new System.Drawing.Point(3, 3);
            this.dgv_trigger.Margin = new System.Windows.Forms.Padding(0);
            this.dgv_trigger.Name = "dgv_trigger";
            this.dgv_trigger.RowHeadersWidth = 42;
            this.dgv_trigger.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_trigger.RowTemplate.Height = 23;
            this.dgv_trigger.ShowRowNumber = false;
            this.dgv_trigger.Size = new System.Drawing.Size(186, 64);
            this.dgv_trigger.TabIndex = 8;
            // 
            // colTriggerIndex
            // 
            this.colTriggerIndex.DataPropertyName = "colTriggerIndex";
            this.colTriggerIndex.HeaderText = "序号";
            this.colTriggerIndex.Name = "colTriggerIndex";
            this.colTriggerIndex.ReadOnly = true;
            this.colTriggerIndex.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colTriggerIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colTriggerIndex.Width = 60;
            // 
            // colTriggerName
            // 
            this.colTriggerName.DataPropertyName = "trigger_name";
            this.colTriggerName.HeaderText = "触发器名称";
            this.colTriggerName.Name = "colTriggerName";
            this.colTriggerName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colTriggerName.Width = 140;
            // 
            // colTriggerType
            // 
            this.colTriggerType.DataPropertyName = "trigger_type";
            this.colTriggerType.HeaderText = "触发器类型";
            this.colTriggerType.Name = "colTriggerType";
            this.colTriggerType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colDescription
            // 
            this.colDescription.DataPropertyName = "trigger_descrip";
            this.colDescription.HeaderText = "描述";
            this.colDescription.Name = "colDescription";
            this.colDescription.Width = 240;
            // 
            // colTableName
            // 
            this.colTableName.DataPropertyName = "table_name";
            this.colTableName.HeaderText = "表名";
            this.colTableName.Name = "colTableName";
            this.colTableName.Visible = false;
            // 
            // colTrigger_statement
            // 
            this.colTrigger_statement.DataPropertyName = "trigger_statement";
            this.colTrigger_statement.HeaderText = "脚本代码";
            this.colTrigger_statement.Name = "colTrigger_statement";
            this.colTrigger_statement.Visible = false;
            // 
            // indexesTabPage
            // 
            this.indexesTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.indexesTabPage.Controls.Add(this.panel4);
            this.indexesTabPage.Location = new System.Drawing.Point(4, 4);
            this.indexesTabPage.Name = "indexesTabPage";
            this.indexesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.indexesTabPage.Size = new System.Drawing.Size(192, 70);
            this.indexesTabPage.TabIndex = 3;
            this.indexesTabPage.Text = "索引";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.dgv_indexes);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(186, 64);
            this.panel4.TabIndex = 10;
            // 
            // dgv_indexes
            // 
            this.dgv_indexes.AllowUserToAddRows = false;
            this.dgv_indexes.AllowUserToOrderColumns = true;
            this.dgv_indexes.BackgroundColor = System.Drawing.Color.White;
            this.dgv_indexes.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_indexes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_indexes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_indexes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIndexRowIndex,
            this.colIndexName,
            this.colIndexType,
            this.colIndexDescriptoin,
            this.colIndexTableName,
            this.colIndexStatement});
            this.dgv_indexes.DatetimeFormat = "yyyy-MM-dd HH:mm:ss";
            this.dgv_indexes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_indexes.EnableHeadersVisualStyles = false;
            this.dgv_indexes.Location = new System.Drawing.Point(0, 0);
            this.dgv_indexes.Margin = new System.Windows.Forms.Padding(0);
            this.dgv_indexes.Name = "dgv_indexes";
            this.dgv_indexes.RowHeadersWidth = 42;
            this.dgv_indexes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_indexes.RowTemplate.Height = 23;
            this.dgv_indexes.ShowRowNumber = false;
            this.dgv_indexes.Size = new System.Drawing.Size(186, 64);
            this.dgv_indexes.TabIndex = 9;
            // 
            // colIndexRowIndex
            // 
            this.colIndexRowIndex.DataPropertyName = "row_index";
            this.colIndexRowIndex.HeaderText = "序号";
            this.colIndexRowIndex.Name = "colIndexRowIndex";
            this.colIndexRowIndex.ReadOnly = true;
            this.colIndexRowIndex.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colIndexRowIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colIndexRowIndex.Width = 60;
            // 
            // colIndexName
            // 
            this.colIndexName.DataPropertyName = "index_name";
            this.colIndexName.HeaderText = "索引名称";
            this.colIndexName.Name = "colIndexName";
            this.colIndexName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colIndexName.Width = 140;
            // 
            // colIndexType
            // 
            this.colIndexType.DataPropertyName = "index_type";
            this.colIndexType.HeaderText = "索引类型";
            this.colIndexType.Name = "colIndexType";
            this.colIndexType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colIndexDescriptoin
            // 
            this.colIndexDescriptoin.DataPropertyName = "index_descrip";
            this.colIndexDescriptoin.HeaderText = "描述";
            this.colIndexDescriptoin.Name = "colIndexDescriptoin";
            this.colIndexDescriptoin.Width = 240;
            // 
            // colIndexTableName
            // 
            this.colIndexTableName.DataPropertyName = "table_name";
            this.colIndexTableName.HeaderText = "表名";
            this.colIndexTableName.Name = "colIndexTableName";
            this.colIndexTableName.Visible = false;
            // 
            // colIndexStatement
            // 
            this.colIndexStatement.DataPropertyName = "index_statement";
            this.colIndexStatement.HeaderText = "脚本代码";
            this.colIndexStatement.Name = "colIndexStatement";
            this.colIndexStatement.Visible = false;
            // 
            // codeTabPage
            // 
            this.codeTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.codeTabPage.Controls.Add(this.btnCopyto);
            this.codeTabPage.Controls.Add(this.textEditorControl1);
            this.codeTabPage.Controls.Add(this.ToolBox);
            this.codeTabPage.Location = new System.Drawing.Point(4, 4);
            this.codeTabPage.Name = "codeTabPage";
            this.codeTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.codeTabPage.Size = new System.Drawing.Size(192, 70);
            this.codeTabPage.TabIndex = 1;
            this.codeTabPage.Text = "脚本";
            // 
            // btnCopyto
            // 
            this.btnCopyto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopyto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCopyto.Image = global::DevelopAssistant.Core.Properties.Resources.copy_doc_16px;
            this.btnCopyto.Location = new System.Drawing.Point(167, 8);
            this.btnCopyto.Name = "btnCopyto";
            this.btnCopyto.Size = new System.Drawing.Size(16, 16);
            this.btnCopyto.TabIndex = 2;
            this.btnCopyto.ToolTipText = null;
            this.btnCopyto.Click += new System.EventHandler(this.btnCopyto_Click);
            // 
            // textEditorControl1
            // 
            this.textEditorControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.textEditorControl1.BookMarkEnableToggle = true;
            this.textEditorControl1.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.textEditorControl1.BorderVisable = true;
            this.textEditorControl1.ComparisonState = false;
            this.textEditorControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textEditorControl1.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.textEditorControl1.IsReadOnly = false;
            this.textEditorControl1.Location = new System.Drawing.Point(3, 29);
            this.textEditorControl1.Name = "textEditorControl1";
            this.textEditorControl1.Padding = new System.Windows.Forms.Padding(1);
            this.textEditorControl1.ShowGuidelines = false;
            this.textEditorControl1.Size = new System.Drawing.Size(186, 38);
            this.textEditorControl1.TabIndex = 0;
            // 
            // ToolBox
            // 
            this.ToolBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ToolBox.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.ToolBox.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.NoBottom;
            this.ToolBox.BottomBlackColor = System.Drawing.Color.Empty;
            this.ToolBox.Controls.Add(this.label4);
            this.ToolBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBox.Location = new System.Drawing.Point(3, 3);
            this.ToolBox.MarginWidth = 0;
            this.ToolBox.Name = "ToolBox";
            this.ToolBox.Size = new System.Drawing.Size(186, 26);
            this.ToolBox.TabIndex = 1;
            this.ToolBox.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "SQL脚本：";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblTableDescription);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(623, 36);
            this.panel2.TabIndex = 8;
            // 
            // lblTableDescription
            // 
            this.lblTableDescription.Location = new System.Drawing.Point(292, 9);
            this.lblTableDescription.Name = "lblTableDescription";
            this.lblTableDescription.Size = new System.Drawing.Size(301, 21);
            this.lblTableDescription.TabIndex = 12;
            this.lblTableDescription.Text = "用户表";
            this.lblTableDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTableDescription.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(245, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 11;
            this.label3.Text = "描述：";
            this.label3.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tabControl1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 36);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.panel3.Size = new System.Drawing.Size(623, 360);
            this.panel3.TabIndex = 10;
            // 
            // TableDesignnerDocument
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(623, 455);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TableDesignnerDocument";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "设计表";
            this.Load += new System.EventHandler(this.TableDesignnerDocument_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_design)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tableTabPage.ResumeLayout(false);
            this.triggerTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_trigger)).EndInit();
            this.indexesTabPage.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_indexes)).EndInit();
            this.codeTabPage.ResumeLayout(false);
            this.ToolBox.ResumeLayout(false);
            this.ToolBox.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NToolStrip toolStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonAddColumn;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonDeleteColumn;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonToUp;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonToDown;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NButton btnApplyOk;
        private ICSharpCode.WinFormsUI.Controls.NButton btnCancle;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonSave;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NTextBox textBox1;
        private ICSharpCode.WinFormsUI.Controls.NDataGridView dgv_design;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonReturn;
        private ICSharpCode.WinFormsUI.Controls.NToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ImageList imageList1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonPrimaryKey;
        private ICSharpCode.WinFormsUI.Controls.NContextMenuStrip contextMenuStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 添加列ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 删除列ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 上移ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 下移ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 设为主键ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NTabControl tabControl1;
        private System.Windows.Forms.TabPage tableTabPage;
        private System.Windows.Forms.TabPage codeTabPage;
        private ICSharpCode.TextEditor.TextEditorControl textEditorControl1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabPage triggerTabPage;
        private System.Windows.Forms.Label label3;
        private ICSharpCode.WinFormsUI.Controls.NDataGridView dgv_trigger;
        private ICSharpCode.WinFormsUI.Controls.NPanel ToolBox;
        private System.Windows.Forms.Label label4;
        private ICSharpCode.WinFormsUI.Controls.NImageButton btnCopyto;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTriggerIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTriggerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTriggerType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTableName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTrigger_statement;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDataType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDataLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPrimaryKey;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnIsnull;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIdentity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDescrib;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDefaultValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIcon;
        private System.Windows.Forms.TabPage indexesTabPage;
        private ICSharpCode.WinFormsUI.Controls.NDataGridView dgv_indexes;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIndexRowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIndexName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIndexType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIndexDescriptoin;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIndexTableName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIndexStatement;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblTableDescription;
    }
}