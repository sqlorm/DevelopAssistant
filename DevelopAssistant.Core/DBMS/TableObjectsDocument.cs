﻿using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Docking;
using NORM.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace DevelopAssistant.Core.DBMS
{
    public partial class TableObjectsDocument : DockContent
    {
        string tableName;
        DataBaseServer dataBaseServer;

        public TableObjectsDocument()
        {
            InitializeComponent();
            InitializeControls();
        }

        public TableObjectsDocument(string name, DataBaseServer server)
            : this()
        {

            this.tableName = name;
            this.dataBaseServer = server;

            if (AppSettings.WindowTheme != null)
            {
                Type type = AppSettings.WindowTheme.GetType();
                if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeVS2012))
                {
                    tabControl1.Radius = 1;
                    tabControl1.ItemSize = new Size(80, 28);
                }
                else if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeMac))
                {
                    tabControl1.Radius = 10;
                    tabControl1.ItemSize = new Size(90, 24);
                }
                else if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeShadow))
                {
                    tabControl1.Radius = 8;
                    tabControl1.ItemSize = new System.Drawing.Size(84, 26);
                }
            }
        }

        private void InitializeControls()
        {
            tabControl1.Radius = 1;
            tabControl1.ShowClose = false;
            tabControl1.TabCaptionLm = 4;
            tabControl1.ItemSize = new System.Drawing.Size(84, 26);
            tabControl1.BorderColor = SystemColors.ControlDark;
            //tabControl1.ArrowColor = Color.White;
            tabControl1.BaseColor = SystemColors.Control;
            tabControl1.BackColor = SystemColors.Control;
            tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
        }

        private void TableObjectsDocument_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new EventArgs());

            System.Threading.Tasks.Task.Run(() => {

                using (var db = Utility.GetAdohelper(this.dataBaseServer))
                {
                    CountDownLatch cdl = new CountDownLatch(3);

                    ThreadPool.QueueUserWorkItem(new WaitCallback((object stateInfo) =>
                    {
                        GetTableIndexes(this.dataGridView1, db);
                        //任务处理完上报状态 完成
                        CountDownLatch counterDownLatch = (CountDownLatch)stateInfo;
                        counterDownLatch.CountDown();

                    }), cdl);

                    ThreadPool.QueueUserWorkItem(new WaitCallback((object stateInfo) =>
                    {
                        GetTableTrigger(this.dataGridView2, db);
                        //任务处理完上报状态 完成
                        CountDownLatch counterDownLatch = (CountDownLatch)stateInfo;
                        counterDownLatch.CountDown();

                    }), cdl);

                    ThreadPool.QueueUserWorkItem(new WaitCallback((object stateInfo) =>
                    {
                        GetTableConstraint(this.dataGridView3, db);
                        //任务处理完上报状态 完成
                        CountDownLatch counterDownLatch = (CountDownLatch)stateInfo;
                        counterDownLatch.CountDown();

                    }), cdl);

                    cdl.Await();

                }
            
            });
        }

        private void GetTableIndexes(DataGridView datagridview, DataBase db)
        {
            using (DataTable dt = db.GetTableIndexes(this.tableName))
            {
                this.Invoke(new MethodInvoker(delegate()
                {
                    datagridview.DataSource = dt;
                }));              
            }
            
        }

        private void GetTableTrigger(DataGridView datagridview, DataBase db)
        {
            using (DataTable dt = db.GetTableTrigger(this.tableName))
            {
                this.Invoke(new MethodInvoker(delegate()
                {
                    datagridview.DataSource = dt;
                }));
            }            
        }

        private void GetTableConstraint(DataGridView datagridview, DataBase db)
        {
            using (DataTable dt = db.GetTableConstraint(this.tableName))
            {
                this.Invoke(new MethodInvoker(delegate() { datagridview.DataSource = dt; }));
            }           
        }

        public override void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.Control;
            Color backColor = SystemColors.Control;

            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;

            switch (themeName)
            {
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 248);
                    backColor = Color.FromArgb(030, 030, 030);
                    tabControl1.ForeColor = SystemColors.Window;
                    tabControl1.BorderColor = SystemColors.ControlDark;
                    tabControl1.ArrowColor = Color.White;
                    tabControl1.BaseColor = Color.FromArgb(045, 045, 048);
                    tabControl1.BackColor = Color.FromArgb(045, 045, 048);
                    tabControl1.SelectedColor = Color.FromArgb(045, 045, 048);
                    tabControl1.BaseTabColor = Color.FromArgb(045, 045, 048);
                    break;
                case "Default":
                    foreColor = SystemColors.ControlText;
                    backColor = SystemColors.Control;
                    tabControl1.ForeColor = Color.Black;
                    tabControl1.BorderColor = SystemColors.ControlDark;
                    tabControl1.ArrowColor = Color.White;
                    tabControl1.BaseColor = SystemColors.Control;
                    tabControl1.BackColor = SystemColors.Control;
                    tabControl1.SelectedColor = SystemColors.Control;
                    tabControl1.BaseTabColor = SystemColors.Control;
                    break;
            }
            
            dataGridView1.SetTheme(themeName);
            dataGridView2.SetTheme(themeName);
            dataGridView3.SetTheme(themeName);

            ForeColor = foreColor;
            BackColor = backColor;
            tabPage1.BackColor = backColor;
            tabPage2.BackColor = backColor;
            tabPage3.BackColor = backColor;
           
        }

    }
}
