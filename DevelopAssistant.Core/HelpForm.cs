﻿using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core
{
    public partial class HelpForm : ICSharpCode.WinFormsUI.Forms.BaseForm
    {
        public HelpForm()
        {
            InitializeComponent();
        }

        public HelpForm(MainForm OwnerForm)
            : this()
        {
            this.XTheme = (ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase)AppSettings.WindowTheme.Clone();
            if (this.XTheme.Name == "Mac")
                this.XTheme.RoundedStyle = ICSharpCode.WinFormsUI.Controls.RoundStyle.All;
        }

        private void HelpForm_Load(object sender, EventArgs e)
        {
            this.groupBox1.Title = "帮助文档";

            OnThemeChange(new EventArgs());

            string updatelog = "updatelog.txt";
            if (System.IO.File.Exists(updatelog))
            {
                string text = string.Empty;
                using (System.IO.StreamReader sr = new System.IO.StreamReader(updatelog))
                {
                    text = sr.ReadToEnd();                   
                }
                this.richTextBox1.Text = text;
                this.richTextBox1.ReadOnly = true;
            }
        }

        private void BtnBorder_Click(object sender, EventArgs e)
        {
            if (this.FormBorderStyle != FormBorderStyle.None)
                this.FormBorderStyle = FormBorderStyle.None;
            else
                this.FormBorderStyle = FormBorderStyle.Sizable;
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_Click(object sender, EventArgs e)
        {
             
        }

        //protected override void OnPaint(PaintEventArgs e)
        //{            
        //    Graphics g = e.Graphics;
        //    g.FillRectangle(Brushes.Black, this.ClientRectangle);
        //    FillRoundRectangle(g, new SolidBrush(SystemColors.ControlDark), this.ClientRectangle, 8);
        //    DrawRoundRectangle(g, new Pen(SystemColors.ControlDarkDark,1.0f), this.ClientRectangle, 8);
        //}

        //public static void DrawRoundRectangle(Graphics g, Pen pen, Rectangle rect, int cornerRadius)
        //{
        //    using (GraphicsPath path = CreateRoundedRectanglePath(rect, cornerRadius))
        //    {
        //        g.DrawPath(pen, path);
        //    }
        //}

        //public static void FillRoundRectangle(Graphics g, Brush brush, Rectangle rect, int cornerRadius)
        //{
        //    using (GraphicsPath path = CreateRoundedRectanglePath(rect, cornerRadius))
        //    {
        //        g.FillPath(brush, path);
        //    }
        //}

        //internal static GraphicsPath CreateRoundedRectanglePath(Rectangle rect, int cornerRadius)
        //{
        //    GraphicsPath roundedRect = new GraphicsPath();
        //    roundedRect.AddArc(rect.X, rect.Y, cornerRadius * 2, cornerRadius * 2, 180, 90);
        //    roundedRect.AddLine(rect.X + cornerRadius, rect.Y, rect.Right - cornerRadius * 2, rect.Y);
        //    roundedRect.AddArc(rect.X + rect.Width - cornerRadius * 2, rect.Y, cornerRadius * 2, cornerRadius * 2, 270, 90);
        //    roundedRect.AddLine(rect.Right, rect.Y + cornerRadius * 2, rect.Right, rect.Y + rect.Height - cornerRadius * 2);
        //    roundedRect.AddArc(rect.X + rect.Width - cornerRadius * 2, rect.Y + rect.Height - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 0, 90);
        //    roundedRect.AddLine(rect.Right - cornerRadius * 2, rect.Bottom, rect.X + cornerRadius * 2, rect.Bottom);
        //    roundedRect.AddArc(rect.X, rect.Bottom - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 90, 90);
        //    roundedRect.AddLine(rect.X, rect.Bottom - cornerRadius * 2, rect.X, rect.Y + cornerRadius * 2);
        //    roundedRect.CloseFigure();
        //    return roundedRect;
        //} 

        protected void OnThemeChange(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color linkColor = System.Drawing.Color.Blue;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    linkColor = Color.FromArgb(051, 153, 153);
                    break;
            }            
            this.ForeColor = foreColor;
            this.BackColor = backColor;
            this.panel1.ForeColor = foreColor;
            this.panel1.BackColor = backColor;
            this.richTextBox1.ForeColor = foreColor;
            this.richTextBox1.BackColor = backColor;
            this.groupBox1.ForeColor = foreColor;
            this.groupBox1.BackColor = backColor;

        }

    }
}
