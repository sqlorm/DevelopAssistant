﻿namespace DevelopAssistant.Core
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartForm));
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.Icon_Clack = new System.Windows.Forms.PictureBox();
            this.Banner = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.Icon_Clack)).BeginInit();
            this.Banner.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // Icon_Clack
            // 
            this.Icon_Clack.Image = global::DevelopAssistant.Core.Properties.Resources.waiting;
            this.Icon_Clack.Location = new System.Drawing.Point(125, 81);
            this.Icon_Clack.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Icon_Clack.Name = "Icon_Clack";
            this.Icon_Clack.Size = new System.Drawing.Size(32, 32);
            this.Icon_Clack.TabIndex = 0;
            this.Icon_Clack.TabStop = false;
            // 
            // Banner
            // 
            this.Banner.BackgroundImage = global::DevelopAssistant.Core.Properties.Resources.Bannar;
            this.Banner.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Banner.Controls.Add(this.Icon_Clack);
            this.Banner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Banner.Location = new System.Drawing.Point(2, 2);
            this.Banner.Name = "Banner";
            this.Banner.Padding = new System.Windows.Forms.Padding(2);
            this.Banner.Size = new System.Drawing.Size(397, 146);
            this.Banner.TabIndex = 1;
            // 
            // StartForm
            // 
            this.ClientSize = new System.Drawing.Size(401, 150);
            this.ControlBox = false;
            this.Controls.Add(this.Banner);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "StartForm";
            this.Padding = new System.Windows.Forms.Padding(2);
            //this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "凌云开发助手";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.StartForm_FormClosed);
            this.Load += new System.EventHandler(this.StartForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Icon_Clack)).EndInit();
            this.Banner.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.PictureBox Icon_Clack;
        private System.Windows.Forms.Panel Banner;
    }
}