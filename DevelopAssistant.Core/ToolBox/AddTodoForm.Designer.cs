﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class AddTodoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddTodoForm));
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnApplyOk = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnCancle = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new ICSharpCode.WinFormsUI.Controls.NTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ntextBoxSummary = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ntextBoxUser = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ntextBoxTitle = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ntextBoxName = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.dateTimePicker1 = new ICSharpCode.WinFormsUI.Controls.NDateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.combImportState = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.NEditorText = new ICSharpCode.WinFormsUI.Controls.CodeFormatControl();
            this.label4 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.btnApplyOk);
            this.panel1.Controls.Add(this.btnCancle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(6, 468);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(530, 56);
            this.panel1.TabIndex = 2;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnApplyOk
            // 
            this.btnApplyOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyOk.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApplyOk.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApplyOk.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApplyOk.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApplyOk.FouseColor = System.Drawing.Color.White;
            this.btnApplyOk.Foused = false;
            this.btnApplyOk.FouseTextColor = System.Drawing.Color.Black;
            this.btnApplyOk.Icon = null;
            this.btnApplyOk.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApplyOk.Location = new System.Drawing.Point(346, 7);
            this.btnApplyOk.Name = "btnApplyOk";
            this.btnApplyOk.Radius = 0;
            this.btnApplyOk.Size = new System.Drawing.Size(75, 36);
            this.btnApplyOk.TabIndex = 1;
            this.btnApplyOk.Text = "确定";
            this.btnApplyOk.UnableIcon = null;
            this.btnApplyOk.UseVisualStyleBackColor = true;
            this.btnApplyOk.Click += new System.EventHandler(this.btnApplyOk_Click);
            // 
            // btnCancle
            // 
            this.btnCancle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancle.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnCancle.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnCancle.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnCancle.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnCancle.FouseColor = System.Drawing.Color.White;
            this.btnCancle.Foused = false;
            this.btnCancle.FouseTextColor = System.Drawing.Color.Black;
            this.btnCancle.Icon = null;
            this.btnCancle.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnCancle.Location = new System.Drawing.Point(435, 7);
            this.btnCancle.Name = "btnCancle";
            this.btnCancle.Radius = 0;
            this.btnCancle.Size = new System.Drawing.Size(75, 36);
            this.btnCancle.TabIndex = 0;
            this.btnCancle.Text = "取消";
            this.btnCancle.UnableIcon = null;
            this.btnCancle.UseVisualStyleBackColor = true;
            this.btnCancle.Click += new System.EventHandler(this.btnCancle_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(6, 34);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(6);
            this.panel2.Size = new System.Drawing.Size(530, 434);
            this.panel2.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(79)))), ((int)(((byte)(125)))));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ImageList = this.imageList1;
            this.tabControl1.ItemSize = new System.Drawing.Size(96, 26);
            this.tabControl1.Location = new System.Drawing.Point(6, 6);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Radius = 1;
            this.tabControl1.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowBorder = true;
            this.tabControl1.ShowClose = false;
            this.tabControl1.ShowWaitMessage = false;
            this.tabControl1.Size = new System.Drawing.Size(518, 422);
            this.tabControl1.TabCaptionLm = -3;
            this.tabControl1.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ntextBoxSummary);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.ntextBoxUser);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.ntextBoxTitle);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.ntextBoxName);
            this.tabPage1.Controls.Add(this.dateTimePicker1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.combImportState);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.ImageIndex = 0;
            this.tabPage1.Location = new System.Drawing.Point(4, 30);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(6);
            this.tabPage1.Size = new System.Drawing.Size(510, 388);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "详细信息";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ntextBoxSummary
            // 
            this.ntextBoxSummary.BackColor = System.Drawing.SystemColors.Window;
            this.ntextBoxSummary.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ntextBoxSummary.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ntextBoxSummary.FousedColor = System.Drawing.Color.Orange;
            this.ntextBoxSummary.Icon = null;
            this.ntextBoxSummary.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.ntextBoxSummary.IsButtonTextBox = false;
            this.ntextBoxSummary.IsClearTextBox = false;
            this.ntextBoxSummary.IsPasswordTextBox = false;
            this.ntextBoxSummary.Location = new System.Drawing.Point(60, 244);
            this.ntextBoxSummary.MaxLength = 32767;
            this.ntextBoxSummary.Multiline = true;
            this.ntextBoxSummary.Name = "ntextBoxSummary";
            this.ntextBoxSummary.PasswordChar = '\0';
            this.ntextBoxSummary.Placeholder = null;
            this.ntextBoxSummary.ReadOnly = false;
            this.ntextBoxSummary.Size = new System.Drawing.Size(397, 113);
            this.ntextBoxSummary.TabIndex = 11;
            this.ntextBoxSummary.UseSystemPasswordChar = false;
            this.ntextBoxSummary.XBackColor = System.Drawing.SystemColors.Window;
            this.ntextBoxSummary.XDisableColor = System.Drawing.Color.Empty;
            this.ntextBoxSummary.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(58, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 10;
            this.label7.Text = "摘要信息：";
            // 
            // ntextBoxUser
            // 
            this.ntextBoxUser.BackColor = System.Drawing.SystemColors.Window;
            this.ntextBoxUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ntextBoxUser.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ntextBoxUser.FousedColor = System.Drawing.Color.Orange;
            this.ntextBoxUser.Icon = null;
            this.ntextBoxUser.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.ntextBoxUser.IsButtonTextBox = false;
            this.ntextBoxUser.IsClearTextBox = false;
            this.ntextBoxUser.IsPasswordTextBox = false;
            this.ntextBoxUser.Location = new System.Drawing.Point(153, 177);
            this.ntextBoxUser.MaxLength = 32767;
            this.ntextBoxUser.Multiline = false;
            this.ntextBoxUser.Name = "ntextBoxUser";
            this.ntextBoxUser.PasswordChar = '\0';
            this.ntextBoxUser.Placeholder = null;
            this.ntextBoxUser.ReadOnly = false;
            this.ntextBoxUser.Size = new System.Drawing.Size(304, 24);
            this.ntextBoxUser.TabIndex = 9;
            this.ntextBoxUser.UseSystemPasswordChar = false;
            this.ntextBoxUser.XBackColor = System.Drawing.SystemColors.Window;
            this.ntextBoxUser.XDisableColor = System.Drawing.Color.Empty;
            this.ntextBoxUser.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(70, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "经办人：";
            // 
            // ntextBoxTitle
            // 
            this.ntextBoxTitle.BackColor = System.Drawing.SystemColors.Window;
            this.ntextBoxTitle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ntextBoxTitle.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ntextBoxTitle.FousedColor = System.Drawing.Color.Orange;
            this.ntextBoxTitle.Icon = null;
            this.ntextBoxTitle.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.ntextBoxTitle.IsButtonTextBox = false;
            this.ntextBoxTitle.IsClearTextBox = false;
            this.ntextBoxTitle.IsPasswordTextBox = false;
            this.ntextBoxTitle.Location = new System.Drawing.Point(154, 59);
            this.ntextBoxTitle.MaxLength = 32767;
            this.ntextBoxTitle.Multiline = false;
            this.ntextBoxTitle.Name = "ntextBoxTitle";
            this.ntextBoxTitle.PasswordChar = '\0';
            this.ntextBoxTitle.Placeholder = null;
            this.ntextBoxTitle.ReadOnly = false;
            this.ntextBoxTitle.Size = new System.Drawing.Size(303, 24);
            this.ntextBoxTitle.TabIndex = 7;
            this.ntextBoxTitle.UseSystemPasswordChar = false;
            this.ntextBoxTitle.XBackColor = System.Drawing.SystemColors.Window;
            this.ntextBoxTitle.XDisableColor = System.Drawing.Color.Empty;
            this.ntextBoxTitle.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(58, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 6;
            this.label5.Text = "显示标题：";
            // 
            // ntextBoxName
            // 
            this.ntextBoxName.BackColor = System.Drawing.SystemColors.Window;
            this.ntextBoxName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ntextBoxName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ntextBoxName.FousedColor = System.Drawing.Color.Orange;
            this.ntextBoxName.Icon = null;
            this.ntextBoxName.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.ntextBoxName.IsButtonTextBox = false;
            this.ntextBoxName.IsClearTextBox = false;
            this.ntextBoxName.IsPasswordTextBox = false;
            this.ntextBoxName.Location = new System.Drawing.Point(155, 20);
            this.ntextBoxName.MaxLength = 32767;
            this.ntextBoxName.Multiline = false;
            this.ntextBoxName.Name = "ntextBoxName";
            this.ntextBoxName.PasswordChar = '\0';
            this.ntextBoxName.Placeholder = null;
            this.ntextBoxName.ReadOnly = false;
            this.ntextBoxName.Size = new System.Drawing.Size(302, 22);
            this.ntextBoxName.TabIndex = 5;
            this.ntextBoxName.UseSystemPasswordChar = false;
            this.ntextBoxName.XBackColor = System.Drawing.SystemColors.Window;
            this.ntextBoxName.XDisableColor = System.Drawing.Color.Empty;
            this.ntextBoxName.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.BackDisabledColor = System.Drawing.SystemColors.Control;
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(153, 102);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(156, 21);
            this.dateTimePicker1.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(58, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "事项状态：";
            // 
            // combImportState
            // 
            this.combImportState.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.combImportState.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.combImportState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combImportState.Font = new System.Drawing.Font("宋体", 9F);
            this.combImportState.FormattingEnabled = true;
            this.combImportState.ItemIcon = null;
            this.combImportState.Location = new System.Drawing.Point(153, 141);
            this.combImportState.Name = "combImportState";
            this.combImportState.Size = new System.Drawing.Size(121, 22);
            this.combImportState.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "计划开始时间：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "事项名称：";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.NEditorText);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.ImageIndex = 1;
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(6);
            this.tabPage2.Size = new System.Drawing.Size(192, 70);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "事项描述";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // NEditorText
            // 
            this.NEditorText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NEditorText.BackColor = System.Drawing.Color.White;
            this.NEditorText.DocumentType = ICSharpCode.WinFormsUI.Controls.DocumentType.Text;
            this.NEditorText.EditModel = false;
            this.NEditorText.Location = new System.Drawing.Point(14, 36);
            this.NEditorText.Name = "NEditorText";
            this.NEditorText.Padding = new System.Windows.Forms.Padding(1);
            this.NEditorText.ShowLineNumber = true;
            this.NEditorText.Size = new System.Drawing.Size(164, 18);
            this.NEditorText.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "详细描述：";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "showrulelinesHS.png");
            this.imageList1.Images.SetKeyName(1, "table_gear.png");
            // 
            // AddTodoForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(542, 530);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "AddTodoForm";
            this.Text = "添加待办事项";
            this.Load += new System.EventHandler(this.AddTodoForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NButton btnApplyOk;
        private ICSharpCode.WinFormsUI.Controls.NButton btnCancle;
        private System.Windows.Forms.Panel panel2;
        private ICSharpCode.WinFormsUI.Controls.NTabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private ICSharpCode.WinFormsUI.Controls.NTextBox ntextBoxSummary;
        private System.Windows.Forms.Label label7;
        private ICSharpCode.WinFormsUI.Controls.NTextBox ntextBoxUser;
        private System.Windows.Forms.Label label6;
        private ICSharpCode.WinFormsUI.Controls.NTextBox ntextBoxTitle;
        private System.Windows.Forms.Label label5;
        private ICSharpCode.WinFormsUI.Controls.NTextBox ntextBoxName;
        private ICSharpCode.WinFormsUI.Controls.NDateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label3;
        private ICSharpCode.WinFormsUI.Controls.NComboBox combImportState;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private ICSharpCode.WinFormsUI.Controls.CodeFormatControl NEditorText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ImageList imageList1;
    }
}