﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class AddinToolBoxes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddinToolBoxes));
            this.btnApply = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnCancel = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.groupBox1 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.dataGridView1 = new ICSharpCode.WinFormsUI.Controls.NDataGridView();
            this.col_check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.col_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_describtion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_uninstall = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new ICSharpCode.WinFormsUI.Controls.NToolStrip();
            this.toolStripButtonUnstall = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonInstall = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonAllCheck = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApply.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApply.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApply.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApply.FouseColor = System.Drawing.Color.White;
            this.btnApply.Foused = false;
            this.btnApply.FouseTextColor = System.Drawing.Color.Black;
            this.btnApply.Icon = null;
            this.btnApply.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApply.Location = new System.Drawing.Point(289, 12);
            this.btnApply.Name = "btnApply";
            this.btnApply.Radius = 0;
            this.btnApply.Size = new System.Drawing.Size(75, 36);
            this.btnApply.TabIndex = 0;
            this.btnApply.Text = "保存";
            this.btnApply.UnableIcon = null;
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnApply);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(6, 325);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(467, 63);
            this.panel1.TabIndex = 1;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnCancel.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnCancel.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnCancel.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnCancel.FouseColor = System.Drawing.Color.White;
            this.btnCancel.Foused = false;
            this.btnCancel.FouseTextColor = System.Drawing.Color.Black;
            this.btnCancel.Icon = null;
            this.btnCancel.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(380, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Radius = 0;
            this.btnCancel.Size = new System.Drawing.Size(75, 36);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "取消";
            this.btnCancel.UnableIcon = null;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.MarginWidth = 0;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(455, 279);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Title = "";
            this.groupBox1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_check,
            this.col_name,
            this.col_author,
            this.col_describtion,
            this.col_Id,
            this.col_uninstall});
            this.dataGridView1.DatetimeFormat = "yyyy-MM-dd HH:mm:ss";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowRowNumber = false;
            this.dataGridView1.Size = new System.Drawing.Size(442, 225);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // col_check
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.NullValue = false;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.col_check.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_check.FalseValue = "False";
            this.col_check.HeaderText = "选择";
            this.col_check.IndeterminateValue = "False";
            this.col_check.Name = "col_check";
            this.col_check.TrueValue = "True";
            this.col_check.Width = 65;
            // 
            // col_name
            // 
            this.col_name.HeaderText = "名称";
            this.col_name.Name = "col_name";
            this.col_name.ReadOnly = true;
            this.col_name.Width = 120;
            // 
            // col_author
            // 
            this.col_author.HeaderText = "作者";
            this.col_author.Name = "col_author";
            this.col_author.ReadOnly = true;
            // 
            // col_describtion
            // 
            this.col_describtion.HeaderText = "描述";
            this.col_describtion.Name = "col_describtion";
            this.col_describtion.ReadOnly = true;
            this.col_describtion.Width = 160;
            // 
            // col_Id
            // 
            this.col_Id.HeaderText = "编号";
            this.col_Id.Name = "col_Id";
            this.col_Id.Visible = false;
            // 
            // col_uninstall
            // 
            this.col_uninstall.HeaderText = "是否加载";
            this.col_uninstall.Name = "col_uninstall";
            this.col_uninstall.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonUnstall,
            this.toolStripButtonInstall,
            this.toolStripButtonAllCheck});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(442, 25);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonUnstall
            // 
            this.toolStripButtonUnstall.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonUnstall.Image = global::DevelopAssistant.Core.Properties.Resources.application_form_delete_16px;
            this.toolStripButtonUnstall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUnstall.Name = "toolStripButtonUnstall";
            this.toolStripButtonUnstall.Size = new System.Drawing.Size(76, 22);
            this.toolStripButtonUnstall.Text = "卸载插件";
            this.toolStripButtonUnstall.Click += new System.EventHandler(this.toolStripButtonUnstall_Click);
            // 
            // toolStripButtonInstall
            // 
            this.toolStripButtonInstall.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonInstall.Image = global::DevelopAssistant.Core.Properties.Resources.application_form_add_16px;
            this.toolStripButtonInstall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInstall.Name = "toolStripButtonInstall";
            this.toolStripButtonInstall.Size = new System.Drawing.Size(76, 22);
            this.toolStripButtonInstall.Text = "加载插件";
            this.toolStripButtonInstall.Click += new System.EventHandler(this.toolStripButtonInstall_Click);
            // 
            // toolStripButtonAllCheck
            // 
            this.toolStripButtonAllCheck.Image = global::DevelopAssistant.Core.Properties.Resources.checkbox_no_16px;
            this.toolStripButtonAllCheck.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAllCheck.Name = "toolStripButtonAllCheck";
            this.toolStripButtonAllCheck.Size = new System.Drawing.Size(52, 22);
            this.toolStripButtonAllCheck.Text = "全选";
            this.toolStripButtonAllCheck.Click += new System.EventHandler(this.toolStripButtonAllCheck_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(6, 34);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(6);
            this.panel3.Size = new System.Drawing.Size(467, 291);
            this.panel3.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Location = new System.Drawing.Point(13, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(442, 250);
            this.panel2.TabIndex = 3;
            // 
            // AddinToolBoxes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 394);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "AddinToolBoxes";
            this.Text = "工具箱管理";
            this.Load += new System.EventHandler(this.ToolBoxes_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NButton btnApply;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox1;
        private ICSharpCode.WinFormsUI.Controls.NButton btnCancel;
        private ICSharpCode.WinFormsUI.Controls.NDataGridView dataGridView1;
        private ICSharpCode.WinFormsUI.Controls.NToolStrip toolStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonUnstall;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonInstall;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonAllCheck;
        private System.Windows.Forms.DataGridViewCheckBoxColumn col_check;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_author;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_describtion;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_uninstall;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
    }
}