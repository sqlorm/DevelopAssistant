﻿using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class AddinToolBoxes : ToolBoxBase
    {
        bool all_checked = false;
        DataGridViewCellStyle cellStyle = new DataGridViewCellStyle();

        public AddinToolBoxes()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            this.groupBox1.Title = "插件列表：";
            cellStyle.SelectionForeColor = Color.Gray;
            cellStyle.SelectionBackColor = SystemColors.Control;
            cellStyle.ForeColor = Color.Gray;
            cellStyle.BackColor = SystemColors.Control;
        }

        private void LoadAddin()
        {
            string message = string.Empty;
            var List = Plug_in_Manager.Instance.List(ref message);

            this.dataGridView1.Rows.Clear();

            int rowIndex = 0; int rowCounts = 0;
            foreach (AddIn.AddInBase addin in List)
            {
                this.dataGridView1.Rows.Add(1);
                DataGridViewRow row = this.dataGridView1.Rows[rowIndex];
                row.Cells[0].Value = false;
                row.Cells[1].Value = addin.Name;
                row.Cells[2].Value = addin.Copyright.Author;
                row.Cells[3].Value = addin.Text;
                row.Cells[4].Value = addin.IdentityID;
                row.Cells[5].Value = addin.Uninstall;
                rowIndex++;
            }
            rowCounts = this.dataGridView1.Rows.Count;
        }

        private void LoadXTheme()
        {
            panel1.BorderColor = this.XTheme.FormBorderOutterColor;
        }

        private void ToolBoxes_Load(object sender, EventArgs e)
        {
            LoadXTheme();
            OnThemeChanged(new EventArgs());
            LoadAddin();
        }   

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButtonAllCheck_Click(object sender, EventArgs e)
        {
            int index = this.dataGridView1.SelectedRows[0].Index;           
            this.dataGridView1.CurrentCell = this.dataGridView1.Rows[index].Cells[1];

            if (!all_checked)
            {
                all_checked = true;
                toolStripButtonAllCheck.Image = DevelopAssistant.Core.Properties.Resources.checkbox_yes_16px;               
            }
            else
            {
                all_checked = false;
                toolStripButtonAllCheck.Image = DevelopAssistant.Core.Properties.Resources.checkbox_no_16px;                
            }

            foreach (DataGridViewRow row in this.dataGridView1.Rows)
            {
                row.Cells[0].Value = all_checked;
            }

        }

        private void toolStripButtonUnstall_Click(object sender, EventArgs e)
        {
            dataGridView1.EndEdit();       
            foreach (DataGridViewRow row in this.dataGridView1.Rows)
            {
                string id = (string)row.Cells[4].Value;
                var value = row.Cells[0].Value;
                if (value != null && Convert.ToBoolean(value))
                {
                    Plug_in_Manager.Instance.Uninstall(id);
                }
            }
            LoadAddin();
        }

        private void toolStripButtonInstall_Click(object sender, EventArgs e)
        {
            dataGridView1.EndEdit();  
            foreach (DataGridViewRow row in this.dataGridView1.Rows)
            {
                string id = (string)row.Cells[4].Value;
                var value = row.Cells[0].Value;
                if (value != null && Convert.ToBoolean(value))
                {
                    Plug_in_Manager.Instance.Install(id);
                }
            }
            LoadAddin();
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var value = dataGridView1.Rows[e.RowIndex].Cells[5].Value;
                if (value != null && Convert.ToBoolean(value))
                {
                    Graphics g = e.Graphics;
                    Rectangle bound = e.RowBounds;
                    g.DrawString("×", this.Font, Brushes.Gray, bound.X + 24, bound.Y + (bound.Height - 12) / 2);
                    dataGridView1.Rows[e.RowIndex].DefaultCellStyle = cellStyle;                     
                }
            }
        }

        public void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color linkColor = System.Drawing.Color.Blue;
            Color toolBackColor = SystemColors.Control;
            Color toolStripBackColor = Color.FromArgb(246, 248, 250);
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    toolBackColor = SystemColors.Control;
                    toolStripBackColor = Color.FromArgb(246, 248, 250);
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    linkColor = Color.FromArgb(051, 153, 153);
                    toolBackColor = Color.FromArgb(038, 038, 038);
                    toolStripBackColor = Color.FromArgb(038, 038, 038);
                    break;
            }
            
            panel2.ForeColor = foreColor;
            panel2.BackColor = backColor;
            panel3.ForeColor = foreColor;
            panel3.BackColor = backColor;
            panel1.BackColor = toolBackColor;
            btnApply.ForeColor = foreColor;
            btnApply.BackColor = toolBackColor;
            btnCancel.ForeColor = foreColor;
            btnCancel.BackColor = toolBackColor;
            groupBox1.ForeColor = foreColor;
            groupBox1.BackColor = backColor;
            toolStrip1.BackColor = toolStripBackColor;
            cellStyle.BackColor = backColor;
            cellStyle.SelectionBackColor = backColor;

            dataGridView1.SetTheme(themeName);
            toolStrip1.SetTheme(themeName);

        }
    }
}
