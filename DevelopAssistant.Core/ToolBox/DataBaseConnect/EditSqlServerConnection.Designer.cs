﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class EditSqlServerConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tester = new System.Windows.Forms.Label();
            this.chkEfficient = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.combDataBases = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.chkLoginStyle = new System.Windows.Forms.CheckBox();
            this.chkInstance = new System.Windows.Forms.CheckBox();
            this.txtInstance = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPort = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.combServer = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gboxLoginPanel = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPassword = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.txtUser_ID = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gboxLoginPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tester
            // 
            this.tester.AutoSize = true;
            this.tester.ForeColor = System.Drawing.Color.Blue;
            this.tester.Location = new System.Drawing.Point(216, 236);
            this.tester.Name = "tester";
            this.tester.Size = new System.Drawing.Size(53, 12);
            this.tester.TabIndex = 25;
            this.tester.Text = "测试连接";
            this.tester.Click += new System.EventHandler(this.tester_Click);
            // 
            // chkEfficient
            // 
            this.chkEfficient.AutoSize = true;
            this.chkEfficient.Location = new System.Drawing.Point(110, 234);
            this.chkEfficient.Name = "chkEfficient";
            this.chkEfficient.Size = new System.Drawing.Size(96, 16);
            this.chkEfficient.TabIndex = 24;
            this.chkEfficient.Text = "高效连接模式";
            this.chkEfficient.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(27, 209);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 23;
            this.label6.Text = "数据库名称：";
            // 
            // combDataBases
            // 
            this.combDataBases.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.combDataBases.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.combDataBases.DropDownButtonWidth = 20;
            this.combDataBases.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combDataBases.DropDownWidth = 0;
            this.combDataBases.Font = new System.Drawing.Font("宋体", 9F);
            this.combDataBases.FormattingEnabled = true;
            this.combDataBases.ItemHeight = 18;
            this.combDataBases.ItemIcon = null;
            this.combDataBases.Location = new System.Drawing.Point(110, 204);
            this.combDataBases.Name = "combDataBases";
            this.combDataBases.SelectedIndex = -1;
            this.combDataBases.SelectedItem = null;
            this.combDataBases.Size = new System.Drawing.Size(159, 22);
            this.combDataBases.TabIndex = 22;
            // 
            // chkLoginStyle
            // 
            this.chkLoginStyle.AutoSize = true;
            this.chkLoginStyle.BackColor = System.Drawing.Color.Transparent;
            this.chkLoginStyle.Checked = true;
            this.chkLoginStyle.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLoginStyle.Location = new System.Drawing.Point(87, 83);
            this.chkLoginStyle.Name = "chkLoginStyle";
            this.chkLoginStyle.Size = new System.Drawing.Size(156, 16);
            this.chkLoginStyle.TabIndex = 13;
            this.chkLoginStyle.Text = "Windows 集成身份登陆：";
            this.chkLoginStyle.UseVisualStyleBackColor = false;
            // 
            // chkInstance
            // 
            this.chkInstance.AutoSize = true;
            this.chkInstance.BackColor = System.Drawing.Color.Transparent;
            this.chkInstance.Location = new System.Drawing.Point(230, 55);
            this.chkInstance.Name = "chkInstance";
            this.chkInstance.Size = new System.Drawing.Size(48, 16);
            this.chkInstance.TabIndex = 20;
            this.chkInstance.Text = "实例";
            this.chkInstance.UseVisualStyleBackColor = false;
            // 
            // txtInstance
            // 
            this.txtInstance.BackColor = System.Drawing.SystemColors.Window;
            this.txtInstance.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInstance.Enabled = false;
            this.txtInstance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtInstance.FousedColor = System.Drawing.Color.Orange;
            this.txtInstance.Icon = null;
            this.txtInstance.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtInstance.IsButtonTextBox = false;
            this.txtInstance.IsClearTextBox = false;
            this.txtInstance.IsPasswordTextBox = false;
            this.txtInstance.Location = new System.Drawing.Point(87, 50);
            this.txtInstance.MaxLength = 32767;
            this.txtInstance.Multiline = false;
            this.txtInstance.Name = "txtInstance";
            this.txtInstance.PasswordChar = '\0';
            this.txtInstance.Placeholder = null;
            this.txtInstance.ReadOnly = false;
            this.txtInstance.Size = new System.Drawing.Size(131, 24);
            this.txtInstance.TabIndex = 19;
            this.txtInstance.UseSystemPasswordChar = false;
            this.txtInstance.XBackColor = System.Drawing.SystemColors.Window;
            this.txtInstance.XDisableColor = System.Drawing.Color.Empty;
            this.txtInstance.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(51, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 18;
            this.label3.Text = "实例：";
            // 
            // txtPort
            // 
            this.txtPort.BackColor = System.Drawing.SystemColors.Window;
            this.txtPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPort.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtPort.FousedColor = System.Drawing.Color.Orange;
            this.txtPort.Icon = null;
            this.txtPort.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPort.IsButtonTextBox = false;
            this.txtPort.IsClearTextBox = false;
            this.txtPort.IsPasswordTextBox = false;
            this.txtPort.Location = new System.Drawing.Point(267, 17);
            this.txtPort.MaxLength = 32767;
            this.txtPort.Multiline = false;
            this.txtPort.Name = "txtPort";
            this.txtPort.PasswordChar = '\0';
            this.txtPort.Placeholder = null;
            this.txtPort.ReadOnly = false;
            this.txtPort.Size = new System.Drawing.Size(63, 24);
            this.txtPort.TabIndex = 17;
            this.txtPort.UseSystemPasswordChar = false;
            this.txtPort.XBackColor = System.Drawing.SystemColors.Window;
            this.txtPort.XDisableColor = System.Drawing.Color.Empty;
            this.txtPort.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(228, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 16;
            this.label2.Text = "端口：";
            // 
            // combServer
            // 
            this.combServer.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.combServer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.combServer.DropDownButtonWidth = 20;
            this.combServer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combServer.DropDownWidth = 0;
            this.combServer.Font = new System.Drawing.Font("宋体", 9F);
            this.combServer.FormattingEnabled = true;
            this.combServer.ItemHeight = 18;
            this.combServer.ItemIcon = null;
            this.combServer.Location = new System.Drawing.Point(87, 17);
            this.combServer.Name = "combServer";
            this.combServer.SelectedIndex = -1;
            this.combServer.SelectedItem = null;
            this.combServer.Size = new System.Drawing.Size(131, 24);
            this.combServer.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(15, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "服务器地址：";
            // 
            // gboxLoginPanel
            // 
            this.gboxLoginPanel.BackColor = System.Drawing.Color.Transparent;
            this.gboxLoginPanel.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.gboxLoginPanel.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.gboxLoginPanel.BottomBlackColor = System.Drawing.Color.Empty;
            this.gboxLoginPanel.Controls.Add(this.label8);
            this.gboxLoginPanel.Controls.Add(this.label7);
            this.gboxLoginPanel.Controls.Add(this.txtPassword);
            this.gboxLoginPanel.Controls.Add(this.txtUser_ID);
            this.gboxLoginPanel.Controls.Add(this.label5);
            this.gboxLoginPanel.Controls.Add(this.label4);
            this.gboxLoginPanel.Enabled = false;
            this.gboxLoginPanel.GridLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(189)))), ((int)(((byte)(189)))));
            this.gboxLoginPanel.Location = new System.Drawing.Point(17, 85);
            this.gboxLoginPanel.MarginWidth = 0;
            this.gboxLoginPanel.Name = "gboxLoginPanel";
            this.gboxLoginPanel.Size = new System.Drawing.Size(313, 107);
            this.gboxLoginPanel.TabIndex = 21;
            this.gboxLoginPanel.TabStop = false;
            this.gboxLoginPanel.Title = "";
            this.gboxLoginPanel.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // label8
            // 
            this.label8.Image = global::DevelopAssistant.Core.Properties.Resources.openeye;
            this.label8.Location = new System.Drawing.Point(258, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 21);
            this.label8.TabIndex = 6;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(258, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "如：sa";
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtPassword.FousedColor = System.Drawing.Color.Orange;
            this.txtPassword.Icon = null;
            this.txtPassword.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPassword.IsButtonTextBox = false;
            this.txtPassword.IsClearTextBox = false;
            this.txtPassword.IsPasswordTextBox = false;
            this.txtPassword.Location = new System.Drawing.Point(93, 63);
            this.txtPassword.MaxLength = 32767;
            this.txtPassword.Multiline = false;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Placeholder = null;
            this.txtPassword.ReadOnly = false;
            this.txtPassword.Size = new System.Drawing.Size(159, 24);
            this.txtPassword.TabIndex = 4;
            this.txtPassword.UseSystemPasswordChar = false;
            this.txtPassword.XBackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.XDisableColor = System.Drawing.Color.Empty;
            this.txtPassword.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // txtUser_ID
            // 
            this.txtUser_ID.BackColor = System.Drawing.SystemColors.Window;
            this.txtUser_ID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUser_ID.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtUser_ID.FousedColor = System.Drawing.Color.Orange;
            this.txtUser_ID.Icon = null;
            this.txtUser_ID.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtUser_ID.IsButtonTextBox = false;
            this.txtUser_ID.IsClearTextBox = false;
            this.txtUser_ID.IsPasswordTextBox = false;
            this.txtUser_ID.Location = new System.Drawing.Point(93, 28);
            this.txtUser_ID.MaxLength = 32767;
            this.txtUser_ID.Multiline = false;
            this.txtUser_ID.Name = "txtUser_ID";
            this.txtUser_ID.PasswordChar = '\0';
            this.txtUser_ID.Placeholder = null;
            this.txtUser_ID.ReadOnly = false;
            this.txtUser_ID.Size = new System.Drawing.Size(159, 24);
            this.txtUser_ID.TabIndex = 3;
            this.txtUser_ID.UseSystemPasswordChar = false;
            this.txtUser_ID.XBackColor = System.Drawing.SystemColors.Window;
            this.txtUser_ID.XDisableColor = System.Drawing.Color.Empty;
            this.txtUser_ID.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "密码：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "用户名：";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chkLoginStyle);
            this.panel2.Controls.Add(this.txtPort);
            this.panel2.Controls.Add(this.combServer);
            this.panel2.Controls.Add(this.txtInstance);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.tester);
            this.panel2.Controls.Add(this.gboxLoginPanel);
            this.panel2.Controls.Add(this.chkEfficient);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.combDataBases);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.chkInstance);
            this.panel2.Location = new System.Drawing.Point(6, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(346, 266);
            this.panel2.TabIndex = 26;
            // 
            // EditSqlServerConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 366);
            this.Controls.Add(this.panel2);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(1171, 582);
            this.MinimumSize = new System.Drawing.Size(187, 75);
            this.Name = "EditSqlServerConnection";
            this.Text = "Sql 数据库连接";
            this.Load += new System.EventHandler(this.EditSqlServerConnection_Load);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.gboxLoginPanel.ResumeLayout(false);
            this.gboxLoginPanel.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label tester;
        private System.Windows.Forms.CheckBox chkEfficient;
        private System.Windows.Forms.Label label6;
        private ICSharpCode.WinFormsUI.Controls.NComboBox combDataBases;
        private System.Windows.Forms.CheckBox chkLoginStyle;
        private System.Windows.Forms.CheckBox chkInstance;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtInstance;
        private System.Windows.Forms.Label label3;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPort;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.WinFormsUI.Controls.NComboBox combServer;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox gboxLoginPanel;
        private System.Windows.Forms.Label label7;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPassword;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtUser_ID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel2;
    }
}