﻿using DevelopAssistant.Service;
using NORM.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class ExecuteCommand : ToolBoxBase
    {

        string _dataBaseType = string.Empty;

        private string _sqltext="";
        public string SqlText
        {
            get { return _sqltext; }
        }

        public ExecuteCommand()
        {
            InitializeComponent();
        }

        public ExecuteCommand(string Name, string Type, DataBaseServer DbServer)
            : this()
        {
            Point pt = this.label2.Location;
            switch (Type)
            {
                case "Procedure":
                    this.label1.Text = "存储过程名称：";
                    pt.Offset(24, 0);
                    break;
                case "Function":
                    this.label1.Text = "函数名称：";
                    pt.Offset(-6, 0);
                    break;
            }
            this.label2.Text = Name;
            this.label2.Location = pt;


            using (var db = Utility.GetAdohelper(DbServer))
            {
                DataTable dt = db.GetTableObject(Name);

                //this.listView1.View = System.Windows.Forms.View.Details;//设置视图  
                //this.listView1.Columns.Add("参数名称", 100, HorizontalAlignment.Left);
                //this.listView1.Columns.Add("数据类型", 120, HorizontalAlignment.Left);
                //this.listView1.Columns.Add("参数值", 160, HorizontalAlignment.Left);               

                //this.listView1.SuspendLayout();
                //this.listView1.BeginUpdate();  

                //foreach (DataRow dr in dt.Rows)
                //{
                //    string ColumnName = dr["ColumnName"] + "";
                //    string TypeName = dr["TypeName"] + "";

                //    ListViewItem lvi = new ListViewItem(new string[] { ColumnName, TypeName ,null });

                //    this.listView1.Items.Add(lvi);
                //}

                //this.listView1.EndUpdate();
                //this.listView1.ResumeLayout();

                DataTable dv = new DataTable();
                dv.Columns.Add(new DataColumn("参数名称", typeof(String)));
                dv.Columns.Add(new DataColumn("数据类型", typeof(String)));
                dv.Columns.Add(new DataColumn("参数值", typeof(String)));

                foreach (DataRow dr in dt.Rows)
                {
                    string ColumnName = dr["ColumnName"] + "";
                    string TypeName = dr["TypeName"] + "";

                    if (!String.IsNullOrEmpty(ColumnName))
                    {
                        DataRow row = dv.NewRow();
                        row["参数名称"] = ColumnName;
                        row["数据类型"] = TypeName;
                        row["参数值"] = null;

                        dv.Rows.Add(row);
                    }

                }

                this.dataGridView1.DataSource = dv;
                this._dataBaseType = DbServer.ProviderName;

            }

        }

        private void btnApplyOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            this._sqltext = string.Empty;
          
            //_sqltext += Environment.NewLine;
            //_sqltext += "(";            
            int nnum = 0;
            if (this.label1.Text == "存储过程名称：")
            {
                this._sqltext += "EXEC ";

                switch (this._dataBaseType)
                {
                    case "System.Data.Sql":
                    case "System.Data.SQL":
                        _sqltext += " [" + this.label2.Text + "]";
                        break;
                    case "System.Data.Sqlite":
                        _sqltext += " [" + this.label2.Text + "]";
                        break;
                    case "System.Data.PostgreSql":
                        _sqltext += " \"" + this.label2.Text + "\"";
                        break;
                }

                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    if (nnum > 0)
                        _sqltext += ",";

                    _sqltext += Environment.NewLine;
                    switch (this._dataBaseType)
                    {
                        case "System.Data.Sql":
                        case "System.Data.SQL":
                            _sqltext += "   " + row.Cells[0].Value + "" + "=" + row.Cells[2].Value;
                            break;
                        case "System.Data.Sqlite":
                            _sqltext += "   " + row.Cells[0].Value + "" + "=" + row.Cells[2].Value;
                            break;
                        case "System.Data.PostgreSql":
                            _sqltext += "   \"" + row.Cells[0].Value + "\"" + "=" + row.Cells[2].Value;
                            break;
                    }

                    nnum++;
                }
            }
            if (this.label1.Text == "函数名称：")
            {
                this._sqltext += "SELECT ";

                switch (this._dataBaseType)
                {
                    case "System.Data.Sql":
                    case "System.Data.SQL":
                        _sqltext += " dbo.[" + this.label2.Text + "]";
                        break;
                    case "System.Data.Sqlite":
                        _sqltext += " [" + this.label2.Text + "]";
                        break;
                    case "System.Data.PostgreSql":
                        _sqltext += " \"" + this.label2.Text + "\"";
                        break;
                }

                //_sqltext += Environment.NewLine;
                _sqltext += "(";                 

                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    if (nnum > 0)
                        _sqltext += ",";
                     
                    switch (this._dataBaseType)
                    {
                        case "System.Data.Sql":
                        case "System.Data.SQL":
                            _sqltext += row.Cells[2].Value;
                            break;
                        case "System.Data.Sqlite":
                            _sqltext += row.Cells[2].Value;
                            break;
                        case "System.Data.PostgreSql":
                            _sqltext += row.Cells[2].Value; 
                            break;
                    }

                    nnum++;
                }

                _sqltext += ")";
            }
            _sqltext += Environment.NewLine;
            //_sqltext += ")";

            this.Close();
        } 

        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
