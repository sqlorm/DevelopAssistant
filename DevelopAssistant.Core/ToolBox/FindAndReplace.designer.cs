﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class FindAndReplace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FindAndReplace));
            this.lblLookFor = new System.Windows.Forms.Label();
            this.txtLookFor = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.btnLook = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnReplace = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.txtReplaceWith = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.lblReplaceWith = new System.Windows.Forms.Label();
            this.chkMatchCase = new System.Windows.Forms.CheckBox();
            this.chkMatchWholeWord = new System.Windows.Forms.CheckBox();
            this.NToolBar = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.groupBox1 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.NToolBar.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblLookFor
            // 
            this.lblLookFor.AutoSize = true;
            this.lblLookFor.Location = new System.Drawing.Point(24, 31);
            this.lblLookFor.Name = "lblLookFor";
            this.lblLookFor.Size = new System.Drawing.Size(65, 12);
            this.lblLookFor.TabIndex = 0;
            this.lblLookFor.Text = "查找字符：";
            // 
            // txtLookFor
            // 
            this.txtLookFor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLookFor.BackColor = System.Drawing.Color.White;
            this.txtLookFor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLookFor.Enabled = true;
            this.txtLookFor.ForeColor = System.Drawing.Color.Gray;
            this.txtLookFor.FousedColor = System.Drawing.Color.Orange;
            this.txtLookFor.Icon = null;
            this.txtLookFor.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtLookFor.IsButtonTextBox = false;
            this.txtLookFor.IsClearTextBox = false;
            this.txtLookFor.IsPasswordTextBox = false;
            this.txtLookFor.Location = new System.Drawing.Point(98, 26);
            this.txtLookFor.MaxLength = 32767;
            this.txtLookFor.Multiline = false;
            this.txtLookFor.Name = "txtLookFor";
            this.txtLookFor.PasswordChar = '\0';
            this.txtLookFor.Placeholder = null;
            this.txtLookFor.ReadOnly = false;
            this.txtLookFor.Size = new System.Drawing.Size(209, 24);
            this.txtLookFor.TabIndex = 1;
            this.txtLookFor.UseSystemPasswordChar = false;
            this.txtLookFor.XBackColor = System.Drawing.Color.White;
            this.txtLookFor.XForeColor = System.Drawing.Color.Gray;
            // 
            // btnLook
            // 
            this.btnLook.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnLook.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnLook.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnLook.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnLook.FouseColor = System.Drawing.Color.White;
            this.btnLook.Foused = false;
            this.btnLook.FouseTextColor = System.Drawing.Color.Black;
            this.btnLook.Icon = null;
            this.btnLook.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnLook.Location = new System.Drawing.Point(135, 14);
            this.btnLook.Name = "btnLook";
            this.btnLook.Radius = 0;
            this.btnLook.Size = new System.Drawing.Size(75, 32);
            this.btnLook.TabIndex = 3;
            this.btnLook.Text = "查找";
            this.btnLook.UnableIcon = null;
            this.btnLook.UseVisualStyleBackColor = true;
            this.btnLook.Click += new System.EventHandler(this.btnLook_Click);
            // 
            // btnReplace
            // 
            this.btnReplace.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnReplace.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnReplace.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnReplace.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnReplace.FouseColor = System.Drawing.Color.White;
            this.btnReplace.Foused = false;
            this.btnReplace.FouseTextColor = System.Drawing.Color.Black;
            this.btnReplace.Icon = null;
            this.btnReplace.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnReplace.Location = new System.Drawing.Point(239, 14);
            this.btnReplace.Name = "btnReplace";
            this.btnReplace.Radius = 0;
            this.btnReplace.Size = new System.Drawing.Size(75, 32);
            this.btnReplace.TabIndex = 4;
            this.btnReplace.Text = "替换";
            this.btnReplace.UnableIcon = null;
            this.btnReplace.UseVisualStyleBackColor = true;
            this.btnReplace.Click += new System.EventHandler(this.btnReplace_Click);
            // 
            // txtReplaceWith
            // 
            this.txtReplaceWith.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReplaceWith.BackColor = System.Drawing.Color.White;
            this.txtReplaceWith.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtReplaceWith.Enabled = true;
            this.txtReplaceWith.ForeColor = System.Drawing.Color.Gray;
            this.txtReplaceWith.FousedColor = System.Drawing.Color.Orange;
            this.txtReplaceWith.Icon = null;
            this.txtReplaceWith.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtReplaceWith.IsButtonTextBox = false;
            this.txtReplaceWith.IsClearTextBox = false;
            this.txtReplaceWith.IsPasswordTextBox = false;
            this.txtReplaceWith.Location = new System.Drawing.Point(98, 67);
            this.txtReplaceWith.MaxLength = 32767;
            this.txtReplaceWith.Multiline = false;
            this.txtReplaceWith.Name = "txtReplaceWith";
            this.txtReplaceWith.PasswordChar = '\0';
            this.txtReplaceWith.Placeholder = null;
            this.txtReplaceWith.ReadOnly = false;
            this.txtReplaceWith.Size = new System.Drawing.Size(209, 24);
            this.txtReplaceWith.TabIndex = 5;
            this.txtReplaceWith.UseSystemPasswordChar = false;
            this.txtReplaceWith.XBackColor = System.Drawing.Color.White;
            this.txtReplaceWith.XForeColor = System.Drawing.Color.Gray;
            // 
            // lblReplaceWith
            // 
            this.lblReplaceWith.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.lblReplaceWith.AutoSize = true;
            this.lblReplaceWith.Location = new System.Drawing.Point(36, 73);
            this.lblReplaceWith.Name = "lblReplaceWith";
            this.lblReplaceWith.Size = new System.Drawing.Size(53, 12);
            this.lblReplaceWith.TabIndex = 6;
            this.lblReplaceWith.Text = "替换为：";
            // 
            // chkMatchCase
            // 
            this.chkMatchCase.AutoSize = true;
            this.chkMatchCase.Location = new System.Drawing.Point(98, 106);
            this.chkMatchCase.Name = "chkMatchCase";
            this.chkMatchCase.Size = new System.Drawing.Size(84, 16);
            this.chkMatchCase.TabIndex = 9;
            this.chkMatchCase.Text = "区分大小写";
            this.chkMatchCase.UseVisualStyleBackColor = true;
            // 
            // chkMatchWholeWord
            // 
            this.chkMatchWholeWord.AutoSize = true;
            this.chkMatchWholeWord.Location = new System.Drawing.Point(225, 109);
            this.chkMatchWholeWord.Name = "chkMatchWholeWord";
            this.chkMatchWholeWord.Size = new System.Drawing.Size(72, 16);
            this.chkMatchWholeWord.TabIndex = 10;
            this.chkMatchWholeWord.Text = "全词找查";
            this.chkMatchWholeWord.UseVisualStyleBackColor = true;
            this.chkMatchWholeWord.Visible = false;
            // 
            // NToolBar
            // 
            this.NToolBar.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.NToolBar.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.NToolBar.BottomBlackColor = System.Drawing.Color.Empty;
            this.NToolBar.Controls.Add(this.btnReplace);
            this.NToolBar.Controls.Add(this.btnLook);
            this.NToolBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.NToolBar.Location = new System.Drawing.Point(6, 180);
            this.NToolBar.MarginWidth = 0;
            this.NToolBar.Name = "NToolBar";
            this.NToolBar.Size = new System.Drawing.Size(349, 61);
            this.NToolBar.TabIndex = 11;
            this.NToolBar.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox1.Controls.Add(this.chkMatchCase);
            this.groupBox1.Controls.Add(this.lblLookFor);
            this.groupBox1.Controls.Add(this.txtLookFor);
            this.groupBox1.Controls.Add(this.txtReplaceWith);
            this.groupBox1.Controls.Add(this.lblReplaceWith);
            this.groupBox1.Location = new System.Drawing.Point(7, 3);
            this.groupBox1.MarginWidth = 0;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(335, 139);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Title = "";
            this.groupBox1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkMatchWholeWord);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(6, 34);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(349, 146);
            this.panel1.TabIndex = 13;
            // 
            // FindAndReplace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 247);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.NToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximumSize = new System.Drawing.Size(1171, 582);
            this.Name = "FindAndReplace";
            this.Text = "查找";
            this.Load += new System.EventHandler(this.FindAndReplace_Load);
            this.NToolBar.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblLookFor;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtLookFor;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtReplaceWith;
        private System.Windows.Forms.Label lblReplaceWith;
        private System.Windows.Forms.CheckBox chkMatchCase;
        private System.Windows.Forms.CheckBox chkMatchWholeWord;
        private ICSharpCode.WinFormsUI.Controls.NPanel NToolBar;
        private System.Windows.Forms.Panel panel1;
        private ICSharpCode.WinFormsUI.Controls.NButton btnLook;
        private ICSharpCode.WinFormsUI.Controls.NButton btnReplace;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox1;
    }
}