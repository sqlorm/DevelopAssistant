﻿using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class MessageDialog : BaseForm
    {
        public MessageDialog()
        {
            InitializeComponent();
            InitializeControls();
        }

        protected void InitializeControls()
        {
            this.MessagePanel.Visible = false;
            this.AlertPanel.Visible = true;
            this.btnApplyOk.Foused = true;
        }

        public string ResultText
        {
            get
            {
                return this.textBox1.Text;
            }
        }

        public MessageDialog(string MessageText)
            : this()
        {            
            this.XTheme = AppSettings.WindowTheme;
            this.panel1.BorderColor = this.XTheme.FormBorderOutterColor;
            this.MessagePanel.Visible = false;
            this.AlertPanel.Visible = true;
            label1.Text = MessageText;
        }

        public MessageDialog(Image Icon, string MessageText, MessageBoxButtons MessageBoxButtons = MessageBoxButtons.OK )
            : this()
        {             
            this.XTheme = AppSettings.WindowTheme;
            this.panel1.BorderColor = this.XTheme.FormBorderOutterColor;
            this.MessageIcon.Image = Icon;
            this.MessageText.Text = MessageText;           
            this.AlertPanel.Visible = false;
            this.MessagePanel.Visible = true;

            switch (MessageBoxButtons)
            {
                case System.Windows.Forms.MessageBoxButtons.OK:
                    btnApplyOk.Location = btnCancle.Location;
                    btnCancle.Visible = false;
                    break;
                case System.Windows.Forms.MessageBoxButtons.OKCancel:
                    btnApplyOk.Visible = true;
                    btnCancle.Visible = true;
                    break;
            }

        }

        private void btnApplyOk_Click(object sender, EventArgs e)
        {
            if (this.AlertPanel.Visible && 
                string.IsNullOrEmpty(this.textBox1.Text))
            {
                MessageBox.Show("请输入名称");
                return;
            }        
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MessageDialog_Load(object sender, EventArgs e)
        {
            Color foreColor = SystemColors.ControlText;
            Color backColor = SystemColors.Control;
            Color toolBarColor= SystemColors.Control;
            Color textBackColor = SystemColors.Window;

            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            string windowTheme = AppSettings.WindowTheme == null ? "Mac" : AppSettings.WindowTheme.Name;

            switch (windowTheme)
            {
                case "Mac":
                    btnApplyOk.Radius = 6;
                    btnCancle.Radius = 6;
                    break;              
                case "Shadow":
                    btnApplyOk.Radius = 4;
                    btnCancle.Radius = 4;
                    break;
                case "VS2012":
                    btnApplyOk.Radius = 0;
                    btnCancle.Radius = 0;
                    break;
            }

            switch (themeName)
            {
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 248);
                    backColor = Color.FromArgb(030, 030, 030);
                    textBackColor = Color.FromArgb(030, 030, 030);
                    toolBarColor = Color.FromArgb(048, 048, 048);
                    break;
                case "Default":
                    foreColor = SystemColors.ControlText;
                    backColor = SystemColors.Control;
                    textBackColor = SystemColors.Window;
                    toolBarColor = SystemColors.Control;
                    break;
            }

            panel1.ForeColor = foreColor;
            panel1.BackColor = toolBarColor;
            AlertPanel.BackColor = backColor;
            MessagePanel.BackColor = backColor;
            btnApplyOk.ForeColor = foreColor;
            btnApplyOk.BackColor = toolBarColor;
            btnCancle.ForeColor = foreColor;
            btnCancle.BackColor = toolBarColor;
            textBox1.XForeColor = foreColor;
            textBox1.XBackColor = textBackColor;

            ForeColor = foreColor;
            BackColor = backColor;

        }


    }
}
