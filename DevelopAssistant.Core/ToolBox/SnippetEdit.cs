﻿using DevelopAssistant.Common;
using DevelopAssistant.Service;
using DevelopAssistant.Service.SnippetCode;
using ICSharpCode.WinFormsUI.Controls;
using ICSharpCode.WinFormsUI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{    
    public partial class SnippetEdit : BaseForm
    {
        private int ID;
        private int ParentID;
        private string Command = "编辑";

        public SnippetEdit()
        {
            InitializeComponent();
            InitializeSupportDocumentTypes();
        }

        public SnippetEdit(int id, string command)
            : this()
        {
            this.ID = id;
            this.ParentID = id;
            this.Command = command;
            this.XTheme = AppSettings.WindowTheme;
            InitializeControls();
        }

        private void InitializeSupportDocumentTypes()
        {
            ListItem item = new ListItem();
            //item.Text = "None";
            //item.Value = "";
            //this.combDocumentType.Items.Add(item);

            item = new ListItem();
            item.Text = "Csharp";
            item.Value = ".cs";
            this.combDocumentType.Items.Add(item);


            item = new ListItem();
            item.Text = "TSql";
            item.Value = ".sql";
            this.combDocumentType.Items.Add(item);                     

            item = new ListItem();
            item.Text = "Json";
            item.Value = ".json";
            this.combDocumentType.Items.Add(item);

            //item = new ListItem();
            //item.Text = "Css";
            //item.Value = ".css";
            //this.combDocumentType.Items.Add(item);

            //item = new ListItem();
            //item.Text = "Xml";
            //item.Value = ".xml";
            //this.combDocumentType.Items.Add(item);

            //item = new ListItem();
            //item.Text = "Python";
            //item.Value = ".py";
            //this.combDocumentType.Items.Add(item);

            //item = new ListItem();
            //item.Text = "Html";
            //item.Value = ".html";
            //this.combDocumentType.Items.Add(item);

            item = new ListItem();
            item.Text = "Text";
            item.Value = ".txt";
            this.combDocumentType.Items.Add(item);

            //item = new ListItem();
            //item.Text = "Word";
            //item.Value = ".doc";
            //this.combDocumentType.Items.Add(item);                   

            //item = new ListItem();
            //item.Text = "Aspx";
            //item.Value = ".aspx";
            //this.combDocumentType.Items.Add(item);

            //item = new ListItem();
            //item.Text = "cshtml";
            //item.Value = ".cshtml";
            //this.combDocumentType.Items.Add(item);

            //item = new ListItem();
            //item.Text = "Configuation";
            //item.Value = ".config";
            //this.combDocumentType.Items.Add(item);

            item = new ListItem();
            item.Text = "Javascript";
            item.Value = ".js";
            this.combDocumentType.Items.Add(item);

            this.combDocumentType.SelectedIndex = 0;
        }

        private void SetComboxSelectedValue(NComboBox comb, object value)
        {
            int selectedIndex = 0;
            foreach (ListItem item in comb.Items)
            {
                if (item.Value.Equals(value + ""))
                    break;
                selectedIndex++;
            }

            if (comb.Items.Count > selectedIndex)
                comb.SelectedIndex = selectedIndex;
        }

        private void SetTextDocumentType(object type)
        {
            var item = this.combDocumentType.Items[Convert.ToInt32(type)];
            //this.txtText.DocumentType = ICSharpCode.WinFormsUI.Controls.DocumentType.Text;
            try
            {
                this.txtText.DocumentType = (ICSharpCode.WinFormsUI.Controls.DocumentType)Enum.Parse(typeof(ICSharpCode.WinFormsUI.Controls.DocumentType), item.ToString());
            }
            catch (Exception ex)
            {

            }
        }

        private void InitializeControls()
        { 
            if (this.Command == "Add")
            {
                SetTextDocumentType(this.combDocumentType.SelectedIndex);
                return;
            }

            using (var db = NORM.DataBase.DataBaseFactory.Default)
            {
                string strSql = "SELECT [ID],[NAME],[Pid],[Snippet],[IsShow],[OrderIndex],[Formats],[Note],[Define] ";
                strSql += ",[Childs] FROM [T_SnippetTree] ";
                strSql += "WHERE [ID]='" + ID + "'";

                DataTable dt = db.QueryDataSet(CommandType.Text, strSql, null).Tables[0];

                foreach (DataRow dr in dt.Rows)
                {                                    
                    SetComboxSelectedValue(this.combDocumentType,dr["Formats"] + "");
                    //SetTextDocumentType(this.combDocumentType.SelectedIndex);
                    this.txtName.Text = dr["NAME"] + "";
                    this.txtText.Text = SnippetConvert.DecodeSnippetContent(dr["Snippet"] + "");   
                }
            }
        }

        private void btnApplyOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text))
            {
                new MessageDialog(DevelopAssistant.Core.Properties.Resources.warning_32px, "名称不能为空").ShowDialog(this);
                return;
            }

            try
            {
                using (var db = NORM.DataBase.DataBaseFactory.Default)
                {
                    string strSql = string.Empty;
                    string strContent = SnippetConvert.EncodeSnippetContent(txtText.Text);
                    //string strDontent = DecodeSnippetContent(strContent);
                    string formats = ((ListItem)this.combDocumentType.SelectedItem).Value;

                    switch (Command)
                    {
                        case "Add":
                            strSql = "INSERT INTO [T_SnippetTree] ( [NAME],[Pid],[Snippet],[IsShow],[OrderIndex],[Note],[Formats],[Define],[Childs] ) ";
                            strSql += "VALUES ( '" + txtName.Text + "','" + ParentID + "','" + strContent + "','1','0','','" + formats + "','Define','0' ) ;";
                            strSql += System.Environment.NewLine + " ";
                            strSql += "UPDATE [T_SnippetTree] SET Childs=Childs+1 WHERE [ID]='" + ParentID + "'";
                            break;
                        case "Edit":
                            strSql = " UPDATE [T_SnippetTree] SET [NAME]='" + txtName.Text + "', Formats='"+formats+"', ";
                            strSql += " [Snippet]='" + strContent + "' WHERE [ID]='" + ID + "' ";
                            break;
                    }

                    db.Execute(CommandType.Text, strSql, null);

                }
            }
            catch (Exception ex)
            {
                new MessageDialog(DevelopAssistant.Core.Properties.Resources.error_32px, ex.Message).ShowDialog(this);
                DevelopAssistant.Common.NLogger.WriteToLine(ex.Message, "错误", DateTime.Now, ex.Source, ex.StackTrace);
            }

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SnippetEdit_Load(object sender, EventArgs e)
        {
            this.groupBox1.Title = "记笔记是一种好习惯";
            this.txtText.EditModel = true;           
            this.panel1.BorderColor = this.XTheme.FormBorderOutterColor;
            OnThemeChanged(new EventArgs());
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.txtText.ReleaseDispose();
            base.OnClosing(e);            
        }

        private void combDocumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTextDocumentType(this.combDocumentType.SelectedIndex); 
        }

        public void OnThemeChanged(EventArgs e)
        {
            Color backColor = SystemColors.Control;
            Color foreColor = SystemColors.ControlText;
            Color textBackColor = SystemColors.Window;
            Color toolBackColor = SystemColors.Window;

            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    backColor = SystemColors.Control;
                    foreColor = SystemColors.ControlText;
                    textBackColor = SystemColors.Window;
                    toolBackColor = SystemColors.Control;
                    break;
                case "Black":
                    backColor = Color.FromArgb(045, 045, 045);
                    foreColor = Color.FromArgb(240, 240, 240);
                    textBackColor = Color.FromArgb(045, 045, 048);
                    toolBackColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            this.ForeColor = foreColor;
            this.BackColor = backColor;
            XBackgroundColor = backColor;

            this.txtName.XForeColor = foreColor;
            this.txtName.XBackColor = textBackColor;
            this.txtText.ForeColor = foreColor;
            this.txtText.BackColor = backColor;
            this.combDocumentType.ForeColor = foreColor;
            this.combDocumentType.BackColor = textBackColor;
            this.txtText.SetTheme(themeName);

            this.panel1.BackColor = toolBackColor;
            btnApplyOk.ForeColor = foreColor;
            btnApplyOk.BackColor = toolBackColor;
            btnCancle.ForeColor = foreColor;
            btnCancle.BackColor = toolBackColor;

        }
    }
}
