﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class SnippetPad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SnippetPad));
            this.EditorTextBox = new ICSharpCode.WinFormsUI.Controls.CodeFormatControl();
            this.SuspendLayout();
            // 
            // EditorTextBox
            // 
            this.EditorTextBox.BackColor = System.Drawing.Color.White;
            this.EditorTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditorTextBox.DocumentType = ICSharpCode.WinFormsUI.Controls.DocumentType.Text;
            this.EditorTextBox.EditModel = false;
            this.EditorTextBox.Location = new System.Drawing.Point(0, 0);
            this.EditorTextBox.Name = "EditorTextBox";
            this.EditorTextBox.Padding = new System.Windows.Forms.Padding(1);
            this.EditorTextBox.Size = new System.Drawing.Size(689, 383);
            this.EditorTextBox.TabIndex = 0;
            // 
            // SnippetPad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 383);
            this.Controls.Add(this.EditorTextBox);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SnippetPad";
            this.Text = "SnippetPad";
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.CodeFormatControl EditorTextBox;
    }
}