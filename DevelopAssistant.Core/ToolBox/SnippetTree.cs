﻿using DevelopAssistant.Core.ToolBar;
using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Controls;
using ICSharpCode.WinFormsUI.Controls.NodeControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class SnippetTree : ICSharpCode.WinFormsUI.Forms.BaseForm
    {
        public int SelectedNodeId = 0;

        public SnippetTree()
        {
            InitializeComponent();
            InitializeSnippetTree();
        }

        public SnippetTree(Form mainForm)
            : this()
        {
            this.XTheme = AppSettings.WindowTheme;
            this.NToolBar.BorderColor = AppSettings.WindowTheme.FormBorderOutterColor;
        }

        private void InitializeSnippetTree()
        {
            NodeStateIcon nodeIconBox1 = new NodeStateIcon();
            nodeIconBox1.DataPropertyName = "Icon";
            SnippetTreeView.NodeControls.Add(nodeIconBox1);

            NodeTextBox nodeTextBox1 = new NodeTextBox();
            nodeTextBox1.DataPropertyName = "Text";
            SnippetTreeView.NodeControls.Add(nodeTextBox1);

            SnippetTreeView.NBorderStyle = NBorderStyle.All;
            SnippetTreeView.SetTheme("Default");
        }

        private void LoadSnippetTreeData(NTreeNode owner, string pid)
        {
            using (var db = NORM.DataBase.DataBaseFactory.Default)
            {
                string sqlText = "SELECT [ID]" +
                    ",[NAME]" +
                    ",[Pid]" +
                    //",[Snippet]" +
                    ",[IsShow]" +
                    ",[OrderIndex]" +
                    ",[Childs]" +
                    ",[Note]" +
                    ",[Define]" +
                    " FROM [T_SnippetTree]" +
                    " WHERE [IsShow]='1'";

                if (!string.IsNullOrEmpty(pid))
                {
                    sqlText += " AND [Pid]='" + pid + "'";
                }
                else
                {
                    sqlText += " AND [Pid]='0'";
                }

                sqlText += " ORDER BY [OrderIndex] ASC";


                DataTable snippetDataTable = db.QueryDataSet(CommandType.Text, sqlText, null).Tables[0];

                foreach (DataRow dataRow in snippetDataTable.Rows)
                {
                    string cid = dataRow["ID"] + "";
                    string childs = dataRow["Childs"] + "";
                    string cpid = dataRow["Pid"] + "";

                    var node = new NSnippetTreeNode();
                    node.Id = Int32.Parse(dataRow["ID"] + "");
                    node.Text = dataRow["NAME"] + "";
                    //node.Snippet = dataRow["Snippet"] + "";
                    node.Pid = Int32.Parse(dataRow["Pid"] + "");
                    node.Note = dataRow["Note"] + "";
                    node.Define = dataRow["Define"] + "";
                    node.Childs = 0;

                    if (!string.IsNullOrEmpty(childs))
                    {
                        node.Childs = Int32.Parse(childs);
                    }

                    if (node.Childs > 0)
                    {
                        node.ImageIndex = 0;
                        node.SelectedImageIndex = 0;
                    }
                    else
                    {
                        node.ImageIndex = 2;
                        node.SelectedImageIndex = 2;
                    }

                    if (owner == null && string.IsNullOrEmpty(pid))
                        SnippetTreeView.Nodes.Add(node);

                    if (owner != null)
                    {
                        owner.Nodes.Add(node);
                        owner.ImageIndex = 1;
                        owner.SelectedImageIndex = 1;
                        if (owner.Level < 2)
                            owner.Expand();
                    }

                    LoadSnippetTreeData(node, cid);
                }

            }
        }

        private void SnippetTreeSelect_Load(object sender, EventArgs e)
        {
            LoadSnippetTreeData(null, "");
        }

        private void SnippetTree_Collapsed(object sender, NTreeViewEventArgs e)
        {
            if (e.Node.Nodes != null && e.Node.Nodes.Count > 0)
            {
                e.Node.ImageIndex = 0;
                e.Node.SelectedImageIndex = e.Node.ImageIndex;
            }
        }

        private void SnippetTree_Expanded(object sender, NTreeViewEventArgs e)
        {
            if (e.Node.Nodes != null && e.Node.Nodes.Count > 0)
            {
                e.Node.ImageIndex = 1;
                e.Node.SelectedImageIndex = e.Node.ImageIndex;
            }
        }

        private void SnippetTree_NodeMouseClick(object sender, NTreeNodeMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.SnippetTreeView.SelectedNode = e.Node;
            }
        }

        private void btnLook_Click(object sender, EventArgs e)
        {
            if (SnippetTreeView.SelectedNode == null)
            {
                MessageBox.Show(this, "还未选择节点", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.SelectedNodeId = ((NTreeNode)this.SnippetTreeView.SelectedNode).Id;
            this.DialogResult = DialogResult.OK;
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

    }
}
