﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class TSQLEditorSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TSQLEditorSettings));
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.nPanel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnSave = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.groupBox1 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.panelFoldMarkStyle = new System.Windows.Forms.Panel();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox2 = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.nPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panelFoldMarkStyle.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(40, 76);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(156, 16);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "开启查询分析器事务机制";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // nPanel1
            // 
            this.nPanel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.nPanel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.nPanel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.nPanel1.Controls.Add(this.btnSave);
            this.nPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nPanel1.Location = new System.Drawing.Point(6, 428);
            this.nPanel1.MarginWidth = 0;
            this.nPanel1.Name = "nPanel1";
            this.nPanel1.Size = new System.Drawing.Size(411, 60);
            this.nPanel1.TabIndex = 1;
            this.nPanel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnSave.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnSave.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnSave.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnSave.FouseColor = System.Drawing.Color.White;
            this.btnSave.Foused = false;
            this.btnSave.FouseTextColor = System.Drawing.Color.Black;
            this.btnSave.Icon = null;
            this.btnSave.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(314, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Radius = 0;
            this.btnSave.Size = new System.Drawing.Size(75, 32);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "保存";
            this.btnSave.UnableIcon = null;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox1.Controls.Add(this.checkBox4);
            this.groupBox1.Controls.Add(this.checkBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.GridLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(189)))), ((int)(((byte)(189)))));
            this.groupBox1.Location = new System.Drawing.Point(4, 4);
            this.groupBox1.MarginWidth = 0;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(403, 386);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Title = "";
            this.groupBox1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(41, 22);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(132, 16);
            this.checkBox4.TabIndex = 4;
            this.checkBox4.Text = "开启关键词自动大写";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(41, 48);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(156, 16);
            this.checkBox3.TabIndex = 3;
            this.checkBox3.Text = "开启自动补全字段标识符";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox2.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox2.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox2.Controls.Add(this.panelFoldMarkStyle);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.comboBox2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.GridLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(189)))), ((int)(((byte)(189)))));
            this.groupBox2.Location = new System.Drawing.Point(7, 132);
            this.groupBox2.MarginWidth = 0;
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(388, 245);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Title = "";
            this.groupBox2.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // panelFoldMarkStyle
            // 
            this.panelFoldMarkStyle.Controls.Add(this.radioButton5);
            this.panelFoldMarkStyle.Controls.Add(this.radioButton4);
            this.panelFoldMarkStyle.Controls.Add(this.radioButton3);
            this.panelFoldMarkStyle.Location = new System.Drawing.Point(97, 199);
            this.panelFoldMarkStyle.Name = "panelFoldMarkStyle";
            this.panelFoldMarkStyle.Size = new System.Drawing.Size(217, 29);
            this.panelFoldMarkStyle.TabIndex = 9;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(159, 6);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(47, 16);
            this.radioButton5.TabIndex = 2;
            this.radioButton5.TabStop = true;
            this.radioButton5.Tag = "2";
            this.radioButton5.Text = "圆形";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(77, 6);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(59, 16);
            this.radioButton4.TabIndex = 1;
            this.radioButton4.TabStop = true;
            this.radioButton4.Tag = "1";
            this.radioButton4.Text = "多边形";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(1, 6);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(59, 16);
            this.radioButton3.TabIndex = 0;
            this.radioButton3.TabStop = true;
            this.radioButton3.Tag = "0";
            this.radioButton3.Text = "正方形";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "折叠样式：";
            // 
            // comboBox2
            // 
            this.comboBox2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.comboBox2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBox2.DropDownButtonWidth = 20;
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.DropDownWidth = 0;
            this.comboBox2.Font = new System.Drawing.Font("宋体", 9F);
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.ItemHeight = 18;
            this.comboBox2.ItemIcon = null;
            this.comboBox2.Location = new System.Drawing.Point(99, 127);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.SelectedIndex = -1;
            this.comboBox2.SelectedItem = null;
            this.comboBox2.Size = new System.Drawing.Size(121, 22);
            this.comboBox2.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "选择主题：";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Location = new System.Drawing.Point(99, 89);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(114, 29);
            this.panel1.TabIndex = 5;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(56, 6);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(47, 16);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "当前";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(1, 6);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(47, 16);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "新开";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "窗口模式：";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox1.FousedColor = System.Drawing.Color.Orange;
            this.textBox1.Icon = null;
            this.textBox1.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.textBox1.IsButtonTextBox = false;
            this.textBox1.IsClearTextBox = false;
            this.textBox1.IsPasswordTextBox = false;
            this.textBox1.Location = new System.Drawing.Point(99, 58);
            this.textBox1.MaxLength = 32767;
            this.textBox1.Multiline = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.PasswordChar = '\0';
            this.textBox1.Placeholder = null;
            this.textBox1.ReadOnly = false;
            this.textBox1.Size = new System.Drawing.Size(95, 24);
            this.textBox1.TabIndex = 3;
            this.textBox1.UseSystemPasswordChar = false;
            this.textBox1.XBackColor = System.Drawing.Color.White;
            this.textBox1.XDisableColor = System.Drawing.Color.Empty;
            this.textBox1.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "字体大小：";
            // 
            // comboBox1
            // 
            this.comboBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.comboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBox1.DropDownButtonWidth = 20;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.DropDownWidth = 0;
            this.comboBox1.Font = new System.Drawing.Font("宋体", 9F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 18;
            this.comboBox1.ItemIcon = null;
            this.comboBox1.Location = new System.Drawing.Point(99, 29);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.SelectedIndex = -1;
            this.comboBox1.SelectedItem = null;
            this.comboBox1.Size = new System.Drawing.Size(121, 22);
            this.comboBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "字体：";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(40, 106);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(150, 16);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "开启SQL编辑器智能提示";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(6, 34);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(4);
            this.panel2.Size = new System.Drawing.Size(411, 394);
            this.panel2.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radioButton6);
            this.panel3.Controls.Add(this.radioButton7);
            this.panel3.Location = new System.Drawing.Point(110, 297);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(114, 29);
            this.panel3.TabIndex = 7;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(56, 6);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(47, 16);
            this.radioButton6.TabIndex = 1;
            this.radioButton6.Text = "竖排";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(1, 6);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(47, 16);
            this.radioButton7.TabIndex = 0;
            this.radioButton7.Text = "横排";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(45, 304);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "视图布局：";
            // 
            // TSQLEditorSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 494);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.nPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "TSQLEditorSettings";
            this.Text = "设置";
            this.Load += new System.EventHandler(this.TSQLEditorSettings_Load);
            this.nPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panelFoldMarkStyle.ResumeLayout(false);
            this.panelFoldMarkStyle.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private ICSharpCode.WinFormsUI.Controls.NPanel nPanel1;
        private ICSharpCode.WinFormsUI.Controls.NButton btnSave;
        private System.Windows.Forms.FontDialog fontDialog1;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox2;
        private ICSharpCode.WinFormsUI.Controls.NTextBox textBox1;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.WinFormsUI.Controls.NComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label3;
        private ICSharpCode.WinFormsUI.Controls.NComboBox comboBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Panel panelFoldMarkStyle;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.Label label6;
    }
}