﻿using DevelopAssistant.Core.DBMS;
using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Docking;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class TSQLEditorSettings : ToolBoxBase
    {
        public TSQLEditorSettings()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            this.comboBox1.Items.Add("Arial");
            this.comboBox1.Items.Add("Courier New");
            this.comboBox1.Items.Add("仿宋");
            this.comboBox1.Items.Add("宋体");
            this.comboBox1.Items.Add("黑体");
            this.comboBox1.Items.Add("微软雅黑");

            this.groupBox1.Title = "选项";
            this.groupBox2.Title = "编辑器设置";

            int selectIndex = 0;

            if (AppSettings.EditorSettings.EditorFont == null)
                return;

            var font = AppSettings.EditorSettings.EditorFont;
            foreach (string s in this.comboBox1.Items)
            {
                if (s.Equals(font.FontFamily.Name, StringComparison.OrdinalIgnoreCase))
                    break;

                selectIndex++;
            }

            this.comboBox1.SelectedIndex = selectIndex;
            this.textBox1.Text = font.Size.ToString();

            if (AppSettings.NewOpenQueryDocument)
            {
                radioButton1.Checked = true;
            }
            else
            {
                radioButton2.Checked = true;
            }

            if (AppSettings.EditorSettings.AutoSupplementary)
            {
                checkBox3.Checked = true;
            }
            else
            {
                checkBox3.Checked = false;
            }

            if (AppSettings.EditorSettings.KeywordsCase)
            {
                checkBox4.Checked = true;
            }
            else
            {
                checkBox4.Checked = false;
            }

            if(AppSettings.EditorSettings.QueryStyle == QueryDataGridStyle.Horizontal)
            {
                radioButton7.Checked = true;
            }
            else
            {
                radioButton6.Checked = true;
            }

            this.comboBox2.Items.Add("白色");
            this.comboBox2.Items.Add("黑色");

            selectIndex = 0;
            switch (AppSettings.EditorSettings
                .TSQLEditorTheme)
            {
                case "Default":
                    selectIndex = 0;
                    break;
                case "Black":
                    selectIndex = 1;
                    break;
            }         

            this.comboBox2.SelectedIndex = selectIndex;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            AppSettings.EditorSettings.KeywordsCase = this.checkBox4.Checked;
            AppSettings.EditorSettings.AutoSupplementary = this.checkBox3.Checked;
            AppSettings.EditorSettings.EnableDataBaseTran = this.checkBox1.Checked;
            AppSettings.EditorSettings.EnableIntellisense = this.checkBox2.Checked;

            AppSettings.EditorSettings.EditorFont = new Font(this.comboBox1.SelectedItem.ToString(), float.Parse(this.textBox1.Text));

            switch (this.comboBox2.SelectedIndex)
            {
                case 0:
                    AppSettings.EditorSettings.TSQLEditorTheme = "Default";
                    break;
                case 1:
                    AppSettings.EditorSettings.TSQLEditorTheme = "Black";
                    break;
            }

            string strEditorTheme = AppSettings.EditorSettings.TSQLEditorTheme;
            //string connectionString = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "baseset.db;Version=3;Password=fas2015;UseUTF16Encoding=True;";
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            connectionString = connectionString.Replace("|DataDirectory|", AppDomain.CurrentDomain.BaseDirectory);
            using (var db = NORM.DataBase.DataBaseFactory.Create(connectionString,
                    NORM.DataBase.DataBaseTypes.SqliteDataBase))
            {
                string sql = "update T_AppSettings set value='" + strEditorTheme + "' where name='EditorTheme' ";
                int val = db.Execute(CommandType.Text, sql, null);
            }

            int foldmarkStyle = 0;
            foreach (Control contrl in panelFoldMarkStyle.Controls)
            {
                RadioButton radioButton = (RadioButton)contrl;
                if (radioButton.Checked)
                {
                    foldmarkStyle = Convert.ToInt16(radioButton.Tag);
                }
            }
            AppSettings.EditorSettings.FoldMarkStyle = foldmarkStyle;
            AppSettings.EditorSettings.QueryStyle = radioButton7.Checked ? 
                QueryDataGridStyle.Horizontal : QueryDataGridStyle.Vertical;

            MainForm mainForm = (MainForm)Application.OpenForms["MainForm"];
            if (mainForm != null)
            {
                var contents = mainForm.GetContents();
                foreach (IDockContent dock in contents)
                {
                    dock.OnThemeChanged(new EventArgs());
                }
                mainForm.SetFormEditorTheme();
            }

            if (radioButton1.Checked)
            {
                AppSettings.NewOpenQueryDocument = true;
            }
            else
            {
                AppSettings.NewOpenQueryDocument = false;
            }                   

            this.Close();
        }

        private void TSQLEditorSettings_Load(object sender, EventArgs e)
        {
            this.nPanel1.BorderColor = AppSettings.WindowTheme.FormBorderOutterColor;
            this.checkBox1.Checked = AppSettings.EditorSettings.EnableDataBaseTran;
            this.checkBox2.Checked = AppSettings.EditorSettings.EnableIntellisense;

            OnThemeChanged(new EventArgs());

            foreach (Control contrl in panelFoldMarkStyle.Controls)
            {
                RadioButton radioButton = (RadioButton)contrl;
                if (Convert.ToInt16(radioButton.Tag) == AppSettings.EditorSettings.FoldMarkStyle)
                {
                    radioButton.Checked = true;
                }
            }

        }

        private void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color toolBackColor = SystemColors.Control;
            Color textColor = SystemColors.Window;
            Color linkColor = System.Drawing.Color.Blue;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    toolBackColor = SystemColors.Control;
                    textColor = SystemColors.Window;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    linkColor = Color.FromArgb(051, 153, 153);
                    toolBackColor = Color.FromArgb(038, 038, 038);
                    textColor = Color.FromArgb(038, 038, 038);
                    break;
            }

            panel2.ForeColor = foreColor;
            panel2.BackColor = backColor;
            nPanel1.ForeColor = foreColor;
            nPanel1.BackColor = toolBackColor;
            groupBox1.ForeColor = foreColor;
            groupBox1.BackColor = backColor;
            groupBox2.ForeColor = foreColor;
            groupBox2.BackColor = backColor;
            comboBox1.ForeColor = foreColor;
            comboBox1.BackColor = textColor;
            comboBox2.ForeColor = foreColor;
            comboBox2.BackColor = textColor;
            btnSave.ForeColor = foreColor;
            btnSave.BackColor = toolBackColor;
            textBox1.XForeColor = foreColor;
            textBox1.XBackColor = textColor;             
        }

    }
}
