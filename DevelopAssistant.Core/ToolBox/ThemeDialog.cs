﻿using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class ThemeDialog : ToolBoxBase 
    {
        public string ThemeName { get; set; }

        private void InitializeControls()
        {
            this.Resizable = false;
        }

        public ThemeDialog()
        {
            InitializeComponent();
            InitializeControls();
        }

        public ThemeDialog(ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase theme)
            : this()
        {
            this.XTheme = theme;

            themeWindows.Text = "Windows";
            themeApple.Text = "Apple";
            themeBootstrip.Text = "Bootstrip";
            themeMore.Text = "More";

            if (this.XTheme.GetType().Equals(typeof(ICSharpCode.WinFormsUI.Theme.ThemeVS2012)))
            {
                themeWindows.Checked = true;             
            }
            else if (this.XTheme.GetType().Equals(typeof(ICSharpCode.WinFormsUI.Theme.ThemeMac)))
            {
                themeApple.Checked = true;               
            }
            else if (this.XTheme.GetType().Equals(typeof(ICSharpCode.WinFormsUI.Theme.ThemeShadow)))
            {
                themeBootstrip.Checked = true;               
            }

            panel1.BorderColor = this.XTheme.FormBorderOutterColor;

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            string checkedThemeName = string.Empty;
            foreach (var theme in this.groupBox1.Controls)
            {
                if (theme.GetType().Equals(typeof(ICSharpCode.WinFormsUI.Controls.NTheme)))
                {
                    if (((ICSharpCode.WinFormsUI.Controls.NTheme)(theme)).Checked)
                    {
                        checkedThemeName = ((ICSharpCode.WinFormsUI.Controls.NTheme)(theme)).Text;
                        break;
                    }
                }
            }

            switch (checkedThemeName)
            {
                case "Apple": 
                    ThemeName = "ICSharpCode.WinFormsUI.Theme.ThemeMac";
                    break;
                case "Windows":
                    ThemeName = "ICSharpCode.WinFormsUI.Theme.ThemeVS2012";
                    break;
                case "Bootstrip":
                    ThemeName = "ICSharpCode.WinFormsUI.Theme.ThemeShadow";
                    break;
            }

            //string strEditorTheme = AppSettings.EditorSettings.TSQLEditorTheme;
            //string connectionString = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "baseset.db;Version=3;Password=fas2015;UseUTF16Encoding=True;";
            //using (var db = NORM.DataBase.DataBaseFactory.Create(connectionString,
            //        NORM.DataBase.DataBaseTypes.SqliteDataBase))
            //{
            //    string sql = "update T_AppSettings set value='"+ strEditorTheme + "' where name='EditorTheme' ";
            //    int val = db.Execute(CommandType.Text, sql, null);
            //}

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void pnlTheme_Click(object sender, EventArgs e)
        {
            ICSharpCode.WinFormsUI.Controls.NTheme theme = (ICSharpCode.WinFormsUI.Controls.NTheme)sender;           

            switch (theme.Text)
            {
                case "Windows":
                    themeApple.Checked = false;
                    themeBootstrip.Checked = false;
                    themeWindows.Checked = true;
                    break;
                case "Apple":
                    themeWindows.Checked = false;
                    themeBootstrip.Checked = false;
                    themeApple.Checked = true;
                    break;
                case "Bootstrip":
                    themeApple.Checked = false;
                    themeWindows.Checked = false;
                    themeBootstrip.Checked = true;
                    break;
                case "More": break;
            }
          
            
        }

        private void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color linkColor = System.Drawing.Color.Blue;
            Color toolBackColor = SystemColors.Window;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    toolBackColor = SystemColors.Control;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(038, 038, 038);
                    linkColor = Color.FromArgb(051, 153, 153);
                    toolBackColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            XBackgroundColor = backColor;
            groupBox1.BackColor = backColor;          
            panel2.BackColor = backColor;
            panel1.BackColor = toolBackColor;
            btnApply.ForeColor = foreColor;
            btnApply.BackColor = toolBackColor;
            btnCancel.ForeColor = foreColor;
            btnCancel.BackColor = toolBackColor;
            BackColor = backColor;
        }

        private void ThemeDialog_Load(object sender, EventArgs e)
        {            
            OnThemeChanged(new EventArgs());
        }
    }
}
