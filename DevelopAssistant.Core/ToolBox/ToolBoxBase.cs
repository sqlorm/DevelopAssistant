﻿
using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Docking;
using ICSharpCode.WinFormsUI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class ToolBoxBase : BaseForm //DockContent
    {
        public ToolBoxBase()
        {
            InitializeComponent();
            this.XTheme = AppSettings.WindowTheme;
        }
    }
}
