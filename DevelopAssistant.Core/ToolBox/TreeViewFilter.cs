﻿using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class TreeViewFilter : ICSharpCode.WinFormsUI.Forms.BaseForm //ToolBoxBase
    {
        public string Keywords = string.Empty;

        public TreeViewFilter()
        {
            InitializeComponent();
        }

        public TreeViewFilter(MainForm OwnerForm, string Keywords)
            : this()
        {
            this.XTheme = AppSettings.WindowTheme;
            this.textBox1.Text = Keywords;
            this.panel1.BorderColor = this.XTheme.FormBorderOutterColor;
        } 

        private void btnApplyOk_Click(object sender, EventArgs e)
        {
            this.Keywords = this.textBox1.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color textColor = SystemColors.Window;
            Color toolBackColor = System.Drawing.Color.Blue;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    textColor = SystemColors.Window;
                    toolBackColor = SystemColors.Control;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    textColor = Color.FromArgb(038, 038, 038);
                    toolBackColor = Color.FromArgb(038, 038, 038);
                    break;
            }

            panel2.ForeColor = foreColor;
            panel2.BackColor = backColor;
            panel1.BackColor = toolBackColor;
            btnApplyOk.ForeColor = foreColor;
            btnApplyOk.BackColor = toolBackColor;
            btnCancle.ForeColor = foreColor;
            btnCancle.BackColor = toolBackColor;
            textBox1.XForeColor = foreColor;
            textBox1.XBackColor = textColor;
        }

        private void TreeViewFilter_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new EventArgs());
        }
    }
}
