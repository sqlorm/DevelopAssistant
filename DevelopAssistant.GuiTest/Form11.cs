﻿using DevelopAssistant.Common;
using ICSharpCode.WinFormsUI.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.GuiTest
{
    public partial class Form11 : Form
    {
        public Form11()
        {
            InitializeComponent();
        }

        private void Form11_Load(object sender, EventArgs e)
        {
            listView1.View = View.Details;
           
            listView1.GridLines = true;          

            listView1.BeginUpdate();

            listView1.SetTheme("Black");

            listView1.View = View.Details;
            listView1.GridLines = true;
            //listView2.LargeImageList = imageList1;
            //listView2.SmallImageList = imageList1;
            //listView1.StateImageList = imageList1;

            var columns1 = new NColumnHeader[] { new NColumnHeader(), new NColumnHeader(), new NColumnHeader() };
            listView1.Columns.AddRange(columns1);

            //this.listView1.Columns.Add("列标题1", 50, HorizontalAlignment.Left); //一步添加
            //this.listView1.Columns.Add("列标题2", 120, HorizontalAlignment.Left); //一步添加
            //this.listView1.Columns.Add("列标题3", 120, HorizontalAlignment.Left); //一步添加

            for (int i = 0; i < 50; i++)

            {
                NListViewItem lvi = new NListViewItem();
                lvi.ImageIndex = 0;
                lvi.Text = "item" + i;
                lvi.SubItems.Add("第2列,第" + i + "行");
                lvi.SubItems.Add("第3列,第" + i + "行");

                this.listView1.Items.Add(lvi);

            }

            listView1.EndUpdate();


            listView2.BeginUpdate();

            listView2.View = View.Details;
            listView2.GridLines = true;
            //listView2.LargeImageList = imageList1;
            //listView2.SmallImageList = imageList1;
            listView2.StateImageList = imageList1;

            var columns2 = new ColumnHeader[] { new ColumnHeader(), new ColumnHeader(), new ColumnHeader() };
            listView2.Columns.AddRange(columns2);

            //this.listView1.Columns.Add("列标题1", 50, HorizontalAlignment.Left); //一步添加
            //this.listView1.Columns.Add("列标题2", 120, HorizontalAlignment.Left); //一步添加
            //this.listView1.Columns.Add("列标题3", 120, HorizontalAlignment.Left); //一步添加

            for (int i = 0; i < 10; i++)

            {

                ListViewItem lvi = new ListViewItem();

                lvi.ImageIndex = 0;              

                lvi.Text = "item" + i;

                lvi.SubItems.Add("第2列,第" + i + "行");
                lvi.SubItems.Add("第3列,第" + i + "行");

                this.listView2.Items.Add(lvi);

            }

            listView2.SelectedIndexChanged += ListView2_SelectedIndexChanged;

            listView2.EndUpdate();

        }

        private void ListView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DigitToChnTextUtils dd = new DigitToChnTextUtils();
            textBox2.Text = dd.Convert(Convert.ToInt32(textBox1.Text).ToString(), true);

            for (int i = 0; i < 120; i++)
            {
                DevelopAssistant.Common.NLogger.LogDebug(dd.Convert(i.ToString(),false));
            }
        }
        
    }
}
