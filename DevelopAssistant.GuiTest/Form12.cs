﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.GuiTest
{
    public partial class Form12 : Form
    {
        public Form12()
        {
            InitializeComponent();
        }

        private void Form12_Load(object sender, EventArgs e)
        {
            comboBox1.DropDownStyle = ComboBoxStyle.DropDown;
            BindDropDownList();
        }

        public void BindDropDownList()
        {
            this.comboBox1.Items.Add("111111111");
            this.comboBox1.Items.Add("222222222");
            //this.comboBox1.Items.Add("333333333");
            //this.comboBox1.Items.Add("444444444");
            //this.comboBox1.Items.Add("555555555");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = comboBox1.Text;
        }
    }
}
