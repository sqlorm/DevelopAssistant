﻿namespace DevelopAssistant.GuiTest
{
    partial class Form7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form7));
            this.nTimelineControl1 = new ICSharpCode.WinFormsUI.Controls.NTimeLine.NTimelineControl();
            this.SuspendLayout();
            // 
            // nTimelineControl1
            // 
            this.nTimelineControl1.BackColor = System.Drawing.Color.White;
            this.nTimelineControl1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.nTimelineControl1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.nTimelineControl1.BottomBlackColor = System.Drawing.Color.Empty;
            this.nTimelineControl1.DataList = ((System.Collections.Generic.List<ICSharpCode.WinFormsUI.Controls.NTimeLine.MonthItem>)(resources.GetObject("nTimelineControl1.DataList")));
            this.nTimelineControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nTimelineControl1.IsEditModel = true;
            this.nTimelineControl1.Location = new System.Drawing.Point(10, 10);
            this.nTimelineControl1.MarginWidth = 0;
            this.nTimelineControl1.Name = "nTimelineControl1";
            this.nTimelineControl1.Size = new System.Drawing.Size(252, 381);
            this.nTimelineControl1.TabIndex = 0;
            this.nTimelineControl1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // Form7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 401);
            this.Controls.Add(this.nTimelineControl1);
            this.Name = "Form7";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Text = "Form7";
            this.Load += new System.EventHandler(this.Form7_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NTimeLine.NTimelineControl nTimelineControl1;
    }
}