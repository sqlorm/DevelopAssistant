﻿using ICSharpCode.WinFormsUI.Controls.NTimeLine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.GuiTest
{
    public partial class Form7 : Form
    {
        public Form7()
        {
            InitializeComponent();
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            List<MonthItem> list = new List<MonthItem>();
            MonthItem item = new MonthItem();           
            item.Date = Convert.ToDateTime("2019-06");
            item.DateLabel = nTimelineControl1.ToDateLabel(item.Date);
            //item.Description = "开发Java SpringBoot 项目代码生成";
            //item.DateTime = Convert.ToDateTime("2019-07-12 12:24:45");
            //item.Level = ImportantLevel.Normal;
            //item.Timeliness = Timeliness.Normal;

            List<DateItem> sublist = new List<DateItem>();
            DateItem subItem = new DateItem();
            subItem.Date = Convert.ToDateTime("2019-06-12");
            sublist.Add(subItem);
            item.List = sublist;

            List<DateTimeItem> subsublist = new List<DateTimeItem>();
            DateTimeItem subsubItem = new DateTimeItem();
            subsubItem.Id = "108";
            subsubItem.Name = "学习SpringBoot代码";
            subsubItem.Title = "学习SpringBoot代码";
            subsubItem.Timeliness = Timeliness.Yellow;
            subsubItem.ResponsiblePerson = "芳";
            subsubItem.Level = ImportantLevel.MostImportant;
            subsubItem.DateTime = Convert.ToDateTime("2019-06-12 12:15:54");
            
            subsublist.Add(subsubItem);

            subsubItem = new DateTimeItem();
            subsubItem.Id = "109";
            subsubItem.Name = "这是一个示例";
            subsubItem.Title = "这是一个示例";
            subsubItem.DateTime = Convert.ToDateTime("2019-06-12 12:15:54");
             
            subsublist.Add(subsubItem);

            subsubItem = new DateTimeItem();
            subsubItem.Id = "110";
            subsubItem.Name = "实现A星寻路算法";
            subsubItem.Title = "实现A星寻路算法";
            subsubItem.Timeliness = Timeliness.Dark;
            subsubItem.Level = ImportantLevel.Important;
            subsubItem.Summary = "晚上回去实现A星寻路算法";
            subsubItem.DateTime = Convert.ToDateTime("2019-06-12 10:15:54");
            
            subsublist.Add(subsubItem);

            subsubItem = new DateTimeItem();
            subsubItem.Id = "112";
            subsubItem.Name = "添加执行计划功能";
            subsubItem.Title = "添加执行计划功能";
            subsubItem.Timeliness = Timeliness.Red;
            subsubItem.DateTime = Convert.ToDateTime("2019-06-12 10:15:54");
             
            subsublist.Add(subsubItem);

            subsubItem = new DateTimeItem();
            subsubItem.Id = "113";
            subsubItem.Name = "看一场电影";
            subsubItem.Title = "看一场电影";
            subsubItem.ResponsiblePerson = "莉";
            subsubItem.Level = ImportantLevel.MoreImportant;
            subsubItem.Timeliness = Timeliness.Black;
            subsubItem.DateTime = Convert.ToDateTime("2019-06-12 10:15:54");
             
            subsublist.Add(subsubItem);

            subItem.List = subsublist;

            list.Add(item);

            item = new MonthItem();            
            item.Date = Convert.ToDateTime("2019-07");
            item.DateLabel = nTimelineControl1.ToDateLabel(item.Date);
            //item.Description = "添加Java代码格式化";
            //item.DateTime = Convert.ToDateTime("2019-07-14 12:24:45");
            //item.Level = ImportantLevel.Normal;
            //item.Timeliness = Timeliness.Normal;

            sublist = new List<DateItem>();
            subItem = new DateItem();
            subItem.Date = Convert.ToDateTime("2019-07-14");           
            sublist.Add(subItem);

            subsublist = new List<DateTimeItem>();
            subsubItem = new DateTimeItem();
            subsubItem.Id = "101";
            subsubItem.Name = "开发UML控件";
            subsubItem.Title = "开发UML控件";
            subsubItem.DateTime = Convert.ToDateTime("2019-09-14 12:45:54");
            
            subsublist.Add(subsubItem);

            subItem.List = subsublist;

            item.List = sublist;

            list.Add(item);

            item = new MonthItem();            
            item.Date = Convert.ToDateTime("2019-09");
            item.DateLabel = nTimelineControl1.ToDateLabel(item.Date);

            sublist = new List<DateItem>();
            subItem = new DateItem();
            subItem.Date = Convert.ToDateTime("2019-09-14");           

            subsublist = new List<DateTimeItem>();
            subsubItem = new DateTimeItem();
            subsubItem.Id = "102";
            subsubItem.Name = "开发时间线控件";
            subsubItem.Title = "开发时间线控件";
            subsubItem.ToolTip= "开发时间线控件";
            subsubItem.Timeliness = Timeliness.Orange;
            subsubItem.DateTime = Convert.ToDateTime("2019-09-14 12:45:54");
             
            subsublist.Add(subsubItem);

            subsubItem = new DateTimeItem();
            subsubItem.Id = "103";
            subsubItem.Name = "Java SpringBoot";
            subsubItem.Title = "Java SpringBoot";
            subsubItem.Icon = DevelopAssistant.GuiTest.Properties.Resources.Clock_24px;
            subsubItem.DateTime = Convert.ToDateTime("2019-09-14 10:45:54");
             
            subsublist.Add(subsubItem);


            subItem.List = subsublist;

            sublist.Add(subItem);            

            subItem = new DateItem();
            subItem.Date = Convert.ToDateTime("2019-09-01");

            subsublist = new List<DateTimeItem>();
            subsubItem = new DateTimeItem();
            subsubItem.Id = "104";
            subsubItem.Name = "学习分布式事务";
            subsubItem.Title = "学习分布式事务";
            subsubItem.Timeliness = Timeliness.Red;
            subsubItem.ResponsiblePerson = "东";
            subsubItem.Summary = "2019-09-01 上优学网学习分布式事务，切记切记!";
            subsubItem.DateTime = Convert.ToDateTime("2019-09-01 09:45:54");
            
            subsublist.Add(subsubItem);
            subItem.List = subsublist;

            sublist.Add(subItem);

            item.List = sublist;

            list.Add(item);

            this.nTimelineControl1.DataList = list.OrderByDescending(w => w.Date).ToList();
            this.nTimelineControl1.DataBind();
        }

        private void DateTimeItem_Click(object sender, TimeLineEventArgs e)
        {
            if (e.Data != null)
                MessageBox.Show(e.Data.Name + " " + e.Command);
        }

    }
}
