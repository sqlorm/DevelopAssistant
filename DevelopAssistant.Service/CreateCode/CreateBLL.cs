﻿using DevelopAssistant.Common;
using DevelopAssistant.Service.TemplatingEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public class CreateBLL
    {
        public static string CreateEntityBLL(TextTemplatingEngine host, string name, out string message)
        {
            StringPlus sp = new StringPlus();

            using (var db = Utility.GetAdohelper(host.ConnectionString, host.ProviderName))
            {
                string code = string.Empty;
                NORM.SQLObject.Generator.BuildEntityBLL(db, name, host.NameSpace, host.SpaceName, out code);
                sp.Append(code);
            }

            message = "" + name + "-DAL 生成完成";

            return sp.Value;
        }

        public static string CreateModelBLL(TextTemplatingEngine host, string name, out string message)
        {
            return Creater.CreateCode(host, "BLL", "t4_bll.tt", "F:\\CODE\\BLL\\", "", out message);
        }
    }
}
