﻿using DevelopAssistant.Common;
using DevelopAssistant.Service.TemplatingEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public class CreateIDAL
    {
        public static string CreateEntityIDAL(TextTemplatingEngine host, string name, out string message)
        {
            StringPlus sp = new StringPlus();

            using (var db = Utility.GetAdohelper(host.ConnectionString, host.ProviderName))
            {
                string code = string.Empty;
                NORM.SQLObject.Generator.BuildEntityIDAL(db, name, host.NameSpace, host.SpaceName, out code);
                sp.Append(code);
            }

            message = "" + name + "-IDAL 生成完成";

            return sp.Value;
        }

        public static string CreateModelDAL(TextTemplatingEngine host, string name, out string message)
        {
            return Creater.CreateCode(host, "IDAL", "t4_idal.tt", "F:\\CODE\\IDAL\\", "", out message);
        }

    }
}
