﻿using System;
using System.Collections.Generic; 
using System.Text;
using NORM.Common;  
using DevelopAssistant.Service.TemplatingEngine;
using System.Windows.Forms; 
using System.Data; 

namespace DevelopAssistant.Service
{
    public class CreateModel
    {
        public static string CreateNormEntity(TextTemplatingEngine host, string name,out string message)
        {
            StringPlus sp = new StringPlus();

            using (var db = Utility.GetAdohelper(host.ConnectionString,host.ProviderName))
            {               
                string code = string.Empty;
                NORM.SQLObject.Generator.BuildEntity(db, name, host.NameSpace, host.SpaceName, out code);
                sp.Append(code);
            }

            message = "" + name + "-Model 生成完成";

            return sp.Value;
        }

        public static string CreateNormModel(TextTemplatingEngine host, string name, out string message)
        {
            StringPlus sp = new StringPlus();

            //using (var db = Utility.GetAdohelper(dataBaseServer))
            //{
            //    string code = string.Empty;
            //    NORM.SQLObject.Generator.BuildModel(db, name, "DevelopAssistant.Test", out code);
            //    sp.Append(code);
            //}

            sp.Append(Creater.CreateCode(host, "Model", "t4_model.tt", "F:\\CODE\\MODEL\\", "", out message));

            return sp.Value;
        }



    }
}
