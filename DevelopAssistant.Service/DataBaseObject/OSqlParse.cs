﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DevelopAssistant.Service
{
    public class OSqlParse
    {
        public static List<string> PrintParse(string InputSql, string ProviderName)
        {
            List<string> Prints = new List<string>();

            if (string.IsNullOrEmpty(InputSql))
                return Prints;

            if (InputSql.ToUpper().Contains("PRINT"))
            {
                System.Text.RegularExpressions.Regex regex = null;
                switch (ProviderName)
                {
                    case "System.Data.Sql":
                    case "System.Data.SQL":
                        regex =new System.Text.RegularExpressions.Regex(@"PRINT(.+)",
                        System.Text.RegularExpressions.RegexOptions.IgnoreCase |
                System.Text.RegularExpressions.RegexOptions.Compiled);
                        break;
                    case "System.Data.PostgreSql":
                        regex = new System.Text.RegularExpressions.Regex(@"PRINT(.+)",
                        System.Text.RegularExpressions.RegexOptions.IgnoreCase |
                System.Text.RegularExpressions.RegexOptions.Compiled);
                        break;
                    default :
                        regex = new System.Text.RegularExpressions.Regex(@"PRINT(.+)",
                              System.Text.RegularExpressions.RegexOptions.IgnoreCase |
                      System.Text.RegularExpressions.RegexOptions.Compiled);
                      break;
                }

                string[] _textPrints = regex.Split(InputSql);

                foreach (string text in _textPrints)
                {
                    string _text = text.Trim('\n').Trim('\r').Trim();
                    if (!string.IsNullOrEmpty(_text))
                    {
                        if (_text.ToUpper().StartsWith("('"))
                        {
                            string print = _text.Replace("('", "").Replace("')", "");
                            Prints.Add(print);
                        }
                    }
                }

            }
            return Prints;
        }

        public static List<string> UseParse(string InputSql, string ProviderName)
        {
            //InputSql = "use LingyunDevelopFramework_Base";
            List<string> UseBases = new List<string>();

            System.Text.RegularExpressions.Regex regx =
                    new System.Text.RegularExpressions.Regex(@"USE .+",
                        System.Text.RegularExpressions.RegexOptions.Compiled |
                        System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Match match =
                regx.Match(InputSql);

            if (!match.Success)
            {
                throw new Exception("SQL语法有错误！");
            }

            while (match.Success)
            {
                string usebase = match.Groups[0].Value;
                if (!string.IsNullOrEmpty(usebase) &&
                    usebase.Length > 3)
                    usebase = usebase.Substring(3);

                UseBases.Add(usebase);
                match = match.NextMatch();
                //切换数据库                
            }

            if (InputSql.ToUpper().IndexOf("SELECT ") > 0)
            {
                throw new Exception("SELECT附近语法有错误");
            }
            else if (InputSql.ToUpper().IndexOf("CREATE ") > 0)
            {
                throw new Exception("CREATE附近语法有错误");
            }
            else if (InputSql.ToUpper().IndexOf("ALTER ") > 0)
            {
                throw new Exception("ALTER附近语法有错误");
            }
            else if (InputSql.ToUpper().IndexOf("INSERT ") > 0)
            {
                throw new Exception("INSERT附近语法有错误");
            }
            else if (InputSql.ToUpper().IndexOf("UPDATE ") > 0)
            {
                throw new Exception("UPDATE附近语法有错误");
            }
            else if (InputSql.ToUpper().IndexOf("DELETE ") > 0)
            {
                throw new Exception("DELETE附近语法有错误");
            }
            else if (InputSql.ToUpper().IndexOf("DROP ") > 0)
            {
                throw new Exception("DROP附近语法有错误");
            }
           
            return UseBases;
        }
    }
}
