﻿using System;
using System.Collections.Generic;
using System.Text;
 
using DevelopAssistant.Service;
using DevelopAssistant.Service.MSQLIntellisense;


namespace DevelopAssistant.Service
{
    public class SqlPromptData
    {
        private readonly Dictionary<string, IList<SqlIntellisenseBoxItem>> columns = new Dictionary<string, IList<SqlIntellisenseBoxItem>>();
        private readonly Dictionary<SqlPromptType, IList<SqlIntellisenseBoxItem>> dbObjects = new Dictionary<SqlPromptType, IList<SqlIntellisenseBoxItem>>();

        public DataBaseServer Db { get; private set; }

        public SqlPromptData(DataBaseServer db)
        {
            this.Db = db;
        }

        internal void AddDbObject(SqlPromptType type, IList<SqlIntellisenseBoxItem> items)
        {
            if (this.dbObjects.ContainsKey(type))
                this.dbObjects.Remove(type);
            this.dbObjects.Add(type, items);
        }

        internal void AddColumns(string owner, IList<SqlIntellisenseBoxItem> cols)
        {
            if (this.columns.ContainsKey(owner))
                this.columns.Remove(owner);
            this.columns.Add(owner, cols);
        }

        public IList<SqlIntellisenseBoxItem> GetColumns(string ownerName)
        {
            List<SqlIntellisenseBoxItem> list = new List<SqlIntellisenseBoxItem>();
            if (this.columns.ContainsKey(ownerName.ToLower()))
            {
                foreach (SqlIntellisenseBoxItem intellisenseBoxItem1 in (IEnumerable<SqlIntellisenseBoxItem>)this.columns[ownerName.ToLower()])
                {
                    SqlIntellisenseBoxItem intellisenseBoxItem2 = new SqlIntellisenseBoxItem(intellisenseBoxItem1.Text, intellisenseBoxItem1.TooltipText, SqlIntellisenseBox.Instance.GetImageIndex(SqlPromptType.Column));
                    list.Add(intellisenseBoxItem2);
                }
            }
            return (IList<SqlIntellisenseBoxItem>)list;
        }

        public IList<SqlIntellisenseBoxItem> GetColumns()
        {
            IList<SqlIntellisenseBoxItem> list = new List<SqlIntellisenseBoxItem>();
            foreach (Table t in Db.Tables)
            {
                IList<SqlIntellisenseBoxItem> _list = GetColumns(t.Name);
                foreach (SqlIntellisenseBoxItem _li in _list)
                {
                    list.Add(_li);
                }
            }
            foreach (View v in Db.Views)
            {
                IList<SqlIntellisenseBoxItem> _list = GetColumns(v.Name);
                foreach (SqlIntellisenseBoxItem _li in _list)
                {
                    list.Add(_li);
                }
            }
            foreach (Function function in Db.Functions)
            {
                SqlIntellisenseBoxItem _li = new SqlIntellisenseBoxItem();
                _li.ImageIndex = 2;
                _li.Text = function.Name;               
                _li.TooltipText = "自定义函数";               
                _li.SqlPromptType = SqlPromptType.UserFunction;
                list.Add(_li);
            }
            return list;
        }

        public IList<Column> GetColumnsOfTable(string table)
        {
            foreach (Table table1 in this.Db.Tables)
            {
                if (string.Compare(table1.Name, table, true) == 0)
                    return (IList<Column>)table1.Columns;
            }
            return (IList<Column>)null;
        }

        public IList<SqlIntellisenseBoxItem> GetColumnItemsOfTables(string tables)
        {
            if (tables.Contains(","))
            {
                string[] tables_array = tables.Split(',');
                return GetColumnItems(tables_array);
            }
            else
            {
                return GetColumnItems(new string[1]{ tables });
            }            
        }

        public IList<SqlIntellisenseBoxItem> GetColumnItems(string[] tables)
        {
            if (tables == null)
                return (IList<SqlIntellisenseBoxItem>)null;
            List<SqlIntellisenseBoxItem> list = new List<SqlIntellisenseBoxItem>();
            foreach (string str in tables)
            {
                if (this.columns.ContainsKey(str.ToLower()))
                {
                    foreach (SqlIntellisenseBoxItem intellisenseBoxItem in (IEnumerable<SqlIntellisenseBoxItem>)this.columns[str.ToLower()])
                    {
                        intellisenseBoxItem.ImageIndex = SqlIntellisenseBox.Instance.GetImageIndex(SqlPromptType.Column);
                        list.Add(intellisenseBoxItem);
                    }
                }
            }
            return (IList<SqlIntellisenseBoxItem>)list;
        }

        public List<SqlIntellisenseBoxItem> GetItems(SqlPromptType pTypes)
        {
            string str1 = ((object)pTypes).ToString();
            if (string.IsNullOrEmpty(str1))
                return (List<SqlIntellisenseBoxItem>)null;
            List<SqlIntellisenseBoxItem> list = new List<SqlIntellisenseBoxItem>();
            string[] strArray = str1.Split(new char[1]
      {
        ','
      });
            SqlPromptType[] sqlPromptTypeArray = new SqlPromptType[strArray.Length];
            int index = 0;
            foreach (string str2 in strArray)
            {
                sqlPromptTypeArray[index] = (SqlPromptType)Enum.Parse(typeof(SqlPromptType), str2);
                ++index;
            }
            foreach (KeyValuePair<SqlPromptType, IList<SqlIntellisenseBoxItem>> keyValuePair in this.dbObjects)
            {
                foreach (SqlPromptType sqlPromptType in sqlPromptTypeArray)
                {
                    if (keyValuePair.Key == sqlPromptType && keyValuePair.Value != null)
                    {
                        foreach (SqlIntellisenseBoxItem intellisenseBoxItem in (IEnumerable<SqlIntellisenseBoxItem>)keyValuePair.Value)
                            list.Add(intellisenseBoxItem);
                    }
                }
            }
            return list;
        }

        public IList<SqlIntellisenseBoxItem> JoinItems(IList<SqlIntellisenseBoxItem> list1, IList<SqlIntellisenseBoxItem> list2)
        {
            IList<SqlIntellisenseBoxItem> list = new List<SqlIntellisenseBoxItem>();
            foreach (SqlIntellisenseBoxItem li1 in list1)
            {
                list.Add(li1);
            }
            foreach (SqlIntellisenseBoxItem li2 in list2)
            {
                list.Add(li2);
            }
            return list;
        }
    }
}
