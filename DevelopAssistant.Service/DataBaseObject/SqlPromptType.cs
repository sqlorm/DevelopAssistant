﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    [Flags]
    public enum SqlPromptType
    {
        None = 0,
        Keyword = 1,
        SysFunction = 2,
        Table = 4,
        Column = 8,
        View = 16,
        Procedure = 32,
        UserFunction = 64,
        Variables = 128,
        Snippet=255
    }
}
