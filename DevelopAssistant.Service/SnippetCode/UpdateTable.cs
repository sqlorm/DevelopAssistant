﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public class UpdateTable : SnippetBase
    {
        public static string ToSnippetCode(string TableName, DataBaseServer DatabaseServer)
        {
            StringBuilder sb = new StringBuilder();

            if (AppSettings.EditorSettings.KeywordsCase)
            {
                sb.Append("UPDATE");
                sb.Append(System.Environment.NewLine);
                sb.Append("  " + getObject(TableName, DatabaseServer.ProviderName));
                sb.Append(System.Environment.NewLine);
                sb.Append("SET ");
                sb.Append(System.Environment.NewLine);
            }
            else
            {
                sb.Append("update");
                sb.Append(System.Environment.NewLine);
                sb.Append("  " + getObject(TableName, DatabaseServer.ProviderName));
                sb.Append(System.Environment.NewLine);
                sb.Append("set ");
                sb.Append(System.Environment.NewLine);
            }


            using (var db = Utility.GetAdohelper(DatabaseServer))
            {
                List<string> pks = new List<string>();

                DataTable dt = db.GetTableObject(TableName);

                int rowIndex = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    string column_name = dr["ColumnName"] + "";
                    string column_isnull = dr["CisNull"] + "";

                    if (column_isnull.Contains("pk"))
                    {
                        pks.Add(column_name);
                    }

                    if (column_isnull.Contains("identity"))
                        continue;

                    if (rowIndex == 0)
                        sb.Append("  " + getObject(column_name, DatabaseServer.ProviderName) + "=@" + column_name + "");
                    else
                        sb.Append("  ," + getObject(column_name, DatabaseServer.ProviderName) + "=@" + column_name + "");

                    sb.Append(System.Environment.NewLine);

                    rowIndex++;
                }

                if (pks != null && pks.Count > 0)
                {
                    if (AppSettings.EditorSettings.KeywordsCase)
                        sb.Append("WHERE ");
                    else
                        sb.Append("where ");

                    for (int i = 0; i < pks.Count; i++)
                    {
                        string pk = pks[i];
                        if (AppSettings.EditorSettings.KeywordsCase)
                        {
                            if (i == 0)
                                sb.Append("" + getObject(pk, DatabaseServer.ProviderName) + "=@" + pk + " ");
                            else
                                sb.Append(" AND " + getObject(pk, DatabaseServer.ProviderName) + "=@" + pk + " ");
                        }
                        else
                        {
                            if (i == 0)
                                sb.Append("" + getObject(pk, DatabaseServer.ProviderName) + "=@" + pk + " ");
                            else
                                sb.Append(" and " + getObject(pk, DatabaseServer.ProviderName) + "=@" + pk + " ");
                        }
                    }
                }
            }

            return sb.ToString();
        }
    }
}
