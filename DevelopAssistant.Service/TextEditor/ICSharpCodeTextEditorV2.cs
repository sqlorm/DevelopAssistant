﻿
 
using DevelopAssistant.Service.Properties;
using DevelopAssistant.Service.TSQLIntellisense;
using ICSharpCode.TextEditor;
using ICSharpCode.TextEditor.Document;
using ICSharpCode.TextEditor.Gui.CompletionWindow;
using ICSharpCode.TextEditor.Gui.InsightWindow;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms; 

namespace DevelopAssistant.Service
{
    public class ICSharpCodeTextEditor2 : TextEditorControl
    {
        ImageList imageList1 = null;
        ICompletionDataProvider completionProvider = null;
        CodeCompletionWindow codeCompletionWindow = null;
        InsightWindow insightWindow = null;

        private bool modified;
        public string SelectedText
        {
            get
            {
                return this.ActiveTextAreaControl.TextArea.SelectionManager.SelectedText;
            }
        }

        public string TextBeforeCaret
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Text))
                    return this.Text.Substring(0, this.ActiveTextAreaControl.TextArea.Caret.Offset);
                else
                    return string.Empty;
            }
        }

        public string TextAfterCaret
        {
            get
            {
                int offset = this.ActiveTextAreaControl.TextArea.Caret.Offset;
                if (this.Text.Length != offset + 1)
                    return this.Text.Substring(offset);
                else
                    return string.Empty;
            }
        }

        public Caret Caret
        {
            get
            {
                return this.ActiveTextAreaControl.TextArea.Caret;
            }
        }

        public TextArea TextArea
        {
            get
            {
                return this.ActiveTextAreaControl.TextArea;
            }
        }

        public bool Modified
        {
            get
            {
                return this.modified;
            }
            set
            {
                this.modified = value;
            }
        }


        Form _ownerForm = null;
        /// <summary>
        /// 承载该控件的Form窗体
        /// </summary>
        public virtual Form OwnerForm
        {
            get
            {
                return _ownerForm;
            }
            set
            {
                _ownerForm = value;
            }
        }

        DevelopAssistant.Service.DataBaseServer _dbserver = null;
        public virtual DevelopAssistant.Service.DataBaseServer DataBaseServer
        {
            get { return _dbserver; }
            set { _dbserver = value; }
        }

        public ICSharpCodeTextEditor2()
        {
            this.InitializeComponent();
            this.InitializeImageList();

            this.ShowVRuler = false;
            this.ShowTabs = false;
            this.ShowSpaces = false;
            this.ShowEOLMarkers = false;
            this.ShowInvalidLines = false;

            this.ActiveTextAreaControl.ScrollBarValueChanged += ActiveTextAreaControl_Scroll;
            this.ActiveTextAreaControl.MouseWheel += new MouseEventHandler(ActiveTextAreaControlMouseWheel);
            this.ActiveTextAreaControl.TextArea.KeyEventHandler += new ICSharpCode.TextEditor.KeyEventHandler(TextArea_KeyPress);
            this.ActiveTextAreaControl.TextArea.DoHandleMousewheel = false;
     
        }

        void InitializeComponent()
        {
            this.SuspendLayout();
            this.textAreaPanel.Size = new Size(476, 360);
            this.Name = "ICSharpCodeTextEditor";
            this.Size = new Size(476, 360);
            this.ResumeLayout(false);
        }

        void InitializeImageList()
        {
            this.imageList1 = new ImageList();

            this.imageList1.Images.Add("allcolumns", Resources.allcolumns);
            this.imageList1.Images.Add("column", Resources.column);           
            this.imageList1.Images.Add("table", Resources.table);
            this.imageList1.Images.Add("func", Resources.func);
            this.imageList1.Images.Add("procs", Resources.procs);
            this.imageList1.Images.Add("snippets", Resources.snippets);
            this.imageList1.Images.Add("sqltext", Resources.sqltext);
            this.imageList1.Images.Add("users", Resources.users);
            
        }

        void ActiveTextAreaControlMouseWheel(object sender, MouseEventArgs e)
        {
            TextAreaControl textAreaControl = (TextAreaControl)sender;
            if (insightWindow != null && !insightWindow.IsDisposed && insightWindow.Visible)
            {
                insightWindow.HandleMouseWheel(e);
            }
            else if (codeCompletionWindow != null && !codeCompletionWindow.IsDisposed && codeCompletionWindow.Visible)
            {
                codeCompletionWindow.HandleMouseWheel(e);
                codeCompletionWindow.Close();          
            }
            else
            {
                textAreaControl.HandleMouseWheel(e);
            }            
        }

        void ActiveTextAreaControl_Scroll(object sender, ScrollEventArgs e)
        {            
            if (codeCompletionWindow != null && !codeCompletionWindow.IsDisposed && codeCompletionWindow.Visible)
            {
                codeCompletionWindow.Close();
            }
        }

        void ShowCompletionWindow(char value)
        {
            completionProvider = new TSQLCompletionDataProvider(_dbserver.ProviderName, this.imageList1);
            codeCompletionWindow = CodeCompletionWindow.ShowCompletionWindow(
                 _ownerForm,                // The parent window for the completion window
                 this, 	     // The text editor to show the window for
                 "",	       	     // Filename - will be passed back to the provider
                 completionProvider,// Provider to get the list of possible completions
                 value		     // Key pressed - will be passed to the provider
            );
            if (this.codeCompletionWindow != null)
            {
                // ShowCompletionWindow can return null when the provider returns an empty list
                this.codeCompletionWindow.Closed += CloseCodeCompletionWindow;
            }
        }

        void CloseCodeCompletionWindow(object sender, EventArgs e)
        {
            //
        }

        bool TextArea_KeyPress(char ch)
        {
            try
            {
                if (AppSettings.EditorSettings.EnableIntellisense
                    && AppSettings.EditorSettings.EnableIntellisense)
                {
                    ShowCompletionWindow(ch);
                }                 
            }
            catch (Exception ex)
            {
                //出错了
            }
            return false;
        }

    }
}
