﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ICSharpCode.TextEditor.Document
{
    public class NewLineSelection : ISelection
    {
        IDocument document;
        bool isCurrent;
        bool isRectangularSelection;
        TextLocation startPosition;
        TextLocation endPosition;

        public TextLocation StartPosition
        {
            get
            {
                return startPosition;
            }
            set
            {
                DefaultDocument.ValidatePosition(document, value);
                startPosition = value;
            }
        }

        public TextLocation EndPosition
        {
            get
            {
                return endPosition;
            }
            set
            {
                DefaultDocument.ValidatePosition(document, value);
                endPosition = value;
            }
        }

        public bool IsCurrent
        {
            get { return isCurrent; }
            internal set { isCurrent = value; }
        }

        public int Offset
        {
            get
            {
                return document.PositionToOffset(startPosition);
            }
        }

        public int EndOffset
        {
            get
            {
                return document.PositionToOffset(endPosition);
            }
        }

        public int Length
        {
            get
            {
                return EndOffset - Offset;
            }
        }

        /// <value>
        /// Returns true, if the selection is empty
        /// </value>
        public bool IsEmpty
        {
            get
            {
                return startPosition == endPosition;
            }
        }

        /// <value>
        /// Returns true, if the selection is rectangular
        /// </value>
        // TODO : make this unused property used.
        public bool IsRectangularSelection
        {
            get
            {
                return isRectangularSelection;
            }
            set
            {
                isRectangularSelection = value;
            }
        }

        /// <value>
        /// The text which is selected by this selection.
        /// </value>
        public string SelectedText
        {
            get
            {
                if (document != null)
                {
                    if (Length < 0)
                    {
                        return null;
                    }
                    return document.GetText(Offset, Length) +
                        System.Environment.NewLine;
                }
                return null;
            }
        }

        /// <summary>
        /// Creates a new instance of <see cref="DefaultSelection"/>
        /// </summary>
        public NewLineSelection(IDocument document, TextLocation startPosition, TextLocation endPosition)
        {
            if(startPosition < endPosition)
            {
                DefaultDocument.ValidatePosition(document, startPosition);
                DefaultDocument.ValidatePosition(document, endPosition);
                Debug.Assert(startPosition <= endPosition);
            }
            this.isCurrent = true;
            this.document = document;
            this.startPosition = startPosition;
            this.endPosition = endPosition;
        }

        /// <summary>
        /// Converts a <see cref="DefaultSelection"/> instance to string (for debug purposes)
        /// </summary>
        public override string ToString()
        {
            return String.Format("[DefaultSelection : StartPosition={0}, EndPosition={1}]", startPosition, endPosition);
        }
        public bool ContainsPosition(TextLocation position)
        {
            if (this.IsEmpty)
                return false;
            return startPosition.Y < position.Y && position.Y < endPosition.Y ||
                startPosition.Y == position.Y && startPosition.X <= position.X && (startPosition.Y != endPosition.Y || position.X <= endPosition.X) ||
                endPosition.Y == position.Y && startPosition.Y != endPosition.Y && position.X <= endPosition.X;
        }

        public bool ContainsOffset(int offset)
        {
            return Offset <= offset && offset <= EndOffset;
        }

        public bool SetSelectIsCurrent(bool iscurrent)
        {
            this.isCurrent = iscurrent;
            return this.isCurrent;
        }

    }
}
