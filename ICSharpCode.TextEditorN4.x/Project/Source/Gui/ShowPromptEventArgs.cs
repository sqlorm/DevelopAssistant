﻿using ICSharpCode.TextEditor.Gui.CompletionWindow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICSharpCode.TextEditor
{
    public class ShowPromptEventArgs : EventArgs
    {
        public string Data { get; set; }

        public ShowPromptEventArgs(string data)
        {
            this.Data = data;
        }
    }

    public delegate ICompletionData[] ShowPromptEventHandler(object sender, ShowPromptEventArgs e);
}
