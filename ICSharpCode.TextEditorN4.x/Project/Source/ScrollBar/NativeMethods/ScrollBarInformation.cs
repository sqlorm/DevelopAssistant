﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICSharpCode.TextEditor
{
    public class ScrollBarInformation
    {
        public const int HorizontalScrollBarHeight = 18;
        public const int VerticalScrollBarWidth = 18;

        public const int HorizontalScrollBarArrowHeight = 18;
        public const int VerticalScrollBarArrowWidth = 18;
    }
}
