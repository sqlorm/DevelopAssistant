﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace ICSharpCode.TextEditor
{
    public class ThemeColorSolver
    {
        public static Color RepairColor(string name, ref Color color)
        {
            string theme = ThemesMapManager.selectedTheme;
            if (theme != "Default")
                color = ThemesMapManager.MatchThemeColor(color, name);
            return color;
        }
    }
}
