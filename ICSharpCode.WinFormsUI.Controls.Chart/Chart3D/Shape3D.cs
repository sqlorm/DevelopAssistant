﻿using ICSharpCode.WinFormsUI.Controls.Chart;
using System;
using System.Collections.Generic;
using System.Drawing;
 
namespace ICSharpCode.WinFormsUI.Controls.Chart3D
{
    internal delegate void delegateEvent(Point3D[] points);

    public class Shape3D : Shape
    {
        protected Camera camera = null;
        protected Coordinate3D coordinate = null;
        protected System.Windows.Forms.Control control = null;

        internal string CursorValueFormat = string.Empty;
        internal delegateEvent Event = null;

        protected float _dashStyleSize = 1;
        public float DashStyleSize
        {
            get { return _dashStyleSize; }
            set
            {
                _dashStyleSize = value;
                DoDashStyleChanged();
            }
        }

        protected Color _dashStyleColor = Color.Black;
        public Color DashStyleColor
        {
            get { return _dashStyleColor; }
            set { _dashStyleColor = value; }
        }

        protected Color _selectedDashStyleColor = Color.OrangeRed;
        public Color SelectedDashStyleColor
        {
            get { return _selectedDashStyleColor; }
            set { _selectedDashStyleColor = value; }
        }

        protected bool _dashStyleIsVisable = false;
        public bool DashStyleIsVisable
        {
            get { return _dashStyleIsVisable; }
            set
            {
                _dashStyleIsVisable = value;
                DoDashStyleChanged();
            }
        }

        public Shape3D()
        {

        }
        internal Shape3D(System.Windows.Forms.Control control, Camera camera, Coordinate3D coordinate)
        {
            this.camera = camera;           
            this.control = control;
            this.coordinate = coordinate;
        }
                
        internal Point3D SelectedPoint3d = null;

        internal bool invalidateable
        {
            get
            {
                return control == null ? false : ((NChart3DControl)control).Invalidateable;
            }
        }

        public virtual void DoDashStyleChanged()
        {
            if (_dashStyleIsVisable)
            {
                coordinate.Xaxis.Padding = Math.Max(_dashStyleSize, coordinate.Xaxis.Padding);
                coordinate.Yaxis.Padding = Math.Max(_dashStyleSize, coordinate.Yaxis.Padding);
                coordinate.Zaxis.Padding = Math.Max(_dashStyleSize, coordinate.Zaxis.Padding);
            }
        }

        public virtual void Maxnum(Point3D[] points)
        {
            if (Event != null)
                Event(points);
        }

        public virtual bool FindNestPoint(Point point, out Point3D result)
        {
            result = null;
            return false;
        }

        public virtual void Draw(Graphics g)
        {

        }

    }
}
