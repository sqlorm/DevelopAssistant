﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls.Chart
{
    public class ChartEventArgs : EventArgs
    {
        public object Tag { get; set; }
        public ICSharpCode.WinFormsUI.Controls.Chart.Shape Shape { get; set; }

        public ChartEventArgs(ICSharpCode.WinFormsUI.Controls.Chart.Shape Shape)
        {
            this.Shape = Shape;
        }

    }
}
