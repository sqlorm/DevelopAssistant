﻿using System;

namespace ICSharpCode.WinFormsUI.Controls.Chart
{
    public delegate void delegateShapeClick(object sender,ChartEventArgs e);

    public class ChartEventHandler 
    {
        internal delegateShapeClick eventHandler = null;
        public ChartEventHandler(delegateShapeClick click)
        {
            this.eventHandler = click;
        }

        public ChartEventHandler(System.Windows.Forms.Control control)
        {
             //
        }

        public static ChartEventHandler operator +(ChartEventHandler handler1, ChartEventHandler handler2)
        {
            if (handler1.eventHandler != null)
            {
                throw new SystemException("There's a \"ChartEventHandler\" event");
            }
            handler1.eventHandler = handler2.eventHandler;
            return handler1;
        }

        public static ChartEventHandler operator +(ChartEventHandler handler1, delegateShapeClick click)
        {
            if (handler1.eventHandler != null)
            {
                throw new SystemException("There's a \"ChartEventHandler\" event");
            }
            handler1.eventHandler = click;
            return handler1;
        }

    }
}
