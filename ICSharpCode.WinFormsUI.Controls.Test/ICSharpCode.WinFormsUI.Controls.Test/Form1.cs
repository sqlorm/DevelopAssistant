﻿using ICSharpCode.WinFormsUI.Controls.Chart;
using ICSharpCode.WinFormsUI.Controls.Chart3D;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls.Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            nChart3DControl1.RotateStyle = RotateStyle.XY;
            nChart3DControl1.ViewPort.Coordinate.Range = new SizeF(160, 200);
            nChart3DControl1.ViewPort.Coordinate.ScaleIsVisable = true;
            //nChart3DControl1.ViewPort.Coordinate.Xaxis.IsVisable = true;
            //nChart3DControl1.ViewPort.Coordinate.Yaxis.IsVisable = true;
            //nChart3DControl1.ViewPort.Coordinate.Zaxis.IsVisable = true;
           
            nChart3DControl1.ShapeClick += new Chart.ChartEventHandler(Shape_Click);
            //nChart3DControl1.ShapeClick += Shape_Click;     

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            nChart3DControl1.BeginUpdate();

            nChart3DControl1.ShowCursorValue = true;
            nChart3DControl1.CursorValueFormat = "{name}: ({0} HZ,{1} m/s^2,{2})";

            SmoothCurve3D shape = null;
            Point3D[] points = null;

            shape = nChart3DControl1.GraphicShapes.AddSmoothCurve3D("图元1", new Shape3D() { LineWidth = 1.5f });
            shape.DashStyleIsVisable = true;
            shape.DashStyleColor = Color.Blue;
            shape.LineColor = Color.Red;

            points = new Point3D[] {
                //new Point3D(0,-10,0),
                //new Point3D(3,-5,0),
                new Point3D(60,-5,0),               
                new Point3D(300,9,0),                         
                new Point3D(600,3,0)
            };
            shape.Points = points;

            //points = new Point3D[] {
            //    //new Point3D(0,-10,0),
            //    //new Point3D(0,-5,0),
            //    new Point3D(-50,20,0),               
            //    new Point3D(0,20,0),                         
            //    new Point3D(10,20,0)
            //};
            //shape.Points = points;

            shape = nChart3DControl1.GraphicShapes.AddSmoothCurve3D("图元2", new Shape3D() { LineWidth = 1.0f });
            shape.DashStyleIsVisable = true;
            shape.DashStyleColor = Color.Yellow;
            shape.LineColor = Color.Yellow;
            points = new Point3D[] {
                new Point3D(0,0,0),
                new Point3D(8,9,0),
                new Point3D(36,5,0),   
                new Point3D(50,0,0),               
                new Point3D(504,10,0),              
                new Point3D(600,6,0)           
            };
            shape.Points = points;


            shape = nChart3DControl1.GraphicShapes.AddSmoothCurve3D("图元3", new Shape3D() { LineWidth = 1.0f });
            shape.DashStyleIsVisable = true;
            shape.DashStyleColor = Color.Black;
            shape.DashStyleSize = 3.0F;
            shape.LineColor = Color.Orange;
            points = new Point3D[] {
                new Point3D(0,0,8),
                new Point3D(8,9,8),
                new Point3D(36,5,8),   
                new Point3D(50,0,8),               
                new Point3D(504,10,8),              
                new Point3D(600,6,8)           
            };
            shape.Points = points;

            shape = nChart3DControl1.GraphicShapes.AddSmoothCurve3D("图元4", new Shape3D() { LineWidth = 1.0f });
            shape.DashStyleIsVisable = true;
            shape.DashStyleColor = Color.Blue;
            shape.LineColor = Color.Blue;
            points = new Point3D[] {
                new Point3D(0,0,-8),
                new Point3D(8,9,-8),
                new Point3D(36,5,-8),       
                new Point3D(50,0,-8),               
                new Point3D(504,10,-8),              
                new Point3D(600,6,-8)           
            };
            shape.Points = points;


            shape = nChart3DControl1.GraphicShapes.AddSmoothCurve3D("图元5", new Shape3D() { LineWidth = 1.0f });
            shape.DashStyleIsVisable = true;
            shape.DashStyleColor = Color.Red;
            shape.DashStyleSize = 3.0F;
            shape.LineColor = Color.Red;
            points = new Point3D[] {
                new Point3D(0,0,4),
                new Point3D(8,9,4),
                new Point3D(36,5,4),       
                new Point3D(50,0,4),               
                new Point3D(504,10,4),              
                new Point3D(600,6,4)           
            };
            shape.Points = points;


            shape = nChart3DControl1.GraphicShapes.AddSmoothCurve3D("图元6", new Shape3D() { LineWidth = 1.0f });
            shape.DashStyleIsVisable = true;
            shape.DashStyleColor = Color.Red;
            shape.DashStyleSize = 3.0F;
            shape.LineColor = Color.Red;
            points = new Point3D[] {
                new Point3D(0,0,2),
                new Point3D(8,9,2),
                new Point3D(36,5,2),       
                new Point3D(50,0,2),               
                new Point3D(504,10,2),              
                new Point3D(600,6,2)           
            };
            shape.Points = points;

            shape = nChart3DControl1.GraphicShapes.AddSmoothCurve3D("图元7", new Shape3D() { LineWidth = 1.0f });
            shape.DashStyleIsVisable = true;
            shape.DashStyleColor = Color.Red;
            shape.DashStyleSize = 3.0F;
            shape.LineColor = Color.Red;
            points = new Point3D[] {
                new Point3D(0,0,-2),
                new Point3D(8,9,-2),
                new Point3D(36,5,-2),       
                new Point3D(50,0,-2),               
                new Point3D(504,10,-2),              
                new Point3D(600,6,-2)           
            };
            shape.Points = points;

            nChart3DControl1.ViewPort.Coordinate.Xaxis.Unit = "HZ";
            nChart3DControl1.ViewPort.Coordinate.Yaxis.Unit = "m/s^2";
            nChart3DControl1.ViewPort.Coordinate.Zaxis.Unit = "时间";

            nChart3DControl1.EndUpdate();

        }

        private void Shape_Click(object sender, ChartEventArgs e)
        {
            var tag = e.Tag;
            MessageBox.Show(e.Shape.Name);
        }
         
    }
}
