﻿using ICSharpCode.WinFormsUI.Controls.Chart;
using ICSharpCode.WinFormsUI.Controls.Chart3D;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls.Test
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            nChart3DControl1.RotateStyle = RotateStyle.XY;
            nChart3DControl1.ViewPort.Coordinate.FillingFace = true;
            nChart3DControl1.ViewPort.Coordinate.ScaleIsVisable = true;

            nChart3DControl1.ShapeClick += new Chart.ChartEventHandler(Shape_Click);

            nChart3DControl1.ShowCursorValue = false;
            nChart3DControl1.CursorValueFormat = "{name}: ({0} HZ,{1} m/s^2,{2})";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            nChart3DControl1.BeginUpdate();

            ColumnBar3D shape = null;           

            shape = nChart3DControl1.GraphicShapes.AddColumnBar3D("图元1", new Shape3D() { LineWidth = 1.0f });
            shape.LineColor = Color.Gray;
            shape.Location = new Point3D(0, 92, 80);

            shape = nChart3DControl1.GraphicShapes.AddColumnBar3D("图元2", new Shape3D() { LineWidth = 1.0f });
            shape.LineColor = Color.Gray;
            shape.Location = new Point3D(20, 40, 120);

            shape = nChart3DControl1.GraphicShapes.AddColumnBar3D("图元3", new Shape3D() { LineWidth = 1.0f });
            shape.LineColor = Color.Gray;
            shape.Location = new Point3D(50, 10, 16);

            shape = nChart3DControl1.GraphicShapes.AddColumnBar3D("图元4", new Shape3D() { LineWidth = 1.0f });
            shape.LineColor = Color.Gray;
            shape.Location = new Point3D(80, 60, 10);


            shape = nChart3DControl1.GraphicShapes.AddColumnBar3D("图元5", new Shape3D() { LineWidth = 1.0f });
            shape.LineColor = Color.Gray;
            shape.FillColor = Color.Red;
            shape.Location = new Point3D(90, 120, 0);


            shape = nChart3DControl1.GraphicShapes.AddColumnBar3D("图元6", new Shape3D() { LineWidth = 1.0f });
            shape.LineColor = Color.Gray;
            shape.FillColor = Color.Green;
            shape.Location = new Point3D(120, 120, 0);


            nChart3DControl1.EndUpdate();
        }

        private void Shape_Click(object sender, ChartEventArgs e)
        {
            MessageBox.Show(e.Shape.Name);
        }

    }
}
