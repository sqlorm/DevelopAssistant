using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Drawing.Design;

namespace ICSharpCode.WinFormsUI.Controls
{
    [DefaultEvent("Click")]
    [ToolboxBitmap(typeof(Image), "ICSharpCode.WinFormsUI.Controls.CrystalButton.png")]
    public partial class CrystalButton : UserControl
    {
        #region private define

        int index = 0;
        int count = 10;

        Bitmap IconDisabled;
        bool leaveIsHappened = false;

        private bool normalToDisabled = false;
        private bool disabledToNormal = false;

        private bool areAnimationsEnabled = false;

        #endregion

        #region Constructor
        public CrystalButton()
        {
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            this.BackColor = Color.Transparent;
            this.ForeColor = Color.FromArgb(69, 105, 152);

            InitializeComponent();

            ButtonText = "Cloud button";
            iconSpacingX = 5;
            iconSpacingY = 5;

            textSpacingX = 5;
            textSpacingY = 5;

            #region MouseDown colors
            mouseDownColors = new Color[7];
            mouseDownColors[0] = Color.FromArgb(208, 193, 135);
            mouseDownColors[1] = Color.FromArgb(217, 208, 171);
            mouseDownColors[2] = Color.FromArgb(234, 220, 167);
            mouseDownColors[3] = Color.FromArgb(247, 239, 205);
            mouseDownColors[4] = Color.FromArgb(247, 215, 112);
            mouseDownColors[5] = Color.FromArgb(247, 200, 49);
            mouseDownColors[6] = Color.FromArgb(247, 221, 132);
            #endregion

            #region MouseOn colors
            mouseOnColors = new Color[7];
            mouseOnColors[0] = Color.FromArgb(191, 168, 113);
            mouseOnColors[1] = Color.FromArgb(208, 185, 129);
            mouseOnColors[2] = Color.FromArgb(243, 231, 182);
            mouseOnColors[3] = Color.FromArgb(255, 248, 217);
            mouseOnColors[4] = Color.FromArgb(255, 226, 133);
            mouseOnColors[5] = Color.FromArgb(255, 213, 77);
            mouseOnColors[6] = Color.FromArgb(255, 232, 151);
            #endregion

            #region Normal colors
            normalColors = new Color[7];
            normalColors[0] = Color.FromArgb(161, 189, 207);
            normalColors[1] = Color.FromArgb(202, 214, 212);
            normalColors[2] = Color.FromArgb(188, 208, 221);
            normalColors[3] = Color.FromArgb(246, 249, 253);
            normalColors[4] = Color.FromArgb(229, 238, 249);
            normalColors[5] = Color.FromArgb(210, 225, 244);
            normalColors[6] = Color.FromArgb(233, 242, 253);
            #endregion

            #region Disabled colors
            disabledColors = new Color[7];
            disabledColors[0] = Color.FromArgb(212, 212, 212);
            disabledColors[1] = Color.FromArgb(239, 239, 239);
            disabledColors[2] = Color.FromArgb(224, 224, 224);
            disabledColors[3] = Color.FromArgb(251, 251, 251);
            disabledColors[4] = Color.FromArgb(244, 244, 244);
            disabledColors[5] = Color.FromArgb(235, 235, 235);
            disabledColors[6] = Color.FromArgb(236, 236, 236);
            #endregion
        }

        #endregion

        #region Default button colors

        private Color borderLineColor = Color.FromArgb(161, 189, 207);
        private Color borderPointColor = Color.FromArgb(202, 214, 212);
        private Color borderPoint2Color = Color.FromArgb(188, 208, 221);
        private Color upperGradientColor1 = Color.FromArgb(246, 249, 253);
        private Color upperGradientColor2 = Color.FromArgb(229, 238, 249);
        private Color lowerGradientColor1 = Color.FromArgb(210, 225, 244);
        private Color lowerGradientColor2 = Color.FromArgb(233, 242, 253);

        #endregion

        #region Tools

        private Pen borderPen;
        private Brush gradientBrush;

        #endregion

        #region Drawing the button
        protected override void OnPaint(PaintEventArgs e)
        {
            // Sometimes when button is disabled in design-time, it doesn't work
            // so let's put this codeblock in here. Due to be removed in future

            //try
            //{
                if (this.Enabled == false)
                {
                    borderLineColor = disabledColors[0];
                    borderPointColor = disabledColors[1];
                    borderPoint2Color = disabledColors[2];
                    upperGradientColor1 = disabledColors[3];
                    upperGradientColor2 = disabledColors[4];
                    lowerGradientColor1 = disabledColors[5];
                    lowerGradientColor2 = disabledColors[6];
                }
            //}
            //catch
            //{
            //}

            #region Long borderlines

            borderPen = new Pen(borderLineColor);

            Point point1 = new Point(0, 2);
            Point point2 = new Point(0, this.Height - 3);

            e.Graphics.DrawLine(borderPen, point1, point2);

            point1 = new Point(this.Width - 1, 2);
            point2 = new Point(this.Width - 1, this.Height - 3);

            e.Graphics.DrawLine(borderPen, point1, point2);

            point1 = new Point(2, 0);
            point2 = new Point(this.Width - 3, 0);

            e.Graphics.DrawLine(borderPen, point1, point2);

            point1 = new Point(2, this.Height - 1);
            point2 = new Point(this.Width - 3, this.Height - 1);

            e.Graphics.DrawLine(borderPen, point1, point2);
            #endregion

            #region Border points

            borderPen = new Pen(borderPointColor);

            e.Graphics.DrawEllipse(borderPen, 1, 0, 1, 1);
            e.Graphics.DrawEllipse(borderPen, 0, 1, 1, 1);

            e.Graphics.DrawEllipse(borderPen, 1, this.Height - 1, 1, 1);
            e.Graphics.DrawEllipse(borderPen, 0, this.Height - 2, 1, 1);

            e.Graphics.DrawEllipse(borderPen, this.Width - 2, 0, 1, 1);
            e.Graphics.DrawEllipse(borderPen, this.Width - 1, 1, 1, 1);

            e.Graphics.DrawEllipse(borderPen, this.Width - 2, this.Height - 1, 1, 1);
            e.Graphics.DrawEllipse(borderPen, this.Width - 1, this.Height - 2, 1, 1);

            borderPen = new Pen(borderPoint2Color);

            e.Graphics.DrawEllipse(borderPen, 1, 1, 1, 1);
            e.Graphics.DrawEllipse(borderPen, 1, this.Height - 2, 1, 1);
            e.Graphics.DrawEllipse(borderPen, this.Width - 2, 1, 1, 1);
            e.Graphics.DrawEllipse(borderPen, this.Width - 2, this.Height - 2, 1, 1);

            #endregion

            #region Gradients

            // Upper gradient
            PointF upperGradientPoint1 = new PointF(1, 1);
            PointF upperGradientPoint2 = new PointF(1, this.Height / 2.4F);

            gradientBrush = new LinearGradientBrush(upperGradientPoint1, upperGradientPoint2, upperGradientColor1, upperGradientColor2);
            e.Graphics.FillRectangle(gradientBrush, upperGradientPoint1.X, upperGradientPoint1.Y, this.Width - 2, upperGradientPoint2.Y);


            // Lower gradient
            PointF lowerGradientPoint1 = new PointF(upperGradientPoint2.X, upperGradientPoint2.Y);
            PointF lowerGradientPoint2 = new PointF(upperGradientPoint2.X, this.Height - 1);

            gradientBrush = new LinearGradientBrush(lowerGradientPoint1, lowerGradientPoint2, lowerGradientColor1, lowerGradientColor2);
            e.Graphics.FillRectangle(gradientBrush, lowerGradientPoint1.X, lowerGradientPoint1.Y, this.Width - 2, this.Height - upperGradientPoint2.Y - 1);

            #endregion

            if (textImageRelation == ButtonTextImageRelation.TextAboveImage)
            {
                drawIcon(e.Graphics);
                drawText(e.Graphics);
            }

            else
            {
                drawText(e.Graphics);
                drawIcon(e.Graphics);
            }
        }

        PointF textPoint;
        Brush textBrush;

        private FontStyle usedFontStyle = FontStyle.Regular;

        [Browsable(true)]
        [Description("What style should the font be in the button")]
        [DefaultValue(FontStyle.Regular)]
        public FontStyle FontsStyle
        {
            get
            {
                return this.usedFontStyle;
            }

            set
            {
                this.usedFontStyle = value;
            }
        }

        private int fontSize = 12;

        [Browsable(true)]
        [Description("Font's size on the button")]
        [DefaultValue(12)]
        public int FontSize
        {
            get
            {
                return this.fontSize;
            }

            set
            {
                this.fontSize = value;
            }
        }

        private float iconTransparency;

        [Browsable(true)]
        [Description("Icon's transparency on the button (0f - 1f)")]
        [DefaultValue(1f)]
        public float IconTransparency
        {
            get
            {
                return this.iconTransparency;
            }

            set
            {
                // Check if the value is between 0 and 1f
                if (value > 0f && value < 1f)
                {
                    this.iconTransparency = value;
                    try
                    {
                        transparentIcon = MakeTransparentImage((Image)icon, this.iconTransparency);
                        transparentDisabledIcon = MakeTransparentImage((Image)IconDisabled, this.iconTransparency);
                    }
                    catch
                    {
                        // Not yet ready or something?
                    }
                }
            }
        }        

        private void drawText(Graphics graphics)
        {
            Font font = new Font("Segoe UI", fontSize, usedFontStyle, GraphicsUnit.Pixel);

            if (this.Enabled == true)
            {
                textBrush = new SolidBrush(this.ForeColor);
            }

            else
            {
                textBrush = new SolidBrush(Color.DarkGray);
            }

            switch (textAlign)
            {
                case ButtonTextAlign.Left:
                    textPoint = new PointF(textSpacingX, this.Height / 2 - graphics.MeasureString(text, font).Height / 2);
                    break;

                case ButtonTextAlign.Right:
                    textPoint = new PointF(this.Width - graphics.MeasureString(text, font).Width - textSpacingX, this.Height / 2 - graphics.MeasureString(text, font).Height / 2);
                    break;

                case ButtonTextAlign.Up:
                    textPoint = new PointF(this.Width / 2 - graphics.MeasureString(text, font).Width / 2, textSpacingY);
                    break;

                case ButtonTextAlign.Down:
                    textPoint = new PointF(this.Width / 2 - graphics.MeasureString(text, font).Width / 2, this.Height - graphics.MeasureString(text, font).Height - textSpacingY);
                    break;

                case ButtonTextAlign.UpperLeft:
                    textPoint = new PointF(textSpacingX, textSpacingY);
                    break;

                case ButtonTextAlign.UpperRight:
                    textPoint = new PointF(this.Width - graphics.MeasureString(text, font).Width - textSpacingX, textSpacingY);
                    break;

                case ButtonTextAlign.BottomLeft:
                    textPoint = new PointF(textSpacingX, this.Height - graphics.MeasureString(text, font).Height - textSpacingY);
                    break;

                case ButtonTextAlign.BottomRight:
                    textPoint = new PointF(this.Width - graphics.MeasureString(text, font).Width - textSpacingX, this.Height - graphics.MeasureString(text, font).Height - textSpacingY);
                    break;

                case ButtonTextAlign.Center:
                    textPoint = new PointF(this.Width / 2 - graphics.MeasureString(text, font).Width / 2, this.Height / 2 - graphics.MeasureString(text, font).Height / 2);
                    break;
            }

            if (text != null)
            {
                graphics.DrawString(text, font, textBrush, textPoint);
            }
        }

        private PointF imagePoint;

        private void drawIcon(Graphics graphics)
        {
            try
            {
                switch (iconAlign)
                {
                    case IconBitmapAlign.Left:
                        imagePoint = new PointF(iconSpacingX, this.Height / 2 - icon.Height / 2);
                        break;

                    case IconBitmapAlign.Right:
                        imagePoint = new PointF(this.Width - icon.Width - iconSpacingX, this.Height / 2 - icon.Height / 2);
                        break;

                    case IconBitmapAlign.Up:
                        imagePoint = new PointF(this.Width / 2 - icon.Width / 2, iconSpacingY);
                        break;

                    case IconBitmapAlign.Down:
                        imagePoint = new PointF(this.Width / 2 - icon.Width / 2, this.Height - iconSpacingY - icon.Height);
                        break;

                    case IconBitmapAlign.UpperLeft:
                        imagePoint = new PointF(iconSpacingX, iconSpacingY);
                        break;

                    case IconBitmapAlign.UpperRight:
                        imagePoint = new PointF(this.Width - icon.Width - iconSpacingX, iconSpacingY);
                        break;

                    case IconBitmapAlign.BottomLeft:
                        imagePoint = new PointF(iconSpacingX, this.Height - icon.Height - iconSpacingY);
                        break;

                    case IconBitmapAlign.BottomRight:
                        imagePoint = new PointF(this.Width - icon.Width - iconSpacingX, this.Height - icon.Height - iconSpacingY);
                        break;

                    case IconBitmapAlign.Center:
                        imagePoint = new PointF((this.Width / 2) - (icon.Width / 2), (this.Height / 2) - (icon.Height / 2));
                        //MessageBox.Show("" + (this.Width / 2));
                        break;
                }
                if (icon != null)
                {
                    if (this.Enabled == true)
                    {
                        // If icon's transparency is 0 or greater, not 1
                        if (iconTransparency != 1)
                        {
                            graphics.DrawImage(transparentIcon, imagePoint.X, imagePoint.Y, icon.Width, icon.Height);
                        }
                        else
                        {
                            graphics.DrawImage(icon, imagePoint.X, imagePoint.Y, icon.Width, icon.Height);
                        }

                    }
                    else
                    {
                        if (iconTransparency != 1)
                        {
                            graphics.DrawImage(transparentDisabledIcon, imagePoint.X, imagePoint.Y, icon.Width, icon.Height);
                        }
                        else
                        {
                            graphics.DrawImage(IconDisabled, imagePoint.X, imagePoint.Y, icon.Width, icon.Height);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        #endregion

        #region Button properties

        #region Button's text

        // Text of the button
        private string text;

        [Browsable(true)]
        [Category("Text")]
        public string ButtonText
        {
            get
            {
                return this.text;
            }

            set
            {
                this.text = value;

                // Paint the control to show the modified text
                this.Invalidate();
            }
        }

        public enum ButtonTextAlign
        {
            Left = 0,
            Right = 1,
            Up = 2,
            Down = 3,
            UpperLeft = 4,
            UpperRight = 5,
            BottomLeft = 6,
            BottomRight = 7,
            Center = 8
        }

        private ButtonTextAlign textAlign = ButtonTextAlign.Center;

        [Browsable(true)]
        [DefaultValue(ButtonTextAlign.Center)]
        public ButtonTextAlign TextAlign
        {
            get
            {
                return this.textAlign;
            }

            set
            {
                this.textAlign = value;
                this.Invalidate();
            }
        }

        int textSpacingX;

        [Browsable(true)]
        public int TextSpacingX
        {
            get
            {
                return this.textSpacingX;
            }

            set
            {
                this.textSpacingX = value;
                this.Invalidate();
            }
        }

        int textSpacingY;

        [Browsable(true)]
        public int TextSpacingY
        {
            get
            {
                return this.textSpacingY;
            }

            set
            {
                this.textSpacingY = value;
                this.Invalidate();
            }
        }

        #endregion

        #region Button's icon

        Bitmap icon;
        Bitmap transparentIcon;
        Bitmap transparentDisabledIcon;

        [Browsable(true)]
        public Bitmap Icon
        {
            get
            {
                return this.icon;
            }

            set
            {
                this.icon = value;
                this.Invalidate();
                generateDisabledIcon();

                // Generate a new transparent icon for normal and disabled states
                try
                {
                    transparentIcon = MakeTransparentImage(icon, iconTransparency);
                    transparentDisabledIcon = MakeTransparentImage(IconDisabled, this.iconTransparency);
                }
                catch
                {
                    // Not yet ready or something?
                }
            }
        }

        public enum IconBitmapAlign
        {
            Left = 0,
            Right = 1,
            Up = 2,
            Down = 3,
            UpperLeft = 4,
            UpperRight = 5,
            BottomLeft = 6,
            BottomRight = 7,
            Center = 8
        }

        private IconBitmapAlign iconAlign = IconBitmapAlign.Left;

        [Browsable(true)]
        public IconBitmapAlign IconAlign
        {
            get
            {
                return this.iconAlign;
            }

            set
            {
                this.iconAlign = value;
                this.Invalidate();
            }
        }

        int iconSpacingX;
        int iconSpacingY;

        //[Browsable(true)]

        //[Description("Icon spacing"), Editor(typeof(SpacingEditor), typeof(UITypeEditor))]
        [Browsable(true)]
        public int IconSpacingX
        {
            get
            {
                return this.iconSpacingX;
            }

            set
            {

                this.iconSpacingX = value;
                this.Invalidate();
            }
        }

        [Browsable(true)]
        public int IconSpacingY
        {
            get
            {
                return this.iconSpacingY;
            }

            set
            {
                this.iconSpacingY = value;
                this.Invalidate();
            }
        }

        #endregion

        #region UseVisualStyleBackColor

        private bool _useVisualStyleBackColor = false;
        public bool UseVisualStyleBackColor
        {
            get { return _useVisualStyleBackColor; }
            set { _useVisualStyleBackColor = value; }
        }

        #endregion

        #region TextImageRelation

        public enum ButtonTextImageRelation
        {
            ImageAboveText = 0,
            TextAboveImage = 1
        }

        private ButtonTextImageRelation textImageRelation = ButtonTextImageRelation.TextAboveImage;

        [Browsable(true)]
        public ButtonTextImageRelation TextImageRelation
        {
            get
            {
                return this.textImageRelation;
            }

            set
            {
                this.textImageRelation = value;
                this.Invalidate();
            }
        }
        #endregion

        #endregion

        #region Define Colors

        #region Arrays
        private Color[] mouseOnColors;

        [Browsable(true)]
        public Color[] MouseOn_Colors
        {
            get
            {
                return this.mouseOnColors;
            }

            set
            {
                this.mouseOnColors = value;
                // Because the colors don't update when they are changed
                // it's possible to do that by calling CloudButtonOwn_MouseLeave event
                CloudButtonOwn_MouseLeave(this, null);
                this.Invalidate();
            }
        }

        private Color[] mouseDownColors;

        [Browsable(true)]
        public Color[] MouseDown_Colors
        {
            get
            {
                return this.mouseDownColors;
            }

            set
            {
                this.mouseDownColors = value;
                // Because the colors don't update when they are changed
                // it's possible to do that by calling CloudButtonOwn_MouseLeave event
                CloudButtonOwn_MouseLeave(this, null);
                this.Invalidate();
            }
        }

        private Color[] normalColors;

        [Browsable(true)]
        public Color[] Normal_Colors
        {
            get
            {
                return this.normalColors;
            }

            set
            {
                this.normalColors = value;
                // Because the colors don't update when they are changed
                // it's possible to do that by calling CloudButtonOwn_MouseLeave event
                CloudButtonOwn_MouseLeave(this, null);
                this.Invalidate();
            }
        }

        private Color[] disabledColors;

        [Browsable(true)]
        public Color[] DisabledColors
        {
            get
            {
                return this.disabledColors;
            }

            set
            {
                this.disabledColors = value;
                this.Invalidate();
            }
        }

        #endregion


        bool normalToMouseEnter = false;
        bool mouseDownToMouseEnter = false;

        private void CloudButtonOwn_MouseEnter(object sender, EventArgs e)
        {
            // If we want a smooth border, default colors are assigned
            //if (smoothBorder == true)
            //{
            //    borderLineColor = Color.FromArgb(222, 208, 156);
            //    borderPointColor = Color.FromArgb(228, 220, 186);
            //}

            // ...otherwise we use more sharp color in MouseOn mode
            //else
            //{
            //}

            // If the button is disabled, it doesn't look at mouse events

            if (this.Enabled == true)
            {
                if (areAnimationsEnabled == false)
                {
                    borderLineColor = Color.FromArgb(191, 168, 113);
                    borderPointColor = Color.FromArgb(208, 185, 129);
                    borderPoint2Color = Color.FromArgb(243, 231, 182);
                    upperGradientColor1 = Color.FromArgb(255, 248, 217);
                    upperGradientColor2 = Color.FromArgb(255, 226, 133);
                    lowerGradientColor1 = Color.FromArgb(255, 213, 77);
                    lowerGradientColor2 = Color.FromArgb(255, 232, 151);
                }

                else
                {
                    normalToMouseEnter = true;
                    timerFade.Start();
                }

                this.Invalidate();
                //    }
                //    else
                //    {
                //        CloudButtonOwn_MouseLeave(this, null);
                //this.Invalidate();
                //    }
            }
        }

        public void CMouseLeave()
        {
            if (areAnimationsEnabled == false)
                {
                    borderLineColor = normalColors[0];
                    borderPointColor = normalColors[1];
                    borderPoint2Color = normalColors[2];
                    upperGradientColor1 = normalColors[3];
                    upperGradientColor2 = normalColors[4];
                    lowerGradientColor1 = normalColors[5];
                    lowerGradientColor2 = normalColors[6];
                }

                else
                {
                    normalToMouseEnter = false;
                    timerFade.Start();
                }
        }

        private void CloudButtonOwn_MouseLeave(object sender, EventArgs e)
        {
            //borderLineColor = Color.FromArgb(161, 189, 207);
            //borderPointColor = Color.FromArgb(202, 214, 212);
            //borderPoint2Color = Color.FromArgb(188, 208, 221);
            //upperGradientColor1 = Color.FromArgb(246, 249, 253);
            //upperGradientColor2 = Color.FromArgb(229, 238, 249);
            //lowerGradientColor1 = Color.FromArgb(210, 225, 244);
            //lowerGradientColor2 = Color.FromArgb(233, 242, 253);

            borderLineColor = normalColors[0];
            borderPointColor = normalColors[1];
            borderPoint2Color = normalColors[2];
            upperGradientColor1 = normalColors[3];
            upperGradientColor2 = normalColors[4];
            lowerGradientColor1 = normalColors[5];
            lowerGradientColor2 = normalColors[6];

            if (isButtonSelected == false)
            {
                if (areAnimationsEnabled == false)
                {
                    borderLineColor = normalColors[0];
                    borderPointColor = normalColors[1];
                    borderPoint2Color = normalColors[2];
                    upperGradientColor1 = normalColors[3];
                    upperGradientColor2 = normalColors[4];
                    lowerGradientColor1 = normalColors[5];
                    lowerGradientColor2 = normalColors[6];
                }

                else
                {
                    normalToMouseEnter = false;
                    timerFade.Start();
                }
            }

            else
            {
            }


            this.Invalidate();
        }

        //bool isMouseClickedWhileOperation = false;

        bool isButtonSelected = false;

        private void CloudButtonOwn_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                borderLineColor = Color.FromArgb(208, 193, 135);
                borderPointColor = Color.FromArgb(217, 208, 171);
                borderPoint2Color = Color.FromArgb(234, 220, 167);
                upperGradientColor1 = Color.FromArgb(247, 239, 205);
                upperGradientColor2 = Color.FromArgb(247, 215, 112);
                lowerGradientColor1 = Color.FromArgb(247, 200, 49);
                lowerGradientColor2 = Color.FromArgb(247, 221, 132);

                //borderLineColor = mouseDownColors[0];
                //borderPointColor = mouseDownColors[1];
                //borderPoint2Color = mouseDownColors[2];
                //upperGradientColor1 = mouseDownColors[3];
                //upperGradientColor2 = mouseDownColors[4];
                //lowerGradientColor1 = mouseDownColors[5];
                //lowerGradientColor2 = mouseDownColors[6];

                // If there is some process going on
                //if (timerFade.Enabled == true && mouseOnToMouseClick == false)
                //{
                //    //isMouseClickedWhileOperation = true;
                //}

                //else
                //{
                //    //borderLineColor = mouseDownColors[0];
                //    //borderPointColor = mouseDownColors[1];
                //    //borderPoint2Color = mouseDownColors[2];
                //    //upperGradientColor1 = mouseDownColors[3];
                //    //upperGradientColor2 = mouseDownColors[4];
                //    //lowerGradientColor1 = mouseDownColors[5];
                //    //lowerGradientColor2 = mouseDownColors[6];
                //}

                isButtonSelected = true;

                this.Invalidate();
            }
        }

        private void CloudButtonOwn_MouseUp(object sender, MouseEventArgs e)
        {
            this.CloudButtonOwn_MouseEnter(this, null);
            timerFade.Start();
            mouseDownToMouseEnter = true;
        }

        #endregion

        private void CloudButtonOwn_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.Capture == true)
            {

            }
        }

        private void timerFade_Tick(object sender, EventArgs e)
        {
            if (normalToMouseEnter == true)
            {
                if (index <= count)
                {
                    borderLineColor = getReadyColor(mouseOnColors[0], normalColors[0]);
                    borderPointColor = getReadyColor(mouseOnColors[1], normalColors[1]);
                    borderPoint2Color = getReadyColor(mouseOnColors[2], normalColors[2]);
                    upperGradientColor1 = getReadyColor(mouseOnColors[3], normalColors[3]);
                    upperGradientColor2 = getReadyColor(mouseOnColors[4], normalColors[4]);
                    lowerGradientColor1 = getReadyColor(mouseOnColors[5], normalColors[5]);
                    lowerGradientColor2 = getReadyColor(mouseOnColors[6], normalColors[6]);

                    index++;
                }

                else
                {
                    if (leaveIsHappened == true)
                    {
                        setNormalColors();
                        leaveIsHappened = false;
                    }
                    timerFade.Stop();
                    index = 0;
                    normalToMouseEnter = false;
                }
            }

            else
            {
                if (index <= count)
                {
                    borderLineColor = getReadyColor(normalColors[0], mouseOnColors[0]);
                    borderPointColor = getReadyColor(normalColors[1], mouseOnColors[1]);
                    borderPoint2Color = getReadyColor(normalColors[2], mouseOnColors[2]);
                    upperGradientColor1 = getReadyColor(normalColors[3], mouseOnColors[3]);
                    upperGradientColor2 = getReadyColor(normalColors[4], mouseOnColors[4]);
                    lowerGradientColor1 = getReadyColor(normalColors[5], mouseOnColors[5]);
                    lowerGradientColor2 = getReadyColor(normalColors[6], mouseOnColors[6]);

                    index++;
                }

                else
                {
                    if (leaveIsHappened == true)
                    {
                        setNormalColors();
                        leaveIsHappened = false;
                    }
                    timerFade.Stop();
                    index = 0;
                }
            }

            if (mouseDownToMouseEnter == true)
            {
                if (index <= count)
                {
                    borderLineColor = getReadyColor(mouseDownColors[0], mouseOnColors[0]);
                    borderPointColor = getReadyColor(mouseDownColors[1], mouseOnColors[1]);
                    borderPoint2Color = getReadyColor(mouseDownColors[2], mouseOnColors[2]);
                    upperGradientColor1 = getReadyColor(mouseDownColors[3], mouseOnColors[3]);
                    upperGradientColor2 = getReadyColor(mouseDownColors[4], mouseOnColors[4]);
                    lowerGradientColor1 = getReadyColor(mouseDownColors[5], mouseOnColors[5]);
                    lowerGradientColor2 = getReadyColor(mouseDownColors[6], mouseOnColors[6]);

                    index++;
                }

                else
                {
                    if (leaveIsHappened == true)
                    {
                        setNormalColors();
                        leaveIsHappened = false;
                    }
                    timerFade.Stop();
                    index = 0;
                    mouseDownToMouseEnter = false;

                    //// If mouse clicked while operation
                    //if (isMouseClickedWhileOperation == true)
                    //{
                    //    borderLineColor = mouseDownColors[0];
                    //    borderPointColor = mouseDownColors[1];
                    //    borderPoint2Color = mouseDownColors[2];
                    //    upperGradientColor1 = mouseDownColors[3];
                    //    upperGradientColor2 = mouseDownColors[4];
                    //    lowerGradientColor1 = mouseDownColors[5];
                    //    lowerGradientColor2 = mouseDownColors[6];
                    //}
                }
            }

            if (normalToDisabled == true)
            {
                if (index <= count)
                {
                    borderLineColor = getReadyColor(normalColors[0], disabledColors[0]);
                    borderPointColor = getReadyColor(normalColors[1], disabledColors[1]);
                    borderPoint2Color = getReadyColor(normalColors[2], disabledColors[2]);
                    upperGradientColor1 = getReadyColor(normalColors[3], disabledColors[3]);
                    upperGradientColor2 = getReadyColor(normalColors[4], disabledColors[4]);
                    lowerGradientColor1 = getReadyColor(normalColors[5], disabledColors[5]);
                    lowerGradientColor2 = getReadyColor(normalColors[6], disabledColors[6]);

                    index++;
                }

                else
                {
                    if (leaveIsHappened == true)
                    {
                        setNormalColors();
                        leaveIsHappened = false;
                    }
                    timerFade.Stop();
                    index = 0;
                    normalToDisabled = false;
                    count = 10;
                    //// If mouse clicked while operation
                    //if (isMouseClickedWhileOperation == true)
                    //{
                    //    borderLineColor = mouseDownColors[0];
                    //    borderPointColor = mouseDownColors[1];
                    //    borderPoint2Color = mouseDownColors[2];
                    //    upperGradientColor1 = mouseDownColors[3];
                    //    upperGradientColor2 = mouseDownColors[4];
                    //    lowerGradientColor1 = mouseDownColors[5];
                    //    lowerGradientColor2 = mouseDownColors[6];
                    //}
                }
            }

            if (disabledToNormal == true)
            {
                if (index <= count)
                {
                    borderLineColor = getReadyColor(disabledColors[0], normalColors[0]);
                    borderPointColor = getReadyColor(disabledColors[1], normalColors[1]);
                    borderPoint2Color = getReadyColor(disabledColors[2], normalColors[2]);
                    upperGradientColor1 = getReadyColor(disabledColors[3], normalColors[3]);
                    upperGradientColor2 = getReadyColor(disabledColors[4], normalColors[4]);
                    lowerGradientColor1 = getReadyColor(disabledColors[5], normalColors[5]);
                    lowerGradientColor2 = getReadyColor(disabledColors[6], normalColors[6]);

                    index++;
                }

                else
                {
                    if (leaveIsHappened == true)
                    {
                        setNormalColors();
                        leaveIsHappened = false;
                    }
                    timerFade.Stop();
                    index = 0;
                    disabledToNormal = false;
                    count = 10;
                    // If mouse clicked while operation
                    //if (isMouseClickedWhileOperation == true)
                    //{
                    //    borderLineColor = mouseDownColors[0];
                    //    borderPointColor = mouseDownColors[1];
                    //    borderPoint2Color = mouseDownColors[2];
                    //    upperGradientColor1 = mouseDownColors[3];
                    //    upperGradientColor2 = mouseDownColors[4];
                    //    lowerGradientColor1 = mouseDownColors[5];
                    //    lowerGradientColor2 = mouseDownColors[6];
                    //}
                }
            }

            this.Invalidate();
        }

        private void setNormalColors()
        {
            borderLineColor = normalColors[0];
            borderPointColor = normalColors[1];
            borderPoint2Color = normalColors[2];
            upperGradientColor1 = normalColors[3];
            upperGradientColor2 = normalColors[4];
            lowerGradientColor1 = normalColors[5];
            lowerGradientColor2 = normalColors[6];

            this.Invalidate();
        }

        private Color getReadyColor(Color color1, Color color2)
        {
            //Color colorFirst = Color.FromArgb(color1.R * index, color1.G * index, color1.B * index);
            //Color colorSecond = Color.FromArgb(color2.R * 100 - index, color2.G * 100 - index, color2.B * 100 - index);

            int FCR = color1.R * index;
            int FCG = color1.G * index;
            int FCB = color1.B * index;
            int SCR = color2.R * (count - index);
            int SCG = color2.G * (count - index);
            int SCB = color2.B * (count - index);

            int readyR = (FCR + SCR) / count;
            int readyG = (FCG + SCG) / count;
            int readyB = (FCB + SCB) / count;

            //if (readyR > 255)
            //{
            //    readyR = 255;
            //}

            //if (readyR < 0)
            //{
            //    readyR = 0;
            //}

            //if (readyG > 255)
            //{
            //    readyG = 255;
            //}

            //if (readyG < 0)
            //{
            //    readyG = 0;
            //}

            //if (readyB > 255)
            //{
            //    readyB = 255;
            //}

            //if (readyB < 0)
            //{
            //    readyB = 0;
            //}

            Color readyColor = Color.FromArgb(readyR, readyG, readyB);

            return readyColor;
        }

        private void CloudButton_EnabledChanged(object sender, EventArgs e)
        {
            if (this.Enabled == true)
            {
                //borderLineColor = disabledColors[0];
                //borderPointColor = disabledColors[1];
                //borderPoint2Color = disabledColors[2];
                //upperGradientColor1 = disabledColors[3];
                //upperGradientColor2 = disabledColors[4];
                //lowerGradientColor1 = disabledColors[5];
                //lowerGradientColor2 = disabledColors[6];

                //index = 0;
                count = 30;
                normalToDisabled = true;
                timerFade.Start();
            }

            else
            {
                count = 30;
                disabledToNormal = true;
                timerFade.Start();
                //borderLineColor = normalColors[0];
                //borderPointColor = normalColors[1];
                //borderPoint2Color = normalColors[2];
                //upperGradientColor1 = normalColors[3];
                //upperGradientColor2 = normalColors[4];
                //lowerGradientColor1 = normalColors[5];
                //lowerGradientColor2 = normalColors[6];
            }
        }

        private void generateDisabledIcon()
        {
            try
            {
                if (icon != null)
                {
                    IconDisabled = new Bitmap(icon.Width, icon.Height);
                    Graphics graphics = Graphics.FromImage(IconDisabled);

                    for (int i = 0; i < icon.Width; i++)
                    {
                        for (int j = 0; j < icon.Height; j++)
                        {
                            if (icon.GetPixel(i, j).A == 0)
                            {
                                Pen pointpen = new Pen(Color.FromArgb(0, 255, 255, 255));

                                graphics.DrawEllipse(pointpen, i, j, 1, 1);
                            }
                            else
                            {
                                int a = icon.GetPixel(i, j).A; 
                                int rgb = (icon.GetPixel(i, j).R + icon.GetPixel(i, j).G + icon.GetPixel(i, j).B) / 3;                          
                                IconDisabled.SetPixel(i, j, Color.FromArgb(a, rgb, rgb, rgb));
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private static Bitmap MakeTransparentImage(Image image, float alpha)
        {

            if (image == null)
                throw new ArgumentNullException("image");

            if (alpha < 0.0f || alpha > 1.0f)
                throw new ArgumentOutOfRangeException("alpha", "Must be between 0.0 and 1.0.");

            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.Matrix33 = 1.0f - alpha;
            Bitmap bmp = new Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb);

            using (ImageAttributes attrs = new ImageAttributes())
            {
                attrs.SetColorMatrix(colorMatrix);
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.DrawImage(image, new Rectangle(Point.Empty, new Size(image.Width, image.Height)), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, attrs);
                }
            }
            return bmp;

        }

        private void CloudButton_Leave(object sender, EventArgs e)
        {
            leaveIsHappened = true;

            borderLineColor = normalColors[0];
            borderPointColor = normalColors[1];
            borderPoint2Color = normalColors[2];
            upperGradientColor1 = normalColors[3];
            upperGradientColor2 = normalColors[4];
            lowerGradientColor1 = normalColors[5];
            lowerGradientColor2 = normalColors[6];
            //isButtonSelected = false;

            this.Invalidate();
        }
    }
}