﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
     
    public class NImageButton : System.Windows.Forms.Label
    {
        protected bool MouseHit = false;

        System.Windows.Forms.ToolTip tip = new System.Windows.Forms.ToolTip();

        private string _tooltiptext;
        public string ToolTipText
        {
            get { return _tooltiptext; }
            set
            {
                _tooltiptext = value;
                if (!string.IsNullOrEmpty(_tooltiptext))
                {                    
                    tip.SetToolTip(this, _tooltiptext);
                }
            }
        }

        public NImageButton()
        {
            this.AutoSize = false;
            this.Cursor = System.Windows.Forms.Cursors.Hand;            
        }

        public override bool AutoSize
        {
            get
            {
                return false;
            }            
        }
         
        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            MouseHit = true;
            this.Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            MouseHit = false;
            this.Invalidate();
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs pe)
        {
            base.OnPaint(pe);
            if (MouseHit)
            {
                System.Drawing.Graphics graphic = pe.Graphics;
                ControlPaint.DrawBorder(graphic, ClientRectangle,
             SystemColors.MenuText, 1, ButtonBorderStyle.Solid, //左边
             SystemColors.MenuText, 1, ButtonBorderStyle.Solid, //上边
             SystemColors.MenuText, 1, ButtonBorderStyle.Solid, //右边
             SystemColors.MenuText, 1, ButtonBorderStyle.Solid);//底边
            }           
        }

    }
}
