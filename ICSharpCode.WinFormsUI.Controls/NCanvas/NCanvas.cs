﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms; 

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NCanvas : System.Windows.Forms.Control, ISupportInitialize
    {
        int _leftborderwidth = 1;
        int _topborderwidth = 1;
        int _rightborderwidth = 1;
        int _bottomborderwidth = 1;

        int _offsetX = 0;
        int _offsetY = 0;

        bool _mousedown = false;

        protected Color _borderColor = SystemColors.ControlDark;
        protected Rectangle DrawRect = Rectangle.Empty;
        protected Rectangle TorgetRect = Rectangle.Empty;
        protected Rectangle SourceRect = Rectangle.Empty;

        protected SaveFileDialog filedialog = new SaveFileDialog();

        private bool _drag = false;
        public bool Drag
        {
            get { return _drag; }
            set { _drag = value; }
        }

        protected Image _bitmap;
        private Image _image;
        public Image Image
        {
            get { return _image; }
            set
            {
                _image = value;
                if (_image != null)                    
                {
                    //_bitmap = _image.Clone(new Rectangle(new Point(0, 0), _image.Size), System.Drawing.Imaging.PixelFormat.Alpha);
                    _bitmap = (Image)_image.Clone();
                    TorgetRect = new Rectangle(0, 0, _image.Width, _image.Height); 
                    SourceRect = new Rectangle(0, 0, _image.Width, _image.Height);
                    DrawRect = new Rectangle(0, 0, _image.Width, _image.Height);
                    this.Invalidate();
                }
            }
        }

        private BorderStyle _borderStyle;
        public BorderStyle BorderStyle
        {
            get { return _borderStyle; }
            set
            {
                _borderStyle = value;
                this.Invalidate();
            }
        }

        private ToolStripMenuItem item1;
        private ToolStripMenuItem item2;
        private ToolStripMenuItem item3;
        private ContextMenuStrip _contextMenuStrip;       

        public NCanvas()
        {
            Init();
            #region 解决重绘时屏幕闪烁

            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            #endregion
        }

        public void Init()
        {
            filedialog.Filter = "*.png|*.png";

            item1 = new ToolStripMenuItem() { Text = "拖动" };
            item1.Click += ToolStripMenuItem_Click;
            item2 = new ToolStripMenuItem() { Text = "复制" };
            item2.Click += ToolStripMenuItem_Click;
            item3 = new ToolStripMenuItem() { Text = "保存" };
            item3.Click += ToolStripMenuItem_Click;
            _contextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            _contextMenuStrip.Items.AddRange(new ToolStripMenuItem[] { 
                 item1,
                 item2,
                 item3
            });
            this.ContextMenuStrip = _contextMenuStrip;
        }

        private void ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            switch (item.Text)
            {
                case "拖动":

                    SetDrag();

                    break;
                case "复制":

                    CopyToClipBoard();

                    break;
                case "保存":

                    SaveToFile();

                    break;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.Clear(this.BackColor);

            if (_image != null)
                //g.DrawImage(this._image, TorgetRect, SourceRect, GraphicsUnit.Pixel);
                g.DrawImage(this._image, DrawRect);
            
            if (this._borderStyle != BorderStyle.None)
            {
                ControlPaint.DrawBorder(e.Graphics, ClientRectangle,
            _borderColor, _leftborderwidth, ButtonBorderStyle.Solid, //左边
            _borderColor, _topborderwidth, ButtonBorderStyle.Solid, //上边
            _borderColor, _rightborderwidth, ButtonBorderStyle.Solid, //右边
            _borderColor, _bottomborderwidth, ButtonBorderStyle.Solid);//底边
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (this._drag && e.Button == MouseButtons.Left)
            {
                DrawRect.X = e.X - _offsetX;
                DrawRect.Y = e.Y - _offsetY;               
                this.Invalidate();
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left)
                this._mousedown = true;
            if (this._drag && this._mousedown)
                this.Cursor = Cursors.SizeAll;

            this._offsetX = e.X - DrawRect.X;
            this._offsetY = e.Y - DrawRect.Y;
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            this._mousedown = false;
            this.Cursor = Cursors.Default;             
        }

        public void BeginInit()
        {

        }

        public void EndInit()
        {

        }

        public void SetDrag()
        {
            if (item1.Checked)
                item1.Checked = false;
            else
                item1.Checked = true;

            if (item1.Checked)
                this.Drag = true;
            else
                this.Drag = false;
        }

        public void SaveToFile()
        {
            if (filedialog.ShowDialog().Equals(DialogResult.OK))
            {
                if (!string.IsNullOrEmpty(filedialog.FileName))
                {
                    SaveToFile(filedialog.FileName);
                }
            }
        }

        public void CopyToClipBoard()
        {
            Clipboard.Clear();
            Clipboard.SetDataObject(this._image, false);
        }
        
        public void SaveToFile(string savePath)
        {
            if (this._image != null)//&& System.IO.File.Exists(savePath)
                this._image.Save(savePath, ImageFormat.Png);
            MessageBox.Show("保存成功");
        }
                
    }
}
