﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class ItemCollection : List<object>
    {
        public NComboBox comboBox = null;

        public ItemCollection(NComboBox ComboBox)
        {
            this.comboBox = ComboBox;
        }

        public new void Add(object Item)
        {
            comboBox.ListBox.Items.Add(Item);
            base.Add(Item);
        }

        public new void AddRange(IEnumerable<object> Items)
        {
            comboBox.ListBox.Items.AddRange(Items);
            base.AddRange(Items);
        }

        public new void Clear()
        {
            comboBox.ListBox.Items.Clear();
            base.Clear();
        }

        public new void Remove(object Item)
        {
            comboBox.ListBox.Items.Remove(Item);
            base.Remove(Item);
        }

        public new void RemoveAt(int Index)
        {
            comboBox.ListBox.Items.RemoveAt(Index);
            base.RemoveAt(Index);
        }
    }
}
