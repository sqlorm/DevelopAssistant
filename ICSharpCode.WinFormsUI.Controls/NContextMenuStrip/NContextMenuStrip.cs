﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NContextMenuStrip : System.Windows.Forms.ContextMenuStrip
    {
        public NContextMenuStrip()
        {
            //this.Font = NToolStripItemTheme.TextFont;
            this.ImageScalingSize = new Size(20, 20);
        }

        public NContextMenuStrip(System.ComponentModel.IContainer container) : base(container)
        {
            //this.Font = NToolStripItemTheme.TextFont;
            this.ImageScalingSize = new Size(20, 20);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.FillRectangle(new SolidBrush(NToolStripItemTheme.BackColor), this.ClientRectangle);
            g.FillRectangle(new SolidBrush(NToolStripItemTheme.PressedColor), new Rectangle(0, 0, 28, this.Height));

            foreach (ToolStripItem item in this.Items)
            {
                var type = item.GetType();
                NToolStripPaintUtils.DrawItem(g, item, ContainerType.NContextMenu);
            }

            g.DrawRectangle(new Pen(SystemColors.ControlDarkDark), new Rectangle(0, 0, Width - 1, Height - 1));
        }
    }
}
