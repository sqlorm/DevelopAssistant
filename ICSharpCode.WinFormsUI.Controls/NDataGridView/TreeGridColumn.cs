﻿// Type: ICSharpCode.WinFormsUI.Controls.TreeGridColumn
// Assembly: ExpandableGridView, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 01AF7F03-14C8-403C-9988-D5C2B7E87D00
// Assembly location: C:\Users\lenovo\Desktop\TreeGridViewTest\TreeGridViewTest\lib\ExpandableGridView.dll

using System.Drawing;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class TreeGridColumn : DataGridViewTextBoxColumn
    {
        internal Image _defaultNodeImage;

        public Image DefaultNodeImage
        {
            get
            {
                return this._defaultNodeImage;
            }
            set
            {
                this._defaultNodeImage = value;
            }
        }

        public TreeGridColumn()
        {
            this.CellTemplate = (DataGridViewCell)new TreeGridCell(); //(DataGridViewCell)new TreeGridCell();
        }

        public override object Clone()
        {
            TreeGridColumn treeGridColumn = (TreeGridColumn)base.Clone();
            treeGridColumn._defaultNodeImage = this._defaultNodeImage;
            return (object)treeGridColumn;
        }
    }
}
