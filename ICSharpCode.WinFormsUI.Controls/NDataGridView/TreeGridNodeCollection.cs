﻿// Type: ICSharpCode.WinFormsUI.Controls.TreeGridNodeCollection
// Assembly: ExpandableGridView, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 01AF7F03-14C8-403C-9988-D5C2B7E87D00
// Assembly location: C:\Users\lenovo\Desktop\TreeGridViewTest\TreeGridViewTest\lib\ExpandableGridView.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class TreeGridNodeCollection : IList<TreeGridNode>, ICollection<TreeGridNode>, IEnumerable<TreeGridNode>, IList, ICollection, IEnumerable
    {
        internal List<TreeGridNode> _list;
        internal TreeGridNode _owner;

        public TreeGridNode this[int index]
        {
            get
            {
                return this._list[index];
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public int Count
        {
            get
            {
                return this._list.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        bool IList.IsReadOnly
        {
            get
            {
                return this.IsReadOnly;
            }
        }

        bool IList.IsFixedSize
        {
            get
            {
                return false;
            }
        }

        int ICollection.Count
        {
            get
            {
                return this.Count;
            }
        }

        bool ICollection.IsSynchronized
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        object ICollection.SyncRoot
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        internal TreeGridNodeCollection(TreeGridNode owner)
        {
            this._owner = owner;
            this._list = new List<TreeGridNode>();
        }

        public void Add(TreeGridNode item)
        {
            item._grid = this._owner._grid;
            bool hasChildren = this._owner.HasChildren;
            item._owner = this;
            this._list.Add(item);
            this._owner.AddChildNode(item);
            if (hasChildren || !this._owner.IsSited)
                return;
            this._owner._grid.InvalidateRow(this._owner.RowIndex);
        }

        public TreeGridNode Add(string text)
        {
            TreeGridNode treeGridNode = new TreeGridNode();
            this.Add(treeGridNode);
            treeGridNode.Cells[0].Value = (object)text;
            return treeGridNode;
        }

        public TreeGridNode Add(params object[] values)
        {
            TreeGridNode treeGridNode = new TreeGridNode();
            this.Add(treeGridNode);
            int index = 0;
            if (values.Length > treeGridNode.Cells.Count)
                throw new ArgumentOutOfRangeException("values");
            foreach (object obj in values)
            {
                treeGridNode.Cells[index].Value = obj;
                ++index;
            }
            return treeGridNode;
        }

        public TreeGridNode AddNode(object Id, params object[] values)
        {
            TreeGridNode treeGridNode = new TreeGridNode();
            treeGridNode.Id = Id;
            this.Add(treeGridNode);
            int index = 0;
            if (values.Length > treeGridNode.Cells.Count)
                throw new ArgumentOutOfRangeException("values");
            foreach (object obj in values)
            {
                treeGridNode.Cells[index].Value = obj;
                ++index;
            }           
            return treeGridNode;
        }

        public TreeGridNode AddNode(object Id, bool Checkbox, params object[] values)
        {
            TreeGridNode treeGridNode = new TreeGridNode();            
            treeGridNode.Id = Id;
            treeGridNode.Checkbox = Checkbox;
            this.Add(treeGridNode);
            int index = 0;
            if (values.Length > treeGridNode.Cells.Count)
                throw new ArgumentOutOfRangeException("values");
            foreach (object obj in values)
            {
                treeGridNode.Cells[index].Value = obj;
                ++index;
            }
            return treeGridNode;
        }

        public void Insert(int index, TreeGridNode item)
        {
            item._grid = this._owner._grid;
            item._owner = this;
            this._list.Insert(index, item);
            this._owner.InsertChildNode(index, item);
        }

        public bool Remove(TreeGridNode item)
        {
            this._owner.RemoveChildNode(item);
            item._grid = (NTreeGridView)null;
            return this._list.Remove(item);
        }

        public void RemoveAt(int index)
        {
            TreeGridNode node = this._list[index];
            this._owner.RemoveChildNode(node);
            node._grid = (NTreeGridView)null;
            this._list.RemoveAt(index);
        }

        public void Clear()
        {
            this._owner.ClearNodes();
            this._list.Clear();
        }

        public int IndexOf(TreeGridNode item)
        {
            return this._list.IndexOf(item);
        }

        public bool Contains(TreeGridNode item)
        {
            return this._list.Contains(item);
        }

        public void CopyTo(TreeGridNode[] array, int arrayIndex)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void IList.Remove(object value)
        {
            this.Remove(value as TreeGridNode);
        }

        int IList.Add(object value)
        {
            TreeGridNode treeGridNode = value as TreeGridNode;
            this.Add(treeGridNode);
            return treeGridNode.Index;
        }

        void IList.RemoveAt(int index)
        {
            this.RemoveAt(index);
        }

        void IList.Clear()
        {
            this.Clear();
        }

        int IList.IndexOf(object item)
        {
            return this.IndexOf(item as TreeGridNode);
        }

        void IList.Insert(int index, object value)
        {
            this.Insert(index, value as TreeGridNode);
        }

        bool IList.Contains(object value)
        {
            return this.Contains(value as TreeGridNode);
        }

        void ICollection.CopyTo(Array array, int index)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        object IList.this[int index]
        {
            get
            {
                return (object)this[index];
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public IEnumerator<TreeGridNode> GetEnumerator()
        {
            return (IEnumerator<TreeGridNode>)this._list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)this.GetEnumerator();
        }
    }
}
