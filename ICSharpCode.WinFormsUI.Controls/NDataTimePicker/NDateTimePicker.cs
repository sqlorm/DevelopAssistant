﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NDateTimePicker : DateTimePicker
    {
        private Color _backDisabledColor;

        public NDateTimePicker() : base()
        {
            this.SetStyle(ControlStyles.UserPaint, true);            
            _backDisabledColor = Color.FromKnownColor(KnownColor.Control);
        }

        /// <summary>
        /// Gets or sets the background color of the control
        /// </summary>
        [Browsable(true)]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set { base.BackColor = value; }
        }

        /// <summary>
        /// Gets or sets the text color of the control
        /// </summary>
        [Browsable(true)]
        public override Color ForeColor
        {
            get { return base.ForeColor; }
            set { base.ForeColor = value; }
        }

        /// <summary>
        ///     Gets or sets the background color of the control when disabled
        /// </summary>
        [Category("Appearance"), Description("The background color of the component when disabled")]
        [Browsable(true)]
        public Color BackDisabledColor
        {
            get { return _backDisabledColor; }
            set { _backDisabledColor = value; }
        }

        protected override Size DefaultSize
        {
            get
            {
                return new Size(Width, 24);
            }
        }

        public override bool AutoSize
        {
            get { return base.AutoSize; }
            set { base.AutoSize = value; }
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            Graphics g = this.CreateGraphics();
            //Graphics g = e.Graphics;

            //The dropDownRectangle defines position and size of dropdownbutton block, 
            //the width is fixed to 17 and height to 16. The dropdownbutton is aligned to right
            Rectangle dropDownRectangle = new Rectangle(ClientRectangle.Width - 20, 0, 20, Height);

            Brush bkgBrush;
            Brush frtBrush;
            ComboBoxState visualState;

            //When the control is enabled the brush is set to Backcolor, 
            //otherwise to color stored in _backDisabledColor
            if (this.Enabled)
            {
                frtBrush = new SolidBrush(this.ForeColor);
                bkgBrush = new SolidBrush(this.BackColor);
                visualState = ComboBoxState.Normal;
            }
            else
            {
                frtBrush = new SolidBrush(this.ForeColor);
                bkgBrush = new SolidBrush(this._backDisabledColor);
                visualState = ComboBoxState.Disabled;
            }

            // Painting...in action

            //Filling the background
            g.FillRectangle(bkgBrush, 0, 0, ClientRectangle.Width, ClientRectangle.Height);

            //Drawing the datetime text
            //g.DrawString(this.Text, this.Font, frtBrush, 0, 2);
            TextRenderer.DrawText(g, this.Text, this.Font, new Rectangle(0, 0, Width - 22, Height), ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.WordBreak);

            //Drawing the dropdownbutton using ComboBoxRenderer
            ComboBoxRenderer.DrawDropDownButton(g, dropDownRectangle, visualState);

            ControlPaint.DrawBorder(g, this.ClientRectangle, SystemColors.ControlDark, ButtonBorderStyle.Solid);

            g.Dispose();

            bkgBrush.Dispose();

        }

    }
}
