﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NDownListItemArray : IEnumerator
    {
        private NDownListItem[] _items;
        private int position;

        public object Current
        {
            get
            {
                return (object)this._items[this.position];
            }
        }

        public NDownListItemArray(NDownListItem[] list)
        {
            this.position = -1;
            this._items = list;
        }

        public bool MoveNext()
        {
            ++this.position;
            return this.position < this._items.Length;
        }

        public void Reset()
        {
            this.position = -1;
        }
    }
}
