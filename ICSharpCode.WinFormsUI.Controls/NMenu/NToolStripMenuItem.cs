﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NToolStripMenuItem : ToolStripMenuItem
    {
        private int iconSize = 16;

        public string ItemType { get; set; }
        public object AddIn { get; set; }
        public int Index { get; set; }
        public int Level { get; set; }
        public string Position { get; set; }       

        public NToolStripMenuItem()
        {
            ItemType = "ToolStripMenuItem";
            this.Font = NToolStripItemTheme.TextFont;
        }

        public NToolStripMenuItem(string Type)
        {
            ItemType = Type;
            this.Font = NToolStripItemTheme.TextFont;
        }

        protected override ToolStripDropDown CreateDefaultDropDown()
        {
            return new NToolStripDropDownMenu();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);

            Brush textBru = new SolidBrush(NToolStripItemTheme.TextColor);
            Rectangle bound = new Rectangle(0, 0, Width, Height);

            if (this.Pressed)
            {
                textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                e.Graphics.FillRectangle(new SolidBrush(NToolStripItemTheme.PressedColor), bound);
            }
            else
            {
                if (this.Selected)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                    e.Graphics.FillRectangle(new SolidBrush(NToolStripItemTheme.HighlightBackColor), bound);
                }
            }

            int pos = 0;

            if (this.Image != null)
            {
                e.Graphics.DrawImage(this.Image, new Rectangle(pos + 6, bound.Y + (this.Height - iconSize) / 2 + 1, iconSize, iconSize),
                        new Rectangle(0, 0, this.Image.Width, this.Image.Height), GraphicsUnit.Pixel);
            }

            pos = pos + 2 * iconSize + 8;

            if (!string.IsNullOrEmpty(this.Text) && (this.DisplayStyle == ToolStripItemDisplayStyle.Text || this.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
            {
                string itemText = this.Text;
                Font itemFont = new Font(this.Font, FontStyle.Regular);
                if (this.Text.StartsWith("&"))
                {
                    itemText = this.Text.Substring(1);                   
                    itemFont = new Font(this.Font, FontStyle.Regular);
                }

                if (this.DisplayStyle == ToolStripItemDisplayStyle.Text || this.Image == null)
                {
                    e.Graphics.DrawString(itemText, itemFont, textBru, new Rectangle(pos + 2, 0, this.Width - 4, this.Height), new StringFormat() { LineAlignment = StringAlignment.Center });
                }
                else
                {
                    e.Graphics.DrawString(itemText, itemFont, textBru, new Rectangle(pos, 0, this.Width - iconSize - 4, this.Height), new StringFormat() { LineAlignment = StringAlignment.Center });
                }

            }

            if (this.Pressed)
            {
                e.Graphics.DrawRectangle(new Pen(SystemColors.ControlDarkDark), new Rectangle(0, 0, Width - 1, Height));
            }

        }

    }

    public class NToolStripMainMenuItem : ToolStripMenuItem
    {
        private int iconSize = 16;

        public string ItemType { get; set; }
        public object AddIn { get; set; }
        public int Index { get; set; }
        public int Level { get; set; }
        public string Position { get; set; }

        public NToolStripMainMenuItem()
        {
            ItemType = "ToolStripMenuItem";
            this.Font = NToolStripItemTheme.TextFont;
        }

        protected override ToolStripDropDown CreateDefaultDropDown()
        {
            return new NToolStripDropDownMenu();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);

            Brush textBru = new SolidBrush(NToolStripItemTheme.TextColor);
            Rectangle bound = new Rectangle(0, 0, Width, Height);

            if (this.Pressed)
            {
                textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                e.Graphics.FillRectangle(new SolidBrush(NToolStripItemTheme.PressedColor), bound);
            }
            else
            {
                if (this.Selected)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                    e.Graphics.FillRectangle(new SolidBrush(NToolStripItemTheme.HighlightBackColor), bound);
                }
            }

            int pos = 0;
            if (this.Image != null && (this.DisplayStyle == ToolStripItemDisplayStyle.Image || this.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
            {
                e.Graphics.DrawImage(this.Image, new Rectangle(pos + 2, (this.Height - iconSize) / 2 + 1, iconSize, iconSize),
                    new Rectangle(0, 0, this.Image.Width, this.Image.Height), GraphicsUnit.Pixel);
                pos = pos + iconSize + 4;
            }

            if (!string.IsNullOrEmpty(this.Text) && (this.DisplayStyle == ToolStripItemDisplayStyle.Text || this.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
            {
                string itemText = this.Text;
                Font itemFont = new Font(this.Font, FontStyle.Regular);
                if (this.Text.StartsWith("&"))
                {
                    itemText = this.Text.Substring(1);                    
                    itemFont = new Font(this.Font, FontStyle.Regular);
                }

                if (this.DisplayStyle == ToolStripItemDisplayStyle.Text || this.Image == null)
                {
                    e.Graphics.DrawString(itemText, itemFont, textBru, new Rectangle(pos + 2, 0, this.Width - 4, this.Height), new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });
                }
                else
                {
                    e.Graphics.DrawString(itemText, itemFont, textBru, new Rectangle(pos, 0, this.Width - iconSize - 4, this.Height), new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });
                }

            }

            if (this.Pressed)
            {
                e.Graphics.DrawRectangle(new Pen(SystemColors.ControlDarkDark), new Rectangle(0, 0, Width - 1, Height));
            }

        }
    }

}
