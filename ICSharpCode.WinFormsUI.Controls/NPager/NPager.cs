﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    using XResource = ICSharpCode.WinFormsUI.Controls.Properties.Resources;

    /// <summary>
    /// 申明委托
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    
    /// <summary>
    /// 分页控件呈现
    /// </summary>
    public partial class NPager : UserControl
    {
        private bool _disposed = false;
        private bool _bindable = false;

        private System.ComponentModel.IContainer components;
        private ToolStripButton btnFirst;
        private ToolStripButton btnPrev;
        private ToolStripButton btnNext;
        private ToolStripButton btnLast;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripLabel toolStripLabel4;
        private NToolStripTextBox txtCurrentPage;
        private ToolStripLabel toolStripLabel1;
        private ToolStripButton btnGo;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripLabel toolStripLabel2;
        private ToolStripLabel lblPageCount;
        private ToolStripLabel toolStripLabel3;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripLabel toolStripLabel6;
        private ToolStripLabel lblTotalRecord;
        private ToolStripLabel toolStripLabel5;
        private ToolStripLabel toolStripLabel7;
        private ToolStripLabel toolStripLabel9;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripButton btnRefresh;
        private ToolStripComboBox lblPageSize;
        private ToolStripLabel lblRemailTime;
        private ToolStripSeparator toolStripSeparator5;

        public BindingSource bindingSource;
        public NBindingNavigator bindingNavigator;

        private void InitializeBindingNavigator()
        {
            PageCount = 0;
            btnPrev.Enabled = false;
            btnFirst.Enabled = false;
        }
    
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bindingNavigator = new NBindingNavigator(this.components);
            this.btnFirst = new System.Windows.Forms.ToolStripButton();
            this.btnPrev = new System.Windows.Forms.ToolStripButton();
            this.btnNext = new System.Windows.Forms.ToolStripButton();
            this.btnLast = new System.Windows.Forms.ToolStripButton();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.txtCurrentPage = new NToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.btnGo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.lblPageCount = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel7 = new System.Windows.Forms.ToolStripLabel();
            this.lblPageSize = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel9 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel6 = new System.Windows.Forms.ToolStripLabel();
            this.lblTotalRecord = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.lblRemailTime = new System.Windows.Forms.ToolStripLabel();
            this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.bindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = null;
            this.bindingNavigator.BackColor = System.Drawing.SystemColors.Control;
            this.bindingNavigator.CountItem = null;
            this.bindingNavigator.DeleteItem = null;
            this.bindingNavigator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnFirst,
            this.btnPrev,
            this.btnNext,
            this.btnLast,
            this.btnRefresh,
            this.toolStripSeparator2,
            this.toolStripLabel4,
            this.txtCurrentPage,
            this.toolStripLabel1,
            this.btnGo,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.lblPageCount,
            this.toolStripLabel3,
            this.toolStripSeparator4,
            this.toolStripLabel7,
            this.lblPageSize,
            this.toolStripLabel9,
            this.toolStripSeparator1,
            this.toolStripLabel6,
            this.lblTotalRecord,
            this.toolStripLabel5,
            this.toolStripSeparator5,
            this.lblRemailTime});
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = null;
            this.bindingNavigator.MoveLastItem = null;
            this.bindingNavigator.MoveNextItem = null;
            this.bindingNavigator.MovePreviousItem = null;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = null;
            this.bindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bindingNavigator.Size = new System.Drawing.Size(908, 30);
            this.bindingNavigator.Stretch = true;
            this.bindingNavigator.TabIndex = 0;
            this.bindingNavigator.Text = " ";
            // 
            // btnFirst
            // 
            this.btnFirst.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.pagination_icons_01;
            this.btnFirst.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(64, 27);
            this.btnFirst.Text = "第一页";
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            this.btnFirst.MouseEnter += new System.EventHandler(this.btnFirst_MouseEnter);
            // 
            // btnPrev
            // 
            this.btnPrev.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.pagination_icons_02;
            this.btnPrev.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(64, 27);
            this.btnPrev.Text = "上一页";
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            this.btnPrev.MouseEnter += new System.EventHandler(this.btnPrev_MouseEnter);
            // 
            // btnNext
            // 
            this.btnNext.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.pagination_icons_03;
            this.btnNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(64, 27);
            this.btnNext.Text = "下一页";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            this.btnNext.MouseEnter += new System.EventHandler(this.btnNext_MouseEnter);
            // 
            // btnLast
            // 
            this.btnLast.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.pagination_icons_04;
            this.btnLast.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(64, 27);
            this.btnLast.Text = "最后页";
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            this.btnLast.MouseEnter += new System.EventHandler(this.btnLast_MouseEnter);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.pagination_icons_05;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(52, 27);
            this.btnRefresh.Text = "刷新";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(20, 27);
            this.toolStripLabel4.Text = "第";
            // 
            // txtCurrentPage
            // 
            this.txtCurrentPage.Name = "txtCurrentPage";
            this.txtCurrentPage.Size = new System.Drawing.Size(40, 30);
            this.txtCurrentPage.Text = "1";
            this.txtCurrentPage.Leave += new System.EventHandler(this.txtCurrentPage_Leave);
            this.txtCurrentPage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCurrentPage_KeyDown);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(20, 27);
            this.toolStripLabel1.Text = "页";
            // 
            // btnGo
            // 
            this.btnGo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnGo.Font = new System.Drawing.Font("微软雅黑", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.btnGo.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnGo.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.pagination_icons_03;
            this.btnGo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGo.Name = "btnGo";
            this.btnGo.Padding = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.btnGo.Size = new System.Drawing.Size(39, 27);
            this.btnGo.Text = "GO";
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(20, 27);
            this.toolStripLabel2.Text = "共";
            // 
            // lblPageCount
            // 
            this.lblPageCount.Name = "lblPageCount";
            this.lblPageCount.Size = new System.Drawing.Size(39, 27);
            this.lblPageCount.Text = "   0   ";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(20, 27);
            this.toolStripLabel3.Text = "页";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripLabel7
            // 
            this.toolStripLabel7.Name = "toolStripLabel7";
            this.toolStripLabel7.Size = new System.Drawing.Size(32, 27);
            this.toolStripLabel7.Text = "每页";
            // 
            // lblPageSize
            // 
            this.lblPageSize.AutoSize = false;
            this.lblPageSize.DropDownWidth = 60;
            this.lblPageSize.Items.AddRange(new object[] {
            "5",
            "10",
            "20",
            "50",
            "100"});
            this.lblPageSize.Name = "lblPageSize";
            this.lblPageSize.Size = new System.Drawing.Size(60, 25);
            this.lblPageSize.Text = "10";
            this.lblPageSize.ToolTipText = "每页显示记录数";
            this.lblPageSize.SelectedIndexChanged += new System.EventHandler(this.lblPageSize_SelectedIndexChanged);
            // 
            // toolStripLabel9
            // 
            this.toolStripLabel9.Name = "toolStripLabel9";
            this.toolStripLabel9.Size = new System.Drawing.Size(20, 27);
            this.toolStripLabel9.Text = "条";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripLabel6
            // 
            this.toolStripLabel6.Name = "toolStripLabel6";
            this.toolStripLabel6.Size = new System.Drawing.Size(20, 27);
            this.toolStripLabel6.Text = "共";
            // 
            // lblTotalRecord
            // 
            this.lblTotalRecord.Name = "lblTotalRecord";
            this.lblTotalRecord.Size = new System.Drawing.Size(39, 27);
            this.lblTotalRecord.Text = "   0   ";
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(44, 27);
            this.toolStripLabel5.Text = "条记录";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            this.toolStripSeparator5.Visible = false;
            // 
            // lblRemailTime
            // 
            this.lblRemailTime.Name = "lblRemailTime";
            this.lblRemailTime.Size = new System.Drawing.Size(92, 27);
            this.lblRemailTime.Text = "本次查询用时：";
            this.lblRemailTime.Visible = false;
            // 
            // Pager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bindingNavigator);
            this.Name = "Pager";
            this.Bindable = false;
            this.Size = new System.Drawing.Size(908, 30);
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.bindingNavigator.ResumeLayout(false);
            this.bindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        /// <summary>
        /// 申明委托
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public delegate int EventPagingHandler(EventPagingArgs e);

        public NPager()
        {           
            InitializeComponent();
            InitializeBindingNavigator();
        }

        private int _PageCount = 0;
        /// <summary>
        /// 标记鼠标是否单击翻页按钮
        /// </summary>
        private bool _flag;

        public event EventPagingHandler EventPaging;
        /// <summary>
        /// 每页显示记录数
        /// </summary>
        private int _pageSize = 0;
        /// <summary>
        /// 每页显示记录数
        /// </summary>
        public int PageSize
        {
            get
            {               
                return _pageSize;
            }
            private set
            {
                _pageSize = value;
                GetPageCount();
            }
        }

        private int _totalRecord;
        /// <summary>
        /// 总记录数
        /// </summary>
        public int TotalRecord
        {
            get { return _totalRecord; }
            set
            {
                _totalRecord = value;
                GetPageCount();
            }
        }

        /// <summary>
        /// 页数=总记录数/每页显示记录数
        /// </summary>
        public int PageCount
        {
            get
            {
                return _PageCount;
            }
            set
            {
                _PageCount = value;
            }
        }

        private int _pageCurrent = 1;
        /// <summary>
        /// 当前页号
        /// </summary>
        public int PageCurrent
        {
            get { return _pageCurrent; }
            set { _pageCurrent = value; }
        }

        public BindingNavigator ToolBar
        {
            get { return bindingNavigator; }
        }

        private bool _fullbutton=true;
        [Browsable(true)]
        [Description("是否显示全部分页控制按钮"), Category("杂项"), DefaultValue(true)]
        public bool FullButton
        {
            set
            {
                _fullbutton = value;

                if (!_fullbutton)
                {
                    this.toolStripLabel7.Visible = false;
                    this.lblPageSize.Visible = false;
                    this.toolStripLabel9.Visible = false;
                    this.toolStripSeparator4.Visible = false;

                    this.btnGo.Visible = false;
                    this.txtCurrentPage.Enabled = false;
                }
                else
                {
                    this.toolStripLabel7.Visible = true;
                    this.lblPageSize.Visible = true;
                    this.toolStripLabel9.Visible = true;
                    this.toolStripSeparator4.Visible = true;

                    this.btnGo.Visible = true;
                    this.txtCurrentPage.Enabled = true;
                }

                this.Invalidate();

            }
            get { return _fullbutton; }
        }

        [Browsable(false)]
        public bool Bindable
        {
            get { return _bindable; }
            private set { _bindable = value; }
        }

        private void GetPageCount()
        {
            if (TotalRecord > 0)
            {
                PageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(TotalRecord) / Convert.ToDouble(PageSize)));
            }
            else
            {
                PageCount = 0;
            }
        }


        /// <summary>
        /// 翻页控件数据绑定的方法
        /// </summary>
        public void Bind()
        {
            _pageSize = Int32.Parse(this.lblPageSize.Text);

            System.Threading.Thread th = new System.Threading.Thread(() => {

                if (EventPaging != null)
                {
                    TotalRecord = EventPaging(new EventPagingArgs(PageCurrent));
                }

                if (_disposed)
                    return;

                if (PageCurrent > PageCount)
                {
                    PageCurrent = PageCount;
                }
                if (PageCount == 1)
                {
                    PageCurrent = 1;
                }

                if (_disposed)
                    return;

                if (this.bindingNavigator.InvokeRequired)
                {
                    if (_disposed)
                        return;

                    this.bindingNavigator.Invoke(new MethodInvoker(delegate()
                    {                        
                        lblPageCount.Text = PageCount.ToString();
                        lblTotalRecord.Text = TotalRecord.ToString();
                        txtCurrentPage.Text = PageCurrent.ToString();

                        if (PageCurrent == 1)
                        {
                            btnPrev.Enabled = false;
                            btnFirst.Enabled = false;
                        }
                        else
                        {
                            btnPrev.Enabled = true;
                            btnFirst.Enabled = true;
                        }

                        if (PageCurrent == PageCount)
                        {
                            btnLast.Enabled = false;
                            btnNext.Enabled = false;
                        }
                        else
                        {
                            btnLast.Enabled = true;
                            btnNext.Enabled = true;
                        }

                        if (TotalRecord == 0)
                        {
                            btnNext.Enabled = false;
                            btnLast.Enabled = false;
                            btnFirst.Enabled = false;
                            btnPrev.Enabled = false;
                        }

                    }));
                }
                else
                {
                    lblPageCount.Text = PageCount.ToString();
                    lblTotalRecord.Text = TotalRecord.ToString();
                    txtCurrentPage.Text = PageCurrent.ToString();

                    if (PageCurrent == 1)
                    {
                        btnPrev.Enabled = false;
                        btnFirst.Enabled = false;
                    }
                    else
                    {
                        btnPrev.Enabled = true;
                        btnFirst.Enabled = true;
                    }

                    if (PageCurrent == PageCount)
                    {
                        btnLast.Enabled = false;
                        btnNext.Enabled = false;
                    }
                    else
                    {
                        btnLast.Enabled = true;
                        btnNext.Enabled = true;
                    }

                    if (TotalRecord == 0)
                    {
                        btnNext.Enabled = false;
                        btnLast.Enabled = false;
                        btnFirst.Enabled = false;
                        btnPrev.Enabled = false;
                    }
                }
            
            });

            th.IsBackground = true;
            th.Start();
            
        }

        public void ConsumingTime(string timer)
        {
            lblRemailTime.Text = "本次查询用时：" + timer;
            if (!lblRemailTime.Visible)
            {
                lblRemailTime.Visible = true;
                toolStripSeparator5.Visible = true;
            }              
        }

        public void SetPageSize(int pageSize)
        {
            this.Bindable = true;
            int selectedIndex = 0;
            foreach (var item in this.lblPageSize.Items)
            {
                if (item.ToString().Equals(pageSize.ToString()))
                {
                    break;
                }
                selectedIndex++;
            }
            if (this.lblPageSize.Items.Count > selectedIndex)
                this.lblPageSize.SelectedIndex = selectedIndex;

            this.Bindable = false;
            this.PageSize = pageSize;
        }

        public void SetTheme(string themeName)
        {
            Color backColor = SystemColors.Control;
            Color foreColor = SystemColors.ControlText;
            switch (themeName)
            {
                case "Default":
                    backColor = SystemColors.Control;
                    foreColor = SystemColors.ControlText;
                    break;
                case "Black":
                    backColor = Color.FromArgb(030, 030, 030);
                    foreColor = Color.FromArgb(240, 240, 240);
                    break;
            }           
            bindingNavigator.ForeColor = foreColor;
            bindingNavigator.BackColor = backColor;
            ForeColor = foreColor;
            BackColor = backColor;            
        }

        public void Dispose()
        {
            this._disposed = true;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            PageCurrent = 1;
            Bind();
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            PageCurrent -= 1;
            if (PageCurrent <= 0)
            {
                PageCurrent = 1;
            }

            Bind();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            PageCurrent += 1;

            if (PageCurrent > PageCount)
            {
                PageCurrent = PageCount;
            }

            Bind();
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            PageCurrent = PageCount;
            Bind();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCurrentPage.Text))
            {
                try
                {
                    PageCurrent = int.Parse(txtCurrentPage.Text.Trim());
                    Bind();
                }
                catch
                {
                    txtCurrentPage.Text = PageCurrent.ToString();
                }                
            }
        }

        private void txtCurrentPage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    PageCurrent = int.Parse(txtCurrentPage.Text.Trim());
                    Bind();
                }
                catch
                {
                    txtCurrentPage.Text = PageCurrent.ToString();
                }
            }
        }

        private void txtCurrentPage_Leave(object sender, EventArgs e)
        {
            if (_flag)
            {
                return;
            }

            if (string.IsNullOrEmpty(txtCurrentPage.Text.Trim()))
            {
                txtCurrentPage.Text = PageCurrent.ToString();
            }
        }

        private void btnFirst_MouseEnter(object sender, EventArgs e)
        {
            _flag = true;
        }

        private void btnPrev_MouseEnter(object sender, EventArgs e)
        {
            _flag = true;
        }

        private void btnNext_MouseEnter(object sender, EventArgs e)
        {
            _flag = true;
        }

        private void btnLast_MouseEnter(object sender, EventArgs e)
        {
            _flag = true;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Bind();
        }

        private void lblPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {          
            lblPageSize.Text = lblPageSize.Items[lblPageSize.SelectedIndex].ToString();
            if (!Bindable)
            {
                this.PageCurrent = 1;
                this.Bind();
            }
        }

    }

    /// <summary>
    /// 自定义事件数据基类
    /// </summary>
    public class EventPagingArgs : EventArgs
    {
        private readonly int _pageIndex;

        public EventPagingArgs(int pageIndex)
        {
            _pageIndex = pageIndex;
        }
    }

    public class NBindingNavigator : BindingNavigator
    {
        public NBindingNavigator(IContainer container) : base(container)
        {

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle rect = new Rectangle(1, 1, Width - 4, Height - 4);
            e.Graphics.SetClip(rect);
            base.OnPaint(e);
        }
    }
}
