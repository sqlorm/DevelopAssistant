﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data; 
using System.Text; 
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace ICSharpCode.WinFormsUI.Controls
{
    public partial class NPanel : UserControl //System.Windows.Forms.ScrollableControl
    {
        private int _topborderwidth = 1;
        private int _bottomborderwidth = 1;
        private int _leftborderwidth = 1;
        private int _rightborderwidth = 1;

        private Color _borderColor = SystemColors.ControlDark;
        [CategoryAttribute("设计")]
        [DescriptionAttribute("边框颜色")]
        public Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                _borderColor = value;
                this.Invalidate();
            }
        }

        private NBorderStyle _borderstyle = NBorderStyle.Top;
        [CategoryAttribute("设计")]
        [DescriptionAttribute("边框样式")]
        public NBorderStyle BorderStyle
        {
            get { return _borderstyle; }
            set
            {
                _borderstyle = value;
                //_borderColor = SystemColors.ControlDark;

                switch (_borderstyle)
                {
                    case NBorderStyle.Top:
                                           
                        _leftborderwidth = 0;
                        _rightborderwidth = 0;
                        _bottomborderwidth = 0;

                        break;
                    case NBorderStyle.Bottom: 

                        _topborderwidth = 0;                      
                        _leftborderwidth = 0;
                        _rightborderwidth = 0;
                        
                        break;
                    case NBorderStyle.Left:

                        _topborderwidth = 0;                    
                        _rightborderwidth = 0;
                        _bottomborderwidth = 0;

                        break;

                    case NBorderStyle.Right:

                        _topborderwidth = 0;
                        _leftborderwidth = 0;
                        _bottomborderwidth = 0;

                        break;

                    case NBorderStyle.LeftRight:

                        _topborderwidth = 0;                       
                        _bottomborderwidth = 0;

                        break;

                    case NBorderStyle.TopBottom:
                       
                        _leftborderwidth = 0;
                        _rightborderwidth = 0;

                        break;

                    case NBorderStyle.NoBottom:

                        _bottomborderwidth = 0;

                        break;

                    case NBorderStyle.NoTop:

                        _topborderwidth = 0;

                        break;

                    case NBorderStyle.All:
                    default: break;

                }

                this.Invalidate();
            }
        }

        private Color _topblackcolor;
        [CategoryAttribute("设计")]
        [DescriptionAttribute("背景色1")]
        public Color TopBlackColor
        {
            get { return _topblackcolor; }
            set { _topblackcolor = value; }
        }


        private Color _bottomblackcolor;
        [CategoryAttribute("设计")]
        [DescriptionAttribute("背景色2")]
        public Color BottomBlackColor
        {
            get { return _bottomblackcolor; }
            set { _bottomblackcolor = value; }
        }

        private int _marginwidth = 0;
        [CategoryAttribute("设计")]        
        public int MarginWidth
        {
            get { return _marginwidth; }
            set { _marginwidth = value; }
        }
        
        public NPanel()
        {
            InitializeComponent();
        }
        
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);             
            DrawBorder(e.Graphics);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.Invalidate();
        }

        protected void DrawBorder(Graphics g)
        {
            if (this._topblackcolor != null && this._bottomblackcolor != null)
            {
                using (LinearGradientBrush lb = new LinearGradientBrush(ClientRectangle,
                    this._topblackcolor,
                    this._bottomblackcolor,
                    LinearGradientMode.Vertical))
                {
                    g.FillRectangle(lb, this.ClientRectangle);
                }
            }

            Rectangle drawRectangle = ClientRectangle;
            if (_borderstyle == NBorderStyle.Top)
                drawRectangle = new Rectangle(ClientRectangle.X + _marginwidth, ClientRectangle.Y, ClientRectangle.Width - 2 * _marginwidth, ClientRectangle.Height);

            if (_borderstyle == NBorderStyle.None)
                return;

            ControlPaint.DrawBorder(g, drawRectangle,
            _borderColor, _leftborderwidth, ButtonBorderStyle.Solid, //左边
            _borderColor, _topborderwidth, ButtonBorderStyle.Solid, //上边
            _borderColor, _rightborderwidth, ButtonBorderStyle.Solid, //右边
            _borderColor, _bottomborderwidth, ButtonBorderStyle.Solid);//底边
        }

    }

    public enum NBorderStyle
    {
        Top,
        Bottom,
        Left,
        Right,
        LeftRight,
        TopBottom,       
        All,
        Borth,
        NoRight,
        NoLeft,
        NoTop,
        NoBottom,        
        None
    }
}
