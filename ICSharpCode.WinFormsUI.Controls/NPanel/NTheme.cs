﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NTheme : NPanel
    {
        private bool _mouseonself;

        private bool _checked;
        public bool Checked
        {
            get { return _checked; }
            set
            {
                _checked = value;
                this.Invalidate();
            }
        }

        private string _text;
        public new string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public NTheme()
        {
            BorderStyle = NBorderStyle.All;
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);

            Graphics graphic = e.Graphics;

            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;

            if (this._mouseonself)
            {                
                if (!this._checked)
                {
                    Rectangle rect = new Rectangle(1, this.Height - 22 - 1, this.Width - 2, 22);
                    graphic.FillRectangle(new SolidBrush(Color.FromArgb(125, Color.Red)), rect);
                    graphic.DrawString(this._text, this.Font, Brushes.Black, rect, sf); 
                }               
            }
            if (this._checked)
            {
                Rectangle rect = new Rectangle(1, this.Height - 22 - 1, this.Width - 2, 22);
                graphic.FillRectangle(new SolidBrush(Color.FromArgb(150, Color.Red)), rect);
                graphic.DrawImage(ICSharpCode.WinFormsUI.Controls.Properties.Resources.selected, rect.Width - 34, rect.Y - 8);
                graphic.DrawString(this._text, this.Font, Brushes.White, rect, sf); 
            }           

        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            this._mouseonself = true;
            this.Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            this._mouseonself = false;
            this.Invalidate();
        }

        

    }
}
