﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NVScrollBar : NScrollBar
    {
        public NVScrollBar()
        {
            this.orientation = ScrollBarOrientation.Vertical;
            this.scrollOrientation = System.Windows.Forms.ScrollOrientation.VerticalScroll;
        }        
    }
}
