﻿
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NStateLabel : UserControl
    {
        private ToolTip _tooltip;
        private bool _autoSize;
        private bool _showBorder;
        private int _lineHeight;
        private string _text;
        private IContainer components;

        public override bool AutoSize
        {
            get
            {
                return this._autoSize;
            }
            set
            {
                this._autoSize = value;
                if (this._autoSize || string.IsNullOrEmpty(this.Text))
                    return;
                this._tooltip.SetToolTip((Control)this, this.Text);
            }
        }

        public bool ShowBorder
        {
            get
            {
                return this._showBorder;
            }
            set
            {
                this._showBorder = value;
                this.Invalidate();
            }
        }

        public int LineHeight
        {
            get
            {
                return this._lineHeight;
            }
            private set
            {
                this._lineHeight = value;
            }
        }

        [Browsable(true)]
        [Description("显示文本")]
        [Category("属性")]
        [DefaultValue("")]
        public override string Text
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
                if (!this._autoSize && !string.IsNullOrEmpty(this.Text))
                    this._tooltip.SetToolTip((Control)this, this.Text);
                this.Invalidate();
            }
        }

        public NStateLabel()
        {          
            this.components = (IContainer)new System.ComponentModel.Container();
            this.InitializeComponent();          
            this.LineHeight = 12;
            this.AutoSize = true;
            this.ShowBorder = false;
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics graphics = e.Graphics;
            new StringFormat().LineAlignment = StringAlignment.Center;
            if (!string.IsNullOrEmpty(this.Text))
            {
                if (this.AutoSize)
                {
                    Size size = TextRenderer.MeasureText((IDeviceContext)e.Graphics, this.Text, this.Font, new Size(this.Width, this.LineHeight), TextFormatFlags.TextBoxControl);
                    double num = Math.Ceiling((double)size.Width * 1.0 / (double)this.Width);
                    this.Height = size.Height * (int)num + 8;
                    TextRenderer.DrawText((IDeviceContext)e.Graphics, this.Text, this.Font, new Rectangle(0, 4, this.Width, this.Height - 8), Color.Empty, Color.Empty, TextFormatFlags.VerticalCenter | TextFormatFlags.WordBreak);
                }
                else
                    TextRenderer.DrawText((IDeviceContext)e.Graphics, this.Text, this.Font, new Rectangle(0, 4, this.Width, this.Height - 8), Color.Empty, Color.Empty, TextFormatFlags.VerticalCenter | TextFormatFlags.WordEllipsis);
            }
            if (!this.ShowBorder)
                return;
            ControlPaint.DrawBorder(e.Graphics, this.ClientRectangle, SystemColors.ControlDark, 1, ButtonBorderStyle.Solid, SystemColors.ControlDark, 1, ButtonBorderStyle.Solid, SystemColors.ControlDark, 1, ButtonBorderStyle.Solid, SystemColors.ControlDark, 1, ButtonBorderStyle.Solid);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();

            this._tooltip = new ToolTip();
            this._lineHeight = 0;

            this.AutoScaleDimensions = new SizeF(6f, 12f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Name = "NStateLabel";
            this.Size = new Size(150, 24);
            this.ResumeLayout(false);
        }
    }
}
