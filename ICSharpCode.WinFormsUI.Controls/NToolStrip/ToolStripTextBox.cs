﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NToolStripTextBox : System.Windows.Forms.ToolStripTextBox
    {
        private bool focusStatus = false;

        public NToolStripTextBox() : base()
        {
            this.Padding = new Padding(2, 2, 2, 2);           
            this.TextBox.Dock = DockStyle.Fill;
            this.TextBox.BorderStyle = BorderStyle.None;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(NToolStripItemTheme.BaseColor), new Rectangle(0, 2, Width, Height - 4));
            e.Graphics.FillRectangle(new SolidBrush(Color.White), new Rectangle(0, (Height - 24) / 2, Width, 24));
            base.OnPaint(e);
            if (focusStatus)
            {
                e.Graphics.DrawRectangle(new Pen(SystemColors.ControlDark), new Rectangle(0, (Height - 24) / 2, Width - 1, 24));
            }
        }

        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            focusStatus = false;
            Invalidate();
        }

        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            focusStatus = true;
            Invalidate();
        }

        //protected override void OnHostedControlResize(EventArgs e)
        //{
        //    base.OnHostedControlResize(e);          
        //    this.TextBox.Width = Width - 2;
        //    this.TextBox.Height = 32;
        //    this.TextBox.Location = new Point(1, (this.Height - 32) / 2);
        //}

         
    }
}
