﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace ICSharpCode.WinFormsUI.Controls
{
    public enum ContainerType
    {
        NToolStrip = 0,
        NContextMenu = 1,
        DropDownMenu = 2
    }

    public class NToolStripPaintUtils
    {
        public static void DrawItem(Graphics g, ToolStripItem item, ContainerType type, int LayoutStyle = 1)
        {
            if (!item.Visible)
                return;

            int iconSize = 16;
            Rectangle bound = item.Bounds;
            int posX = bound.X;
            int posY = bound.Y;

            if (item is ToolStripSeparator)
            {
                if (LayoutStyle == 1)
                {
                    if (type == 0)
                    {
                        posX = posX + bound.Width / 2;
                        g.DrawLine(new Pen(SystemBrushes.ControlDark), new Point(posX, bound.Y + 4), new Point(posX, bound.Y + bound.Height - 8));
                    }
                    else
                    {
                        posX = posX + 2 * iconSize + 8;
                        g.DrawLine(new Pen(SystemBrushes.ControlDark), new Point(posX, bound.Y + bound.Height / 2), new Point(item.Width, bound.Y + bound.Height / 2));
                    }
                }
                else
                {
                    posX = posX + 2 * iconSize + 8;
                    g.DrawLine(new Pen(SystemBrushes.ControlDark), new Point(posX, bound.Y + bound.Height / 2), new Point(item.Width, bound.Y + bound.Height / 2));
                }

            }
            else if (item is NToolStripButton)
            {
                Brush textBru = new SolidBrush(NToolStripItemTheme.TextColor);

                if (item.Pressed)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                    g.FillRectangle(new SolidBrush(NToolStripItemTheme.PressedColor), bound);
                }
                else
                {
                    if (item.Selected)
                    {
                        textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                        g.FillRectangle(new SolidBrush(NToolStripItemTheme.HighlightBackColor), bound);
                    }
                }

                if (!item.Enabled)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.DisableColor);
                }

                if (item.Image != null && (item.DisplayStyle == ToolStripItemDisplayStyle.Image || item.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
                {
                    Image image = item.Image;
                    if (!item.Enabled)
                    {
                        g.DrawImage(CommonHelper.CreateDropColorImage(image, item.Owner.BackColor), new Rectangle(posX + 2, posY + (item.Height - iconSize) / 2 , iconSize, iconSize),
                        new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                    }
                    else
                    {
                        g.DrawImage(CommonHelper.CreateCoverColorImage(image, item.Owner.ForeColor), new Rectangle(posX + 2, posY + (item.Height - iconSize) / 2 , iconSize, iconSize),
                        new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                    }
                    posX = posX + iconSize + 4;
                }

                if (!string.IsNullOrEmpty(item.Text) && (item.DisplayStyle == ToolStripItemDisplayStyle.Text || item.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
                {
                    g.DrawString(item.Text, item.Font, textBru, new Rectangle(posX, posY, bound.Width - iconSize - 4, bound.Height), new StringFormat() { LineAlignment = StringAlignment.Center });
                }

            }
            else if (item is NToolStripDropDownButton)
            {
                Brush textBru = new SolidBrush(NToolStripItemTheme.TextColor);

                if (item.Pressed)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                    g.FillRectangle(new SolidBrush(NToolStripItemTheme.PressedColor), bound);
                }
                else
                {
                    if (item.Selected)
                    {
                        textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                        g.FillRectangle(new SolidBrush(NToolStripItemTheme.HighlightBackColor), bound);
                    }
                }

                if (!item.Enabled)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.DisableColor);
                }

                if (item.Image != null && (item.DisplayStyle == ToolStripItemDisplayStyle.Image || item.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
                {
                    Image image = item.Image;
                    if (!item.Enabled)
                    {
                        g.DrawImage(CommonHelper.CreateDropColorImage(image, item.Owner.BackColor), new Rectangle(posX + 2, posY + (item.Height - iconSize) / 2 , iconSize, iconSize),
                        new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                    }
                    else
                    {
                        g.DrawImage(CommonHelper.CreateCoverColorImage(image, item.Owner.ForeColor), new Rectangle(posX + 2, posY + (item.Height - iconSize) / 2 , iconSize, iconSize),
                        new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                    }
                    posX = posX + iconSize + 4;
                }

                if (!string.IsNullOrEmpty(item.Text) && (item.DisplayStyle == ToolStripItemDisplayStyle.Text || item.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
                {
                    g.DrawString(item.Text, item.Font, textBru, new Rectangle(posX, posY, bound.Width - iconSize - 4, bound.Height), new StringFormat() { LineAlignment = StringAlignment.Center });
                }

                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                Point[] pntArr = new Point[] {
                    new Point(bound.Left + bound.Width-4-8,posY + (bound.Height - 3)/2),
                    new Point(bound.Left + bound.Width-4,posY + (bound.Height - 3)/2),
                    new Point(bound.Left + bound.Width-4-8/2,posY + (bound.Height - 3)/2 + 3)
                };
                g.FillPolygon(textBru, pntArr);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;

                if (item.Pressed)
                {
                    g.DrawRectangle(new Pen(SystemColors.ControlDarkDark), new Rectangle(bound.Left, bound.Top, bound.Width - 1, bound.Height + 4));
                }

            }
            else if (item is NToolStripMainMenuItem)
            {
                Brush textBru = new SolidBrush(NToolStripItemTheme.TextColor);

                if (item.Pressed)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                    g.FillRectangle(new SolidBrush(NToolStripItemTheme.PressedColor), bound);
                }
                else
                {
                    if (item.Selected)
                    {
                        textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                        g.FillRectangle(new SolidBrush(NToolStripItemTheme.HighlightBackColor), bound);
                    }
                }

                if (!item.Enabled)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.DisableColor);
                }

                iconSize = 0;

                if (item.Image != null && (item.DisplayStyle == ToolStripItemDisplayStyle.Image || item.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
                {
                    iconSize = 16;

                    Image image = item.Image;
                    if (!item.Enabled)
                    {
                        g.DrawImage(CommonHelper.CreateDropColorImage(image, item.Owner.BackColor), new Rectangle(posX + 2, posY + (item.Height - iconSize) / 2 + 1, iconSize, iconSize),
                        new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                    }
                    else
                    {
                        g.DrawImage(CommonHelper.CreateCoverColorImage(image, item.Owner.ForeColor), new Rectangle(posX + 2, posY + (item.Height - iconSize) / 2 + 1, iconSize, iconSize),
                        new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                    }
                    posX = posX + iconSize + 4;
                }

                if (!string.IsNullOrEmpty(item.Text) && (item.DisplayStyle == ToolStripItemDisplayStyle.Text || item.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
                {
                    Font itemFont = item.Font;
                    string itemText = item.Text;

                    if (item.Text.StartsWith("&"))
                    {
                        itemText = item.Text.Substring(1);
                        itemFont = new Font(item.Font, FontStyle.Regular);
                    }

                    g.DrawString(itemText, itemFont, textBru, new Rectangle(posX, posY, bound.Width - iconSize - 4, bound.Height), new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                }

                if (item.Pressed)
                {
                    g.DrawRectangle(new Pen(SystemColors.ControlDarkDark), new Rectangle(bound.Left, bound.Top, bound.Width - 1, bound.Height + 4));
                    //g.DrawRectangle(new Pen(SystemColors.ControlDarkDark), bound);
                }
            }
            else if (item is NToolStripMenuItem || item is ToolStripMenuItem)
            {
                Brush textBru = new SolidBrush(NToolStripItemTheme.TextColor);

                if (item.Pressed)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                    g.FillRectangle(new SolidBrush(NToolStripItemTheme.PressedColor), bound);
                }
                else
                {
                    if (item.Selected)
                    {
                        textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                        g.FillRectangle(new SolidBrush(NToolStripItemTheme.HighlightBackColor), bound);
                    }
                }

                if (item.Image != null)
                {
                    Color foreColor = Color.White;
                    switch (NToolStripItemTheme.Name)
                    {
                        case "Default":
                            foreColor = Color.Black;
                            break;
                        case "Black":
                            foreColor = Color.White;
                            break;
                    }
                    g.DrawImage(CommonHelper.CreateCoverColorImage(item.Image, foreColor), new Rectangle(posX + 6, posY + (item.Height - iconSize) / 2 + 1, iconSize, iconSize),
                            new Rectangle(0, 0, item.Image.Width, item.Image.Height), GraphicsUnit.Pixel);
                }

                bool hasDrapdownList = false;

                if (item is NToolStripMenuItem)
                {
                    NToolStripMenuItem toolStripItem = (NToolStripMenuItem)item;

                    if (toolStripItem.HasDropDown)
                        hasDrapdownList = true;

                    if (toolStripItem.Checked)
                    {
                        Image iconImage = ICSharpCode.WinFormsUI.Controls.Properties.Resources.checked24;
                        g.DrawImage(CommonHelper.CreateCoverColorImage(iconImage, item.Owner.BackColor), new Rectangle(posX + 6, posY + (item.Height - iconSize) / 2 + 1, iconSize, iconSize),
                                new Rectangle(0, 0, iconImage.Width, iconImage.Height), GraphicsUnit.Pixel);
                    }
                }

                posX = posX + 2 * iconSize + 8;

                if (!string.IsNullOrEmpty(item.Text) && (item.DisplayStyle == ToolStripItemDisplayStyle.Text || item.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
                {
                    string itemText = item.Text;
                    Font itemFont = new Font(item.Font, FontStyle.Regular);
                    if (item.Text.StartsWith("&"))
                    {
                        itemText = item.Text.Substring(1);
                        itemFont = new Font(item.Font, FontStyle.Regular);
                    }

                    if (item.DisplayStyle == ToolStripItemDisplayStyle.Text || item.Image == null)
                    {
                        g.DrawString(itemText, itemFont, textBru, new Rectangle(posX + 2, posY, item.Width - 4, item.Height), new StringFormat() { LineAlignment = StringAlignment.Center });
                    }
                    else
                    {
                        g.DrawString(itemText, itemFont, textBru, new Rectangle(posX, posY, item.Width - iconSize - 4, item.Height), new StringFormat() { LineAlignment = StringAlignment.Center });
                    }

                }

                if (hasDrapdownList)
                {
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    Point[] pntArr = {
                        new Point(bound.Right-12,posY +(bound.Height -12)/2),
                        new Point(bound.Right-7,posY +(bound.Height -12)/2+5),
                        new Point(bound.Right-12,posY +(bound.Height -12)/2+10)
                    };
                    g.FillPolygon(textBru, pntArr);
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
                }

            }
            else if (item is NToolStripCheckBox)
            {
                Brush textBru = new SolidBrush(NToolStripItemTheme.TextColor);

                if (item.Pressed)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                    g.FillRectangle(new SolidBrush(NToolStripItemTheme.PressedColor), bound);
                }
                else
                {
                    if (item.Selected)
                    {
                        textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                        g.FillRectangle(new SolidBrush(NToolStripItemTheme.HighlightBackColor), bound);
                    }
                }

                if (!item.Enabled)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.DisableColor);
                }

                Size txtSize = TextRenderer.MeasureText(item.Text, item.Font);
                Point pLocation = new Point(bound.Left + 3, posY + ((bound.Height - 14) / 2));
                Rectangle rectText = new Rectangle(pLocation.X + 16, (bound.Height - txtSize.Height) / 2, txtSize.Width, txtSize.Height);
                var _checkState = ((NToolStripCheckBox)item).CheckState;
                CheckBoxState chkState = _checkState ? CheckBoxState.CheckedNormal : CheckBoxState.UncheckedNormal;
                CheckBoxRenderer.DrawCheckBox(g, pLocation, rectText, "", item.Font, TextFormatFlags.Left | TextFormatFlags.VerticalCenter, false, chkState);

                posX = posX + iconSize + 4;

                if (!string.IsNullOrEmpty(item.Text) && (item.DisplayStyle == ToolStripItemDisplayStyle.Text || item.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
                {
                    g.DrawString(item.Text, item.Font, textBru, new Rectangle(posX, posY, bound.Width - iconSize - 4, bound.Height), new StringFormat() { LineAlignment = StringAlignment.Center });
                }
            }
            else
            {
                Brush textBru = new SolidBrush(NToolStripItemTheme.TextColor);

                if (item.Pressed)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                    g.FillRectangle(new SolidBrush(NToolStripItemTheme.PressedColor), bound);
                }
                else
                {
                    if (item.Selected)
                    {
                        textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                        g.FillRectangle(new SolidBrush(NToolStripItemTheme.HighlightBackColor), bound);
                    }
                }

                if (!item.Enabled)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.DisableColor);
                }

                if (item.Image != null && (item.DisplayStyle == ToolStripItemDisplayStyle.Image || item.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
                {
                    iconSize = 16;
                    Image image = item.Image;
                    if (!item.Enabled)
                    {
                        g.DrawImage(CommonHelper.CreateDropColorImage(image, item.Owner.BackColor), new Rectangle(posX + 2, posY + (item.Height - iconSize) / 2 + 1, iconSize, iconSize),
                        new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                    }
                    else
                    {
                        g.DrawImage(CommonHelper.CreateCoverColorImage(image, item.Owner.ForeColor), new Rectangle(posX + 2, posY + (item.Height - iconSize) / 2 + 1, iconSize, iconSize),
                        new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                    }
                    posX = posX + iconSize + 4;
                }
                else
                {
                    iconSize = 0;
                    posX = posX + iconSize + 4;
                }

                if (!string.IsNullOrEmpty(item.Text) && (item.DisplayStyle == ToolStripItemDisplayStyle.Text || item.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
                {
                    g.DrawString(item.Text, item.Font, textBru, new Rectangle(posX, posY, bound.Width - iconSize - 4, bound.Height), new StringFormat() { LineAlignment = StringAlignment.Center });
                }
            }
        }
    }
}
