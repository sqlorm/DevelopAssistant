using System;
using System.Collections.Generic;
using System.Text;

namespace ICSharpCode.WinFormsUI.Controls
{
	public struct DropPosition
	{
		private NTreeNode _node;
		public NTreeNode Node
		{
			get { return _node; }
			set { _node = value; }
		}

		private NodePosition _position;
		public NodePosition Position
		{
			get { return _position; }
			set { _position = value; }
		}
	}
}
