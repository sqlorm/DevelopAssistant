using System;
using System.Collections.Generic;
using System.Text;

namespace ICSharpCode.WinFormsUI.Controls
{
	public interface IToolTipProvider
	{
		string GetToolTip(NTreeNode node);
	}
}
