using System;
using System.Collections.Generic;
using System.Text;

namespace ICSharpCode.WinFormsUI.Controls
{
	internal class InputWithControl: NormalInputState
	{
		public InputWithControl(NTreeView tree): base(tree)
		{
		}

		protected override void DoMouseOperation(NTreeNodeMouseEventArgs args)
		{
			if (Tree.SelectionMode == NTreeViewSelectionMode.Single)
			{
				base.DoMouseOperation(args);
			}
			else if (CanSelect(args.Node))
			{
				args.Node.IsSelected = !args.Node.IsSelected;
				Tree.SelectionStart = args.Node;
			}
		}

		protected override void MouseDownAtEmptySpace(NTreeNodeMouseEventArgs args)
		{
		}
	}
}
