﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NTreeViewBrushStyle
    {
        public System.Drawing.Brush Highlight = System.Drawing.SystemBrushes.Highlight;

        public System.Drawing.Brush HighlightText = System.Drawing.SystemBrushes.HighlightText;

        public System.Drawing.Brush ControlText = System.Drawing.SystemBrushes.ControlText;

        public System.Drawing.Brush Inactive = System.Drawing.SystemBrushes.Highlight;

        public System.Drawing.Brush InactiveText = System.Drawing.SystemBrushes.HighlightText;

        public System.Drawing.Brush UnabledText = System.Drawing.SystemBrushes.GrayText;
    }
}
