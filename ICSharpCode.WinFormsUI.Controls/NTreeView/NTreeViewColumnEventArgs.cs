using System;
using System.Collections.Generic;
using System.Text;

namespace ICSharpCode.WinFormsUI.Controls
{
	public class NTreeViewColumnEventArgs : EventArgs
	{
		private NTreeViewColumn _column;
		public NTreeViewColumn Column
		{
			get { return _column; }
		}

		public NTreeViewColumnEventArgs(NTreeViewColumn column)
		{
			_column = column;
		}
	}
}
