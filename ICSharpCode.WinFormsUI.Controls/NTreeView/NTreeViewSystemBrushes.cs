﻿using System;

namespace ICSharpCode.WinFormsUI.Controls
{
    public static class NTreeViewSystemBrushes
    {
        public static System.Drawing.Brush Highlight= System.Drawing.SystemBrushes.Highlight;

        public static System.Drawing.Brush HighlightText = System.Drawing.SystemBrushes.HighlightText;

        public static System.Drawing.Brush ControlText = System.Drawing.SystemBrushes.ControlText;

        public static System.Drawing.Brush Inactive = System.Drawing.SystemBrushes.Highlight;

        public static System.Drawing.Brush InactiveText = System.Drawing.SystemBrushes.HighlightText;

        public static System.Drawing.Brush UnabledText = System.Drawing.SystemBrushes.GrayText;

    }
}
