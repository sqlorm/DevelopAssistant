using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

namespace ICSharpCode.WinFormsUI.Controls.NodeControls
{
	[DesignTimeVisible(false), ToolboxItem(false)]
	public abstract class NodeControl: Component
	{
		#region Properties

		private NTreeView _parent;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public NTreeView Parent
		{
			get { return _parent; }
			set 
			{
				if (value != _parent)
				{
					if (_parent != null)
						_parent.NodeControls.Remove(this);

					if (value != null)
						value.NodeControls.Add(this);
				}
			}
		}

		private IToolTipProvider _toolTipProvider;
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public IToolTipProvider ToolTipProvider
		{
			get { return _toolTipProvider; }
			set { _toolTipProvider = value; }
		}

		private int _column;
		[DefaultValue(0)]
		public int Column
		{
			get { return _column; }
			set 
			{
				if (_column < 0)
					throw new ArgumentOutOfRangeException("value");

				_column = value;
				if (_parent != null)
					_parent.FullUpdate();
			}
		}

		#endregion

		internal void AssignParent(NTreeView parent)
		{
			_parent = parent;
		}

		public abstract Size MeasureSize(NTreeNode node);

		public abstract void Draw(NTreeNode node, DrawContext context);

		public virtual string GetToolTip(NTreeNode node)
		{
			if (ToolTipProvider != null)
				return ToolTipProvider.GetToolTip(node);
			else
				return string.Empty;
		}

		public virtual void MouseDown(NTreeNodeMouseEventArgs args)
		{
		}

		public virtual void MouseUp(NTreeNodeMouseEventArgs args)
		{
		}

        public virtual void MouseClick(NTreeNodeMouseEventArgs args)
        {
        }

        public virtual void MouseDoubleClick(NTreeNodeMouseEventArgs args)
		{
		}

		public virtual void KeyDown(KeyEventArgs args)
		{
		}

		public virtual void KeyUp(KeyEventArgs args)
		{
		}
	}
}
