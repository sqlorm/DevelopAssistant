using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using ICSharpCode.WinFormsUI.Controls.Properties;

namespace ICSharpCode.WinFormsUI.Controls.NodeControls
{
	internal class NodePlusMinus : NodeControl
	{
        public const int Width = 16;
        public const int ImageSize = 10;
		
		private Bitmap _plus;
		private Bitmap _minus;

		public NodePlusMinus()
		{
			_plus = Resources.plus;
			_minus = Resources.minus;
		}

		public override Size MeasureSize(NTreeNode node)
		{
			return new Size(Width, Width);
		}

		public override void Draw(NTreeNode node, DrawContext context)
		{
			if (node.CanExpand)
			{
				Rectangle r = context.Bounds;
				int dy = (int)Math.Round((float)(r.Height - ImageSize) / 2);

                //if (!Application.RenderWithVisualStyles)
                //{
                //	VisualStyleRenderer renderer;
                //	if (node.IsExpanded)
                //		renderer = new VisualStyleRenderer(VisualStyleElement.TreeView.Glyph.Opened);
                //	else
                //		renderer = new VisualStyleRenderer(VisualStyleElement.TreeView.Glyph.Closed);
                //	renderer.DrawBackground(context.Graphics, new Rectangle(r.X, r.Y + dy, ImageSize, ImageSize));
                //}
                //else
                //{
                //	Image img;
                //	if (node.IsExpanded)
                //		img = _minus;
                //	else
                //		img = _plus;
                //	context.Graphics.DrawImageUnscaled(img, new Point(r.X, r.Y + dy));
                //}

                Color lineColor = node.TreeView.LineColor;
                Color backColor = node.TreeView.BackColor;
                Color foreColor = node.TreeView.ForeColor;

                RectangleF bound = new RectangleF(r.X, r.Y + dy, ImageSize, ImageSize);

                context.Graphics.FillRectangle(new SolidBrush(backColor), bound);

                if (node.IsExpanded)
                {
                    context.Graphics.DrawLine(new Pen(foreColor), new PointF(bound.X + 2, bound.Y + bound.Height / 2), new PointF(bound.X + bound.Width - 2, bound.Y + bound.Height / 2));
                }
                else
                {
                    context.Graphics.DrawLine(new Pen(foreColor), new PointF(bound.X + 2, bound.Y + bound.Height / 2), new PointF(bound.X + bound.Width - 2, bound.Y + bound.Height / 2));
                    context.Graphics.DrawLine(new Pen(foreColor), new PointF(bound.X + bound.Width / 2, bound.Y + 2), new PointF(bound.X + bound.Width / 2, bound.Y + bound.Height - 2));
                }

                context.Graphics.DrawRectangle(new Pen(lineColor), bound.X, bound.Y, bound.Width, bound.Height);

			}
		}

		public override void MouseDown(NTreeNodeMouseEventArgs args)
		{
			if (args.Button == MouseButtons.Left)
			{
				args.Handled = true;
				if (args.Node.CanExpand)
					args.Node.IsExpanded = !args.Node.IsExpanded;
			}
		}

		public override void MouseDoubleClick(NTreeNodeMouseEventArgs args)
		{
			args.Handled = true; // Supress expand/collapse when double click on plus/minus
		}
	}
}
