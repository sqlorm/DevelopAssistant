﻿using System;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NWorkbenchItem
    {
        public string Title { get; set; }
        public DateTime DateTime { get; set; }

        public bool MouseInsideState = false;
        public System.Drawing.Rectangle Bound { get; set; }

        public string Tag { get; set; }

        public NWorkbenchItem(string title,DateTime dateTime)
        {
            this.Title = title;
            this.DateTime = dateTime;
        }
    }
}
