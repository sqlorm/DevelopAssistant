﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Docking
{
    public class ContainerPanel : System.Windows.Forms.Control
    {
        public ContainerPanel()
        {
            this.SetStyle(
              ControlStyles.UserPaint |
              ControlStyles.OptimizedDoubleBuffer |
              ControlStyles.AllPaintingInWmPaint |
              ControlStyles.ResizeRedraw |
              ControlStyles.SupportsTransparentBackColor, true);
            this.UpdateStyles();
        }
    }
}
