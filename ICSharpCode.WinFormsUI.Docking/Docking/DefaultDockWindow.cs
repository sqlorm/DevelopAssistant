﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing; 
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Docking
{
    /// <summary>
    /// Dock window of Visual Studio 2003/2005 theme.
    /// </summary>
    [ToolboxItem(false)]
    internal class DefaultDockWindow : DockWindow
    {
        internal DefaultDockWindow(DockPanel dockPanel, DockState dockState)
            : base(dockPanel, dockState)
        {
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            // if DockWindow is document, draw the border
            //if (DockState == DockState.Document)
            //    e.Graphics.DrawRectangle(SystemPens.ControlDark, ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width - 1, ClientRectangle.Height - 1);

            ControlPaint.DrawBorder(e.Graphics, this.ClientRectangle,
    SystemColors.ControlDark, 1, ButtonBorderStyle.Solid, //左边
    SystemColors.ControlDark, 1, ButtonBorderStyle.Solid, //上边
    SystemColors.ControlDark, 1, ButtonBorderStyle.Solid, //右边
    SystemColors.ControlDark, 0, ButtonBorderStyle.None);//底边

            base.OnPaint(e);
        }
    }
}
