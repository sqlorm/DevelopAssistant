﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Docking
{
    public class DockPanelThemeStyle
    {
        public static ColorStyle ColorStyle = new ColorStyle("Default");
        public static ControlStyle ControlStyle = new ControlStyle("Default");
    }

    public class ColorStyle
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public ColorStyle(string name)
        {
            this._name = name;
        }
    }

    public class ControlStyle
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public ControlStyle(string name)
        {
            this._name = name;
        }
    }


}
