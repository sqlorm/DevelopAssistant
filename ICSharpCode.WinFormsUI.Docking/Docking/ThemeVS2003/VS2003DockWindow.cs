﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Docking
{
    /// <summary>
    /// Dock window of Visual Studio 2003/2005 theme.
    /// </summary>
    [ToolboxItem(false)]
    internal class VS2013DockWindow : DockWindow
    {
        internal VS2013DockWindow(DockPanel dockPanel, DockState dockState)
            : base(dockPanel, dockState)
        {
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e); 
            
            //Rectangle rect = ClientRectangle;
            //e.Graphics.DrawLine(new Pen(SystemColors.ControlDark),
            //    new Point(rect.X, rect.Y), new Point(rect.X + rect.Width, rect.Y));
        }

        public override Rectangle DisplayingRectangle
        {
            get
            {
                Rectangle rect = ClientRectangle;

                if (DockState == DockState.DockLeft)
                    rect.Width -= Measures.SplitterSize;
                else if (DockState == DockState.DockRight)
                {
                    rect.X += Measures.SplitterSize;
                    rect.Width -= Measures.SplitterSize;
                }
                else if (DockState == DockState.DockTop)
                    rect.Height -= Measures.SplitterSize;
                else if (DockState == DockState.DockBottom)
                {
                    rect.Y += Measures.SplitterSize;
                    rect.Height -= Measures.SplitterSize;
                }

                return rect;
            }
        }
        
        internal class VS2013SplitterControl : SplitterBase
        {
            protected override int SplitterSize
            {
                get { return Measures.SplitterSize; }
            }

            protected override void StartDrag()
            {
                DockWindow window = Parent as DockWindow;
                if (window == null)
                    return;

                window.DockPanel.BeginDrag(window, window.RectangleToScreen(Bounds));
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                Rectangle rect = ClientRectangle;
                using (Pen pen = new Pen(SystemColors.ControlDark))
                {
                    e.Graphics.DrawLine(pen, new Point(rect.X, rect.Y), new Point(rect.X, rect.Height));
                    e.Graphics.DrawLine(pen, new Point(rect.X + rect.Width - 1, rect.Y), new Point(rect.X + rect.Width - 1, rect.Height));
                }
            }

        }

    }
}
