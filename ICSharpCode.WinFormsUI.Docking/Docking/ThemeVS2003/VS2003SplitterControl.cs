﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Docking
{
    public class VS2003SplitterControl : ICSharpCode.WinFormsUI.Docking.DockPane.SplitterControlBase
    {
        public VS2003SplitterControl(DockPane pane)
            : base(pane)
        {
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (DockPane.DockState != DockState.Document)
                return;

            System.Drawing.Graphics g = e.Graphics;
            Rectangle rect = ClientRectangle;
            if (Alignment == DockAlignment.Top || Alignment == DockAlignment.Bottom)
                g.DrawLine(SystemPens.ControlDark, rect.Left, rect.Bottom - 1, rect.Right, rect.Bottom - 1);
            else if (Alignment == DockAlignment.Left || Alignment == DockAlignment.Right)
                g.DrawLine(SystemPens.ControlDarkDark, rect.Right - 1, rect.Top, rect.Right - 1, rect.Bottom);
        }
    }

}
