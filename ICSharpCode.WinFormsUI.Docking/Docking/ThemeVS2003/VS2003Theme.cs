using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace ICSharpCode.WinFormsUI.Docking
{
    /// <summary>
    /// Visual Studio 2005 theme (default theme).
    /// </summary>
    public class VS2003Theme : DockPanelThemeBase
    {
        /// <summary>
        /// Applies the specified theme to the dock panel.
        /// </summary>
        /// <param name="dockPanel">The dock panel.</param>
        public override void Apply(DockPanel dockPanel)
        {
            if (dockPanel == null)
            {
                throw new NullReferenceException("dockPanel");
            }

            Measures.SplitterSize = 4;
            dockPanel.Extender.DockPaneCaptionFactory = new VS2003DockPaneCaptionFactory();
            dockPanel.Extender.AutoHideStripFactory = new VS2003AutoHideStripFactory();
            dockPanel.Extender.AutoHideWindowFactory = new VS2003AutoHideWindowFactory();
            dockPanel.Extender.DockPaneStripFactory = new VS2003DockPaneStripFactory();
            dockPanel.Extender.DockPaneSplitterControlFactory = new VS2003DockPaneSplitterControlFactory();
            dockPanel.Extender.DockWindowSplitterControlFactory = new VS2003DockWindowSplitterControlFactory();
            dockPanel.Extender.DockWindowFactory = new VS2003DockWindowFactory();
            dockPanel.Extender.PaneIndicatorFactory = new VS2003PaneIndicatorFactory();
            dockPanel.Extender.PanelIndicatorFactory = new VS2003PanelIndicatorFactory();
            dockPanel.Extender.DockOutlineFactory = new VS2003DockOutlineFactory();
            DockPanelThemeStyle.ControlStyle = new ControlStyle("VS2003");
            dockPanel.Skin = CreateVisualStudio2003();            
        }

        public override void Apply(DockPanel dockPanel, string colorStyle)
        {
            DockPanelThemeStyle.ControlStyle = new ControlStyle("VS2003");
            dockPanel.Skin = CreateVisualStudio2003();            
        }

        internal static DockPanelSkin CreateVisualStudio2003()
        {
            DockPanelSkin skin = new DockPanelSkin();

            Color TabBaseColor = Color.FromArgb(214, 219, 233);
            Color TabOtherColor = Color.FromArgb(214, 219, 233);
            Color TabActiveColor = SystemColors.Control;
            Color TabActiveTextColor = SystemColors.ControlText;

            Color ToolBaseColor = Color.FromArgb(214, 219, 233);
            Color ToolActiveColor = Color.FromArgb(51, 51, 255);
            Color ToolActiveTextColor = SystemColors.ControlText;

            Color TabBaseTextColor = SystemColors.ControlText;
            Color ToolBaseTextColor = SystemColors.ControlText;
            Color ActiveCaptionColor = SystemColors.GradientActiveCaption;

            Color AutoHideStripColor = SystemColors.ControlLightLight;
            Color AutoHideStripTabColor = SystemColors.ControlLightLight;

            switch (DockPanelThemeStyle.ColorStyle.Name)
            {
                case "Black":
                    TabBaseColor = Color.FromArgb(050, 050, 050);                    
                    TabBaseTextColor = Color.FromArgb(240, 240, 240);
                    ToolBaseTextColor = Color.FromArgb(240, 240, 240);
                    TabOtherColor = Color.FromArgb(050, 050, 050);
                    TabActiveColor = Color.FromArgb(045, 045, 048);
                    ToolActiveColor = Color.FromArgb(045, 045, 048);
                    TabActiveTextColor = Color.FromArgb(240, 240, 240);
                    ToolActiveTextColor = Color.FromArgb(240, 240, 240);

                    ActiveCaptionColor = Color.FromArgb(030, 030, 030);
                    AutoHideStripColor = Color.FromArgb(050, 050, 050);
                    AutoHideStripTabColor = Color.FromArgb(050, 050, 050);

                    break;
                case "Default":
                    TabBaseColor = Color.FromArgb(214, 219, 233);
                    TabOtherColor = Color.FromArgb(214, 219, 233);
                    TabBaseTextColor = SystemColors.ControlText;
                    ToolBaseTextColor = SystemColors.GrayText;
                    TabActiveColor = SystemColors.Control;
                    ToolActiveColor = Color.FromArgb(51, 51, 255);
                    TabActiveTextColor = SystemColors.ControlText;
                    ToolActiveTextColor = SystemColors.ControlText;

                    ActiveCaptionColor = SystemColors.GradientActiveCaption;
                    AutoHideStripColor = SystemColors.ControlLight;
                    AutoHideStripTabColor = SystemColors.ControlLight;

                    break;
            }

            skin.AutoHideStripSkin.DockStripGradient.StartColor = AutoHideStripColor;
            skin.AutoHideStripSkin.DockStripGradient.EndColor = AutoHideStripColor;

            skin.AutoHideStripSkin.TabGradient.StartColor = AutoHideStripTabColor;
            skin.AutoHideStripSkin.TabGradient.EndColor = AutoHideStripTabColor;
            skin.AutoHideStripSkin.TabGradient.TextColor = ToolBaseTextColor;

            skin.DockPaneStripSkin.DocumentGradient.DockStripGradient.StartColor = TabBaseColor;
            skin.DockPaneStripSkin.DocumentGradient.DockStripGradient.EndColor = TabBaseColor;
            skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.TextColor = TabActiveTextColor;
            skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.StartColor = TabActiveColor;
            skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.EndColor = TabActiveColor;
            skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.TextColor = TabBaseTextColor;
            skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.StartColor = TabOtherColor;
            skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.EndColor = TabOtherColor;

            skin.DockPaneStripSkin.ToolWindowGradient.DockStripGradient.StartColor = TabBaseColor;
            skin.DockPaneStripSkin.ToolWindowGradient.DockStripGradient.EndColor = TabBaseColor;

            skin.DockPaneStripSkin.ToolWindowGradient.ActiveTabGradient.StartColor = TabBaseColor;
            skin.DockPaneStripSkin.ToolWindowGradient.ActiveTabGradient.EndColor = TabBaseColor;

            skin.DockPaneStripSkin.ToolWindowGradient.InactiveTabGradient.StartColor = TabBaseColor;
            skin.DockPaneStripSkin.ToolWindowGradient.InactiveTabGradient.EndColor = TabBaseColor;
            skin.DockPaneStripSkin.ToolWindowGradient.InactiveTabGradient.TextColor = ToolBaseTextColor;

            skin.DockPaneStripSkin.ToolWindowGradient.ActiveCaptionGradient.StartColor = ActiveCaptionColor;
            skin.DockPaneStripSkin.ToolWindowGradient.ActiveCaptionGradient.EndColor = ActiveCaptionColor;
            //skin.DockPaneStripSkin.ToolWindowGradient.ActiveCaptionGradient.LinearGradientMode = LinearGradientMode.Vertical;
            skin.DockPaneStripSkin.ToolWindowGradient.ActiveCaptionGradient.TextColor = ToolActiveTextColor;

            skin.DockPaneStripSkin.ToolWindowGradient.InactiveCaptionGradient.StartColor = TabBaseColor;// SystemColors.GradientInactiveCaption;
            skin.DockPaneStripSkin.ToolWindowGradient.InactiveCaptionGradient.EndColor = TabBaseColor;// SystemColors.InactiveCaption;
            //skin.DockPaneStripSkin.ToolWindowGradient.InactiveCaptionGradient.LinearGradientMode = LinearGradientMode.Vertical;
            skin.DockPaneStripSkin.ToolWindowGradient.InactiveCaptionGradient.TextColor = ToolActiveTextColor;

            return skin;
        }

        private class VS2003DockPaneSplitterControlFactory : DockPanelExtender.IDockPaneSplitterControlFactory
        {
            public DockPane.SplitterControlBase CreateSplitterControl(DockPane pane)
            {
                //return new DockPane.DefaultSplitterControl(pane);
                return new VS2003SplitterControl(pane);
            }
        }

        private class VS2003DockWindowSplitterControlFactory : DockPanelExtender.IDockWindowSplitterControlFactory
        {
            public SplitterBase CreateSplitterControl()
            {
                //return new DockWindow.DefaultSplitterControl();
                return new VS2013DockWindow.VS2013SplitterControl();
            }
        }

        private class VS2003DockWindowFactory : DockPanelExtender.IDockWindowFactory
        {
            public DockWindow CreateDockWindow(DockPanel dockPanel, DockState dockState)
            {
                return new VS2013DockWindow(dockPanel, dockState);
            }
        }

        private class VS2003DockPaneCaptionFactory : DockPanelExtender.IDockPaneCaptionFactory
        {
            public DockPaneCaptionBase CreateDockPaneCaption(DockPane pane)
            {
                return new VS2003DockPaneCaption(pane);
            }
        }

        private class VS2003DockPaneStripFactory : DockPanelExtender.IDockPaneStripFactory
        {
            public DockPaneStripBase CreateDockPaneStrip(DockPane pane)
            {
                return new VS2003DockPaneStrip(pane);
            }
        }

        private class VS2003AutoHideStripFactory : DockPanelExtender.IAutoHideStripFactory
        {
            public AutoHideStripBase CreateAutoHideStrip(DockPanel panel)
            {
                return new VS2003AutoHideStrip(panel);
            }
        }       

        private class VS2003AutoHideWindowFactory : DockPanelExtender.IAutoHideWindowFactory
        {
            public DockPanel.AutoHideWindowControl CreateAutoHideWindow(DockPanel panel)
            {
                //return new DockPanel.DefaultAutoHideWindowControl(panel);
                return new VS2003AutoHideWindowControl(panel);
            }
        }

        public class VS2003PaneIndicatorFactory : DockPanelExtender.IPaneIndicatorFactory
        {
            public DockPanel.IPaneIndicator CreatePaneIndicator()
            {
                return new DockPanel.DefaultPaneIndicator();
            }
        }

        public class VS2003PanelIndicatorFactory : DockPanelExtender.IPanelIndicatorFactory
        {
            public DockPanel.IPanelIndicator CreatePanelIndicator(System.Windows.Forms.DockStyle style)
            {
                return new DockPanel.DefaultPanelIndicator(style);
            }
        }

        public class VS2003DockOutlineFactory : DockPanelExtender.IDockOutlineFactory
        {
            public DockOutlineBase CreateDockOutline()
            {
                return new DockPanel.DefaultDockOutline();
            }
        }


    }   

}