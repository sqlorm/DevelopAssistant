﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Forms
{
    public partial class MessageForm : BaseForm
    {
        Bitmap iconImage = null;      
        string ntext = string.Empty;
        string ncaption = string.Empty;

        internal Type ReflectionType = null;
        internal Form ReflectionSender = null;

        private ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase _xtheme = null;
        public ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase XTheme
        {
            get
            {
                return _xtheme;
            }
            set
            {
                _xtheme = value;
                base.XTheme = _xtheme;
                string typeName = _xtheme.GetType().FullName;
                switch (typeName)
                {
                    case "ICSharpCode.WinFormsUI.Theme.ThemeMac":
                        this.btnApply.Radius = 8;
                        this.btnCancel.Radius = 8;
                        this.btnApply.BorderColor = SystemColors.ControlDark;
                        this.btnCancel.BorderColor = SystemColors.ControlDark;
                        break;
                    case "ICSharpCode.WinFormsUI.Theme.ThemeShadow": 
                        this.btnApply.Radius = 0;
                        this.btnCancel.Radius = 0;
                        this.btnApply.BorderColor = Color.FromArgb(255, 123, 079, 202);
                        this.btnCancel.BorderColor = Color.FromArgb(255, 123, 079, 202);
                        break;
                    case "ICSharpCode.WinFormsUI.Theme.ThemeVS2012": 
                        this.btnApply.Radius = 0;
                        this.btnCancel.Radius = 0;
                        this.btnApply.BorderColor = Color.FromArgb(000, 122, 204);
                        this.btnCancel.BorderColor = Color.FromArgb(000, 122, 204);
                        break;

                }

                this.btnApply.OnFocus = true;

            }
        }

        public MessageForm()
        {
            InitializeComponent();
        }

        public MessageForm(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            InitializeComponent();
            ControlRender(text, caption, buttons, icon);             
        }         

        protected void ControlRender(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            this.Text = caption;
            this.ntext = text;
            this.ncaption = caption;
            this.iconImage = ICSharpCode.WinFormsUI.Properties.Resources.question;

            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageForm));
            switch (icon)
            {
                case MessageBoxIcon.Warning:
                    this.iconImage = ICSharpCode.WinFormsUI.Properties.Resources.warning;
                    this.Icon = ((System.Drawing.Icon)(resources.GetObject("warning")));
                    break;
                case MessageBoxIcon.Information:
                    this.iconImage = ICSharpCode.WinFormsUI.Properties.Resources.information;
                    this.Icon = ((System.Drawing.Icon)(resources.GetObject("information")));
                    break;
                case MessageBoxIcon.Question:
                    this.iconImage = ICSharpCode.WinFormsUI.Properties.Resources.question;
                    this.Icon = ((System.Drawing.Icon)(resources.GetObject("question")));
                    break;
            }
           
            ICSharpCode.WinFormsUI.Core.HtmlTextParse.StringCollection result = null;
            ICSharpCode.WinFormsUI.Core.HtmlTextParse.Parse(this.ntext, out result);        

            if (result.Count > 10)
            {
                //throw new Exception("不支等超过两个Light关键词组");
                result.Exception("不支等超过10个Light关键词组");
            }

            htmlTextBox1.DocumentIcon = iconImage;
            htmlTextBox1.DocumentText = result;

        }

        public static DialogResult Show(IWin32Window context, string text, string caption, MessageBoxButtons buttons = MessageBoxButtons.OK,
            MessageBoxIcon icon = MessageBoxIcon.Information, MessageBoxSize clientSize = null, Control eventSource = null)
        {
            MessageForm MessageBox = new MessageForm(text, caption, buttons, icon);
            if (eventSource != null)
                MessageBox.ReflectionType = eventSource.GetType();
            if (eventSource != null)
                MessageBox.ReflectionSender = (Form)eventSource;
            if (context != null && context.GetType().BaseType.Equals(typeof(BaseForm)))
                MessageBox.XTheme = ((BaseForm)context).XTheme;
            if (clientSize != null)
                MessageBox.Size = new Size(clientSize.Width, clientSize.Height);
            return MessageBox.ShowDialog(context);
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

    }

    public class MessageBoxSize
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public MessageBoxSize(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

    }
}
