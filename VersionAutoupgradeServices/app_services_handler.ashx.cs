﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace VersionAutoupgradeServices
{
    /// <summary>
    /// app_services_handler 的摘要说明
    /// </summary>
    public class app_services_handler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string funcode = context.Request["funcode"] ?? "";
            context.Response.Clear();
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.ContentType = "application/json";
            context.Response.AddHeader("Access-Control-Allow-Origin", "*");
            context.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            switch (funcode)
            {
                case "login":
                     
                    break;
                case "list":
                     
                    break;
                case "add":
                     
                    break;
                case "get":
                     
                    break;
                case "delete":
                     
                    break;
                case "category":
                     
                    break;
            }
            context.Response.Flush();
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}