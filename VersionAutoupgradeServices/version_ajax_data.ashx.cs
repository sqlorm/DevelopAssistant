﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

namespace VersionAutoupgradeServices
{
    /// <summary>
    /// version_ajax_data 的摘要说明
    /// </summary>
    public class version_ajax_data : IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";
            string strArg = context.Request.QueryString["arg"];
            if (string.IsNullOrEmpty(strArg))
            {
                System.Text.StringBuilder xmlContent = new System.Text.StringBuilder();
                xmlContent.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                xmlContent.Append("<AutoupgradeVersion>");

                string AutoupgradePath = context.Server.MapPath("~/Versions/Autoupgrade.exe");
                if (System.IO.File.Exists(AutoupgradePath))
                {
                    FileVersionInfo versionInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(AutoupgradePath);
                    xmlContent.Append(versionInfo.ProductVersion);
                }
                else
                {
                    xmlContent.Append("0.0.0");
                }
                
                xmlContent.AppendLine("</AutoupgradeVersion>");
                context.Response.Write(xmlContent.ToString());
            }
            else
            {
                DirectoryInfo di = new DirectoryInfo(context.Server.MapPath("~/Versions/" + strArg + "/Updates/"));
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(context.Server.MapPath("~/Versions/" + strArg + "/version.xml"));
                XmlNode xmlNode = xmlDoc.SelectSingleNode("//Items");
                xmlNode.RemoveAll();
                this.GetFileFromDirectory(di, (XmlElement)xmlNode, xmlDoc, string.Empty);
                context.Response.Write(xmlDoc.OuterXml);
            }
        }

        public void GetFileFromDirectory(DirectoryInfo di, XmlElement element, XmlDocument xmlDoc, string prex)
        {
            foreach (DirectoryInfo di1 in di.GetDirectories())
            {
                string prex1 = di1.FullName.Substring(di.FullName.Length);
                this.GetFileFromDirectory(di1, element, xmlDoc, prex1);
            }
            foreach (FileInfo fileInfo in di.GetFiles())
            {
                XmlElement element1 = xmlDoc.CreateElement("Item");
                element1.SetAttribute("name", fileInfo.Name);
                element1.SetAttribute("file", string.IsNullOrEmpty(prex) ? fileInfo.Name : prex + "\\" + fileInfo.Name);
                element1.SetAttribute("version", fileInfo.LastWriteTime.ToString("yyyyMMddHHmmss"));
                element1.SetAttribute("size", fileInfo.Length.ToString());
                element1.SetAttribute("type", fileInfo.Extension);
                element.AppendChild((XmlNode)element1);
            }
        }


    }
}